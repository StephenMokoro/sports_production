-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2018 at 08:50 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sports`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_auto_id` int(10) NOT NULL,
  `admin_staff_id` int(10) NOT NULL,
  `admin_username` varchar(50) NOT NULL,
  `admin_fname` varchar(30) NOT NULL,
  `admin_lname` varchar(30) NOT NULL,
  `admin_nid` varchar(30) NOT NULL,
  `admin_phone` varchar(30) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `date_registered` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_role_id` int(5) NOT NULL,
  `password` varchar(150) NOT NULL,
  `id_type` varchar(20) NOT NULL,
  `admin_profile_photo` varchar(150) DEFAULT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) NOT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_general_uploads`
--

CREATE TABLE `admin_general_uploads` (
  `upload_auto_id` int(10) NOT NULL,
  `upload_file_name` varchar(150) NOT NULL,
  `upload_descriptive_name` varchar(100) NOT NULL,
  `upload_date_uploaded` datetime NOT NULL,
  `upload_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file_ext` varchar(10) NOT NULL,
  `uploading_user_id` int(10) DEFAULT NULL,
  `upload_updating_user_id` int(10) DEFAULT NULL,
  `uploading_user_group_id` int(5) DEFAULT NULL,
  `upload_updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `role_auto_id` int(5) NOT NULL,
  `role_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_uploads`
--

CREATE TABLE `admin_uploads` (
  `upload_auto_id` int(10) NOT NULL,
  `upload_file_name` varchar(150) NOT NULL,
  `upload_descriptive_name` varchar(100) NOT NULL,
  `upload_date_uploaded` datetime NOT NULL,
  `upload_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file_ext` varchar(10) NOT NULL,
  `uploading_user_id` int(10) DEFAULT NULL,
  `upload_updating_user_id` int(10) DEFAULT NULL,
  `uploading_user_group_id` int(5) DEFAULT NULL,
  `upload_updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `captains`
--

CREATE TABLE `captains` (
  `captain_auto_id` int(10) NOT NULL,
  `captain_player_id` int(10) NOT NULL,
  `player_team_id` int(5) NOT NULL,
  `user_agreement` tinyint(1) NOT NULL DEFAULT '1',
  `date_appointed` datetime NOT NULL,
  `end_of_tenure` datetime DEFAULT NULL,
  `capt_before` tinyint(1) NOT NULL DEFAULT '0',
  `date_registered` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coaches`
--

CREATE TABLE `coaches` (
  `coach_auto_id` int(10) NOT NULL,
  `coach_staff_id` int(10) NOT NULL,
  `coach_nid` varchar(30) NOT NULL,
  `coach_username` varchar(30) NOT NULL,
  `id_type` varchar(20) NOT NULL,
  `coach_fname` varchar(30) NOT NULL,
  `coach_lname` varchar(30) NOT NULL,
  `coach_other_names` varchar(50) DEFAULT NULL,
  `coach_gender` varchar(10) NOT NULL,
  `coach_phone` varchar(30) NOT NULL,
  `coach_email` varchar(100) NOT NULL,
  `coach_residence` varchar(100) NOT NULL,
  `coach_sport_id` int(10) NOT NULL,
  `prev_coaching_state` tinyint(1) NOT NULL DEFAULT '0',
  `prev_team` varchar(100) NOT NULL,
  `coach_h_achievement` varchar(150) DEFAULT NULL,
  `user_agreement` tinyint(1) NOT NULL DEFAULT '1',
  `date_appointed` datetime NOT NULL,
  `end_of_tenure` datetime DEFAULT NULL,
  `coach_profile_photo` varchar(150) DEFAULT NULL,
  `date_registered` datetime NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coach_reports`
--

CREATE TABLE `coach_reports` (
  `report_auto_id` int(10) NOT NULL,
  `report_file_name` varchar(150) NOT NULL,
  `report_descriptive_name` varchar(100) NOT NULL,
  `report_date_uploaded` datetime NOT NULL,
  `report_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `report_team_id` int(10) NOT NULL,
  `file_ext` varchar(10) NOT NULL,
  `uploading_user_id` int(10) NOT NULL,
  `upload_update_user_id` int(10) DEFAULT NULL,
  `uploading_user_group_id` int(5) DEFAULT NULL,
  `upload_updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coach_roles`
--

CREATE TABLE `coach_roles` (
  `coach_role_auto_id` int(10) NOT NULL,
  `coach_auto_id` int(10) NOT NULL,
  `coach_role_team_id` int(10) NOT NULL,
  `coach_role_status` tinyint(1) NOT NULL DEFAULT '1',
  `date_appointed` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL,
  `updating_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_id` varchar(15) NOT NULL,
  `course_name` varchar(150) NOT NULL,
  `faculty` varchar(100) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures`
--

CREATE TABLE `expenditures` (
  `expense_auto_id` int(10) NOT NULL,
  `expense_team_auto_id` int(10) NOT NULL,
  `expense_date` datetime NOT NULL,
  `expense_cash` double(10,2) NOT NULL,
  `expense_lpo_no` varchar(15) DEFAULT NULL,
  `expense_lpo_amount` double(10,2) DEFAULT NULL,
  `actual_expenditure` double(10,2) NOT NULL,
  `expense_comment` varchar(150) NOT NULL,
  `receipt_file_name` varchar(120) DEFAULT NULL,
  `receipt_file_ext` varchar(10) DEFAULT NULL,
  `expense_record_date` datetime NOT NULL,
  `expense_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `game_auto_id` int(10) NOT NULL,
  `game_team` int(10) NOT NULL,
  `game_match_type` int(2) NOT NULL,
  `game_title` varchar(100) NOT NULL,
  `game_start_date` datetime NOT NULL,
  `game_end_date` datetime NOT NULL,
  `game_summary` varchar(1500) DEFAULT NULL,
  `game_date_recorded` datetime NOT NULL,
  `game_date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_matches`
--

CREATE TABLE `game_matches` (
  `match_auto_id` int(10) NOT NULL,
  `match_game_id` int(10) NOT NULL,
  `match_date` datetime NOT NULL,
  `match_start_time` time NOT NULL,
  `match_opponents` varchar(50) NOT NULL,
  `match_venue` varchar(30) DEFAULT NULL,
  `match_end_time` time DEFAULT NULL,
  `match_opponents_score` int(5) DEFAULT '0',
  `match_level` int(5) NOT NULL,
  `match_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_coach_uploads`
--

CREATE TABLE `general_coach_uploads` (
  `upload_auto_id` int(10) NOT NULL,
  `upload_file_name` varchar(150) NOT NULL,
  `upload_descriptive_name` varchar(100) NOT NULL,
  `upload_date_uploaded` datetime NOT NULL,
  `upload_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `upload_sport_id` int(10) NOT NULL,
  `file_ext` varchar(10) NOT NULL,
  `uploading_user_id` int(10) DEFAULT NULL,
  `upload_update_user_id` int(10) DEFAULT NULL,
  `uploading_user_group_id` int(5) DEFAULT NULL,
  `upload_updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hockey_match_scores`
--

CREATE TABLE `hockey_match_scores` (
  `auto_id` int(10) NOT NULL,
  `player_id` int(10) NOT NULL,
  `scores` int(10) DEFAULT '0',
  `green_cards` int(10) DEFAULT '0',
  `yellow_cards` int(10) DEFAULT '0',
  `red_cards` int(10) DEFAULT '0',
  `match_id` int(10) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `injury_records`
--

CREATE TABLE `injury_records` (
  `injury_auto_id` int(10) NOT NULL,
  `player_auto_id` int(10) NOT NULL,
  `injury_date` datetime NOT NULL,
  `date_seen` datetime DEFAULT NULL,
  `injury_description` varchar(150) DEFAULT NULL,
  `diagnosis` varchar(150) DEFAULT NULL,
  `treatment` varchar(150) DEFAULT NULL,
  `physio_remarks` varchar(100) DEFAULT NULL,
  `scoach_remarks` varchar(100) DEFAULT NULL,
  `physio_status` tinyint(1) NOT NULL DEFAULT '0',
  `physio_start_date` datetime DEFAULT NULL,
  `date_recorded` datetime NOT NULL,
  `physio_end_date` datetime DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `match_players`
--

CREATE TABLE `match_players` (
  `auto_id` int(10) NOT NULL,
  `match_id` int(10) NOT NULL,
  `match_player_id` int(10) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `match_types`
--

CREATE TABLE `match_types` (
  `match_type_auto_id` int(2) NOT NULL,
  `match_type_name` varchar(20) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `physio_players`
--

CREATE TABLE `physio_players` (
  `physio_auto_id` int(10) NOT NULL,
  `physio_player_id` int(10) NOT NULL,
  `physio_date_added` datetime NOT NULL,
  `physio_release_state` tinyint(1) NOT NULL DEFAULT '0',
  `physio_release_date` datetime DEFAULT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `player_auto_id` int(10) NOT NULL,
  `player_nid` varchar(30) NOT NULL,
  `id_type` varchar(50) NOT NULL,
  `player_fname` varchar(30) NOT NULL,
  `player_lname` varchar(30) NOT NULL,
  `player_other_names` varchar(50) NOT NULL,
  `player_dob` datetime NOT NULL,
  `stud_id` varchar(10) DEFAULT NULL,
  `player_phone` varchar(30) NOT NULL,
  `player_email` varchar(100) NOT NULL,
  `player_residence` varchar(100) NOT NULL,
  `player_height` double(10,2) NOT NULL,
  `player_weight` double(10,2) NOT NULL,
  `stud_course_id` varchar(15) DEFAULT NULL,
  `kin_fname` varchar(30) NOT NULL,
  `kin_lname` varchar(30) NOT NULL,
  `kin_other_names` varchar(50) NOT NULL,
  `player_gender` varchar(10) NOT NULL,
  `kin_nid` varchar(30) NOT NULL,
  `kin_phone` varchar(30) NOT NULL,
  `kin_email` varchar(100) DEFAULT NULL,
  `kin_alt_phone` varchar(30) NOT NULL,
  `kin_residence` varchar(100) NOT NULL,
  `prev_hschool` varchar(100) NOT NULL,
  `prev_play_state` tinyint(1) NOT NULL DEFAULT '0',
  `prev_team` varchar(100) NOT NULL,
  `player_team_id` int(10) DEFAULT NULL,
  `h_achievement` varchar(100) DEFAULT NULL,
  `agreement` tinyint(1) DEFAULT NULL,
  `player_profile_photo` varchar(150) NOT NULL,
  `date_registered` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alumni_status` tinyint(1) NOT NULL DEFAULT '0',
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scoach_reports`
--

CREATE TABLE `scoach_reports` (
  `report_auto_id` int(10) NOT NULL,
  `report_file_name` varchar(150) NOT NULL,
  `report_descriptive_name` varchar(100) NOT NULL,
  `report_date_uploaded` datetime NOT NULL,
  `report_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file_ext` varchar(10) NOT NULL,
  `upload_user_id` int(10) DEFAULT NULL,
  `upload_update_user_id` int(10) DEFAULT NULL,
  `uploading_user_group_id` int(5) DEFAULT NULL,
  `upload_updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scoregroups`
--

CREATE TABLE `scoregroups` (
  `levelAutoId` int(10) NOT NULL,
  `levelName` varchar(30) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sc_coaches`
--

CREATE TABLE `sc_coaches` (
  `sc_auto_id` int(10) NOT NULL,
  `sc_staff_id` varchar(30) NOT NULL,
  `sc_nid` varchar(30) NOT NULL,
  `sc_fname` varchar(30) NOT NULL,
  `sc_lname` varchar(30) NOT NULL,
  `sc_other_names` varchar(50) NOT NULL,
  `sc_username` varchar(30) NOT NULL,
  `sc_dob` datetime NOT NULL,
  `sc_phone` varchar(30) NOT NULL,
  `sc_email` varchar(100) NOT NULL,
  `sc_residence` varchar(100) NOT NULL,
  `user_agreement` tinyint(1) NOT NULL DEFAULT '1',
  `date_appointed` datetime NOT NULL,
  `end_of_tenure` datetime DEFAULT NULL,
  `date_registered` datetime NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `sc_password` varchar(200) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_type` text NOT NULL,
  `sc_profile_photo` text NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports`
--

CREATE TABLE `sports` (
  `sport_auto_id` int(10) NOT NULL,
  `sport_name` varchar(50) NOT NULL,
  `sport_status` tinyint(1) NOT NULL DEFAULT '1',
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_auto_id` int(10) NOT NULL,
  `team_sport_id` int(10) NOT NULL,
  `team_name` varchar(30) NOT NULL,
  `team_alias` varchar(30) NOT NULL,
  `team_category` varchar(15) NOT NULL,
  `team_status` tinyint(1) NOT NULL DEFAULT '1',
  `date_registered` datetime NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_attendance`
--

CREATE TABLE `training_attendance` (
  `ta_auto_id` int(10) NOT NULL,
  `player_auto_id` int(10) NOT NULL,
  `training_date` date NOT NULL,
  `attendance_state` varchar(15) NOT NULL DEFAULT 'ABSENT',
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `training_year` int(5) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `training_days`
--

CREATE TABLE `training_days` (
  `trd_auto_id` int(10) NOT NULL,
  `trd_year` int(5) NOT NULL,
  `trd_team_id` int(5) NOT NULL,
  `january` int(5) NOT NULL,
  `february` int(5) NOT NULL,
  `march` int(5) NOT NULL,
  `april` int(5) NOT NULL,
  `may` int(5) NOT NULL,
  `june` int(5) NOT NULL,
  `july` int(5) NOT NULL,
  `august` int(5) NOT NULL,
  `september` int(5) NOT NULL,
  `october` int(5) NOT NULL,
  `november` int(5) NOT NULL,
  `december` int(5) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `travel_documents`
--

CREATE TABLE `travel_documents` (
  `passport_auto_id` int(10) NOT NULL,
  `player_id` int(10) NOT NULL,
  `passport_number` varchar(30) NOT NULL,
  `issue_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `issue_country` varchar(10) NOT NULL,
  `date_recorded` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `passport_photo` varchar(100) NOT NULL,
  `registering_user_id` int(10) DEFAULT NULL,
  `updating_user_id` int(10) DEFAULT NULL,
  `registering_user_group_id` int(5) DEFAULT NULL,
  `updating_user_group_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `group_auto_id` int(5) NOT NULL,
  `group_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_auto_id`),
  ADD UNIQUE KEY `admin_staff_id` (`admin_staff_id`),
  ADD KEY `admin_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `admin_role_id_fk` (`admin_role_id`),
  ADD KEY `admin_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `admin_general_uploads`
--
ALTER TABLE `admin_general_uploads`
  ADD PRIMARY KEY (`upload_auto_id`),
  ADD KEY `admin_general_uploads_user_group_id_fk` (`uploading_user_group_id`),
  ADD KEY `admin_gen_uploads_updating_user_group_id_fk` (`upload_updating_user_group_id`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`role_auto_id`);

--
-- Indexes for table `admin_uploads`
--
ALTER TABLE `admin_uploads`
  ADD PRIMARY KEY (`upload_auto_id`),
  ADD KEY `admin_uploads_user_group_id_fk` (`uploading_user_group_id`),
  ADD KEY `admin_uploads_updating_user_group_id_fk` (`upload_updating_user_group_id`);

--
-- Indexes for table `captains`
--
ALTER TABLE `captains`
  ADD PRIMARY KEY (`captain_auto_id`),
  ADD KEY `player_team_id_fk` (`player_team_id`),
  ADD KEY `captains_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `captains_updating_user_group_id_fk` (`updating_user_group_id`),
  ADD KEY `ALTER TABLE ``captains`` ADD CONSTRAINT captain_player_auto_id_fk` (`captain_player_id`);

--
-- Indexes for table `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`coach_auto_id`),
  ADD UNIQUE KEY `coach_phone` (`coach_phone`),
  ADD UNIQUE KEY `coach_email` (`coach_email`),
  ADD UNIQUE KEY `coach_username` (`coach_username`),
  ADD UNIQUE KEY `coach_nid` (`coach_nid`),
  ADD UNIQUE KEY `coach_staff_id` (`coach_staff_id`),
  ADD KEY `coaches_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `coaches_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `coach_reports`
--
ALTER TABLE `coach_reports`
  ADD PRIMARY KEY (`report_auto_id`),
  ADD KEY `report_sport_id` (`report_team_id`),
  ADD KEY `coach_reports_upload_user_group_id_fk` (`uploading_user_group_id`),
  ADD KEY `upload_updating_user_group_id_fk` (`upload_updating_user_group_id`);

--
-- Indexes for table `coach_roles`
--
ALTER TABLE `coach_roles`
  ADD PRIMARY KEY (`coach_role_auto_id`),
  ADD KEY `coach_role_staff_id_fk` (`coach_auto_id`),
  ADD KEY `coach_role_team_id_fk` (`coach_role_team_id`),
  ADD KEY `coach_role_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `coach_role_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`),
  ADD KEY `courses_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `courses_updating_user_group_id` (`updating_user_group_id`);

--
-- Indexes for table `expenditures`
--
ALTER TABLE `expenditures`
  ADD PRIMARY KEY (`expense_auto_id`),
  ADD KEY `expense_team_auto_id` (`expense_team_auto_id`),
  ADD KEY `expenditures_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `expenses_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`game_auto_id`),
  ADD KEY `hkm_type_id_fk` (`game_match_type`),
  ADD KEY `game_category_fk` (`game_team`),
  ADD KEY `games_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `games_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `game_matches`
--
ALTER TABLE `game_matches`
  ADD PRIMARY KEY (`match_auto_id`),
  ADD KEY `hk_game_match_id_fk` (`match_game_id`),
  ADD KEY `match_game_id` (`match_game_id`),
  ADD KEY `game_matches_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `game_matches_updating_user_group_id_fk` (`updating_user_group_id`),
  ADD KEY `game_matches_match_level_fk` (`match_level`);

--
-- Indexes for table `general_coach_uploads`
--
ALTER TABLE `general_coach_uploads`
  ADD PRIMARY KEY (`upload_auto_id`),
  ADD KEY `upload_sport_id` (`upload_sport_id`),
  ADD KEY `general_coach_uploads_user_group_id_fk` (`uploading_user_group_id`),
  ADD KEY `gen_coach_uploads_updating_user_group_id_fk` (`upload_updating_user_group_id`);

--
-- Indexes for table `hockey_match_scores`
--
ALTER TABLE `hockey_match_scores`
  ADD PRIMARY KEY (`auto_id`),
  ADD KEY `hms_match_id_fk` (`match_id`),
  ADD KEY `hms_player_id_fk` (`player_id`),
  ADD KEY `hk_match_scores_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `hms_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `injury_records`
--
ALTER TABLE `injury_records`
  ADD PRIMARY KEY (`injury_auto_id`),
  ADD KEY `player_auto_id_fk` (`player_auto_id`),
  ADD KEY `injury_record_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `injury_records_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `match_players`
--
ALTER TABLE `match_players`
  ADD PRIMARY KEY (`auto_id`),
  ADD KEY `match_players_player_id` (`match_player_id`),
  ADD KEY `match_players_id_fk` (`match_id`),
  ADD KEY `match_players_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `match_players_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `match_types`
--
ALTER TABLE `match_types`
  ADD PRIMARY KEY (`match_type_auto_id`),
  ADD KEY `match_types_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `match_types_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `physio_players`
--
ALTER TABLE `physio_players`
  ADD PRIMARY KEY (`physio_auto_id`),
  ADD KEY `physio_player_id_fk` (`physio_player_id`),
  ADD KEY `physio_players_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `physio_players_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`player_auto_id`),
  ADD UNIQUE KEY `player_nid` (`player_nid`),
  ADD UNIQUE KEY `player_phone` (`player_phone`),
  ADD UNIQUE KEY `player_email` (`player_email`),
  ADD UNIQUE KEY `stud_id` (`stud_id`),
  ADD KEY `player_course_id_fk` (`stud_course_id`),
  ADD KEY `player_team_id` (`player_team_id`),
  ADD KEY `players_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `players_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `scoach_reports`
--
ALTER TABLE `scoach_reports`
  ADD PRIMARY KEY (`report_auto_id`),
  ADD KEY `scoach_reports_user_group_id_fk` (`uploading_user_group_id`),
  ADD KEY `scoach_reports_updating_user_group_id_fk` (`upload_updating_user_group_id`);

--
-- Indexes for table `scoregroups`
--
ALTER TABLE `scoregroups`
  ADD PRIMARY KEY (`levelAutoId`),
  ADD KEY `scoregroups_user_id_fk` (`registering_user_group_id`),
  ADD KEY `scoregroups_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `sc_coaches`
--
ALTER TABLE `sc_coaches`
  ADD PRIMARY KEY (`sc_auto_id`),
  ADD UNIQUE KEY `sc_staff_id` (`sc_staff_id`),
  ADD KEY `sc_coaches_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `sc_coaches_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`sport_auto_id`),
  ADD KEY `sports_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `sports_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_auto_id`),
  ADD KEY `team_group_team_id_fk` (`team_sport_id`),
  ADD KEY `teams_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `teams_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `training_attendance`
--
ALTER TABLE `training_attendance`
  ADD PRIMARY KEY (`ta_auto_id`),
  ADD KEY `ta_player_nid_fk` (`player_auto_id`),
  ADD KEY `ta_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `ta_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `training_days`
--
ALTER TABLE `training_days`
  ADD PRIMARY KEY (`trd_auto_id`),
  ADD KEY `trd_team_id` (`trd_team_id`),
  ADD KEY `trd_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `trd_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `travel_documents`
--
ALTER TABLE `travel_documents`
  ADD PRIMARY KEY (`passport_auto_id`),
  ADD UNIQUE KEY `passport_number` (`passport_number`),
  ADD UNIQUE KEY `player_id` (`player_id`,`passport_number`),
  ADD KEY `td_user_group_id_fk` (`registering_user_group_id`),
  ADD KEY `traveldocs_updating_user_group_id_fk` (`updating_user_group_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`group_auto_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_general_uploads`
--
ALTER TABLE `admin_general_uploads`
  MODIFY `upload_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `role_auto_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_uploads`
--
ALTER TABLE `admin_uploads`
  MODIFY `upload_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `captains`
--
ALTER TABLE `captains`
  MODIFY `captain_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coaches`
--
ALTER TABLE `coaches`
  MODIFY `coach_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coach_reports`
--
ALTER TABLE `coach_reports`
  MODIFY `report_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coach_roles`
--
ALTER TABLE `coach_roles`
  MODIFY `coach_role_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures`
--
ALTER TABLE `expenditures`
  MODIFY `expense_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `game_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_matches`
--
ALTER TABLE `game_matches`
  MODIFY `match_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_coach_uploads`
--
ALTER TABLE `general_coach_uploads`
  MODIFY `upload_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hockey_match_scores`
--
ALTER TABLE `hockey_match_scores`
  MODIFY `auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `injury_records`
--
ALTER TABLE `injury_records`
  MODIFY `injury_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `match_players`
--
ALTER TABLE `match_players`
  MODIFY `auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `match_types`
--
ALTER TABLE `match_types`
  MODIFY `match_type_auto_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `physio_players`
--
ALTER TABLE `physio_players`
  MODIFY `physio_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `player_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scoach_reports`
--
ALTER TABLE `scoach_reports`
  MODIFY `report_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scoregroups`
--
ALTER TABLE `scoregroups`
  MODIFY `levelAutoId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sports`
--
ALTER TABLE `sports`
  MODIFY `sport_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_attendance`
--
ALTER TABLE `training_attendance`
  MODIFY `ta_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_days`
--
ALTER TABLE `training_days`
  MODIFY `trd_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `travel_documents`
--
ALTER TABLE `travel_documents`
  MODIFY `passport_auto_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `group_auto_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_role_id_fk` FOREIGN KEY (`admin_role_id`) REFERENCES `admin_roles` (`role_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `admin_general_uploads`
--
ALTER TABLE `admin_general_uploads`
  ADD CONSTRAINT `admin_gen_uploads_updating_user_group_id_fk` FOREIGN KEY (`upload_updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_gen_uploads_uploading_user_group_id_fk` FOREIGN KEY (`uploading_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `admin_uploads`
--
ALTER TABLE `admin_uploads`
  ADD CONSTRAINT `admin_uploads_updating_user_group_id_fk` FOREIGN KEY (`upload_updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_uploads_uploading_user_group_id_fk` FOREIGN KEY (`uploading_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `captains`
--
ALTER TABLE `captains`
  ADD CONSTRAINT `ALTER TABLE ``captains`` ADD CONSTRAINT captain_player_auto_id_fk` FOREIGN KEY (`captain_player_id`) REFERENCES `players` (`player_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `captains_player_team_id_fk` FOREIGN KEY (`player_team_id`) REFERENCES `teams` (`team_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `captains_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `captains_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coaches`
--
ALTER TABLE `coaches`
  ADD CONSTRAINT `coaches_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coaches_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coach_reports`
--
ALTER TABLE `coach_reports`
  ADD CONSTRAINT `coach_reports_report_team_id_fk` FOREIGN KEY (`report_team_id`) REFERENCES `teams` (`team_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coach_reports_upload_user_group_id_fk` FOREIGN KEY (`uploading_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `upload_updating_user_group_id_fk` FOREIGN KEY (`upload_updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coach_roles`
--
ALTER TABLE `coach_roles`
  ADD CONSTRAINT `coach_role_coach_auto_id_fk` FOREIGN KEY (`coach_auto_id`) REFERENCES `coaches` (`coach_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coach_role_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coach_role_team_id_fk` FOREIGN KEY (`coach_role_team_id`) REFERENCES `teams` (`team_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coach_role_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_updating_user_group_id` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expenditures`
--
ALTER TABLE `expenditures`
  ADD CONSTRAINT `expenditures_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `game_match_type_fk` FOREIGN KEY (`game_match_type`) REFERENCES `match_types` (`match_type_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_team_fk` FOREIGN KEY (`game_team`) REFERENCES `teams` (`team_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `games_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `games_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game_matches`
--
ALTER TABLE `game_matches`
  ADD CONSTRAINT `game_matches_match_level_fk` FOREIGN KEY (`match_level`) REFERENCES `scoregroups` (`levelAutoId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_matches_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_matches_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_id_fk` FOREIGN KEY (`match_game_id`) REFERENCES `games` (`game_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `general_coach_uploads`
--
ALTER TABLE `general_coach_uploads`
  ADD CONSTRAINT `gen_coach_uploads_sport_id_fk` FOREIGN KEY (`upload_sport_id`) REFERENCES `sports` (`sport_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gen_coach_uploads_updating_user_group_id_fk` FOREIGN KEY (`upload_updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gen_coach_uploads_uploading_user_group_id_fk` FOREIGN KEY (`uploading_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hockey_match_scores`
--
ALTER TABLE `hockey_match_scores`
  ADD CONSTRAINT `hms_match_id_fk` FOREIGN KEY (`match_id`) REFERENCES `game_matches` (`match_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hms_player_id_fk` FOREIGN KEY (`player_id`) REFERENCES `match_players` (`match_player_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hms_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hms_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `injury_records`
--
ALTER TABLE `injury_records`
  ADD CONSTRAINT `injury_player_auto_id_fk` FOREIGN KEY (`player_auto_id`) REFERENCES `players` (`player_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `injury_record_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `injury_records_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `match_players`
--
ALTER TABLE `match_players`
  ADD CONSTRAINT `match_player_id_fk` FOREIGN KEY (`match_player_id`) REFERENCES `players` (`player_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_players_id_fk` FOREIGN KEY (`match_id`) REFERENCES `game_matches` (`match_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_players_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_players_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `match_types`
--
ALTER TABLE `match_types`
  ADD CONSTRAINT `match_types_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_types_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `physio_players`
--
ALTER TABLE `physio_players`
  ADD CONSTRAINT `physio_player_id_fk` FOREIGN KEY (`physio_player_id`) REFERENCES `players` (`player_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `physio_players_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `physio_players_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `player_course_id` FOREIGN KEY (`stud_course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `players_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `players_team_id_fk` FOREIGN KEY (`player_team_id`) REFERENCES `teams` (`team_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `players_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `scoach_reports`
--
ALTER TABLE `scoach_reports`
  ADD CONSTRAINT `scoach_reports_updating_user_group_id_fk` FOREIGN KEY (`upload_updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scoach_reports_uploading_user_group_id_fk` FOREIGN KEY (`uploading_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `scoregroups`
--
ALTER TABLE `scoregroups`
  ADD CONSTRAINT `scoregroups_registering_user_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scoregroups_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sc_coaches`
--
ALTER TABLE `sc_coaches`
  ADD CONSTRAINT `sc_coaches_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sc_coaches_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sports`
--
ALTER TABLE `sports`
  ADD CONSTRAINT `sports_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sports_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `team_sport_id_fk` FOREIGN KEY (`team_sport_id`) REFERENCES `sports` (`sport_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teams_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teams_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_attendance`
--
ALTER TABLE `training_attendance`
  ADD CONSTRAINT `ta_player_auto_id_fk` FOREIGN KEY (`player_auto_id`) REFERENCES `players` (`player_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ta_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ta_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_days`
--
ALTER TABLE `training_days`
  ADD CONSTRAINT `trd_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trd_team_id_fk` FOREIGN KEY (`trd_team_id`) REFERENCES `teams` (`team_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trd_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `travel_documents`
--
ALTER TABLE `travel_documents`
  ADD CONSTRAINT `traveldocs_player_id_fk` FOREIGN KEY (`player_id`) REFERENCES `players` (`player_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `traveldocs_registering_user_group_id_fk` FOREIGN KEY (`registering_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `traveldocs_updating_user_group_id_fk` FOREIGN KEY (`updating_user_group_id`) REFERENCES `user_groups` (`group_auto_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
