-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2018 at 09:25 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


INSERT INTO `sports` (`sport_auto_id`, `sport_name`, `sport_status`, `registering_user_id`, `updating_user_id`, `registering_user_group_id`, `updating_user_group_id`) VALUES
(1, 'Rugby', 1, NULL, NULL, NULL, NULL),
(2, 'Volleyball', 1, NULL, NULL, NULL, NULL),
(3, 'Basketball', 1, NULL, NULL, NULL, NULL),
(4, 'Soccer', 1, NULL, NULL, NULL, NULL),
(5, 'Hockey', 1, NULL, NULL, NULL, NULL),
(6, 'Handball', 1, NULL, NULL, NULL, NULL),
(7, 'Karate', 1, NULL, NULL, NULL, NULL),
(8, 'Swimming', 1, NULL, NULL, NULL, NULL),
(9, 'Archery', 1, NULL, NULL, NULL, NULL),
(10, 'Chess', 1, NULL, NULL, NULL, NULL),
(11, 'Scrabble', 1, NULL, NULL, NULL, NULL),
(12, 'Tennis', 1, NULL, NULL, NULL, NULL),
(13, 'Strength & Conditioning', 1, NULL, NULL, NULL, NULL);


INSERT INTO `user_groups` (`group_auto_id`, `group_name`) VALUES
(1, 'Administrators'),
(2, 'Coaches'),
(3, 'Strength and Conditioning');


INSERT INTO `admin_roles` (`role_auto_id`, `role_name`) VALUES
(1, 'Super Administrator'),
(2, 'Sports Administrator');


INSERT INTO `match_types` (`match_type_auto_id`, `match_type_name`, `registering_user_id`, `updating_user_id`, `registering_user_group_id`, `updating_user_group_id`) VALUES
(1, 'Tournament', NULL, NULL, NULL, NULL),
(2, 'League', NULL, NULL, NULL, NULL);



INSERT INTO `scoregroups` (`levelAutoId`, `levelName`, `registering_user_id`, `updating_user_id`, `registering_user_group_id`, `updating_user_group_id`) VALUES
(1, 'Preliminary Scores', NULL, NULL, NULL, NULL),
(2, 'Quarters Score', NULL, NULL, NULL, NULL);



INSERT INTO `admin` (`admin_auto_id`, `admin_staff_id`, `admin_username`, `admin_fname`, `admin_lname`, `admin_nid`, `admin_phone`, `admin_email`, `active_status`, `date_registered`, `date_updated`, `admin_role_id`, `password`, `id_type`, `admin_profile_photo`, `registering_user_id`, `updating_user_id`, `registering_user_group_id`, `updating_user_group_id`) VALUES
(1, 2902, 'smokoro', 'Stephen', 'Mokoro', '31537790', '0719508386', 'smokoro@strathmore.edu', 1, '2018-05-24 00:00:00', '2018-05-24 13:39:59', 1, '', 'National', NULL, 1, 0, 1, NULL);



INSERT INTO `courses` (`course_id`, `course_name`, `faculty`, `registering_user_id`, `updating_user_id`, `registering_user_group_id`, `updating_user_group_id`) VALUES
('ACCA', 'Association of Chartered Certified Accountant', 'School of Accountancy', NULL, NULL, NULL, NULL),
('BA-Dev.S&P', 'Bachelor of Arts in Development Studies and Philosophy', 'School of Humanities and Social Sciences', NULL, NULL, NULL, NULL),
('BAC', 'Bachelor of Arts in Communication', 'School of Humanities and Social Sciences', NULL, NULL, NULL, NULL),
('BAIS', 'Bachelor of Arts in International Studies', 'School of Humanities and Social Sciences', NULL, NULL, NULL, NULL),
('BBIT', 'Bachelor of Business Information Technology', 'Faculty of Information Technology', NULL, NULL, NULL, NULL),
('BBSFE', 'Business Science in Financial Economics', 'Strathmore Institute of Mathematical Sciences', NULL, NULL, NULL, NULL),
('BBSFIN', 'Bachelor of Business Science in Finance', 'Strathmore Institute of Mathematical Sciences', NULL, NULL, NULL, NULL),
('BCOM', 'Bachelor of Commerce', 'Commerce', NULL, NULL, NULL, NULL),
('BHM', 'Bachelor of Hospitality Management', 'School of Hospitality', NULL, NULL, NULL, NULL),
('BIF', 'Bachelor of Science in  Informatics and Computer Science', 'Faculty of Information Technology', NULL, NULL, NULL, NULL),
('BTC', 'Bachelor of Science in Telecommunications', 'Faculty of Information Technology', NULL, NULL, NULL, NULL),
('CIM', 'Chartered Institute of Marketing', 'School of Management and Commerce', NULL, NULL, NULL, NULL),
('CPA', 'Certified Public Accountants', 'School of Accountancy', NULL, NULL, NULL, NULL),
('DBIT', 'Diploma in Information Technology', 'Faculty of Information Technology', NULL, NULL, NULL, NULL),
('DBM', 'Diploma in Business Management', 'School of Management and Commerce', NULL, NULL, NULL, NULL),
('ICS', 'ICS', 'FIT', NULL, NULL, NULL, NULL),
('LLB', 'Bachelor of Laws', 'Law School', NULL, NULL, NULL, NULL),
('MScIT', 'Master of Science in Information Technology', 'Faculty of Information Technology', NULL, NULL, NULL, NULL);


COMMIT;
