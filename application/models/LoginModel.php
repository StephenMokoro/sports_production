<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LoginModel extends CI_Model
{
	function __construct()
		{
		     parent::__construct();
		     $this->load->database();
		}

    //validate admin(sports administrator)
    public function validate_admin($username) 
        {
            $this->db->select('*');
            $this->db->from('admin');
            $this->db->where('admin_username', $username);
            $this->db->where('active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 

                } else 
                        {
                            return false;
                        }
        }
     //validate coach
    public function validate_coach($username) 
        {
            $this->db->select('cs.*,sports.*');
            $this->db->from('coaches cs');
            $this->db->join('sports sports','sports.sport_auto_id=cs.coach_sport_id');
            $this->db->where('cs.coach_username', $username);
            $this->db->where('cs.active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 

                } else 
                        {
                            return false;
                        }
    }
    //validate physio therapist
    public function validate_sc_coach($username) 
        {
            $this->db->select('*');
            $this->db->from('sc_coaches');
            $this->db->where('sc_username', $username);
            $this->db->where('active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 

                } else 
                        {
                            return false;
                        }
        }
}