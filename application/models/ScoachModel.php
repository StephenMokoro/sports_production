<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class ScoachModel extends CI_Model
{
    function __construct()
    {
         parent::__construct();
         $this->load->database();
    }
    //list of active team players
    public function activePlayersList()
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('active_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count all active players 
    public function countAllActivePlayers()
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('active_status',1);
        $result=$this->db->get();
        if ($result->num_rows() > 0 )
        {
            return $result->num_rows();
        }else{
                return 0;
            }
    }
    //all injury  records
    public function allInjuries()
    {
        $this->db->select('p.player_fname, p.player_lname,p.player_auto_id,p.player_other_names,ir.*,t.team_name');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->order_by('ir.injury_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //fetch all injuries per team
    public function injuryRecords($teamId)
    {
        $this->db->select('p.player_fname, p.player_lname,p.player_auto_id,p.player_other_names,ir.*');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where("p.player_team_id",$teamId);
        $this->db->order_by('ir.injury_date','desc');
        $result2=$this->db->get()->result_array();
        return $result2;
    }
    public function injuryRecord($recordId)
    {
        $this->db->select('ir.*, p.player_fname, p.player_lname,p.player_other_names,p.player_auto_id,p.player_profile_photo');
        $this->db->from('injury_records ir');
        $this->db->where('ir.injury_auto_id',$recordId);
        $this->db->join('players p','ir.player_auto_id=p.player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    public function deleteInjuryRecord($recordId)
    {
        $this->db->where('injury_auto_id',$recordId);
        $this->db->delete('injury_records');
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }

    //list of active teams
    public function activeTeamsList()
    {
        $this->db->select('team.*,sport.sport_name');
        $this->db->from('teams team');
        $this->db->join('sports sport','sport.sport_auto_id=team.team_sport_id');
        $this->db->where('team.team_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of active players per team e.g. hockey's scorpions
    public function activePlayersPerTeam($teamId)
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('player_team_id',$teamId);
        $this->db->where('active_status',1);
        $this->db->group_by('player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //get team_specific reports
    public function scuploads()
    {
       $this->db->select('*');
        $this->db->from('scoach_reports');
        return $this->db->get()->result_array();
    }
    public function newSCoachReport($report_details)
    {

        if($this->db->insert('scoach_reports',$report_details))
            {
                // $insert_id = $this->db->insert_id();
                return  true;
            }
             else
                {
                    return false;
                }
    }
    public function activePlayersCount()
    {

        $this->db->select('p.player_auto_id');
        $this->db->from('players p');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where('p.active_status',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //new injury record
    public function newInjuryRecord($injury_details)
    {
         if($this->db->insert('injury_records',$injury_details))
            {
                return  true;
            }
             else
                {
                    return false;
                }
    }
    //Count all injuries to date for active players 
    public function allInjuriesToDateCount()
    {
        $this->db->select('ir.injury_auto_id');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->where('p.active_status',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
     //count of active players who are  on physio
    public function activePlayersOnPhysioCount()
    {

        //select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();

        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//select only players whose player ids  appear on the $result1 above query
        $this->db->where('active_status',1);
        $this->db->group_by('player_auto_id');
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
     //count of active players who are fit to play i.e not on physio
    public function activePlayersNotOnPhysioCount()
    {


        //select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();

        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result1)", NULL, FALSE);//select only players whose player ids do not appear on the $result1 above query
        $this->db->group_by('player_auto_id');
        $this->db->where('active_status',1);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
     public function activePlayersNotOnPhysio()
    {

         //select ids of players who are not on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();
        return $result1;
        
    }
     //list of players for attendance marking
    public function activePlayersForAttendance($teamId)
    {
        //get Ids of players whose attendance date is equivalent today's 
        $this->db->select('attendance.player_auto_id');
        $this->db->from('training_attendance attendance');
        $this->db->where('attendance.training_date ',date('Y-m-d'));
        $result1=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result1)", NULL, FALSE);//select only players whose ids dont appear on the above query
        $this->db->where("player_team_id", $teamId);
        $this->db->where('active_status',1);
        $this->db->order_by('player_fname');
        $result2=$this->db->get()->result_array();
        return $result2;

    }
     //players attendance grouped by date
    public function playerAttendanceGroupedPerDate($teamId)
    {
        //get Ids of players who belong to the teams under the team
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        //select training dates for only the players in the $result2 above
        $this->db->select('training_date');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->group_by('training_date');
        $this->db->order_by('training_date','desc');
        $result2=$this->db->get()->result_array();
        return $result2;
    }
     //count of all players marked present, absent or excused in a particular date
    public function countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();


        //count only attendance status for the above players who are recorded in attendance per given date
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
     //count players marked absent in a particular date
    public function countOfPlayersPresent($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','PRESENT');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
     //count players marked absent in a particular date
    public function countOfPlayersAbsent($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','ABSENT');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players marked excused in a particular date
    public function countOfPlayersExcused($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','EXCUSED');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players on physio in a particular date
    public function countOfPlayersOnPhysio($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','PHYSIO');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }





}