<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class SuserModel extends CI_Model
{
    function __construct()
    {
         parent::__construct();
         $this->load->database();
    }
    //list of courses
    public function coursesList()
    {
        $this->db->select('*');
        $this->db->from('courses');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of active sports list
    public function activeSportsList()
    {
        $this->db->select('*');
        $this->db->from('sports');
        $this->db->where('sport_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
     //list of all sports list
    public function allSportsList()
    {
        $this->db->select('*');
        $this->db->from('sports');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of all teams
    public function allTeamsList()
    {
        $this->db->select('team.*,sport.sport_name');
        $this->db->from('teams team');
        $this->db->join('sports sport','sport.sport_auto_id=team.team_sport_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    public function newTeam($team_info)
    {
        if($this->db->insert('teams',$team_info))
        {
           return true;
        }
         else
            {
                return false;
            }

    }
    public function getTeamCoach($teamId)
    {
        $this->db->select('coach.coach_fname,coach.coach_lname,coach.coach_auto_id');
        $this->db->from('coaches coach');
        $this->db->join('coach_roles cr','cr.coach_auto_id=coach.coach_auto_id');
        $this->db->where('cr.coach_role_team_id',$teamId);
        $this->db->where('cr.coach_role_status',1);
         $result=$this->db->get();
        return $result->row();;

    }
    public function newTeamCoach($teacm_coach_info)
    {
        if($this->db->insert('coach_roles',$teacm_coach_info))
        {
           return true;
        }
         else
            {
                return false;
            }
    }
    //list of active teams
    public function activeTeamsList()
    {
        $this->db->select('team.*,sport.sport_name');
        $this->db->from('teams team');
        $this->db->join('sports sport','sport.sport_auto_id=team.team_sport_id');
        $this->db->where('team.team_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
      //list of sports
    public function sportsList()
    {
        $this->db->select('*');
        $this->db->from('sports');
        $result=$this->db->get()->result_array();
        return $result;
    }
     //players per coach_team(s)
    public function activePlayersCount()
    {
        $this->db->select('p.player_auto_id');
        $this->db->from('players p');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where('p.active_status',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
        {
            return $result->num_rows();
        }else
            {
                return 0;
            }
    }
    //get players per team
    public function activePlayersPerTeam($teamId)
    {

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('player_team_id',$teamId);
        $this->db->where('active_status',1);
        $this->db->order_by('player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //get players per team
    public function activeStudentPlayersPerTeam($teamId)
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('player_team_id',$teamId);
        $this->db->where('active_status',1);
        $this->db->where('stud_id !=',"");
        $this->db->order_by('player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //get all active players
    public function allActivePlayers()
    {

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('active_status',1);
        $this->db->order_by('player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
     //get all active student players
    public function allActiveStudentPlayers()
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('active_status',1);
        $this->db->where('stud_id !=',"");
        $this->db->order_by('player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of players for attendance marking
    public function activePlayersForAttendance($teamId)
    {
        //get Ids of players whose attendance date is equivalent today's 
        $this->db->select('attendance.player_auto_id');
        $this->db->from('training_attendance attendance');
        $this->db->where('attendance.training_date ',date('Y-m-d'));
        $result1=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result1)", NULL, FALSE);//select only players whose ids dont appear on the above query
        $this->db->where("player_team_id", $teamId);
        $this->db->where('active_status',1);
        $this->db->order_by('player_fname');
        $result2=$this->db->get()->result_array();
        return $result2;

    }
    //specific expense details 
    public function expenditureDetails($expenseId)
    {
        $this->db->select('expense.*,team.team_name');
        $this->db->from('expenditures expense');
        $this->db->join('teams team','team.team_auto_id=expense.expense_team_auto_id');
        $this->db->where('expense.expense_auto_id',$expenseId);
        $result=$this->db->get()->result_array();
        return $result;
    }

    // expenditure per team
    public function allExpenses()
    {
        $this->db->select('exs.*');
        $this->db->from('expenditures exs');
        $this->db->order_by('exs.expense_date','DESC');
        $result=$this->db->get()->result_array();
        return $result;
    }
    
    // expenditure per team
    public function expensesPerTeam($teamId)
    {
        $this->db->select('exs.*');
        $this->db->from('expenditures exs');
        $this->db->where('exs.expense_team_auto_id',$teamId);
        $this->db->order_by('exs.expense_date','DESC');
        $result=$this->db->get()->result_array();
        return $result;
    }

    // get tournaments
    public function getTournaments($teamId)
    {
        $this->db->select('g.*');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id');
        $this->db->where('g.game_team',$teamId);
        $this->db->where('g.game_match_type',1);//tournament
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    // get specific game details
    public function gameDetails($gameId,$gameType)
    {
        $this->db->select('g.*,team.team_name');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id');
        $this->db->where('g.game_auto_id',$gameId);
        $this->db->where('g.game_match_type',$gameType);//tournament or league
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of active players per team e.g. hockey's scorpions who are not on physio
    public function activeUninjuredPlayersPerTeam($teamId)
    {
        //first, select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result1)", NULL, FALSE);//select only players whose player ids  don't appear on the above query
        $this->db->where('player_team_id',$teamId);
        $this->db->where('active_status',1);
        $this->db->group_by('player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count of active players who are  on physio
    public function activePlayersOnPhysioCount()
    {

        //select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();

        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//select only players whose player ids  appear on the $result1 above query
        $this->db->where('active_status',1);
        $this->db->group_by('player_auto_id');
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count of active players who are fit to play i.e not on physio
    public function activePlayersNotOnPhysioCount()
    {


        //select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();

        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result1)", NULL, FALSE);//select only players whose player ids do not appear on the $result1 above query
        $this->db->group_by('player_auto_id');
        $this->db->where('active_status',1);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }

    //  game matches
    public function gameMatches($gameId)
    {
        $this->db->select('gs.*,team.team_alias');
        $this->db->from('game_matches gs');
        $this->db->join('games game','game.game_auto_id=gs.match_game_id');
        $this->db->join('teams team','game.game_team=team.team_auto_id');
        $this->db->where('gs.match_game_id',$gameId);
        $this->db->order_by('gs.match_date','DESC');
        $this->db->order_by('gs.match_start_time','desc');
        // $this->db->limit(3);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //get match players
    public function getMatchPlayersAndScores($matchId)
    {
        $this->db->select('mp.*, hms.scores,hms.green_cards,hms.yellow_cards,hms.red_cards,pls.player_fname,pls.player_lname,pls.player_auto_id,pls.player_profile_photo');
        $this->db->from('match_players mp');
        $this->db->join('players pls', 'pls.player_auto_id=mp.match_player_id');
        $this->db->join('hockey_match_scores hms', 'hms.player_id=mp.match_player_id','left');
        $this->db->where('mp.match_id',$matchId);
        return $this->db->get()->result_array();
    }
    //get match Scores
    public function getHockeyMatchScores($matchId)
    {
        $this->db->select('scores.*');
        $this->db->from('hockey_match_scores scores');
        $this->db->join('game_matches gms', 'gms.match_auto_id=scores.match_id');
        $this->db->where('gms.match_auto_id',$matchId);
        $this->db->where('scores.score_time!=',NULL);
        return $this->db->get()->result_array();
    }
    
    //get match Yellow Cards
    public function getMatchCards($matchId)
    {
       $query=" SELECT SUM(yellow_cards)  yellows, SUM(red_cards)  reds, SUM(green_cards)  greens,  SUM(scores) as suscore
        FROM hockey_match_scores WHERE match_id='$matchId'";

        return $this->db->query($query)->result_array();
    }

     //list of all active coaches
    public function activeCoachesList()
    {
        $this->db->select('c.*,sport.*');
        $this->db->from('coaches c');
        $this->db->join('sports sport','c.coach_sport_id=sport.sport_auto_id');
        $this->db->where('c.active_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
   




    //list of all active captains
    public function activeCaptainsList()
    {
        $this->db->select('c.*, t.*, p.player_fname, p.player_lname,p.player_auto_id, p.kin_phone,p.kin_alt_phone, p.player_phone');
        $this->db->from('captains c, teams t');
        $this->db->join('players p','p.player_auto_id=c.captain_player_id');
        $this->db->where('c.player_team_id=t.team_auto_id');
        $this->db->where('c.active_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    

    public function activeadminList()
    {
        $this->db->select('admin.*, roles.role_name');
        $this->db->from('admin admin');
        $this->db->join('admin_roles roles','admin.admin_role_id=roles.role_auto_id');
        $this->db->where('admin.active_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    public function activeScoachList()
{
    $this->db->select('*');
    $this->db->from('sc_coaches');
    $this->db->where('active_status',1);
    $result=$this->db->get()->result_array();
    return $result;
}
    //coach profile
    public function coachProfile($coachId)
    {
        $this->db->select('coach.*,sport.sport_name');
        $this->db->from('coaches coach');
        $this->db->join('sports sport','sport.sport_auto_id=coach.coach_sport_id');
        $this->db->where('coach.coach_auto_id',$coachId);
        $result=$this->db->get()->result_array();
        return $result;
    }
     public function updateCoachWithPhoto($coach_details,$coachId,$initFile)
    {
            $this->db->where('coach_auto_id',$coachId);
            $this->db->update('coaches',$coach_details);
            unlink("uploads/profile_photos/coaches/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
    }
    //coach update
    public function updateCoachNoPhoto($coach_details,$coachId)
    {
            $this->db->where('coach_auto_id',$coachId);
            $this->db->update('coaches',$coach_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //captain registration
    public function newCaptain($captain_details)
    {
    if($this->db->insert('captains',$captain_details))
        {
           return true;
        }
         else
            {
                return false;
            }
    }
    //coach registration
    public function newCoach($coach_details)
    {
        if($this->db->insert('coaches',$coach_details))
            {
                return true;
            }else
                {
                    return false;
                }
    }
   
    public function newAdmin($admin_details)
    {
        if($this->db->insert('admin',$admin_details))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    public function newScoach($sc_details)
    {
        if($this->db->insert('sc_coaches',$sc_details))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    public function scoachProfile($scId)
    {
        $this->db->select('*');
        $this->db->from('sc_coaches');
        $this->db->where('sc_auto_id',$scId);
        $result=$this->db->get()->result_array();
        return $result;
    }
        
    public function adminProfile($adminId)
    {
        $this->db->select('admin.*, roles.role_name');
        $this->db->from('admin admin');
        $this->db->join('admin_roles roles','admin.admin_role_id=roles.role_auto_id');
        $this->db->where('admin.active_status',1);
        $this->db->where('admin.admin_auto_id',$adminId);
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function updateAdminNoPhoto($admin_details,$adminId)
    {
            $this->db->where('admin_auto_id',$adminId);
            $this->db->update('admin',$admin_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
     public function updateInjury($injury_details,$recordId)
    {
            $this->db->where('injury_auto_id',$recordId);
            $this->db->update('injury_records',$injury_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }

    public function updateAdminWithPhoto($admin_details,$adminId,$initFile)
    {
            $this->db->where('admin_auto_id',$adminId);
            $this->db->update('admin',$admin_details);
            unlink("uploads/profile_photos/admins/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    public function updateScoachNoPhoto($sc_details,$scId)
    {
            $this->db->where('sc_auto_id',$scId);
            $this->db->update('sc_coaches',$sc_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }

    public function updateScoach($sc_details,$scId,$initFile)
    {
            $this->db->where('sc_auto_id',$scId);
            $this->db->update('sc_coaches',$sc_details);
            unlink("uploads/profile_photos/scoaches/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    public function captainProfile($playerId,$captainId)
    {
        $this->db->select('cap.*,t.*,players.*');
        $this->db->from('captains cap');
        $this->db->join('teams t','cap.player_team_id=t.team_auto_id');
        $this->db->join('players players','cap.captain_player_id=players.player_auto_id');
        $this->db->where('cap.captain_player_id',$playerId);
        $this->db->where('cap.captain_auto_id',$captainId);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //captain profile update
    public function updateCaptain($captain_details,$captainId)
    {
        $this->db->where('captain_auto_id',$captainId);
            $this->db->update('captains',$captain_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //player profile
    public function playerProfile($playerId)
    {
        $this->db->select('p.*,team.*');
        $this->db->from('players p');
        $this->db->join('teams team','team.team_auto_id=p.player_team_id');
        $this->db->where('p.player_auto_id',$playerId);
        $this->db->group_by('p.player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
     //deactivate player
    public function deactivatePlayer($newstate,$playerId)
    {
            $this->db->where('player_auto_id',$playerId);
            $this->db->update('players',$newstate);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //player profile update: new photo uploaded
    public function updatePlayer($player_details,$player_auto_id,$initFile)
    {
            $this->db->where('player_auto_id',$player_auto_id);
            $this->db->update('players',$player_details);
            unlink("uploads/profile_photos/players/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    
    //player profile update: no photo uploaded
    public function updatePlayerNoPhoto($player_details,$player_auto_id)
    {
            $this->db->where('player_auto_id',$player_auto_id);
            $this->db->update('players',$player_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //list of training days per team
    public function trainingDaysPerTeam($teamId)
    {

        $this->db->select('*');
        $this->db->from('training_days');
        $this->db->where("trd_team_id",$teamId);
        $this->db->order_by("trd_year","DESC");
        $result2=$this->db->get()->result_array();
        return $result2;
    }
    //update training days
    public function updateTrDays($tr_days_details,$trdId)
    {
         $this->db->where('trd_year',$trdId);
            $this->db->update('training_days',$tr_days_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    public function trdList($trdId)
    {
        $this->db->select('trds.*,t.team_name');
        $this->db->from('training_days trds');
        $this->db->join('teams t','t.team_auto_id=trds.trd_team_id');
        $this->db->where('trds.trd_auto_id',$trdId);
        $result=$this->db->get()->result_array();
        return $result;
    }
    public function trainingList($trainingTeamId,$trainingDate)
    {
      //get Ids of players who belong to the team
        $this->db->select('player_auto_id AS playerId');
        $this->db->from('players');
        $this->db->where("player_team_id", $trainingTeamId);
        $result1=$this->db->get_compiled_select();

        //select training info for only the players in the $result2 above
        $this->db->select('attendance.*, player.player_fname,player.player_lname,player.player_profile_photo');
        $this->db->from('training_attendance attendance');
        $this->db->join('players player','player.player_auto_id=attendance.player_auto_id');
        $this->db->where("attendance.player_auto_id IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance.training_date',$trainingDate);
        $this->db->order_by('player.player_fname');
        $result2=$this->db->get()->result_array();
        return $result2;
    }
    //get player attendance state per record
    public function getPlayerAttendance($recordId)
    {
        $this->db->select('attendance.*, player.player_fname, player.player_lname');
        $this->db->from('training_attendance attendance');
        $this->db->join('players player','player.player_auto_id=attendance.player_auto_id');
        $this->db->where('attendance.ta_auto_id',$recordId);
         $result=$this->db->get()->result_array();
        return $result;
    }
    //update player attendance
    public function updatePlayerAttendance($attendanceInfo,$recordId)
    {
            $this->db->where('ta_auto_id',$recordId);
            $this->db->update('training_attendance',$attendanceInfo);
            $affected=$this->db->affected_rows();
            if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
    }
    //delete player training attendance
    public function deletePlayerAttendance($recordId)
    {
        $this->db->where('ta_auto_id',$recordId);
        $this->db->delete('training_attendance');
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
     //delete team training attendance per date
    public function deleteTeamAttendancePerDate($trainingTeamId,$trainingDate)
    {
        //get Ids of players who belong to the team
        $this->db->select('player_auto_id AS playerId');
        $this->db->from('players');
        $this->db->where("player_team_id", $trainingTeamId);
        $result1=$this->db->get_compiled_select();

        //delete training info for only the players in the $result2 above
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('training_date',$trainingDate);
        $this->db->delete('training_attendance');
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }

    //players attendance grouped by date
    public function playerAttendanceGroupedPerDate($teamId)
    {
        //get Ids of players who belong to the teams under the team
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        //select training dates for only the players in the $result2 above
        $this->db->select('training_date');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->group_by('training_date');
        $this->db->order_by('training_date','desc');
        $result2=$this->db->get()->result_array();
        return $result2;
    }
    //count players marked absent in a particular date
    public function countOfPlayersPresent($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','PRESENT');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players marked absent in a particular date
    public function countOfPlayersAbsent($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','ABSENT');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players marked excused in a particular date
    public function countOfPlayersExcused($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','EXCUSED');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count of all players marked present, absent or excused in a particular date
    public function countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();


        //count only attendance status for the above players who are recorded in attendance per given date
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players on physio in a particular date
    public function countOfPlayersOnPhysio($attendanceDate,$teamId)
    {
        //get Ids of players of a particular team 
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("player_team_id", $teamId);
        $result1=$this->db->get_compiled_select();

        
        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result1)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance_state','PHYSIO');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //list of all active players per team e.g. hockey and their travel documents
    public function activePlayersAndPassports($teamId)
    {
        $this->db->select('p.*,td.*');
        $this->db->from('players p');
        $this->db->join('travel_documents td','p.player_auto_id=td.player_id','left');
        $this->db->where("p.player_team_id",$teamId);
        $this->db->where('p.active_status',1);
        $this->db->order_by('p.player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //player and travel document
    public function playerPass($passId)
    {
        $this->db->select('p.*,td.*');
        $this->db->from('players p');
        $this->db->join('travel_documents td','p.player_auto_id=td.player_id');
        $this->db->where("td.passport_auto_id",$passId);
        $this->db->where('p.active_status',1);
        $this->db->order_by('p.player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //delete player passport
    public function deletePlayerPassport($passportId)
    {
        $this->db->where('passport_auto_id',$passportId);
        $this->db->delete('travel_documents');
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }

     //list of all active players from all teams and their travel documents
    
    public function allActivePlayersAndPassports()
    {
        $this->db->select('p.*,td.*');
        $this->db->from('players p');
        $this->db->join('travel_documents td','p.player_auto_id=td.player_id','left');
        $this->db->where('p.active_status',1);
        $this->db->order_by('p.player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //get passport details
    public function getPassportDetails($passportId)
    {
        $this->db->select('pls.*,td.*');
        $this->db->from('players pls');
        $this->db->join('travel_documents td','pls.player_auto_id=td.player_id','right');
        $this->db->where('td.passport_auto_id',$passportId);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //update passport info when no new passport photo is uploaded
    public function updatePassportNoPhoto($passport_details,$passportId)
    {
        $this->db->where('passport_auto_id',$passportId);
        $this->db->update('travel_documents',$passport_details);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }

    }
    //update passport info when no new passport photo is uploaded. Includes deleting the previous passport image
    public function updatePassport($passport_details,$passportId,$initFile)
    {
            $this->db->where('passport_auto_id',$passportId);
            $this->db->update('travel_documents',$passport_details);
            unlink("uploads/travel_documents".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //injury management records
    public function injuryRecords($teamId)
    {
        $this->db->select('p.player_fname, p.player_lname,p.player_auto_id,p.player_other_names,p. player_profile_photo, ir.*');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where("p.player_team_id",$teamId);
        $this->db->order_by('ir.injury_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
     //Count all injuries to date for active players 
    public function allInjuriesToDateCount()
    {
        $this->db->select('ir.injury_auto_id');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->where('p.active_status',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //all injury  records
    public function allInjuries()
    {
        $this->db->select('p.player_fname, p.player_lname,p.player_auto_id,p.player_other_names,ir.*,t.team_name');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->order_by('ir.injury_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //individual injury record
    public function injuryRecord($recordId)
    {
        $this->db->select('ir.*, p.player_fname, p.player_lname,p.player_other_names,p.player_auto_id,p.player_profile_photo');
        $this->db->from('injury_records ir');
        $this->db->where('ir.injury_auto_id',$recordId);
        $this->db->join('players p','ir.player_auto_id=p.player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //delete injury record
    public function deleteInjuryRecord($recordId)
    {
        $this->db->where('injury_auto_id',$recordId);
        $this->db->delete('injury_records');
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
    //get team_specific reports
    public function teamUploads($teamId)
    {
       $this->db->select('report.*,team.team_name');
        $this->db->from('coach_reports report');
        $this->db->join('teams team', 'team.team_auto_id=report.report_team_id');
        $this->db->where('report.report_team_id',$teamId);
        return $this->db->get()->result_array();
    }
    //get coach general uploads[sport uploads]
    public function sportUploads($sportId)
    {
       $this->db->select('uploads.*,sport.sport_name');
        $this->db->from('general_coach_uploads uploads');
        $this->db->join('sports sport', 'sport.sport_auto_id=uploads.upload_sport_id');
        $this->db->where('uploads.upload_sport_id',$sportId);
        return $this->db->get()->result_array();
    }
    public function addPlayerPspt($passport_details)
    {
        if($this->db->insert('travel_documents',$passport_details))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    //get admin-specific uploads
    public function adminUploads()
    {
       $this->db->select('uploads.*,admin.admin_fname,admin.admin_lname');
        $this->db->from('admin_uploads uploads');
        $this->db->join('admin admin', 'admin.admin_auto_id=uploading_user_id');
        $this->db->where('uploads.uploading_user_group_id',1);
        return $this->db->get()->result_array();
    }
     //get general admin uploads
    public function adminBroadcastUploads()
    {
       $this->db->select('uploads.*,admin.admin_fname,admin.admin_lname');
        $this->db->from('admin_general_uploads uploads');
        $this->db->join('admin admin', 'admin.admin_auto_id=uploading_user_id');
        $this->db->where('uploads.uploading_user_group_id',1);
        return $this->db->get()->result_array();
    }
    

}