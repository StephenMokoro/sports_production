<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Coachmodel extends CI_Model
{
    function __construct()
    {
         parent::__construct();
         $this->load->database();
    }

    //player registration
    public function newPlayer($player_details)
    {
    	if($this->db->insert('players',$player_details))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //Tournament match registration
    public function newTournMatch($match_details)
    {
        if($this->db->insert('game_matches',$match_details))
            {
                $insert_id = $this->db->insert_id();
                return  $insert_id;
            }
             else
                {
                    return false;
                }
    }
    //delete/undo the latest insert if players list insertion fails
    public function deleteTournMatch($tournMatchId)
     {
            $this->db->where('match_auto_id',$tournMatchId);
            $this->db->delete('game_matches');
            $affected=$this->db->affected_rows();
            if($affected>0)
                {
                    return true;

                }else
                     {
                        return false;
                     }
     }
    //Tournament match players registration
    public function tournMatchPlayers($playInfo)
    {
        if($this->db->insert_batch('match_players',$playInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //get match players
    public function getMatchPlayersAndScores($matchId)
    {
        $this->db->select('mp.*, hms.scores,hms.green_cards,hms.yellow_cards,hms.red_cards,pls.player_fname,pls.player_lname,pls.player_auto_id,pls.player_profile_photo');
        $this->db->from('match_players mp');
        $this->db->join('players pls', 'pls.player_auto_id=mp.match_player_id');
        $this->db->join('hockey_match_scores hms', 'hms.player_id=mp.match_player_id','left');
        $this->db->where('mp.match_id',$matchId);
        return $this->db->get()->result_array();
    }
     //get match players whose score has not been recorded
    public function getMatchPlayers($matchId)
    {
        //select match players whose scores and cards have been recorded->for exclusion in the list of those whose scores are yet to be recorded
        $this->db->select('match_player_id');
        $this->db->from('match_players mp');
        $this->db->join('hockey_match_scores hms', 'hms.player_id=mp.match_player_id');
        $this->db->where('mp.match_id',$matchId);
        $result2=$this->db->get_compiled_select();


        $this->db->select('mp.*, pls.player_fname,pls.player_lname,pls.player_auto_id,pls.player_profile_photo');
        $this->db->from('match_players mp');
        $this->db->join('players pls', 'pls.player_auto_id=mp.match_player_id');
        $this->db->where("mp.match_player_id NOT IN ($result2)", NULL, FALSE);//select only players whose ids dont appear on the above query
        $this->db->where('mp.match_id',$matchId);
        return $this->db->get()->result_array();
    }
    //get match Scores
    public function getHockeyMatchScores($matchId)
    {
        $this->db->select('scores.*');
        $this->db->from('hockey_match_scores scores');
        $this->db->join('game_matches gms', 'gms.match_auto_id=scores.match_id');
        $this->db->where('gms.match_auto_id',$matchId);
        $this->db->where('scores.score_time!=',NULL);
        return $this->db->get()->result_array();
    }
    //Insert hockey match scores 
    public function newHockeyScore($scoreDetails)
    {
        if($this->db->insert('hockey_match_scores',$scoreDetails))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //Insert hockey match scores 
    public function newHockeyOpponentScore($opponentScoreDetails,$matchId)
    {
        $this->db->where('match_auto_id',$matchId);
        $this->db->update('game_matches',$opponentScoreDetails);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }

     
    //get match Yellow Cards
    public function getMatchCards($matchId)
    {
       $query=" SELECT SUM(yellow_cards)  yellows, SUM(red_cards)  reds, SUM(green_cards)  greens,  SUM(scores) as suscore
        FROM hockey_match_scores WHERE match_id='$matchId'";

        return $this->db->query($query)->result_array();
    }

    //player profile
    public function playerProfile($playerId)
    {
        $this->db->select('p.*,team.*');
        $this->db->from('players p');
        $this->db->join('teams team','team.team_auto_id=p.player_team_id');
        $this->db->where('p.player_auto_id',$playerId);
        $this->db->group_by('p.player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //player profile update: no photo uploaded
    public function updatePlayerNoPhoto($player_details,$player_auto_id)
    {
            $this->db->where('player_auto_id',$player_auto_id);
            $this->db->update('players',$player_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //player profile update: new photo uploaded
    public function updatePlayer($player_details,$player_auto_id,$initFile)
    {
            $this->db->where('player_auto_id',$player_auto_id);
            $this->db->update('players',$player_details);
            unlink("uploads/profile_photos/players/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //disable player
    public function disablePlayer($updateDetails,$player_auto_id)
    {
        $this->db->where('player_auto_id',$player_auto_id);
        $this->db->update('players',$updateDetails);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;
                }else
                    {
                        return false;
                    }
    }
    //delete player with all records
    public function deletePlayer($player_auto_id)
    {
        $this->db->where('auto_id',$auto_id);
        $this->db->delete('players');
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;
                }else
                    {
                        return $this->db->error();
                    }
    }
    //list of all active players per sport e.g. hockey
    public function activePlayersListPerCoachTeams($coachId)
    {
        //get ids of teams the coach is assigned and use 
        //the array of team ids to select only players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('p.*,t.team_name');
        $this->db->from('players p');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where("p.player_team_id IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on the above query
        $this->db->where('p.active_status',1);
        $this->db->order_by('p.player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of all active student players per coach Teams e.g. hockey
    public function activeStudentPlayersListPerCoachTeams($coachId)
    {
          //get ids of teams the coach is assigned and use 
        //the array of team ids to select only players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('p.*,t.team_name');
        $this->db->from('players p');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        // $this->db->where('player_sport_id',$sportId);
        $this->db->where("p.player_team_id IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on the above query
        $this->db->where('p.stud_id !=',"");
        $this->db->where('p.active_status',1);
        $this->db->order_by('p.player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of all active players per team e.g. hockey and their travel documents
    public function activePlayersAndPassports($coachId)
    {
          //get ids of teams the coach is assigned and use 
        //the array of team ids to select only players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('p.*,td.*');
        $this->db->from('players p');
        $this->db->join('travel_documents td','p.player_auto_id=td.player_id','left');
        $this->db->where("p.player_team_id IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on the above query
        $this->db->where('p.active_status',1);
        $this->db->order_by('p.player_fname');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of players for attendance marking
    public function activePlayersForAttendance($coachId)
    {
         //get ids of teams the coach is assigned and use 
        //the array of team ids to select only players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players whose attendance date is equivalent today's 
        $this->db->select('attendance.player_auto_id');
        $this->db->from('training_attendance attendance');
        $this->db->where('attendance.training_date ',date('Y-m-d'));
        $result2=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result2)", NULL, FALSE);//select only players whose ids dont appear on the above query
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $this->db->where('active_status',1);
        $this->db->order_by('player_fname');
        $result3=$this->db->get()->result_array();
        return $result3;

    }
    //players attendance grouped by date
    public function playerAttendanceGroupedPerDate($coachId)
    {

          //get ids of teams the coach is assigned and use 
        //the array of team ids to select only attendance for players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players who belong to the teams under the coach
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $result2=$this->db->get_compiled_select();

        //select training dates for only the players in the $result2 above
        $this->db->select('training_date');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result2)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->group_by('training_date');
        $this->db->order_by('training_date','desc');
        $result3=$this->db->get()->result_array();
        return $result3;
    }


    //count players marked present in a particular date
    public function countOfPlayersPresent($attendanceDate,$coachId)
    {
        //get ids of teams the coach is assigned and use 
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players who belong to the teams under the coach
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $result2=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result2)", NULL, FALSE);//select only attendance for players whose ids appear on the above query
        $this->db->where('attendance_state','PRESENT');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players marked absent in a particular date
    public function countOfPlayersAbsent($attendanceDate,$coachId)
    {
        //get ids of teams the coach is assigned and use 
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players who belong to the teams under the coach
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $result2=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result2)", NULL, FALSE);//select only attendance for players whose ids appear on the above query
        $this->db->where('attendance_state','ABSENT');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count players marked excused in a particular date
    public function countOfPlayersExcused($attendanceDate,$coachId)
    {
        //get ids of teams the coach is assigned and use 
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players who belong to the teams under the coach
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $result2=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result2)", NULL, FALSE);//select only attendance for players whose ids appear on the above query
        $this->db->where('attendance_state','EXCUSED');
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count of all players marked present, absent or excused in a particular date
    public function countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$coachId)
    {
        //get ids of teams the coach is assigned and use 
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players who belong to the teams under the coach
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $result2=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('training_attendance');
        $this->db->where("`player_auto_id` IN ($result2)", NULL, FALSE);//select only attendance for players whose ids appear on the above query
        $this->db->where('training_date',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //list of active players per team e.g. hockey's scorpions
    public function activePlayersPerTeam($teamId)
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('player_team_id',$teamId);
        $this->db->where('active_status',1);
        $this->db->group_by('player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of active players per team e.g. hockey's scorpions who are not on physio
    public function activeUninjuredPlayersPerTeam($teamId)
    {
        //first, select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result1=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('players');
        $this->db->where("`player_auto_id` NOT IN ($result1)", NULL, FALSE);//select only players whose player ids  don't appear on the above query
        $this->db->where('player_team_id',$teamId);
        $this->db->where('active_status',1);
        $this->db->group_by('player_auto_id');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count of active players per coach team(s) who are  on physio
    public function activePlayersOnPhysioPerTeamCount($coachId)
    {

        //get ids of teams the coach is assigned and use 
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();


        //select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result2=$this->db->get_compiled_select();

        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on $result1 above
        $this->db->where("`player_auto_id` IN ($result2)", NULL, FALSE);//select only players whose player ids  appear on the $result2 above query
        $this->db->group_by('player_auto_id');
        $this->db->where('active_status',1);
        $result3=$this->db->get();
        if ( $result3->num_rows() > 0 )
            {
                return $result3->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count of active players per coach team(s) who are fit to play i.e not on physio
    public function activePlayersNotOnPhysioPerTeamCount($coachId)
    {
        //get ids of teams the coach is assigned and use 
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();


        //select ids of players who are on physio from table injuries
        $this->db->select('player_auto_id');
        $this->db->from('injury_records');
        $this->db->where('physio_status',1);
        $result2=$this->db->get_compiled_select();

        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team ids  do not appear on $result1 above
        $this->db->where("`player_auto_id` NOT IN ($result2)", NULL, FALSE);//select only players whose player ids  appear on the $result2 above query
        $this->db->group_by('player_auto_id');
        $this->db->where('active_status',1);
        $result3=$this->db->get();
        if ( $result3->num_rows() > 0 )
            {
                return $result3->num_rows();
            }else
                {
                    return 0;
                }
    }

    //list of all active  players
    public function allActivePlayersList()
    {
        $this->db->select('*');
        $this->db->from('players');
        $this->db->where('active_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
 


    //list of active teams per coach team(s) e.g. Scorpions and Gladiators for Hockey Sport
    public function activeTeamsListPerCoachTeams($coachId)
    {
         //get ids of teams the coach is assigned and use 
        //the array of team ids to select only players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('teams');
        $this->db->where("team_auto_id IN ($result1)", NULL, FALSE);//select only teams whose team ids  appear on the above query
        $this->db->where('team_status',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of courses
    public function coursesList()
    {
        $this->db->select('*');
        $this->db->from('courses');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //save training days
    public function newTrDays($tr_days_details)
    {
        if($this->db->insert('training_days',$tr_days_details))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //list of training days per team
    public function trDaysList($coachId)
    {
        //get ids of teams the coach is assigned and use 
        //the array of team ids to select only trds
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('trds.*,t.team_alias');
        $this->db->from('training_days trds');
        $this->db->join('teams t','t.team_auto_id=trds.trd_team_id');
        $this->db->where("trds.trd_team_id IN ($result1)", NULL, FALSE);
        $this->db->order_by("trds.trd_year","DESC");
        $result2=$this->db->get()->result_array();
        return $result2;
    }
    //training days for per team
    public function trdList($trdId)
    {
        $this->db->select('trds.*,t.team_name');
        $this->db->from('training_days trds');
        $this->db->join('teams t','t.team_auto_id=trds.trd_team_id');
        $this->db->where('trds.trd_auto_id',$trdId);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //update training days
    public function updateTrDays($tr_days_details,$trdId)
    {
         $this->db->where('trd_auto_id',$trdId);
            $this->db->update('training_days',$tr_days_details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //new injury record
    public function newInjuryRecord($injury_details)
    {
         if($this->db->insert('injury_records',$injury_details))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //injury management records
    public function injuryRecords($coachId)
    {

        //get ids of teams the coach is assigned and use 
        //the array of team ids to select only injury records for players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('p.player_fname, p.player_lname,p.player_auto_id,p.player_other_names,ir.*');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where("p.player_team_id IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on the above query
        $this->db->order_by('ir.injury_date','desc');
        $result2=$this->db->get()->result_array();
        return $result2;
    }
    //Count all injuries to date for active players per coach team(s)
    public function allInjuriesToDateCount($coachId)
    {

        //get ids of teams the coach is assigned and use 
        //the array of team ids to select only injury records for players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('ir.injury_auto_id');
        $this->db->from('injury_records ir');
        $this->db->join('players p','p.player_auto_id=ir.player_auto_id');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where("p.player_team_id IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on the above query
        $this->db->where('p.active_status',1);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }

    //get specific injury management record
    public function getInjuryRecord($injury_id)
    {
        $this->db->select('ir.*, p.player_fname, p.player_lname,p.player_auto_id');
        $this->db->from('injury_records ir');
        $this->db->join('players p','ir.player_auto_id=p.player_auto_id');
        $this->db->where('ir.injury_auto_id',$injury_id);
        $result=$this->db->get()->result_array();
        return $result;
    }

    // expenditure per team
    public function expenses($teamId)
        {
            $this->db->select('exs.*');
            $this->db->from('expenditures exs');
            $this->db->where('exs.expense_team_auto_id',$teamId);
            $this->db->order_by('exs.expense_date','DESC');
            $result=$this->db->get()->result_array();
            return $result;
        }

    //specific expense details 
    public function expenseDetails($expsId)
    {
        $this->db->select('exs.*');
        $this->db->from('expenditures exs');
        $this->db->where('exs.expense_auto_id',$expsId);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //new expenditure without receipt
    public function newExpenseWithoutReceipt($expdetails)
    {
        if($this->db->insert('expenditures',$expdetails))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //new expenditure without receipt
    public function newExpenseWithReceipt($expdetails)
    {
        if($this->db->insert('expenditures',$expdetails))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }

    public function expenditureDetails($expenseId)
        {
            $this->db->select('expense.*,team.team_name');
            $this->db->from('expenditures expense');
            $this->db->join('teams team','team.team_auto_id=expense.expense_team_auto_id');
            $this->db->where('expense.expense_auto_id',$expenseId);
            $result=$this->db->get()->result_array();
            return $result;
        }
     // get specific game details
    public function gameDetails($gameId,$gameType)
    {
        $this->db->select('g.*,team.team_name');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id');
        $this->db->where('g.game_auto_id',$gameId);
        $this->db->where('g.game_match_type',$gameType);//tournament or league
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    // get tournaments
    public function getTournaments($teamId)
    {
        $this->db->select('g.*,team.team_auto_id,team.team_name');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id');
        $this->db->where('g.game_team',$teamId);
        $this->db->where('g.game_match_type',1);//tournament
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    // Hockey women leagues
    public function getScorpionLeagues($teamId)
    {
        $this->db->select('g.*,team.team_alias');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id');
        $this->db->where('g.game_sport_auto_id',$teamId);
        $this->db->where('g.game_team',2);//scorpions
        $this->db->where('g.game_match_type',2);//league
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    // Hockey Men leagues
    public function getGladiatorsLeagues($teamId)
    {
        $this->db->select('g.*,team.*');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id');
        $this->db->where('g.game_sport_auto_id',$teamId);
        $this->db->where('g.game_team',1);//gladiators
        $this->db->where('g.game_match_type',2);//league
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    // men tournaments
    public function getMenTournas($sportId)
    {
        $this->db->select('g.*,team.*');
        $this->db->from('games g');
        $this->db->join('teams team', 'g.game_team=team.team_auto_id AND team.team_category="Men"');
        $this->db->where('g.game_sport_auto_id',$sportId);
        $this->db->where('g.game_match_type',1);//tournament
        $this->db->order_by('g.game_start_date','desc');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //  game matches
    public function gameMatches($gameId)
    {
        $this->db->select('gs.*,team.team_alias');
        $this->db->from('game_matches gs');
        $this->db->join('games game','game.game_auto_id=gs.match_game_id');
        $this->db->join('teams team','game.game_team=team.team_auto_id');
        $this->db->where('gs.match_game_id',$gameId);
        $this->db->order_by('gs.match_date','DESC');
        $this->db->order_by('gs.match_start_time','desc');
        // $this->db->limit(3);
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function newTournament($tournamentdetails)
    {
        if($this->db->insert('games',$tournamentdetails))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    public function trainingList($trainingDate,$coachId)
    {
          //get ids of teams the coach is assigned and use 
        //the array of team ids to select only attendance for players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        //get Ids of players who belong to the teams under the coach
        $this->db->select('player_auto_id');
        $this->db->from('players');
        $this->db->where("`player_team_id` IN ($result1)", NULL, FALSE);//select only players whose team_ids appear on the above query
        $result2=$this->db->get_compiled_select();

        //select training info for only the players in the $result2 above
        $this->db->select('attendance.*, player.player_fname,player.player_lname,player.player_profile_photo');
        $this->db->from('training_attendance attendance');
        $this->db->join('players player','player.player_auto_id=attendance.player_auto_id');
        $this->db->where("attendance.player_auto_id IN ($result2)", NULL, FALSE);//only players whose ids appear on the above query
        $this->db->where('attendance.training_date',$trainingDate);
        $this->db->order_by('player.player_fname');
        $result3=$this->db->get()->result_array();
        return $result3;
    }
    public function trainingAttendance($attendanceInfo)
    {
        if($this->db->insert_batch('training_attendance',$attendanceInfo))
            {
                return true;
            }else
                {
                    return false;
                }
    }

    //save new passport details
    public function addPlayerPspt($passport_details)
    {
        if($this->db->insert('travel_documents',$passport_details))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    //get passport details
    public function getPassportDetails($passportId)
    {
        $this->db->select('pls.*,td.*');
        $this->db->from('players pls');
        $this->db->join('travel_documents td','pls.player_auto_id=td.player_id','right');
        $this->db->where('td.passport_auto_id',$passportId);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //update passport info when no new passport photo is uploaded
    public function updatePassportNoPhoto($passport_details,$passportId)
    {
        $this->db->where('passport_auto_id',$passportId);
        $this->db->update('travel_documents',$passport_details);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }

    }
    //update passport info when no new passport photo is uploaded. Includes deleting the previous passport image
    public function updatePassport($passport_details,$passportId,$initFile)
    {
            $this->db->where('passport_auto_id',$passportId);
            $this->db->update('travel_documents',$passport_details);
            unlink("uploads/travel_documents".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //dashboard counts
    //players per coach_team(s)
    public function activePlayersCount($coachId)
    {

        //get ids of teams the coach is assigned and use 
        //the array of team ids to select only players from those teams
        $this->db->select('cr.coach_role_team_id');
        $this->db->from('coach_roles cr');
        $this->db->where('cr.coach_auto_id',$coachId);
        $result1=$this->db->get_compiled_select();

        $this->db->select('p.player_auto_id');
        $this->db->from('players p');
        $this->db->join('teams t','t.team_auto_id=p.player_team_id');
        $this->db->where("p.player_team_id IN ($result1)", NULL, FALSE);//select only players whose team ids  appear on the above query
        $this->db->where('p.active_status',1);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
    //get team_specific reports
    public function teamUploads($teamId)
    {
       $this->db->select('report.*,team.team_name');
        $this->db->from('coach_reports report');
        $this->db->join('teams team', 'team.team_auto_id=report.report_team_id');
        $this->db->where('report.report_team_id',$teamId);
        return $this->db->get()->result_array();
    }
    //get team_specific general uploads
    public function generalCoachUploads($sportId)
    {
       $this->db->select('uploads.*,sport.sport_name');
        $this->db->from('general_coach_uploads uploads');
        $this->db->join('sports sport', 'sport.sport_auto_id=uploads.upload_sport_id');
        $this->db->where('uploads.upload_sport_id',$sportId);
        return $this->db->get()->result_array();
    }
      //get general admin uploads
    public function generalAdminUploads()
    {
       $this->db->select('uploads.*,admin.admin_fname,admin.admin_lname');
        $this->db->from('admin_general_uploads uploads');
        $this->db->join('admin admin', 'admin.admin_auto_id=uploading_user_id');
        $this->db->where('uploads.uploading_user_group_id',1);
        return $this->db->get()->result_array();
    }
    
    //save coach report details
    public function newCoachReport($report_details)
    {

        if($this->db->insert('coach_reports',$report_details))
            {
                // $insert_id = $this->db->insert_id();
                return  true;
            }
             else
                {
                    return false;
                }
    }
     //save coach report details
    public function newGeneralUpload($upload_details)
    {

        if($this->db->insert('general_coach_uploads',$upload_details))
            {
                // $insert_id = $this->db->insert_id();
                return  true;
            }
             else
                {
                    return false;
                }
    }

}//close Coachmodel