<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*@author Mokoro Stephen 
*/
class Admin extends CI_Controller
{

function __construct()
	{
	     parent::__construct();
	     $this->load->model('AdminModel','adminmodel');
	}
	public function index()
	{
		$this->load->view('login');
	}
	public function logout()
    {
        $this->cas->logout();
    }
	//dashboard view page for admin
	public function dashboard()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['activeplayerscount']=$this->adminmodel->activePlayersCount();//count all players active 
			$list['allinjuriestodatecount']=$this->adminmodel->allInjuriesToDateCount();//count all injuries to date for active players 
			$list['allactiveplayersonphysio']=$this->adminmodel->activePlayersOnPhysioCount();//count all active players who are on physio for active players 
			$list['allactiveplayersnotonphysio']=$this->adminmodel->activePlayersNotOnPhysioCount();//count all active players who are not on physio for active players 

			$this->load->view('admin/dashboard',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
		
	}
	//captains view page
	public function captains()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$captains_list['teams']=$this->adminmodel->activeTeamsList();
			$captains_list['captains']=$this->adminmodel->activeCaptainsList();
			$this->load->view('admin/captain_registration',$captains_list);
		}else{
            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
            redirect(base_url(('admin')));
        }
	}
	//physio therapist page
	public function physiotherapists()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$phy_thera_list['phythes']=$this->adminmodel->activePhysiotheraPistList();
			$this->load->view('admin/physio_therapist_registration',$phy_thera_list);
		}else{
            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
            redirect(base_url(('admin')));
        }
	}
	//coach profile
	public function coachprofile()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$staffId=$this->input->post('staffId', TRUE);
			$coach_profile['coach_profile']=$this->adminmodel->coachProfile($staffId);
			$this->load->view('admin/coach_profile',$coach_profile);
		}else{
            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
            redirect(base_url(('admin')));
        }
	}
	//Captain profile 
	public function captainprofile()
	{
		$player_profile['captains']=$this->adminmodel->captainProfile($this->input->post('playerId', TRUE));
		$this->load->view('admin/captain_profile',$player_profile);
	}


	//list of players from each team
	public function players()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			$list['players'] =$this->adminmodel->allActivePlayers();
			$list['teams']=$this->adminmodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)//loop and create another array for players per team append it to original array $list['teams']
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->adminmodel->activePlayersPerTeam($teamId);
			}

			$this->load->view('admin/players',$list);

		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//list of student players from each team
	public function studentplayers()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			$list['players'] =$this->adminmodel->allActiveStudentPlayers();
			$list['teams']=$this->adminmodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)//loop and create another array for players per team append it to original array $list['teams']
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->adminmodel->activeStudentPlayersPerTeam($teamId);
			}

			$this->load->view('admin/studentplayers',$list);// 

		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//Player profile (more_about_player) for super admin
	public function playerprofile()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);
			if(isset ($playerId) && strlen($playerId)){
				$player_profile['player_profile']=$this->adminmodel->playerProfile($playerId);
				$this->load->view('admin/player_profile',$player_profile);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('admin/players')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//get player info for disable 
	public function getplayer()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId',TRUE);
			$list =$this->adminmodel->playerProfile($playerId);

	        $data = array();
	        foreach ($list as $player) 
	            {
	                $fullname=$player['player_fname']. " ". $player['player_lname'];
	                $data= array('playerId' => $player['player_auto_id'],'fullName'=>$fullname);
	            }

	        	echo json_encode($data);//data should be a plain array...
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//get player passport info for delete 
	public function getpass()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$passId=$this->input->post('passId',TRUE);
			$list =$this->adminmodel->playerPass($passId);

	        $data = array();
	        foreach ($list as $player) 
	            {
	                $fullname='Passport for '.$player['player_fname']. " ". $player['player_lname'];
	                $data= array('passId' => $player['passport_auto_id'],'fullName'=>$fullname);
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//disable player
	public function disableplayer()
	{
		$playerId=$this->input->post('playerId',TRUE);
		$reasonInactive=$this->input->post('reasonInactive',TRUE);
		$newstate=array('active_status'=>0,'reason_inactive'=>$reasonInactive);
		$result=$this->adminmodel->deactivatePlayer($newstate,$playerId);
		if($result)
		{
			$_SESSION['msg'] = array('error' => "",'success' => "Player disabled!");
           redirect(base_url(('admin/players')));
		}else 
			{
				$_SESSION['msg'] = array('error' => "Not disabled",'success' => "");
               redirect(base_url(('admin/players')));
			}

	}
	//player profile edit (edit player )
	public function editplayer()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);//
			if(isset ($playerId) && strlen($playerId)){
				$player_profile['courses']=$this->adminmodel->coursesList();
				$player_profile['player_profile']=$this->adminmodel->playerProfile($playerId);
				$list['teams']=$this->adminmodel->activeTeamsList();
				$this->load->view('admin/edit_player',$player_profile);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('admin/players')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
		
	}
	//update player information
	public function  updateplayer()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$player_auto_id=$this->input->post('playerAutoId',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			//check if student id was not entered. If it was not entered, save null for both student id and course
			$studid=$this->input->post('studentID', TRUE);
			if($studid=="")
				{
					$student_id=NULL;
					$student_course_id=NULL;
				}else{
						$student_id=$this->input->post('studentID', TRUE);
		    			$student_course_id=$this->input->post('course', TRUE);
					}

			$player_fname=$this->input->post('firstName', TRUE);
			$player_lname=$this->input->post('lastName', TRUE);
			$player_other_names=$this->input->post('otherNames', TRUE);
			$player_dob=$this->input->post('dateOfBirth', TRUE);
			$player_gender=$this->input->post('gender', TRUE);
			$player_nid=$this->input->post('idNo', TRUE);
			$player_id_type=$this->input->post('idType', TRUE);
			$player_phone_no=$this->input->post('phoneNumber', TRUE);
			$player_email=$this->input->post('emailAddress', TRUE);
		   
			$kin_fname=$this->input->post('kinFirstName', TRUE);
			$kin_lname=$this->input->post('kinLastName', TRUE);
		    $kin_other_names=$this->input->post('kinOtherNames', TRUE);
			$kin_nid_no=$this->input->post('kinNationalID', TRUE);
		    $kin_phone_no=$this->input->post('kinPhoneNumber', TRUE);
		    $kin_alt_phone_no=$this->input->post('kinAltPhoneNumber', TRUE);
		    $kin_email=$this->input->post('kinEmailAddress', TRUE);
		    $kin_current_residence=$this->input->post('kinCurrentResidence', TRUE);
		    $current_height=$this->input->post('currentHeight', TRUE);
		    $current_weight=$this->input->post('currentWeight', TRUE);
		    $prev_high_school=$this->input->post('previousHighSchool', TRUE);
		    $prev_play_status=$this->input->post('prevStatus', TRUE);
		    $prev_team=$this->input->post('previousTeam', TRUE);
		    $teamId=$this->input->post('teamId', TRUE);
		    $highest_achievement=$this->input->post('highestAchievement', TRUE);
			$player_residence=$this->input->post('currentResidence', TRUE);
		    $agreement=$this->input->post('agreement',TRUE);

		    $userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];


			$config['upload_path']          = 'uploads/profile_photos/players';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		      // $config['max_size']             = 100;
		    $config['max_width']            = 500;
		    $config['max_height']           = 500;
			$config['file_name']			=md5(time().$player_phone_no.$player_fname.$player_lname);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) {
		    	$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'agreement'=>$agreement,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

					$result=$this->adminmodel->updatePlayerNoPhoto($player_details,$player_auto_id);
					if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Player updated!");
			           redirect(base_url(('admin/players')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made",'success' => "");
			               redirect(base_url(('admin/players')));
						}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'agreement'=>$agreement,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

						$result=$this->adminmodel->updatePlayerNoPhoto($player_details,$player_auto_id);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Player info updated. </span> However, ".$this->upload->display_errors());
					           redirect(base_url(('admin/players')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
					               redirect(base_url(('admin/players')));
								}
					}else{
						   	$data =$this->upload->data();//details of uploaded file

							$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'agreement'=>$agreement,'player_profile_photo'=>$config['file_name'].$data['file_ext'],'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

							$result=$this->adminmodel->updatePlayer($player_details,$player_auto_id,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Mentee info updated");
						           redirect(base_url(('admin/players')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
						               redirect(base_url(('admin/players')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
		
	}
	//player next of kin profile
	public function nextofkin()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);
			if(isset ($playerId) && strlen($playerId)){
				$kin['nxtkin']=$this->adminmodel->playerProfile($playerId);
			$this->load->view('admin/nxt_kin',$kin);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('admin/players')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
		
	}

	//player passports view page
	public function playerpassports()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
	    	$list['players']=$this->adminmodel->allActivePlayersAndPassports();
	    	$list['teams']=$this->adminmodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players']=$this->adminmodel->activePlayersAndPassports($teamId);
			}
			$this->load->view('admin/playerpassports',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	public function playerpsptdetails()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$passportId=$this->input->post('passportId',TRUE);
			if(isset($passportId))
			{
				//create a session to handle browser refresh i.e not lose posted passport id
				$_SESSION['sessdata'] = array_merge($_SESSION['sessdata'], array('passportId'=>$passportId));

				$list['passport_details']=$this->adminmodel->getPassportDetails($passportId);
				$this->load->view('admin/player_passport_details',$list);

			}else {
					$passportId=$_SESSION['sessdata']['passportId'];//use session if web browser hard refreshed
					$list['passport_details']=$this->adminmodel->getPassportDetails($passportId);
					$this->load->view('admin/player_passport_details',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//load player passport editing page
	public function editplayerpspt()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$passportId=$this->input->post('passportId',TRUE);
			$playerName=$this->input->post('playerName',TRUE);

			if(isset ($passportId) && strlen($passportId) && isset($playerName) && strlen($playerName))
			{
				$_SESSION['sessdata'] = array_merge($_SESSION['sessdata'],array('pst_player_id'=>$passportId,'pst_player_name'=>$playerName));

				$list['passport_details']=$this->adminmodel->getPassportDetails($passportId);

				$this->load->view('admin/edit_passport',$list);
			}else{
					$passportId=$_SESSION['sessdata']['passportId'];//use session if web browser hard refreshed
					$list['passport_details']=$this->adminmodel->getPassportDetails($passportId);
					$this->load->view('admin/edit_passport',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	
	//update player passport
	public function updateplayerpspt()
	{
    	if($_SESSION['sessdata']['admin_login']==TRUE)
    	{
	    	$passportId=$this->input->post('passportId',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$passport_number=$this->input->post('passportNo',TRUE);
			$issue_date=$this->input->post('dateOfIssue',TRUE);
			$expiry_date=$this->input->post('dateOfExpiry',TRUE);
			$issue_country=$this->input->post('issueCountry',TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];


			$config['upload_path']          = 'uploads/travel_documents';
			$config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
			$config['max_size']             = 700;
			$config['max_width']            = 700;
			$config['max_height']           = 700;
			$config['file_name']			=md5(time().$passport_number);
			$config['overwrite']           = true;
			$config['file_ext_tolower']     = true;

			$this->load->library('upload', $config);

			if (empty($_FILES['photo']['name'])) 
			{
			    	$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

					$result=$this->adminmodel->updatePassportNoPhoto($passport_details,$passportId);
					if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "Passport updated");
				           redirect(base_url(('admin/playerpassports')));
						}else 
							{
								$_SESSION['msg']= array('error' => "No changes made ",'success' => "");
				               redirect(base_url(('admin/playerpassports')));
							}

				}else{ 

						if(!$this->upload->do_upload('photo'))
						{
							
							$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);
							$result=$this->adminmodel->updatePassportNoPhoto($passport_details,$passportId);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>passport details updated. </span> However, ".$this->upload->display_errors());
						           redirect(base_url(('admin/playerpassports')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
						               redirect(base_url(('admin/playerpassports')));
									}
						}else{

							   	$data =$this->upload->data();//details of uploaded file

								$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'passport_photo'=>$config['file_name'].$data['file_ext'],'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

								//new session of admin profile photo
			                    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array("passport_photo"=>$config['file_name'].$data['file_ext']));

								$result=$this->adminmodel->updatePassport($passport_details,$passportId,$initFile);
								if($result)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "Passport info updated");
							           redirect(base_url(('admin/playerpassports')));
									}else 
										{
											$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
							               redirect(base_url(('admin/playerpassports')));
										}


						}

				}
		}else{
            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
            redirect(base_url(('admin')));
    	}

	}
	//list of training days per team from each team
	public function trainingdays()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			$list['teams']=$this->adminmodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['tr_days'] =$this->adminmodel->trainingDaysPerTeam($teamId);
			}

			$this->load->view('admin/training_days',$list);

		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//load page for editing training days
	public function editdays()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

				$trdId=$this->input->post('trdId', TRUE);
				$training_days['tr_days']=$this->adminmodel->trdList($trdId);
				$this->load->view('admin/edit_trds',$training_days);

		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
		
	}
	//updating training days
	public function updatedays()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$trdId=$this->input->post('trdId', TRUE);
			$training_year=$this->input->post('trainingYear', TRUE);
			$january=$this->input->post('january', TRUE);
			$february=$this->input->post('february', TRUE);
			$march=$this->input->post('march', TRUE);
			$april=$this->input->post('april', TRUE);
			$may=$this->input->post('may', TRUE);
			$june=$this->input->post('june', TRUE);
			$july=$this->input->post('july', TRUE);
			$august=$this->input->post('august', TRUE);
			$september=$this->input->post('september', TRUE);
			$october=$this->input->post('october', TRUE);
			$november=$this->input->post('november', TRUE);
			$december=$this->input->post('december', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$tr_days_details=array('january'=>$january,'february'=>$february,'march'=>$march,'april'=>$april,'may'=>$may,'june'=>$june,'july'=>$july,'august'=>$august,'september'=>$september,'october'=>$october,'november'=>$november,'december'=>$december,'trd_year'=>$training_year,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);
			
			$result=$this->adminmodel->updateTrDays($tr_days_details,$trdId);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Training days updated");
		           redirect(base_url(('admin/trainingdays')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made",'success' => "");
		               redirect(base_url(('admin/trainingdays')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//training attendance view
	public function trainingattendance()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['teams']=$this->adminmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->adminmodel->activePlayersForAttendance($teamId);

				$list['teams'][$id]['trainingattendance'] =$this->adminmodel->playerAttendanceGroupedPerDate($teamId);

				$count=0;//create a counter. this will be used to create unique attendance status count arrays within attendance array in teams array as below. The $count and team_auto_id are used to achieve this unique naming

				foreach($list['teams'][$id]['trainingattendance'] as $id2 => $attendance)
				{
					$attendanceDate=$attendance['training_date'];//get the training date. To be used to get attendance per training date
					$countOfPlayersMarkedPresentAbsentOrExcused=$this->adminmodel->countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$teamId);//pass attendance date and teamid (from first loop above) to get the attendance per team per date

					$countOfPlayersPresent=$this->adminmodel->countOfPlayersPresent($attendanceDate,$teamId);//counting no of players in each attendance status
					$countOfPlayersAbsent=$this->adminmodel->countOfPlayersAbsent($attendanceDate,$teamId);
					$countOfPlayersExcused=$this->adminmodel->countOfPlayersExcused($attendanceDate,$teamId);

					$countOfPlayersOnPhysio=$this->adminmodel->countOfPlayersOnPhysio($attendanceDate,$teamId);
					//here is how we create unique arrays as above described
					$list['teams'][$id]['trainingattendance'][$id2]['countAll_'.$team['team_auto_id'].$count] =$countOfPlayersMarkedPresentAbsentOrExcused;
					$list['teams'][$id]['trainingattendance'][$id2]['countPresent_'.$team['team_auto_id'].$count] =$countOfPlayersPresent;
					$list['teams'][$id]['trainingattendance'][$id2]['countAbsent_'.$team['team_auto_id'].$count] =$countOfPlayersAbsent;
					$list['teams'][$id]['trainingattendance'][$id2]['countExcused_'.$team['team_auto_id'].$count] =$countOfPlayersExcused;
					$list['teams'][$id]['trainingattendance'][$id2]['countPhysio_'.$team['team_auto_id'].$count] =$countOfPlayersOnPhysio;
					$count=$count+1;
				}

			}
			$this->load->view('admin/training_attendance',$list);
			// print_r($list);
		}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
	                redirect(base_url(('admin')));
	        }
	}
	//list of training attendance 
	public function traininglist()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

	    	$trainingTeamId=$this->input->post('teamId',TRUE);
	    	$trainingDate=$this->input->post('trainingDate',TRUE);
			if(isset($trainingTeamId) && strlen($trainingTeamId)&& isset($trainingDate) && strlen($trainingDate)){
				// set session for  date and team id
			    $_SESSION['sessdata'] = array_merge($_SESSION['sessdata'], array('trainingDate' =>$trainingDate,'trainingTeamId'=>$trainingTeamId));
		    	$list['traininglist']=$this->adminmodel->trainingList($trainingTeamId,$trainingDate);
				$this->load->view('admin/players_attendance_list',$list);
			}else{
				$trainingTeamId=$_SESSION['sessdata']['trainingTeamId'];
	    		$trainingDate=$_SESSION['sessdata']['trainingDate'];
	    		$list['traininglist']=$this->adminmodel->trainingList($trainingTeamId,$trainingDate);
				$this->load->view('admin/players_attendance_list',$list);
			}

    	}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
    }


	//injuries
	public function injuries()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			$list['injuries']=$this->adminmodel->allInjuries();//get all injuries
			$list['teams']=$this->adminmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['injuries']=$this->adminmodel->injuryRecords($teamId);
			}
				$this->load->view('admin/injury_records',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//more about injury record
	public function injurydetails()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId', TRUE);
			if(isset ($recordId) && strlen($recordId)){
				$record['injury_record']=$this->adminmodel->injuryRecord($recordId);
				$this->load->view('admin/injury_more',$record);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('admin/injuries')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
		
		
	}
	//team reports uploads view page
	public function teamuploads()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['teams']=$this->adminmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['team_uploads']=$this->adminmodel->teamUploads($teamId);
			}


			$this->load->view('admin/team_uploads',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//sport reports uploads view page
	public function sportuploads()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['sports']=$this->adminmodel->sportsList();
			foreach($list['sports'] as $id => $sport)
			{
				$sportId=$sport['sport_auto_id'];
				$list['sports'][$id]['sport_uploads']=$this->adminmodel->sportUploads($sportId);
			}

			$this->load->view('admin/sport_uploads',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//admin uploads view page
	public function adminuploads()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['admin_uploads']=$this->adminmodel->adminUploads();
			$this->load->view('admin/admin_uploads',$list);
			
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//general admin uploads- viewable by all sports
	public function generaluploads()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['general_uploads']=$this->adminmodel->generalAdminUploads();
			$this->load->view('admin/admin_general_uploads',$list);
			
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//download coach_report
	public function download_coach_report($filename = NULL) 
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/coach_uploads/coach_reports/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//team_expenditures
	public function expenditures()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			$list['expenses']=$this->adminmodel->allExpenses();
			$list['teams']=$this->adminmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['expenses']=$this->adminmodel->expensesPerTeam($teamId);
			}
				$this->load->view('admin/team_expenses',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//team_expenditures
	public function addexpenditure()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			
			$list['teams']=$this->adminmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['injuries']=$this->adminmodel->injuryRecords($teamId);
			}
				$this->load->view('admin/new_team_expense',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}

	//record new team expenditure
	public function newexpenditure()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {

			$teamId=$this->input->post('teamId',TRUE);
			$expenseDate=$this->input->post('expenseDate', TRUE);
			$cash=$this->input->post('cash', TRUE);
			
			if(strlen($this->input->post('lpoNo', TRUE))=="")
			{
				$lpo=NULL;
				$lpoNo=NULL;
			}else{

				$lpo=$this->input->post('lpo', TRUE);
				$lpoNo=$this->input->post('lpoNo', TRUE);
			}
			$actualCost=$this->input->post('actualCost', TRUE);
			$receipt=$this->input->post('receipt',TRUE);
			$exComment=$this->input->post('comments', TRUE);
			$date_recorded= date("Y-m-d H:i:s");

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$config['upload_path']          = 'uploads/expenditures/teams/';
		    $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx';
		    // $config['max_size']             = 100;
		    // $config['max_width']            = 500;
		    // $config['max_height']           = 500;
		    $config['file_name']			=md5(time().$expenseDate.$teamId.$date_recorded);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

			 $this->load->library('upload', $config);

			 //if no photo uploaded
			  if (empty($_FILES['receipt']['name'])) 
			  {

				$expdetails=array('expense_team_auto_id'=>$teamId,'expense_date'=>$expenseDate,'expense_cash'=>$cash,'expense_lpo_no'=>$lpoNo,'expense_lpo_amount'=>$lpo,'actual_expenditure'=>$actualCost,'expense_comment'=>$exComment,'expense_record_date'=>$date_recorded,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);
				$result=$this->adminmodel->newExpenseWithoutReceipt($expdetails);
					if($result)
						{
							$_SESSION['msg'] = array('edit'=>'','error' => "",'success' => "Expenditure added ");
		                   redirect(base_url(('admin/expenditures')));
						}else 
							{
								$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add expenditure",'success' => "");
			                   redirect(base_url(('admin/expenditures')));
							}
				}else{
						if (!$this->upload->do_upload('receipt'))
						    {   
						        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						       	redirect(base_url(('admin/expenditures')));
						    }else{
					   
						       		$data =$this->upload->data();
						    		//details of uploaded file
						    			$expdetails=array('expense_team_auto_id'=>$teamId,'expense_date'=>$expenseDate,'expense_cash'=>$cash,'expense_lpo_no'=>$lpoNo,'expense_lpo_amount'=>$lpo,'actual_expenditure'=>$actualCost,'expense_comment'=>$exComment,'expense_record_date'=>$date_recorded,'receipt_file_name'=>$config['file_name'].$data['file_ext'],'receipt_file_ext'=>$$data['file_ext'],'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

							    	$result=$this->adminmodel->newExpenseWithReceipt($expdetails);
									if($result)
										{
											$_SESSION['msg'] = array('error' => "",'success' => "New expense added");
								           redirect(base_url(('admin/expenditures')));
										}else 
											{
												$_SESSION['msg'] = array('error' => "Expense failed to add.",'success' => "");
								               redirect(base_url(('admin/expenditures')));
											}
							}
			
					}
			}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	public function viewteamexpense()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$expenseId=$this->input->post('expenseId',TRUE);
			$list =$this->adminmodel->expenditureDetails($expenseId);

	        $data = array();
	        foreach ($list as $expense) 
	            {
	                $data= array('recordId' => $expense['expense_auto_id'],'teamName'=>$expense['team_name'],'info'=>'
	                	<div class="col-md-6">
	                	<br>
		                	<div style="margin-bottom:10px;">
		                		<b>Expenditure Date</b>
		                	</div> '.date_format(date_create($expense['expense_date']),"D j<\s\up>S</\s\up> M, Y").'<br><br>
		                </div>
		                <div class="col-md-6">
	                	<br>
		                	<div style="margin-bottom:10px;">
		                		<b>Date Recorded</b>
		                	</div> '.date_format(date_create($expense['expense_record_date']),"D j<\s\up>S</\s\up> M, Y").'<br><br>
		                </div>
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b> Cash (Kshs)</b>
		                	</div> '.$expense['expense_cash'].'<br><br> 
		               </div>
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b>Actual Costs (Kshs)</b>
		                	</div> '.$expense['actual_expenditure'].'<br><br> 
		                </div>
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b>LPO (Kshs)</b>
		                	</div> '.$expense['expense_lpo_amount'].'<br><br> 
		                </div>
		               
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b>LPO No.</b>
		                	</div> '.$expense['expense_lpo_no'].'<br><br>
		                </div>
		                <div class="col-md-12">
		                	<div style="margin-bottom:10px;">
		                		<b>Comments</b>
		                	</div> '.$expense['expense_comment'].'<br><br>
		                </div>');
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	public function download_receipt($filename = NULL) 
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/expenditures/teams/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//team tournaments view page
	public function tournaments()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$list['teams']=$this->adminmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['tournaments']=$this->adminmodel->getTournaments($teamId);
			}


			$this->load->view('admin/tournaments',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//more about team expenditure
	public function getgameinfo()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$gameId=$this->input->post('gameId',TRUE);
			$gameType=$this->input->post('gameType',TRUE);
			$action=$this->input->post('action',TRUE);
			$list =$this->adminmodel->gameDetails($gameId,$gameType);

	        $data = array();
	        foreach ($list as $game) 
	            {
	            	if($action=="add")
	            	{
		                $data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	<div class="col-md-12">
		                	<br>
			                	<div style="margin-bottom:10px;">
			                		<b>Start Date: </b>
			                	'.date_format(date_create($game['game_start_date']),"D j<\s\up>S</\s\up> M, Y").'</div> 		                
			                </div>
			                <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
			                <div class="form-group col-md-12 col-lg-12">
	                            <label for="recordId" class="control-label" style="font-weight:normal;"></label>
	                            <textarea type="number" name="recordId"  class=" form-control" id="recordId" required="required" placeholder="Summary of the tournament" style="height:150px;"></textarea>
	                        </div>
		                	');
	            	}else if($action=="summaryedit"){
	            		$data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	<div class="col-md-12">
		                	<br>
			                	<div style="margin-bottom:10px;">
			                		<b>Start Date: </b>
			                	'.date_format(date_create($game['game_start_date']),"D j<\s\up>S</\s\up> M, Y").'</div> 		                
			                </div>
			                <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
			                <div class="form-group col-md-12 col-lg-12">
	                            <label for="recordId" class="control-label" style="font-weight:normal;"></label>
	                            <textarea type="text" name="recordId"  class=" form-control" id="recordId" required="required" placeholder="Summary of the tournament" style="height:150px;">'.$game['game_summary'].'</textarea>
	                        </div>
		                	');
	            	}else if($action=="tournedit"){
	            		$data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	
			                 
	                        <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
	                        <div class="col-md-12 col-lg-12">
	                		<br>
                              <label for="gameStartDate" class="control-label">Start Date</label>
                              <div class="form-group">
                                  <div class="input-group date" id="gameStartDate">
                                      <input type="text" class="form-control" readonly="true" name="gameStartDate" value="'.$game['game_start_date'].'"'.'/>
                                      <span class="input-group-addon">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                            </div>
                             <div class="form-group col-md-12 col-lg-12" style="display:;">
	                            <label for="gameTitle" class="control-label" style="font-weight:normal;">Title<span class="star">*</span></label>
	                            <input type="text" name="gameTitle"  class=" form-control" id="gameTitle" required="required" value="'.$game['game_title'].'">
	                        </div>
			                
		                	');
	            	}
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//load tournament matches page
	public function tournmatches()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
		    $teamId=$this->input->post('teamId',TRUE);
			$sportName=$this->input->post('sportName',TRUE);
			$gameId=$this->input->post('gameId',TRUE);
			$tournamentTitle=$this->input->post('tournamentTitle',TRUE);

			if(isset($gameId) && strlen($gameId)){
				// set session for tournament id
				$team = $this->db->query("SELECT team.team_name FROM teams team WHERE team.team_auto_id='$teamId' LIMIT 1")->row();//get team name
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('tournamentId' =>$gameId,'tournamentTitle'=>$tournamentTitle,'sportName'=>$sportName,'tournamentTeamName'=>$team->team_name)); 
			    

				$list['players']=$this->adminmodel->activeUninjuredPlayersPerTeam($teamId);

				$list['matches']=$this->adminmodel->gameMatches($gameId);

				foreach($list['matches'] as $id =>$match)//loop and create another array for matchPlayers and append it to original array of matches
				{
					$matchId=$match['match_auto_id'];
					$list['matches'][$id]['matchplayers'] =$this->adminmodel->getMatchPlayersAndScores($matchId);//list of match players and scores--- append match players to array matches
					$list['matches'][$id]['cards'] =$this->adminmodel->getMatchCards($matchId);//get count of cards per match

				}


				//dynamically generate links to tournament match views. Ensure that view pages of tournament matches for each sport are in folder -sportname- and are named following the convention sportnametournamentmatches Note: your sports table must have sport names as one word e.g. volleyball not volley ball
				$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'tournamentmatches';

				$this->load->view('admin/'.$link_to_view_per_sport,$list);
			}else{

				$sportName=$_SESSION['sessdata']['sportName'];//from session
				$teamId=$_SESSION['sessdata']['tournamentTeamId'];//from session
				$gameId=$_SESSION['sessdata']['tournamentId'];//from session
				
				$list['players']=$this->adminmodel->activeUninjuredPlayersPerTeam($teamId);
				$list['team_name']=$_SESSION['sessdata']['tournamentTeamName'];//from session;
				$list['matches']=$this->adminmodel->gameMatches($gameId);

				foreach($list['matches'] as $id =>$match)//loop and create another array for matchPlayers and append it to original array of matches
				{
					$matchId=$match['match_auto_id'];
					$list['matches'][$id]['matchplayers'] =$this->adminmodel->getMatchPlayersAndScores($matchId);//list of match players and scores--- append match players to array matches
					$list['matches'][$id]['cards'] =$this->adminmodel->getMatchCards($matchId);//get count of cards per match

				}
				$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'tournamentmatches';

				$this->load->view('admin/'.$link_to_view_per_sport,$list);
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}

	//record new tournament match 
	public function newtournmatch()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$tournamentId=$_SESSION['sessdata']['tournamentId'];//from session created in loading tournaments matches page
			
			$matchDate=$this->input->post('matchDate',TRUE);
			$matchVenue=$this->input->post('matchVenue',TRUE);

			$matchStartTime= date("H:i", strtotime($this->input->post('matchStartTime',TRUE)));//convert to 24 hr
			$matchOpponents=$this->input->post('matchOpponents',TRUE);
			$matchLevel=$this->input->post('matchLevel',TRUE);

			$playerList=$this->input->post('playerList[]',TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$match_details=array('match_game_id'=>$tournamentId,'match_date'=>$matchDate,'match_start_time'=>$matchStartTime,'match_opponents'=>$matchOpponents,'match_venue'=>$matchVenue,'match_level'=>$matchLevel,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

			if(!empty($playerList)){
				//create array of players list to be inserted to a different table once the match details are added successfully

			    $_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('playerList' =>$playerList));

			    $result=$this->adminmodel->newTournMatch($match_details);
			    if(!$result)
					{
						$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add match",'success' => "");
		               redirect(base_url(('admin/tournmatches')));
					}else{
							$tournMatchId=$result;//auto id of the just inserted row. this will be used to undo/delete the record if players insertion fails.
							$playInfo=array();
						     foreach($playerList as $playerId ) /*loop through and get individual player Ids*/
					            {
					                $playInfo[]=array('match_id'=>$tournMatchId,'match_player_id'=>$playerId,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

					                //pass array of data to model for saving
					                // $success=$this->model->tournMatchPlayers($playInfo);
					            }
						         	$success=$this->adminmodel->tournMatchPlayers($playInfo);
						         	if($success){
						                	$_SESSION['msg'] = array('edit'=>'','error' => "",'success' => "Match successfully added");
						                   redirect(base_url(('admin/tournmatches')));
						               }else 
											{
												//delete last inserted match as a rollback
												$delresult=$this->adminmodel->deleteTournMatch($tournMatchId);
												if($delresult)
													{
														$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add match",'success' => "");
									                   	redirect(base_url(('admin/tournmatches')));
									                 }else{
									                 	$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add match. Only match details added",'success' => "");
									                 }
											}

							
						}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }



	}
	//new  tournament score input page
	public function tournamentscoresheet()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$sportName=$_SESSION['sessdata']['sportName'];//from login session
			
			$matchId=$this->input->post('matchId',TRUE);
			$matchLevel=$this->input->post('matchLevel',TRUE);
			
			$matchDetails=$this->input->post('matchDetails',TRUE);
			if(isset ($matchId) && strlen($matchId) && isset($matchDetails) && strlen($matchDetails))
				{
					// set session for tournament id
				    $_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('tournamentMatchId' =>$matchId,'matchDetails'=>$matchDetails,'tournamentMatchLevel'=>$matchLevel)); 

					$list['players']=$this->adminmodel->getMatchPlayers($matchId);
					$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'scoresheet';
					
					$this->load->view('admin/'.$link_to_view_per_sport,$list);
				}else{
						$matchId=$_SESSION['sessdata']['tournamentMatchId'];//from session set above
						$list['players']=$this->adminmodel->getMatchPlayers($matchId);
						$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'scoresheet';
						$this->load->view('admin/'.$link_to_view_per_sport,$list);
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	//saving new hockey score
	public function newhockeyscore()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$matchId=$_SESSION['sessdata']['tournamentMatchId'];//from session created on loading score sheet page
			$playerId=$this->input->post('playerId',TRUE);
			$scores=$this->input->post('scores',TRUE);
			$greenCards=$this->input->post('greenCards',TRUE);
			$yellowCards=$this->input->post('yellowCards',TRUE);
			$redCards=$this->input->post('redCards',TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			if(isset($playerId) && strlen($playerId) && isset($matchId) && strlen($matchId) ){
				$scoreDetails=array('player_id'=>$playerId,'match_id'=>$matchId,'scores'=>$scores,'green_cards'=>$greenCards,'yellow_cards'=>$yellowCards,'red_cards'=>$redCards,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);
				
				$result=$this->adminmodel->newHockeyScore($scoreDetails);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New scores added");
                		redirect(base_url(('admin/tournamentscoresheet')));

					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to record scores",'success' => "");
                			redirect(base_url(('admin/tournamentscoresheet')));

						}
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}
	//get hockey match score
	public function hockeymatchscore()
	{
		if($_SESSION['sessdata']['admin_login']==TRUE)
	    {
			$matchId=$this->input->post('pid',TRUE);
			$list =$this->adminmodel->getHockeyMatchScores($matchId);
			$data = array();
			$count=0;
			foreach ($list as $score) 
			    {
			    	$count=$count+1;
			        
			    }
			echo json_encode($count);//data should be a plain array...
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}
	

}
