<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*@author Mokoro Stephen 
*/
class Suser extends CI_Controller
{

	function __construct()
	{
	     parent::__construct();
	     $this->load->model('SuserModel','susermodel');
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	public function logout()
    {
        $this->cas->logout();
    }
	//dashboard view page for admin
	public function dashboard()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['activeplayerscount']=$this->susermodel->activePlayersCount();//count all players active 
			$list['allinjuriestodatecount']=$this->susermodel->allInjuriesToDateCount();//count all injuries to date for active players 
			$list['allactiveplayersonphysio']=$this->susermodel->activePlayersOnPhysioCount();//count all active players who are on physio for active players 
			$list['allactiveplayersnotonphysio']=$this->susermodel->activePlayersNotOnPhysioCount();//count all active players who are not on physio for active players 

			$this->load->view('suser/dashboard',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	public function newteam()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$sportId=$this->input->post('sportId',TRUE);
			$teamName=$this->input->post('teamName',TRUE);
			$teamAlias=$this->input->post('teamAlias',TRUE);
			$teamCategory=$this->input->post('teamCategory',TRUE);
			$dateRegistered= date("Y-m-d H:i:s"); 

			//from login session
			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$team_info=array('team_sport_id'=>$sportId,'team_name'=>$teamName,'team_alias'=>$teamAlias,'team_category'=>$teamCategory,'date_registered'=>$dateRegistered,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId,'registering_user_id'=>$userAutoId);
			$result=$this->susermodel->newTeam($team_info);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "New Team added");
					
		          redirect(base_url(('suser/teamslist')));
				}else 
					{
						$_SESSION['msg']= array('error' => "Could not add team",'success' => "");
						
		               redirect(base_url(('suser/teamslist')));
					}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }

	}
	//list of active teams
	public function teamslist()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		{
			$list['teams']=$this->susermodel->allTeamsList();
			foreach($list['teams'] as $id=>$team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['coach']=$this->susermodel->getTeamCoach($teamId);
			}
			$list['sports']=$this->susermodel->allSportsList();
			$this->load->view('suser/teams',$list);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	    	}
	}
	//load list of coaches page
	public function coaches()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$coaches_list['sports']=$this->susermodel->activeSportsList();
			$coaches_list['coaches']=$this->susermodel->activeCoachesList();
			$this->load->view('suser/coach_registration',$coaches_list);
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }
	}
	//captains view page
	public function captains()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$captains_list['teams']=$this->susermodel->activeTeamsList();
			$captains_list['captains']=$this->susermodel->activeCaptainsList();
			$this->load->view('suser/captain_registration',$captains_list);
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }
	}
	
	public function administrators()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['admins']=$this->susermodel->activeadminList();
			$this->load->view('suser/admin_registration',$list);
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }
	}
	public function sc_coaches()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['coaches']=$this->susermodel->activeScoachList();
			$this->load->view('suser/scoach_registration',$list);
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }
	}
	
	//new coach registration 
	public function newcoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			//coach registration
			$coach_staff_id=$this->input->post('staffID', TRUE);
			$first_name=$this->input->post('firstName', TRUE);
			$last_name=$this->input->post('lastName', TRUE);
			$other_names=$this->input->post('otherNames', TRUE);
			$gender=$this->input->post('gender', TRUE);
			$coach_nid=$this->input->post('nationalID', TRUE);
			$phone_no=$this->input->post('phoneNumber', TRUE);
			$email=$this->input->post('emailAddress', TRUE);
			$residence=$this->input->post('currentResidence', TRUE);
			$coaching_before=$this->input->post('prevStatus', TRUE);
			$previous_team=$this->input->post('previousTeam', TRUE);
			$sport_id=$this->input->post('sportId', TRUE);
			$coach_highest_achievement=$this->input->post('highestAchievement', TRUE);
			$coach_username=$this->input->post('userName', TRUE);
			$password=md5($coach_username.$coach_staff_id);
			$sportId=$this->input->post('sportId', TRUE);
			$dateRegistered= date("Y-m-d H:i:s");

			//from login session
			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId']; 

			//check if already registered
			$this->db->select('*');
			$this->db->from('coaches');
			$this->db->where('coach_nid',$coach_nid);
			$this->db->or_where('coach_staff_id',$coach_staff_id);
			$query = $this->db->get();
		    $num=$query->num_rows(); 
		    if($num>0)
		    {
		    	$_SESSION['msg']= array('error' => "Already registered",'success' => "");
				
				redirect(base_url(('suser/administrators')));
			}else  
	            {


	            	$this->db->select('*');
					$this->db->from('coaches');
					$this->db->where('coach_username',$coach_username);
					$query = $this->db->get();
		            $num=$query->num_rows(); 
		            if($num>0)
			            {
			            	$_SESSION['msg']= array('error' => "Username already taken",'success' => "");
							
				            redirect(base_url(('suser/coaches')));
			            }else{

				            	$config['upload_path']          = 'uploads/profile_photos/coaches';
							    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
							    // $config['max_size']             = 100;
							    // $config['max_width']            = 500;
							    // $config['max_height']           = 500;
							    $config['file_name']			=md5(time().$phone_no.$first_name.$last_name);
							    $config['overwrite']           = true;
							    $config['file_ext_tolower']     = true;

								 $this->load->library('upload', $config);

								 //if no photo uploaded
								  if (empty($_FILES['photo']['name'])) 
								  {
							  		//create an array of the data to be inserted at once
									$coach_details = array('coach_staff_id' => $coach_staff_id, 'coach_fname'=>$first_name, 'coach_lname'=>$last_name, 'coach_other_names'=>$other_names,'coach_gender'=>$gender,'coach_nid'=>$coach_nid,'coach_phone'=>$phone_no,'coach_email'=>$email, 'coach_residence'=>$residence,'prev_coaching_state'=>$coaching_before,'prev_team'=>$previous_team, 'coach_h_achievement'=>$coach_highest_achievement,'coach_username'=>$coach_username,'date_registered'=>$dateRegistered,'coach_sport_id'=>$sportId,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

							    	$result=$this->susermodel->newCoach($coach_details);

									if($result)
										{
											$_SESSION['msg']= array('error' => "",'success' => "New Coach registered");
											
											redirect(base_url(('suser/coaches')));
										}else 
											{
												$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
												
								             	redirect(base_url(('suser/coaches')));
											}
								  	}else{

										    if (!$this->upload->do_upload('photo'))
										    {   
										        //create an array of the data to be inserted at once
											$coach_details = array('coach_staff_id' => $coach_staff_id, 'coach_fname'=>$first_name, 'coach_lname'=>$last_name, 'coach_other_names'=>$other_names,'coach_gender'=>$gender,'coach_nid'=>$coach_nid,'coach_phone'=>$phone_no,'coach_email'=>$email, 'coach_residence'=>$residence,'prev_coaching_state'=>$coaching_before,'prev_team'=>$previous_team, 'coach_h_achievement'=>$coach_highest_achievement,'coach_username'=>$coach_username,'date_registered'=>$dateRegistered,'coach_sport_id'=>$sportId,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

									    	$result=$this->susermodel->newCoach($coach_details);
									    	
											if($result)
												{
													$_SESSION['msg']= array('error' => "",'success' => "<span style='color:#FFFF00'>New Coach registered. </span> However, ".$this->upload->display_errors());
													
													redirect(base_url(('suser/coaches')));
												}else 
													{
														$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
														
										             	redirect(base_url(('suser/coaches')));
													}
										    }else{
								       
									       		$data =$this->upload->data();
									    		//details of uploaded file
									    		$coach_details = array('coach_staff_id' => $coach_staff_id, 'coach_fname'=>$first_name, 'coach_lname'=>$last_name, 'coach_other_names'=>$other_names,'coach_gender'=>$gender,'coach_nid'=>$coach_nid,'coach_phone'=>$phone_no,'coach_email'=>$email, 'coach_residence'=>$residence,'prev_coaching_state'=>$coaching_before,'prev_team'=>$previous_team, 'coach_h_achievement'=>$coach_highest_achievement,'coach_username'=>$coach_username,'date_registered'=>$dateRegistered,'coach_profile_photo'=>$config['file_name'].$data['file_ext'],'coach_sport_id'=>$sportId,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

										    	$result=$this->susermodel->newCoach($coach_details);
												if($result)
													{
														$_SESSION['msg']= array('error' => "",'success' => "New coach added");
														
											          redirect(base_url(('suser/coaches')));
													}else 
														{
															$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
															
											               redirect(base_url(('suser/coaches')));
														}
											  
											}
							    		}
							}
				}
			}else{
	            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
	            redirect(base_url(('suser')));
	    }

	}

	//coach profile
	public function coachprofile()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$coachId=$this->input->post('coachId', TRUE);
			$coach_profile['coach_profile']=$this->susermodel->coachProfile($coachId);
			$this->load->view('suser/coach_profile',$coach_profile);
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }
	}
	//coach profile edit page
	public function editcoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$coachId=$this->input->post('coachId', TRUE);
			$coach_profile['sports']=$this->susermodel->activeSportsList();
			$coach_profile['coach_profile']=$this->susermodel->coachProfile($coachId);
			$this->load->view('suser/edit_coach',$coach_profile);
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }
	}
	//save updated coach profile info
	public function  updatecoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			//coach registration
			$coachId=$this->input->post('coachAutoId', TRUE);

			$initFile=$this->input->post('initFile', TRUE);


			$coach_staff_id=$this->input->post('staffID', TRUE);
			$first_name=$this->input->post('firstName', TRUE);
			$last_name=$this->input->post('lastName', TRUE);
			$other_names=$this->input->post('otherNames', TRUE);
			$gender=$this->input->post('gender', TRUE);
			$coach_nid=$this->input->post('nationalID', TRUE);
			$phone_no=$this->input->post('phoneNumber', TRUE);
			$email=$this->input->post('emailAddress', TRUE);
			$residence=$this->input->post('currentResidence', TRUE);
			$coaching_before=$this->input->post('prevStatus', TRUE);
			$previous_team=$this->input->post('previousTeam', TRUE);
			$sport_id=$this->input->post('sportId', TRUE);
			$coach_highest_achievement=$this->input->post('highestAchievement', TRUE);
			$coach_username=$this->input->post('userName', TRUE);
			$password=md5($coach_username.$coach_staff_id);
			$sportId=$this->input->post('sportId', TRUE);

			//from login session
			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];


			$config['upload_path']          = 'uploads/profile_photos/coaches';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		      // $config['max_size']             = 100;
		    // $config['max_width']            = 500;
		    // $config['max_height']           = 500;
			$config['file_name']			=md5(time().$phone_no.$first_name.$last_name);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    if (empty($_FILES['photo']['name'])) {
		    	$coach_details = array('coach_staff_id' => $coach_staff_id, 'coach_fname'=>$first_name, 'coach_lname'=>$last_name, 'coach_other_names'=>$other_names,'coach_gender'=>$gender,'coach_nid'=>$coach_nid,'coach_phone'=>$phone_no,'coach_email'=>$email, 'coach_residence'=>$residence,'prev_coaching_state'=>$coaching_before,'prev_team'=>$previous_team, 'coach_h_achievement'=>$coach_highest_achievement,'coach_username'=>$coach_username,'coach_sport_id'=>$sportId,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

					$result=$this->susermodel->updateCoachNoPhoto($coach_details,$coachId);
					if($result)
						{
							$_SESSION['msg']= array('error' => "",'success' => "Coach updated!");
							
				           redirect(base_url(('suser/coaches')));
						}else 
							{
								$_SESSION['msg']= array('error' => "No changes made",'success' => "");
								
				               redirect(base_url(('suser/coaches')));
							}

			}else{ 
		    		$this->load->library('upload', $config);

					if(!$this->upload->do_upload('photo'))
					{
						
						$coach_details = array('coach_staff_id' => $coach_staff_id, 'coach_fname'=>$first_name, 'coach_lname'=>$last_name, 'coach_other_names'=>$other_names,'coach_gender'=>$gender,'coach_nid'=>$coach_nid,'coach_phone'=>$phone_no,'coach_email'=>$email, 'coach_residence'=>$residence,'prev_coaching_state'=>$coaching_before,'prev_team'=>$previous_team, 'coach_h_achievement'=>$coach_highest_achievement,'coach_username'=>$coach_username,'coach_sport_id'=>$sportId,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

					$result=$this->susermodel->updateCoachNoPhoto($coach_details,$coachId);
						if($result)
							{
								$_SESSION['msg']= array('error' => "",'success' => "<span style='color:#FFFF00'>Coach info updated. </span> However, ".$this->upload->display_errors());
								
					            redirect(base_url(('suser/coaches')));
							}else 
								{
									$_SESSION['msg']= array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					                redirect(base_url(('suser/coaches')));
								}
					}else{
						   	$data =$this->upload->data();//details of uploaded file

							$coach_details = array('coach_staff_id' => $coach_staff_id, 'coach_fname'=>$first_name, 'coach_lname'=>$last_name, 'coach_other_names'=>$other_names,'coach_gender'=>$gender,'coach_nid'=>$coach_nid,'coach_phone'=>$phone_no,'coach_email'=>$email, 'coach_residence'=>$residence,'prev_coaching_state'=>$coaching_before,'prev_team'=>$previous_team, 'coach_h_achievement'=>$coach_highest_achievement,'coach_username'=>$coach_username,'date_registered'=>$dateRegistered,'coach_sport_id'=>$sportId, 'coach_profile_photo'=>$config['file_name'].$data['file_ext'],'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

							$result=$this->susermodel->updateCoachWithPhoto($coach_details,$coachId,$initFile);

							// print_r($result);
							if($result)
								{
									$_SESSION['msg']= array('error' => "",'success' => "Coach info updated");
									
						           redirect(base_url(('suser/coaches')));
								}else 
									{
										$_SESSION['msg']= array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('suser/coaches')));
									}
						}

			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	// load coach team assignment
	public function assigncoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
		
			$teamId=$this->input->post('teamId');
			$teamName=$this->input->post('teamName');
			if(isset($teamId))
			{
				$_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('assignCoachTeamId'=>$teamId,'assignCoachTeamName'=>$teamName));
				$this->load->view('suser/assign_team_coach');
			}else if(isset($_SESSION['sessdata']['assignCoachTeamId']))
			{
				$this->load->view('suser/assign_team_coach');

			}else{
				$_SESSION['msg']= array('error' => "Session not set ",'success' => "");
				redirect(base_url(('suser/teamslist')));
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }

	}
	//get coach for autocomplete in team assignment
	public function getcoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
		
			$keyword=$this->input->get("q");
			$json = [];
			if(!empty($this->input->get("q"))){
				$this->db->or_like(array('coach_fname' => $keyword, 'coach_lname' => $keyword, 'coach_other_names' => $keyword));
				$this->db->select('coach_auto_id as id,CONCAT(coach_fname," ",coach_lname) as text');
				$this->db->limit(10);
				$query=$this->db->get("coaches");
				$json = $query->result();
			}
			echo json_encode($json);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	public function newteamcoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	    	$coachId=$this->input->post('coachId');
	    	$teamId=$_SESSION['sessdata']['assignCoachTeamId'];
	    	$dateAppointed=$this->input->post('dateAppointed');
			$dateRegistered= date("Y-m-d H:i:s"); 

			//from login session
			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

	    	$teacm_coach_info=array('coach_auto_id'=>$coachId,'coach_role_team_id'=>$teamId,'date_appointed'=>$dateAppointed,'date_added'=>$dateRegistered,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);
	    	$result=$this->susermodel->newTeamCoach($teacm_coach_info);

			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "Team Coach registered");
					
                   redirect(base_url(('suser/teamslist')));
				}else 
					{
						$_SESSION['msg']= array('error' => "Team Coach not registered",'success' => "");
						
	                   redirect(base_url(('suser/teamslist')));
					}	

	    }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		

	}
	//captain registration
	public function captainprofile()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		{
			$captainId=$this->input->post('captainId', TRUE);
			$playerId=$this->input->post('playerId', TRUE);

			$player_profile['captains']=$this->susermodel->captainProfile($playerId,$captainId);
			$this->load->view('suser/captain_profile',$player_profile);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	    	}
	}
	
	//captain registration
	public function newcaptain()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		{
			$playerId=$this->input->post('playerId', TRUE);
			$player_team_id=$this->input->post('teamID', TRUE);
			$date_appointed=$this->input->post('dateAppointed', TRUE);
			$capt_before=$this->input->post('prevStatus', TRUE);

			$dateRegistered= date("Y-m-d H:i:s"); 

			//from login session
			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$captain_details=array('player_auto_id'=>$playerId,'player_team_id'=>$player_team_id, 'date_appointed'=>$date_appointed,'capt_before'=>$capt_before,'date_registered'=>$dateRegistered,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);


			
		    	//check if the captain is registered as player in the selected team
				$this->db->select('*');
				$this->db->from('players pl');
				$this->db->where("`player_team_id` IN ($player_team_id)", NULL, FALSE);
				$query = $this->db->get();
		        $num=$query->num_rows(); 
		        if($num>0)
		           {
		            	//check if the captain is already registered
						$this->db->select('*');
						$this->db->from('captains');
						$this->db->where('player_auto_id',$playerId);
						$query = $this->db->get();
			            $num=$query->num_rows(); 
			            if($num>0)
			               {
								$_SESSION['msg']= array('error' => "Already registered/disabled",'success' => "");
								
					            redirect(base_url(('suser/captains')));
			        		}else
			                	{
			                		$result=$this->susermodel->newCaptain($captain_details);

									if($result)
										{
											$_SESSION['msg']= array('error' => "",'success' => "New captain registered");
											
						                   redirect(base_url(('suser/captains')));
										}else 
											{
												$_SESSION['msg']= array('error' => "Oops! Something went wrong.",'success' => "");
												
							                   redirect(base_url(('suser/captains')));
											}	
			                	}
			        }else
				        {
				        	$_SESSION['msg']= array('error' => "Not a member of the selected team",'success' => "");
							
		                   	redirect(base_url(('suser/captains')));
				        }
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	    	}
	}
	//captain edit (edit captain : editcaptain)
	public function editcaptain()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		{
			$captainId=$this->input->post('captainId', TRUE);
			$playerId=$this->input->post('playerId', TRUE);
			$captain_profile['teams']=$this->susermodel->activeTeamsList();

			$captain_profile['captain_profile']=$this->susermodel->captainProfile($playerId,$captainId);
			$this->load->view('suser/edit_captain',$captain_profile);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	    	}
	}

	//captain update
	public function updatecaptain()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		{
			$captainId=$this->input->post('captainId', TRUE);
			$player_team_id=$this->input->post('teamID', TRUE);
			$date_appointed=$this->input->post('dateAppointed', TRUE);
			$capt_before=$this->input->post('prevStatus', TRUE);

			//from login session
			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];
// print_r($captainId);
		
			$captain_details=array('player_team_id'=>$player_team_id, 'date_appointed'=>$date_appointed,'capt_before'=>$capt_before,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);
			//check if the captain is registered as player in the selected team
				$this->db->select('*');
				$this->db->from('players pl');
				$this->db->where("`player_team_id` IN ($player_team_id)", NULL, FALSE);
				$query = $this->db->get();
		        $num=$query->num_rows(); 
		        if($num>0)
		           {
						$result=$this->susermodel->updateCaptain($captain_details,$captainId);
						if($result)
							{
								$_SESSION['msg']= array('error' => "",'success' => "Captain updated");
								
					           redirect(base_url(('suser/captains')));
							}else 
								{
									$_SESSION['msg']= array('error' => "No changes made",'success' => "");
									
					               redirect(base_url(('suser/captains')));
								}
					}else{
							$_SESSION['msg']= array('error' => "Not a member of the selected team",'success' => "");
							
					        redirect(base_url(('suser/captains')));
					}
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	    	}
	}

	//list of players from each team
	public function players()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
        {

			$list['players'] =$this->susermodel->allActivePlayers();
			$list['teams']=$this->susermodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)//loop and create another array for players per team append it to original array $list['teams']
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->susermodel->activePlayersPerTeam($teamId);
			}

			$this->load->view('suser/players',$list);

		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//list of student players 
	public function studentplayers()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

			$list['studentplayers'] =$this->susermodel->allActiveStudentPlayers();
			$list['teams']=$this->susermodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)//loop and create another array for players per team append it to original array $list['teams']
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->susermodel->activeStudentPlayersPerTeam($teamId);
			}

			$this->load->view('suser/studentplayers',$list);// 

		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//Player profile (more_about_player) for super admin
	public function playerprofile()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);
			if(isset ($playerId) && strlen($playerId)){
				$player_profile['player_profile']=$this->susermodel->playerProfile($playerId);
				$this->load->view('suser/player_profile',$player_profile);
			}else{
				$_SESSION['msg']= array('edit'=>'','error' => "Session was unset",'success' => "");
				
		       redirect(base_url(('suser/players')));
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//get player info for disable 
	public function getplayer()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId',TRUE);
			$list =$this->susermodel->playerProfile($playerId);

	        $data = array();
	        foreach ($list as $player) 
	            {
	                $fullname=$player['player_fname']. " ". $player['player_lname'];
	                $data= array('playerId' => $player['player_auto_id'],'fullName'=>$fullname);
	            }

	        	echo json_encode($data);//data should be a plain array...
	    }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//get player passport info for delete 
	public function getpass()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$passId=$this->input->post('passId',TRUE);
			$list =$this->susermodel->playerPass($passId);

	        $data = array();
	        foreach ($list as $player) 
	            {
	                $fullname='Passport for '.$player['player_fname']. " ". $player['player_lname'];
	                $data= array('passId' => $player['passport_auto_id'],'fullName'=>$fullname);
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//disable player
	public function disableplayer()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

			$playerId=$this->input->post('playerId',TRUE);
			$reasonInactive=$this->input->post('reasonInactive',TRUE);
			$newstate=array('active_status'=>0,'reason_inactive'=>$reasonInactive);
			$result=$this->susermodel->deactivatePlayer($newstate,$playerId);
			if($result)
			{
				$_SESSION['msg']= array('error' => "",'success' => "Player disabled!");
				
	           redirect(base_url(('suser/players')));
			}else 
				{
					$_SESSION['msg']= array('error' => "Not disabled",'success' => "");
					
	               redirect(base_url(('suser/players')));
				}
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
        }

	}
	//player profile edit (edit player )
	public function editplayer()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);//
			if(isset ($playerId) && strlen($playerId)){
				$player_profile['courses']=$this->susermodel->coursesList();
				$player_profile['player_profile']=$this->susermodel->playerProfile($playerId);
				$list['teams']=$this->susermodel->activeTeamsList();
				$this->load->view('suser/edit_player',$player_profile);
			}else{
				$_SESSION['msg']= array('edit'=>'','error' => "Session was unset",'success' => "");
				
		       redirect(base_url(('suser/players')));
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	//update player information
	public function  updateplayer()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$player_auto_id=$this->input->post('playerAutoId',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			//check if student id was not entered. If it was not entered, save null for both student id and course
			$studid=$this->input->post('studentID', TRUE);
			if($studid=="")
				{
					$student_id=NULL;
					$student_course_id=NULL;
				}else{
						$student_id=$this->input->post('studentID', TRUE);
		    			$student_course_id=$this->input->post('course', TRUE);
					}

			$player_fname=$this->input->post('firstName', TRUE);
			$player_lname=$this->input->post('lastName', TRUE);
			$player_other_names=$this->input->post('otherNames', TRUE);
			$player_dob=$this->input->post('dateOfBirth', TRUE);
			$player_gender=$this->input->post('gender', TRUE);
			$player_nid=$this->input->post('idNo', TRUE);
			$player_id_type=$this->input->post('idType', TRUE);
			$player_phone_no=$this->input->post('phoneNumber', TRUE);
			$player_email=$this->input->post('emailAddress', TRUE);
		   
			$kin_fname=$this->input->post('kinFirstName', TRUE);
			$kin_lname=$this->input->post('kinLastName', TRUE);
		    $kin_other_names=$this->input->post('kinOtherNames', TRUE);
			$kin_nid_no=$this->input->post('kinNationalID', TRUE);
		    $kin_phone_no=$this->input->post('kinPhoneNumber', TRUE);
		    $kin_alt_phone_no=$this->input->post('kinAltPhoneNumber', TRUE);
		    $kin_email=$this->input->post('kinEmailAddress', TRUE);
		    $kin_current_residence=$this->input->post('kinCurrentResidence', TRUE);
		    $current_height=$this->input->post('currentHeight', TRUE);
		    $current_weight=$this->input->post('currentWeight', TRUE);
		    $prev_high_school=$this->input->post('previousHighSchool', TRUE);
		    $prev_play_status=$this->input->post('prevStatus', TRUE);
		    $prev_team=$this->input->post('previousTeam', TRUE);
		    $teamId=$this->input->post('teamId', TRUE);
		    $highest_achievement=$this->input->post('highestAchievement', TRUE);
			$player_residence=$this->input->post('currentResidence', TRUE);


			$config['upload_path']          = 'uploads/profile_photos/players';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		      // $config['max_size']             = 100;
		    $config['max_width']            = 500;
		    $config['max_height']           = 500;
			$config['file_name']			=md5(time().$player_phone_no.$player_fname.$player_lname);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) {
		    	$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id);

					$result=$this->susermodel->updatePlayerNoPhoto($player_details,$player_auto_id);
					if($result)
						{
							$_SESSION['msg']= array('error' => "",'success' => "Player updated!");
							
				           redirect(base_url(('suser/players')));
						}else 
							{
								$_SESSION['msg']= array('error' => "No changes made",'success' => "");
								
				               redirect(base_url(('suser/players')));
							}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id);

						$result=$this->susermodel->updatePlayerNoPhoto($player_details,$player_auto_id);
						if($result)
							{
								$_SESSION['msg']= array('error' => "",'success' => "<span style='color:#FFFF00'>Player info updated. </span> However, ".$this->upload->display_errors());
								
					           redirect(base_url(('suser/players')));
							}else 
								{
									$_SESSION['msg']= array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					               redirect(base_url(('suser/players')));
								}
					}else{
						   	$data =$this->upload->data();//details of uploaded file

							$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'player_profile_photo'=>$config['file_name'].$data['file_ext']);

							$result=$this->susermodel->updatePlayer($player_details,$player_auto_id,$initFile);
							if($result)
								{
									$_SESSION['msg']= array('error' => "",'success' => "Mentee info updated");
									
						           redirect(base_url(('suser/players')));
								}else 
									{
										$_SESSION['msg']= array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('suser/players')));
									}


					}

			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	//player next of kin profile
	public function nextofkin()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);
			if(isset ($playerId) && strlen($playerId)){
				$kin['nxtkin']=$this->susermodel->playerProfile($playerId);
			$this->load->view('suser/nxt_kin',$kin);
			}else{
				$_SESSION['msg']= array('edit'=>'','error' => "Session was unset",'success' => "");
				
		       redirect(base_url(('suser/players')));
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	//load new player passport adding page
	public function addplayerpspt()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId',TRUE);
			$playerName=$this->input->post('playerName',TRUE);

			if(isset ($playerId) && strlen($playerId) && isset($playerName) && strlen($playerName)){
				$_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('pst_player_id'=>$playerId,'pst_player_name'=>$playerName));
				
				$this->load->view('suser/addplayerpassport');
			}else{
				$this->load->view('suser/addplayerpassport');
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}


	//save new player passport
	public function newplayerpspt() 
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$player_id=$this->input->post('playerId', TRUE);
			$passportNumber=$this->input->post('passportNo', TRUE);
			$dateOfIssue=$this->input->post('dateOfIssue', TRUE);
			$dateOfExpiry=$this->input->post('dateOfExpiry', TRUE);
			$issueCountry=$this->input->post('issueCountry', TRUE);

			$config['upload_path']          = 'uploads/travel_documents';
			$config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
			$config['max_size']             = 700;
			$config['max_width']            = 700;
			$config['max_height']           = 700;
			$config['file_name']			=md5(time().$passportNumber);
			$config['overwrite']           = true;
			$config['file_ext_tolower']     = true;

			$this->load->library('upload', $config);

			if(isset($player_id) && strlen($player_id) && isset($passportNumber) && strlen($passportNumber) && isset($dateOfIssue) && strlen($dateOfIssue) && isset($dateOfExpiry) && strlen($dateOfExpiry) && isset($issueCountry) && strlen($issueCountry))
				{
				    $this->db->select('*');
					$this->db->from('travel_documents');
					$this->db->where('player_id',$player_id);
					$this->db->or_where('passport_number',$passportNumber);
					$query = $this->db->get();
			        $num=$query->num_rows(); 

			        if($num>0)
			            {
			            	$_SESSION['msg']= array('error' => "This player passport exists",'success' => "");
							
				        	redirect(base_url(('suser/playerpassports')));
			        	}else{
			        				
				           		//if no photo has been selected  uploaded
				           		if (empty($_FILES['photo']['name'])) 
								  	{
							           	$_SESSION['msg']= array('error' => "Please select passport photo file",'success' => "");
										
							        	redirect(base_url(('suser/addplayerpspt')));
							        }else{
								  			//confirm upload success
											if (!$this->upload->do_upload('photo'))
										    	{   
										        	$_SESSION['msg']= array('error' => $this->upload->display_errors(),'success' => "");
													
										       		redirect(base_url(('suser/addplayerpspt')));
										    	}else{
														$data =$this->upload->data(); //load data to page
											    		$passport_details =array('player_id'=>$player_id,'passport_number'=>$passportNumber,'issue_date'=>$dateOfIssue,'expiry_date'=>$dateOfExpiry,'issue_country'=>$issueCountry,'passport_photo'=>$config['file_name'].$data['file_ext']);

															$result=$this->susermodel->addPlayerPspt($passport_details);

															if($result)
																{
																	$_SESSION['msg']= array('error' => "",'success' => "Passport for ".$_SESSION['sessdata']['pst_player_name']." added.");
																	
													           		redirect(base_url(('suser/playerpassports')));
																}else 
																	{
																		$_SESSION['msg']= array('error' => "Passport not added",'success' => "");
																		
													               		redirect(base_url(('suser/playerpassports')));
																	}

												    }
							   			}
							}
				}else{
						$_SESSION['msg']= array('error' => "A required field is missing",'success' => "");
						
			        	redirect(base_url(('suser/addplayerpspt')));
				}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
					
			
	}
	//load player passport editing page
	public function editplayerpspt()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$passportId=$this->input->post('passportId',TRUE);
			$playerName=$this->input->post('playerName',TRUE);

			if(isset ($passportId) && strlen($passportId) && isset($playerName) && strlen($playerName))
			{
				$_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('pst_player_id'=>$passportId,'pst_player_name'=>$playerName));

				$list['passport_details']=$this->susermodel->getPassportDetails($passportId);

				$this->load->view('suser/edit_passport',$list);
			}else{
					$passportId=$_SESSION['sessdata']['passportId'];//use session if web browser hard refreshed
					$list['passport_details']=$this->susermodel->getPassportDetails($passportId);
					$this->load->view('suser/edit_passport',$list);
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	
	//update player passport
	public function updateplayerpspt()
	{
    	if($_SESSION['sessdata']['suser_login']==TRUE)
    	{
	    	$passportId=$this->input->post('passportId',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$passport_number=$this->input->post('passportNo',TRUE);
			$issue_date=$this->input->post('dateOfIssue',TRUE);
			$expiry_date=$this->input->post('dateOfExpiry',TRUE);
			$issue_country=$this->input->post('issueCountry',TRUE);


			$config['upload_path']          = 'uploads/travel_documents';
			$config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
			$config['max_size']             = 700;
			$config['max_width']            = 700;
			$config['max_height']           = 700;
			$config['file_name']			=md5(time().$passport_number);
			$config['overwrite']           = true;
			$config['file_ext_tolower']     = true;

			$this->load->library('upload', $config);

			if (empty($_FILES['photo']['name'])) 
			{
			    	$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country);

					$result=$this->susermodel->updatePassportNoPhoto($passport_details,$passportId);
					if($result)
						{
							$_SESSION['msg']= array('error' => "",'success' => "Passport updated");
							
				           redirect(base_url(('suser/playerpassports')));
						}else 
							{
								$_SESSION['msg']= array('error' => "No changes made ",'success' => "");
								
				               redirect(base_url(('suser/playerpassports')));
							}

				}else{ 

						if(!$this->upload->do_upload('photo'))
						{
							
							$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country);
							$result=$this->susermodel->updatePassportNoPhoto($passport_details,$passportId);
							if($result)
								{
									$_SESSION['msg']= array('error' => "",'success' => "<span style='color:#FFFF00'>passport details updated. </span> However, ".$this->upload->display_errors());
									
						           redirect(base_url(('suser/playerpassports')));
								}else 
									{
										$_SESSION['msg']= array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
										
						               redirect(base_url(('suser/playerpassports')));
									}
						}else{

							   	$data =$this->upload->data();//details of uploaded file

								$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'passport_photo'=>$config['file_name'].$data['file_ext']);

								//new session of admin profile photo
			                    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array("passport_photo",$config['file_name'].$data['file_ext']));

								$result=$this->susermodel->updatePassport($passport_details,$passportId,$initFile);
								if($result)
									{
										$_SESSION['msg']= array('error' => "",'success' => "Passport info updated");
										
							           redirect(base_url(('suser/playerpassports')));
									}else 
										{
											$_SESSION['msg']= array('error' => "Failed to update ",'success' => "");
											
							               redirect(base_url(('suser/playerpassports')));
										}


						}

				}
		}else{
            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
	        
            redirect(base_url(('suser')));
    	}

	}
	//player passports view page
	public function playerpassports()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	    	$list['players']=$this->susermodel->allActivePlayersAndPassports();
	    	$list['teams']=$this->susermodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players']=$this->susermodel->activePlayersAndPassports($teamId);
			}
			$this->load->view('suser/playerpassports',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//get more details of a player's passport
	public function playerpsptdetails()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$passportId=$this->input->post('passportId',TRUE);
			if(isset($passportId))
			{
				//create a session to handle browser refresh i.e not lose posted passport id
				$_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('passportId'=>$passportId));

				$list['passport_details']=$this->susermodel->getPassportDetails($passportId);
				$this->load->view('suser/player_passport_details',$list);

			}else {
					$passportId=$_SESSION['sessdata']['passportId'];//use session if web browser hard refreshed
					$list['passport_details']=$this->susermodel->getPassportDetails($passportId);
					$this->load->view('suser/player_passport_details',$list);
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//delete player passport
	public function delplayerpspt()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	    	$passportId=$this->input->post('passId',TRUE);
	    	$playerName=$this->input->post('playerName',TRUE);
	    	$result=$this->susermodel->deletePlayerPassport($passportId);
	    	if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' =>$playerName." was deleted");
					
		           redirect(base_url(('suser/playerpassports')));
				}else 
					{
						$_SESSION['msg']= array('error' => "Could not delete ".$playerName,'success' => "");
						
		               redirect(base_url(('suser/playerpassports')));
					}

	    }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
    }
	//list of training days per team from each team
	public function trainingdays()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

			$list['teams']=$this->susermodel->activeTeamsList();

			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['tr_days'] =$this->susermodel->trainingDaysPerTeam($teamId);
			}

			$this->load->view('suser/training_days',$list);

		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//load page for editing training days
	public function editdays()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

				$trdId=$this->input->post('trdId', TRUE);
				$training_days['tr_days']=$this->susermodel->trdList($trdId);
				$this->load->view('suser/edit_trds',$training_days);

		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	
	//updating training days
	public function updatedays()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$trdId=$this->input->post('trdId', TRUE);
			$training_year=$this->input->post('trainingYear', TRUE);
			$january=$this->input->post('january', TRUE);
			$february=$this->input->post('february', TRUE);
			$march=$this->input->post('march', TRUE);
			$april=$this->input->post('april', TRUE);
			$may=$this->input->post('may', TRUE);
			$june=$this->input->post('june', TRUE);
			$july=$this->input->post('july', TRUE);
			$august=$this->input->post('august', TRUE);
			$september=$this->input->post('september', TRUE);
			$october=$this->input->post('october', TRUE);
			$november=$this->input->post('november', TRUE);
			$december=$this->input->post('december', TRUE);

			$tr_days_details=array('january'=>$january,'february'=>$february,'march'=>$march,'april'=>$april,'may'=>$may,'june'=>$june,'july'=>$july,'august'=>$august,'september'=>$september,'october'=>$october,'november'=>$november,'december'=>$december,'trd_year'=>$training_year);
			
			$result=$this->susermodel->updateTrDays($tr_days_details,$trdId);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "Training days updated");
					
		           redirect(base_url(('suser/trainingdays')));
				}else 
					{
						$_SESSION['msg']= array('error' => "No changes made",'success' => "");
						
		               redirect(base_url(('suser/trainingdays')));
					}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//get individual player attendance for update
	public function getplayerattendance()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
        {
			$recordId=$this->input->post('recordId',TRUE);
			$action=$this->input->post('action',TRUE);


	        $list =$this->susermodel->getPlayerAttendance($recordId);

	        $data = array();
	        foreach ($list as $record) 
	            {
	            	$state=$record['attendance_state'];

	            	if($action=="edit")
	            	{
		            	if($state=="PRESENT")
			            	{
	            		$status='
	            			<div class="row setup-content">
	            				
	                            <div class="col-md-12">
		                            <div class="form-group col-md-12 col-lg-12" >
		                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
		                            </div>
	                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
	                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
	                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
	                                </div>
	                            	<div class="form-group col-md-12 col-lg-12">
	                                <label class="">STATUS <span class="star">*</span></label><br>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" checked="checked">PRESENT
	                                </label>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off">ABSENT
	                                </label>
	                                 <label class="radio-inline ">
	                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off">EXCUSED
	                                </label>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="PHYSIO" required autocomplete="off" >PHYSIO
	                                </label>
	                            </div>
	                        </div>';
	            	}else if($state=="ABSENT")
	            	{
	            		$status='
	            			<div class="row setup-content">
	                            <div class="col-md-12">
	                            <div class="form-group col-md-12 col-lg-12" >
	                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
	                            </div>
	                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
	                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
	                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
	                                </div>
	                            	<div class="form-group col-md-12 col-lg-12">
	                                <label class="">STATUS <span class="star">*</span></label><br>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
	                                </label>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" checked="checked">ABSENT
	                                </label>
	                                 <label class="radio-inline ">
	                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off">EXCUSED
	                                </label>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="PHYSIO" required autocomplete="off" >PHYSIO
	                                </label>
	                            </div>
	                        </div>';
	            	}else if($state=="EXCUSED")
	            	{
	            		$status='
	            			<div class="row setup-content">
	                            <div class="col-md-12">
	                           	<div class="form-group col-md-12 col-lg-12" >
	                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
	                            </div>
	                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
	                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
	                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
	                                </div>
	                            	<div class="form-group col-md-12 col-lg-12">
	                                <label class="">STATUS <span class="star">*</span></label><br>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
	                                </label>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" >ABSENT
	                                </label>
	                                 <label class="radio-inline ">
	                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off" checked="checked">EXCUSED
	                                </label>
	                                <label class="radio-inline ">
	                                    <input type="radio" name="status" value="PHYSIO" required autocomplete="off" >PHYSIO
	                                </label>
	                            </div>
	                        </div>';
			            	}else  if($state=="PHYSIO")
			            	{
			            		$status='
		            			<div class="row setup-content">
		                            <div class="col-md-12">
		                           	<div class="form-group col-md-12 col-lg-12" >
		                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
		                            </div>
		                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
		                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
		                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
		                                </div>
		                            	<div class="form-group col-md-12 col-lg-12">
		                                <label class="">STATUS <span class="star">*</span></label><br>
		                                <label class="radio-inline ">
		                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
		                                </label>
		                                <label class="radio-inline ">
		                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" >ABSENT
		                                </label>
		                                 <label class="radio-inline ">
		                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off">EXCUSED
		                                </label>
		                                <label class="radio-inline ">
		                                    <input type="radio" name="status" value="PHYSIO" required autocomplete="off" checked="checked">PHYSIO
		                                </label>
		                            </div>
		                        </div>';
			            	}
		           	}else if($action=='del')
		           	{
		           		$state=$record['attendance_state'];
		            	if($state=="PRESENT")
		            	{
		            		$status='
		            			<div class="row setup-content">
		            				
		                            <div class="col-md-12">
		                            <div class="form-group col-md-12 col-lg-12" >
		                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
		                            </div>
		                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
		                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
		                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
		                                </div>
		                            	<div class="form-group col-md-12 col-lg-12">
		                                <label class="">STATUS : <span style="font-weight:normal;">PRESENT</span>
		                                </label>
		                            </div>
		                        </div>';
		            	}else if($state=="ABSENT")
		            	{
		            		$status='
		            			<div class="row setup-content">
		            				
		                            <div class="col-md-12">
		                            <div class="form-group col-md-12 col-lg-12" >
		                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
		                            </div>
		                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
		                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
		                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
		                                </div>
		                            	<div class="form-group col-md-12 col-lg-12">
		                                <label class="">STATUS : <span style="font-weight:normal;">ABSENT</span>
		                                </label>
		                            </div>
		                        </div>';
		            	}else if($state=="EXCUSED")
		            	{
		            		$status='
		            			<div class="row setup-content">
		            				
		                            <div class="col-md-12">
		                            <div class="form-group col-md-12 col-lg-12" >
		                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
		                            </div>
		                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
		                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
		                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
		                                </div>
		                            	<div class="form-group col-md-12 col-lg-12">
		                                <label class="">STATUS : <span style="font-weight:normal;">EXCUSED</span>
		                                </label>
		                            </div>
		                        </div>';
		            	}else if($state=="PHYSIO")
		            	{
		            		$status='
		            			<div class="row setup-content">
		            				
		                            <div class="col-md-12">
		                            <div class="form-group col-md-12 col-lg-12" >
		                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
		                            </div>
		                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
		                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
		                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['ta_auto_id'].'">
		                                </div>
		                            	<div class="form-group col-md-12 col-lg-12">
		                                <label class="">STATUS : <span style="font-weight:normal;">PHYSIO</span>
		                                </label>
		                            </div>
		                        </div>';
		            	}
		               
		           	}
	               
	                $fullname=$record['player_fname']. " ". $record['player_lname'];
	                $data= array('recordId' => $record['ta_auto_id'],'fullName'=>$fullname, 'status'=>$status);
	            }
	        	echo json_encode($data);//data should be a plain array...

	        }else{
	            $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
	            redirect(base_url(('suser')));
	    }
    }
    //update player training attendance
    public function updateplayerattendance()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
        {
			$recordId=$this->input->post('recordId',TRUE);
			$status=$this->input->post('status',TRUE);
			$attendanceInfo=array('attendance_state'=>$status);
			$result=$this->susermodel->updatePlayerAttendance($attendanceInfo,$recordId);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "Update successful");
					
		           redirect(base_url(('suser/traininglist')));
				}else 
					{
						$_SESSION['msg']= array('error' => "No changes made",'success'=>'');
						
		               redirect(base_url(('suser/traininglist')));
					}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	  //delete player training attendance
    public function delplayerattendance()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
        {
			$recordId=$this->input->post('recordId',TRUE);
			$result=$this->susermodel->deletePlayerAttendance($recordId);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "Record deleted");
					
		           redirect(base_url(('suser/traininglist')));
				}else 
					{
						$_SESSION['msg']= array('error' => "Could not delete record",'success'=>'');
						
		               redirect(base_url(('suser/traininglist')));
					}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//confirm page to delete team attendance for specific date
	public function deltrainconfirm()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	    	$trainingTeamId=$this->input->post('teamId',TRUE);
	    	$trainingDate=$this->input->post('trainingDate',TRUE);
			if(isset($trainingTeamId) && strlen($trainingTeamId)&& isset($trainingDate) && strlen($trainingDate)){
				$team = $this->db->query("SELECT team.team_name FROM teams team WHERE team.team_auto_id='$trainingTeamId' LIMIT 1")->row();
				// set session for  date and team id and team name
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('trainingDate' =>$trainingDate,'trainingTeamId'=>$trainingTeamId,'trainingTeamName'=>$team->team_name));

				$_SESSION['msg']= array('error' => "Permanently delete all ".$_SESSION['sessdata']['trainingTeamName'].' attendance for '.date_format(date_create($_SESSION['sessdata']['trainingDate']),"D j<\s\up>S</\s\up> M, Y").'? <form method="post" action="'.base_url().'suser/deltrainattendance" style="display:inline">  
					<div class="form-group col-md-12 col-lg-12" style="display:none">
                      <label for="trainingDate" class="control-label">Training Date<span class="star">*</span></label>
                      <input required="required" class="form-control" name="trainingDate" id="trainingDate" placeholder="" value="'.$_SESSION['sessdata']['trainingDate'].'">
                  	</div>
                  	<div class="form-group col-md-12 col-lg-12" style="display:none">
                      <label for="teamId" class="control-label">Team Id<span class="star">*</span></label>
                      <input required="required" class="form-control" name="teamId" id="teamId" placeholder="" value="'.$_SESSION['sessdata']['trainingTeamId'].'">
                  	</div>  
                  	<button class="btn btn-default btn-s" title="Confirm & Delete"   type="submit"> Confirm Delete </button></form>','success' => "");
				

				 redirect(base_url(('suser/trainingattendance')));
				
			}else{
				$trainingTeamId=$_SESSION['sessdata']['trainingTeamId'];
	    		$trainingDate=$_SESSION['sessdata']['trainingDate'];
				 redirect(base_url(('suser/trainingattendance')));
	    		
			}

    	}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//delete team training attendance per date
	public function deltrainattendance()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	    	$trainingTeamId=$this->input->post('teamId',TRUE);
	    	$trainingDate=$this->input->post('trainingDate',TRUE);

	    	$result=$this->susermodel->deleteTeamAttendancePerDate($trainingTeamId,$trainingDate);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "Attendance deleted");
					
		           redirect(base_url(('suser/trainingattendance')));
				}else 
					{
						$_SESSION['msg']= array('error' => "Could not delete attendance",'success'=>'');
						
		               redirect(base_url(('suser/trainingattendance')));
					}


    	}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//training attendance view
	public function trainingattendance()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['teams']=$this->susermodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->susermodel->activePlayersForAttendance($teamId);

				$list['teams'][$id]['trainingattendance'] =$this->susermodel->playerAttendanceGroupedPerDate($teamId);

				$count=0;//create a counter. this will be used to create unique attendance status count arrays within attendance array in teams array as below. The $count and team_auto_id are used to achieve this unique naming

				foreach($list['teams'][$id]['trainingattendance'] as $id2 => $attendance)
				{
					$attendanceDate=$attendance['training_date'];//get the training date. To be used to get attendance per training date
					$countOfPlayersMarkedPresentAbsentOrExcused=$this->susermodel->countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$teamId);//pass attendance date and teamid (from first loop above) to get the attendance per team per date

					$countOfPlayersPresent=$this->susermodel->countOfPlayersPresent($attendanceDate,$teamId);//counting no of players in each attendance status
					$countOfPlayersAbsent=$this->susermodel->countOfPlayersAbsent($attendanceDate,$teamId);
					$countOfPlayersExcused=$this->susermodel->countOfPlayersExcused($attendanceDate,$teamId);
					$countOfPlayersOnPhysio=$this->susermodel->countOfPlayersOnPhysio($attendanceDate,$teamId);

					//here is how we create unique arrays as above described
					$list['teams'][$id]['trainingattendance'][$id2]['countAll_'.$team['team_auto_id'].$count] =$countOfPlayersMarkedPresentAbsentOrExcused;
					$list['teams'][$id]['trainingattendance'][$id2]['countPresent_'.$team['team_auto_id'].$count] =$countOfPlayersPresent;
					$list['teams'][$id]['trainingattendance'][$id2]['countAbsent_'.$team['team_auto_id'].$count] =$countOfPlayersAbsent;
					$list['teams'][$id]['trainingattendance'][$id2]['countExcused_'.$team['team_auto_id'].$count] =$countOfPlayersExcused;

					$list['teams'][$id]['trainingattendance'][$id2]['countPhysio_'.$team['team_auto_id'].$count] =$countOfPlayersOnPhysio;

					$count=$count+1;
				}

			}
			$this->load->view('suser/training_attendance',$list);
			// print_r($list);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	        }
	}
	//list of training attendance 
	public function traininglist()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

	    	$trainingTeamId=$this->input->post('teamId',TRUE);
	    	$trainingDate=$this->input->post('trainingDate',TRUE);
			if(isset($trainingTeamId) && strlen($trainingTeamId)&& isset($trainingDate) && strlen($trainingDate)){
				// set session for  date and team id
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('trainingDate' =>$trainingDate,'trainingTeamId'=>$trainingTeamId));

		    	$list['traininglist']=$this->susermodel->trainingList($trainingTeamId,$trainingDate);
				$this->load->view('suser/players_attendance_list',$list);
			}else{
				$trainingTeamId=$_SESSION['sessdata']['trainingTeamId'];
	    		$trainingDate=$_SESSION['sessdata']['trainingDate'];
	    		$list['traininglist']=$this->susermodel->trainingList($trainingTeamId,$trainingDate);
				$this->load->view('suser/players_attendance_list',$list);
			}

    	}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
    }

	//injuries
	public function injuries()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

			$list['injuries']=$this->susermodel->allInjuries();//get all injuries
			$list['teams']=$this->susermodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['injuries']=$this->susermodel->injuryRecords($teamId);
			}
				$this->load->view('suser/injury_records',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//more about injury record
	public function injurydetails()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId', TRUE);
			if(isset ($recordId) && strlen($recordId)){
				$record['injury_record']=$this->susermodel->injuryRecord($recordId);
				$this->load->view('suser/injury_more',$record);
			}else{
				$_SESSION['msg']= array('edit'=>'','error' => "Session was unset",'success' => "");
				
		       redirect(base_url(('suser/injuries')));
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
		
	}
	//edit player injury record
	public function editinjury()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId', TRUE);
			if(isset ($recordId) && strlen($recordId)){
				$record['injury_record']=$this->susermodel->injuryRecord($recordId);
				$this->load->view('suser/edit_injury',$record);

			}else{
					$_SESSION['msg']= array('edit'=>'','error' => "Session was unset",'success' => "");
					
			       	redirect(base_url(('suser/injuries')));
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	
	public function updateinjury()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId', TRUE);
			$injury_date=$this->input->post('injuryDate', TRUE);
			$date_seen=$this->input->post('dateSeen', TRUE);
			$description=$this->input->post('injuryDescription', TRUE);
			$diagnosis=$this->input->post('diagnosis', TRUE);
			$treatment=$this->input->post('treatment', TRUE);
			$physio_remarks=$this->input->post('physioRemarks', TRUE);
			$scoach_remarks=$this->input->post('scoachRemarks', TRUE);
			$physio_status=$this->input->post('physioStatus', TRUE);
			$start_date=$this->input->post('startDate', TRUE);
			$end_date=$this->input->post('endDate', TRUE);
			

			$injury_details=array('injury_date'=>$injury_date,'date_seen'=>$date_seen,'injury_description'=>$description,'diagnosis'=>$diagnosis,'treatment'=>$treatment,'physio_remarks'=>$physio_remarks,'scoach_remarks'=>$scoach_remarks,'physio_status'=>$physio_status,'physio_start_date'=>$start_date,'physio_end_date'=>$end_date);
			
			$result=$this->susermodel->updateinjury($injury_details,$recordId);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' => "injury information updated");
					
		           redirect(base_url(('suser/injuries')));
				}else 
					{
						$_SESSION['msg']= array('error' => "No changes made",'success' => "");
						
		               redirect(base_url(('suser/injuries')));
					}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//get player injury info for delete 
	public function getinjuredplayer()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId',TRUE);
			$list =$this->susermodel->injuryRecord($recordId);

	        $data = array();
	        foreach ($list as $injury) 
	            {
	                $fullname=$injury['player_fname']. " ". $injury['player_lname'];
	                $data= array('recordId' => $injury['injury_auto_id'],'fullName'=>$fullname,'injuryDate'=>$injury['injury_date'],'info'=>'

	                	<div class="form-group col-md-12 col-lg-12" style="display: none;">
	                		<label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
	                		<input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" value="'.$injury['injury_auto_id'].'" readonly="true">
	                	</div>
	                	<div style="margin-bottom:10px;"><b>Injury Date</b></div> '.$injury['injury_date'].'<br><br><div style="margin-bottom:10px;"><b> Description</b></div> '.$injury['injury_description'].'<br><br> <div style="margin-bottom:10px;"><b>Diagnosis</b></div> '.$injury['diagnosis']);
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//delete injury record on confirm
	public function delinjury()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId',TRUE);
			$result=$this->susermodel->deleteInjuryRecord($recordId);
			if($result)
				{
					$_SESSION['msg']= array('error' => "",'success' =>"Injury record was deleted");
					
		           redirect(base_url(('suser/injuries')));
				}else 
					{
						$_SESSION['msg']= array('error' => "Could not delete injury record",'success' => "");
						
		               redirect(base_url(('suser/injuries')));
					}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//team reports uploads view page
	public function teamuploads()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['teams']=$this->susermodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['team_uploads']=$this->susermodel->teamUploads($teamId);
			}


			$this->load->view('suser/team_uploads',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//sport reports uploads view page
	public function sportuploads()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['sports']=$this->susermodel->sportsList();
			foreach($list['sports'] as $id => $sport)
			{
				$sportId=$sport['sport_auto_id'];
				$list['sports'][$id]['sport_uploads']=$this->susermodel->sportUploads($sportId);
			}

			$this->load->view('suser/sport_uploads',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//admin uploads view page
	public function adminuploads()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['admin_uploads']=$this->susermodel->adminUploads();
			$this->load->view('suser/admin_uploads',$list);
			
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//general admin uploads- viewable by all sports
	public function adminbroadcast()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['general_uploads']=$this->susermodel->adminBroadcastUploads();
			$this->load->view('suser/admin_broadcast_uploads',$list);
			
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//download coach_report
	public function download_coach_report($filename = NULL) 
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/coach_uploads/coach_reports/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//team_expenditures
	public function expenditures()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {

			$list['expenses']=$this->susermodel->allExpenses();
			$list['teams']=$this->susermodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['expenses']=$this->susermodel->expensesPerTeam($teamId);
			}
				$this->load->view('suser/team_expenses',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	
	//more about team expenditure
	public function viewteamexpense()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$expenseId=$this->input->post('expenseId',TRUE);
			$list =$this->susermodel->expenditureDetails($expenseId);

	        $data = array();
	        foreach ($list as $expense) 
	            {
	                $data= array('recordId' => $expense['expense_auto_id'],'teamName'=>$expense['team_name'],'info'=>'
	                	<div class="col-md-6">
	                	<br>
		                	<div style="margin-bottom:10px;">
		                		<b>Expenditure Date</b>
		                	</div> '.date_format(date_create($expense['expense_date']),"D j<\s\up>S</\s\up> M, Y").'<br><br>
		                </div>
		                <div class="col-md-6">
	                	<br>
		                	<div style="margin-bottom:10px;">
		                		<b>Date Recorded</b>
		                	</div> '.date_format(date_create($expense['expense_record_date']),"D j<\s\up>S</\s\up> M, Y").'<br><br>
		                </div>
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b> Cash (Kshs)</b>
		                	</div> '.$expense['expense_cash'].'<br><br> 
		               </div>
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b>Actual Costs (Kshs)</b>
		                	</div> '.$expense['actual_expenditure'].'<br><br> 
		                </div>
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b>LPO (Kshs)</b>
		                	</div> '.$expense['expense_lpo_amount'].'<br><br> 
		                </div>
		               
	                	<div class="col-md-6">
		                	<div style="margin-bottom:10px;">
		                		<b>LPO No.</b>
		                	</div> '.$expense['expense_lpo_no'].'<br><br>
		                </div>
		                <div class="col-md-12">
		                	<div style="margin-bottom:10px;">
		                		<b>Comments</b>
		                	</div> '.$expense['expense_comment'].'<br><br>
		                </div>');
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//more about team expenditure
	public function geteamexpense()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$expenseId=$this->input->post('expenseId',TRUE);
			$action=$this->input->post('action',TRUE);

			$list =$this->susermodel->expenditureDetails($expenseId);

	        $data = array();
	        foreach ($list as $expense) 
	            {
	               if(isset($action)&&$action=="edit") 
	               	{
	               		$data= array('recordId' => $expense['expense_auto_id'],'teamName'=>$expense['team_name'],'info'=>'
	                		<div class="col-md-6 col-lg-6">
	                		<br>
                              <label for="expenseDate" class="control-label">Expenditure Date</label>
                              <div class="form-group">
                                  <div class="input-group date" id="expenseDate">
                                      <input type="text" class="form-control" readonly="true" name="expenseDate" value="'.$expense['expense_date'].'"'.'/>
                                      <span class="input-group-addon">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                            </div>
	                		<div class="form-group col-md-6 col-lg-6 " >
	                		<br>
	                            <label for="expenseCash" class="control-label">Expenditure Cash*</label>
	                            <input type="text" name="expenseCash" class="form-control" id="expenseCash" required="required"  value="'.$expense['expense_cash'].'"'.'>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="lpoAmount" class="control-label">LPO Amount</label>
	                            <input type="text" name="lpoAmount" class="form-control" id="lpoAmount" required="required"  value="'.$expense['expense_lpo_amount'].'"'.' >
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="lpoNo" class="control-label">LPO No</label>
	                            <input type="text" name="lpoNo" class="form-control" id="lpoNo"   value="'.$expense['expense_lpo_no'].'"'.' >
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="lunches" class="control-label">Actual Costs</label>
	                            <input type="text" name="lunches" class="form-control" id="lunches"   value="'.$expense['actual_expenditure'].'"'.' >
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="comments" class="control-label">Comments *</label>
	                            <input type="text" name="comments" class="form-control" id="comments" required="required"  value="'.$expense['expense_comment'].'"'.' >
                            </div>
	                	');
	                }else if(isset($action)&&$action=="del"){
	                	$data= array('recordId' => $expense['expense_auto_id'],'teamName'=>$expense['team_name'],'info'=>'
	                		<div class="col-md-6 col-lg-6">
	                		<br>
                              <label for="dateOfBirth" class="control-label">Expenditure Date</label>
                              <div class="form-group">
                                	<input type="text" class="form-control" readonly="true" name="expenseDate" value="'.$expense['expense_date'].'"'.' disabled="true"/>
                              </div>
                            </div>
	                		<div class="form-group col-md-6 col-lg-6 " >
	                		<br>
	                            <label for="expenseCash" class="control-label">Expenditure Cash*</label>
	                            <input type="text" name="expenseCash" class="form-control" id="expenseCash" required="required"  value="'.$expense['expense_cash'].'"'.' disabled="true">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="lpoAmount" class="control-label">LPO Amount</label>
	                            <input type="text" name="lpoAmount" class="form-control" id="lpoAmount" required="required"  value="'.$expense['expense_lpo_amount'].'"'.' disabled="true">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="lpoNo" class="control-label">LPO No</label>
	                            <input type="text" name="lpoNo" class="form-control" id="lpoNo"   value="'.$expense['expense_lpo_no'].'"'.' disabled="true">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="lunches" class="control-label">Actual Costs</label>
	                            <input type="text" name="lunches" class="form-control" id="lunches"   value="'.$expense['actual_expenditure'].'"'.' disabled="true">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 " >
	                            <label for="comments" class="control-label">Comments *</label>
	                            <input type="text" name="comments" class="form-control" id="comments" required="required"  value="'.$expense['expense_comment'].'"'.' disabled="true">
                            </div>
	                	');
	                }
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//add new admin
	public function newadmin()
		{
			if($_SESSION['sessdata']['suser_login']==TRUE)
	    	{
	    		$first_name=$this->input->post('firstName', TRUE);
				$last_name=$this->input->post('lastName', TRUE);
				$user_name=$this->input->post('userName', TRUE);
				$staff_id=$this->input->post('staff_id', TRUE);
				$nationalId=$this->input->post('nationalId', TRUE);
				$id_type=$this->input->post('idType', TRUE);
				$phone_no=$this->input->post('phoneNumber', TRUE);
				$email=$this->input->post('email', TRUE);

	    		$adminRole=$this->input->post('adminRole', TRUE);


				$dateRegistered= date("Y-m-d H:i:s");

				$this->db->select('*');
				$this->db->from('admin');
				$this->db->where('admin_staff_id',$staff_id);
				$query = $this->db->get();
			    $num=$query->num_rows();  
		        if($num>0)
			    {
			    	$_SESSION['msg']= array('error' => "Already registered",'success' => "");
					
					redirect(base_url(('suser/administrators')));
				}else 
			            {
			            	$config['upload_path']          = 'uploads/profile_photos/admins';
						    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
						    // $config['max_size']             = 100;
						    $config['max_width']            = 500;
						    $config['max_height']           = 500;
						    $config['file_name']			=md5(time().$phone_no.$first_name.$last_name);
						    $config['overwrite']           = true;
						    $config['file_ext_tolower']     = true;

							 $this->load->library('upload', $config);

							 //if no photo uploaded
							  if (empty($_FILES['photo']['name'])) 
							  {
							  	$admin_details=array('date_registered'=>$dateRegistered, 'admin_fname'=>$first_name,'admin_lname'=>$last_name,'admin_username'=>$user_name,'admin_nid'=>$nationalId,'id_type'=>$id_type,'admin_phone'=>$phone_no,'admin_email'=>$email,'admin_staff_id'=>$staff_id,'admin_role_id'=>$adminRole);

						    	$result=$this->susermodel->newadmin($admin_details);
						    	
								if($result)
									{
										$_SESSION['msg']= array('error' => "",'success' => "New Administrator registered");
										
										redirect(base_url(('suser/administrators')));
									}else 
										{
											$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
											
							             redirect(base_url(('suser/administrators')));
										}
							  	}else{

									    if (!$this->upload->do_upload('photo'))
									    {   
									        $_SESSION['msg']= array('error' => $this->upload->display_errors(),'success' => "");
											
									       	 redirect(base_url(('suser/administrators')));
									    }else{
							       
								       		$data =$this->upload->data();
								    		//details of uploaded file
								    			$admin_details=array('date_registered'=>$dateRegistered, 'admin_fname'=>$first_name,'admin_lname'=>$last_name,'admin_username'=>$user_name,'admin_nid'=>$nationalId,'id_type'=>$id_type,'admin_phone'=>$phone_no,'admin_email'=>$email,'admin_staff_id'=>$staff_id,'admin_profile_photo'=>$config['file_name'].$data['file_ext'],'admin_role_id'=>$adminRole);

									    	$result=$this->susermodel->newadmin($admin_details);
											if($result)
												{
													$_SESSION['msg']= array('error' => "",'success' => "New Administrator added");
													
										          redirect(base_url(('suser/administrators')));
												}else 
													{
														$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
														
										               redirect(base_url(('suser/administrators')));
													}
										  
										}
						    		}
						   	}
			}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }

	}

	public function edit_admin()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		    {
				$adminId=$this->input->post('adminId', TRUE);
				$list['admin_profile']=$this->susermodel->adminProfile($adminId);
				$this->load->view('suser/edit_admin',$list);
			}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	        }
	}
	//update admin
	public function  updateadmin()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$initFile=$this->input->post('initFile',TRUE);
			$adminId=$this->input->post('adminId', TRUE);
			$first_name=$this->input->post('firstName', TRUE);
			$last_name=$this->input->post('lastName', TRUE);
			$user_name=$this->input->post('userName', TRUE);
			$staff_id=$this->input->post('staff_id', TRUE);
			$nationalId=$this->input->post('nationalId', TRUE);
			$id_type=$this->input->post('idType', TRUE);
			$phone_no=$this->input->post('phoneNumber', TRUE);
			$email=$this->input->post('emailAddress', TRUE);

	    	$adminRole=$this->input->post('adminRole', TRUE);

			$config['upload_path']          = 'uploads/profile_photos/admins';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		      // $config['max_size']             = 100;
		    $config['max_width']            = 500;
		    $config['max_height']           = 500;
			$config['file_name']			=md5(time().$phone_no.$first_name.$last_name);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) {
		    	$admin_details=array('admin_fname'=>$first_name,'admin_lname'=>$last_name,'admin_username'=>$user_name,'admin_nid'=>$nationalId,'id_type'=>$id_type,'admin_phone'=>$phone_no,'admin_email'=>$email,'admin_staff_id'=>$staff_id,'admin_role_id'=>$adminRole);

					$result=$this->susermodel->updateAdminNoPhoto($admin_details,$adminId);
					if($result)
						{
							$_SESSION['msg']= array('error' => "",'success' => "Administrator updated!");
							
				           redirect(base_url(('suser/administrators')));
						}else 
							{
								$_SESSION['msg']= array('error' => "No changes made",'success' => "");
								
				               redirect(base_url(('suser/administrators')));
							}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$admin_details=array('admin_fname'=>$first_name,'admin_lname'=>$last_name,'admin_username'=>$user_name,'admin_nid'=>$nationalId,'id_type'=>$id_type,'admin_phone'=>$phone_no,'admin_email'=>$email,'admin_staff_id'=>$staff_id,'admin_role_id'=>$adminRole);

						$result=$this->susermodel->updateAdminNoPhoto($admin_details,$adminId);
						if($result)
							{
								$_SESSION['msg']= array('error' => "",'success' => "<span style='color:#FFFF00'>Super User info updated. </span> However, ".$this->upload->display_errors());
								
					            redirect(base_url(('suser/administrators')));
							}else 
								{
									$_SESSION['msg']= array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					                redirect(base_url(('suser/administrators')));
								}
					}else{
						   	$data =$this->upload->data();//details of uploaded file

							$admin_details=array('admin_fname'=>$first_name,'admin_lname'=>$last_name,'admin_username'=>$user_name,'admin_nid'=>$nationalId,'id_type'=>$id_type,'admin_phone'=>$phone_no,'admin_email'=>$email,'admin_staff_id'=>$staff_id,'admin_profile_photo'=>$config['file_name'].$data['file_ext'],'admin_role_id'=>$adminRole);

							$result=$this->susermodel->updateAdminWithPhoto($admin_details,$adminId,$initFile);

							// print_r($result);
							if($result)
								{
									$_SESSION['msg']= array('error' => "",'success' => "Administrator info updated");
									
						           redirect(base_url(('suser/administrators')));
								}else 
									{
										$_SESSION['msg']= array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('suser/administrators')));
									}
						}

			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	public function download_adminphoto($filename = NULL) 
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/admins/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	public function admin_profile()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		    {
				$adminId=$this->input->post('adminId', TRUE);
				$list['admin_profile']=$this->susermodel->adminProfile($adminId);
				$this->load->view('suser/admin_profile',$list);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	        }
	}
	public function scoach_profile()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		    {
				$scId=$this->input->post('scId', TRUE);
				$list['coach_profile']=$this->susermodel->scoachProfile($scId);
				$this->load->view('suser/scoach_profile',$list);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	        }
	}
	
	public function newscoach()
		{
			if($_SESSION['sessdata']['suser_login']==TRUE)
	    	{
	    		$first_name=$this->input->post('firstName', TRUE);
				$last_name=$this->input->post('lastName', TRUE);
				$other_names=$this->input->post('otherNames', TRUE);
				$user_name=$this->input->post('userName', TRUE);
				$dateOfBirth=$this->input->post('dateOfBirth', TRUE);
				$staffId=$this->input->post('staffId', TRUE);
				$nationalId=$this->input->post('nationalId', TRUE);
				$id_type=$this->input->post('idType', TRUE);
				$phone_no=$this->input->post('phoneNumber', TRUE);
				$email=$this->input->post('email', TRUE);
				$residence=$this->input->post('residence', TRUE);
				$date_appointed=$this->input->post('dateAppointed', TRUE);

				$dateRegistered= date("Y-m-d H:i:s");


				$this->db->select('*');
				$this->db->from('sc_coaches');
				$this->db->where('date_appointed',$date_appointed);
				$query = $this->db->get();
			    $num=$query->num_rows();  
		        if($num>0)
			    {
			    	$_SESSION['msg']= array('error' => "Already registered",'success' => "");
					
					redirect(base_url(('suser/sc_coaches')));
				}else 
			            {
			            	$config['upload_path']          = 'uploads/profile_photos/scoaches';
						    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
						    // $config['max_size']             = 100;
						    $config['max_width']            = 500;
						    $config['max_height']           = 500;
						    $config['file_name']			=md5(time().$phone_no.$first_name.$last_name);
						    $config['overwrite']           = true;
						    $config['file_ext_tolower']     = true;

							 $this->load->library('upload', $config);

							 //if no photo uploaded
							  if (empty($_FILES['photo']['name'])) 
							  {
							  	$sc_details=array('sc_staff_id'=>$staffId,'date_appointed'=>$date_appointed,'date_registered'=>$dateRegistered, 'sc_fname'=>$first_name,'sc_lname'=>$last_name,'sc_username'=>$user_name,'sc_dob'=>$dateOfBirth,'sc_other_names'=>$other_names,'sc_nid'=>$nationalId,'id_type'=>$id_type,'sc_phone'=>$phone_no,'sc_email'=>$email,'sc_residence'=>$residence);

						    	$result=$this->susermodel->newScoach($sc_details);
								if($result)
									{
										$_SESSION['msg']= array('error' => "",'success' => "New S&C Coach registered");
										
										redirect(base_url(('suser/sc_coaches')));
									}else 
										{
											$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
											
							              redirect(base_url(('suser/sc_coaches')));
										}
							  	}else{

									    if (!$this->upload->do_upload('photo'))
									    {   
									        $_SESSION['msg']= array('error' => $this->upload->display_errors(),'success' => "");
											
									       	 redirect(base_url(('suser/sc_coaches')));
									    }else{
							       
								       		$data =$this->upload->data();
								    		//details of uploaded file
								    		$sc_details=array('sc_staff_id'=>$staffId,'date_appointed'=>$date_appointed,'date_registered'=>$dateRegistered, 'sc_fname'=>$first_name,'sc_lname'=>$last_name,'sc_username'=>$user_name,'sc_dob'=>$dateOfBirth,'sc_other_names'=>$other_names,'sc_nid'=>$nationalId,'id_type'=>$id_type,'sc_phone'=>$phone_no,'sc_email'=>$email,'sc_residence'=>$residence, 'sc_profile_photo'=>$config['file_name'].$data['file_ext']);

									    	$result=$this->susermodel->newScoach($sc_details);
											if($result)
												{
													$_SESSION['msg']= array('error' => "",'success' => "New S&C Coach added");
													
										           redirect(base_url(('suser/sc_coaches')));
												}else 
													{
														$_SESSION['msg']= array('error' => "Registration failed",'success' => "");
														
										               redirect(base_url(('suser/sc_coaches')));
													}
										  
										}
						    		}
						   	}
			}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }

	}
	public function edit_scoach()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
		    {
				$scId=$this->input->post('scId', TRUE);
				$list['coach_profile']=$this->susermodel->scoachProfile($scId);
				$this->load->view('suser/edit_scoach',$list);
		}else{
	                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('suser')));
	        }
	}
	
	//download receipt
	public function download_receipt($filename = NULL) 
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/expenditures/teams/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	
	
	//team tournaments view page
	public function tournaments()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$list['teams']=$this->susermodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['tournaments']=$this->susermodel->getTournaments($teamId);
			}


			$this->load->view('suser/tournaments',$list);
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//more about team expenditure
	public function getgameinfo()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$gameId=$this->input->post('gameId',TRUE);
			$gameType=$this->input->post('gameType',TRUE);
			$action=$this->input->post('action',TRUE);
			$list =$this->susermodel->gameDetails($gameId,$gameType);

	        $data = array();
	        foreach ($list as $game) 
	            {
	            	if($action=="add")
	            	{
		                $data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	<div class="col-md-12">
		                	<br>
			                	<div style="margin-bottom:10px;">
			                		<b>Start Date: </b>
			                	'.date_format(date_create($game['game_start_date']),"D j<\s\up>S</\s\up> M, Y").'</div> 		                
			                </div>
			                <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
			                <div class="form-group col-md-12 col-lg-12">
	                            <label for="recordId" class="control-label" style="font-weight:normal;"></label>
	                            <textarea type="number" name="recordId"  class=" form-control" id="recordId" required="required" placeholder="Summary of the tournament" style="height:150px;"></textarea>
	                        </div>
		                	');
	            	}else if($action=="summaryedit"){
	            		$data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	<div class="col-md-12">
		                	<br>
			                	<div style="margin-bottom:10px;">
			                		<b>Start Date: </b>
			                	'.date_format(date_create($game['game_start_date']),"D j<\s\up>S</\s\up> M, Y").'</div> 		                
			                </div>
			                <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
			                <div class="form-group col-md-12 col-lg-12">
	                            <label for="recordId" class="control-label" style="font-weight:normal;"></label>
	                            <textarea type="text" name="recordId"  class=" form-control" id="recordId" required="required" placeholder="Summary of the tournament" style="height:150px;">'.$game['game_summary'].'</textarea>
	                        </div>
		                	');
	            	}else if($action=="tournedit"){
	            		$data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	
			                 
	                        <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
	                        <div class="col-md-12 col-lg-12">
	                		<br>
                              <label for="gameStartDate" class="control-label">Start Date</label>
                              <div class="form-group">
                                  <div class="input-group date" id="gameStartDate">
                                      <input type="text" class="form-control" readonly="true" name="gameStartDate" value="'.$game['game_start_date'].'"'.'/>
                                      <span class="input-group-addon">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                            </div>
                             <div class="form-group col-md-12 col-lg-12" style="display:;">
	                            <label for="gameTitle" class="control-label" style="font-weight:normal;">Title<span class="star">*</span></label>
	                            <input type="text" name="gameTitle"  class=" form-control" id="gameTitle" required="required" value="'.$game['game_title'].'">
	                        </div>
			                
		                	');
	            	}
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//load tournament matches page
	public function tournmatches()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
		    $teamId=$this->input->post('teamId',TRUE);
			$sportName=$this->input->post('sportName',TRUE);
			$gameId=$this->input->post('gameId',TRUE);
			$tournamentTitle=$this->input->post('tournamentTitle',TRUE);

			if(isset($gameId) && strlen($gameId)){
				// set session for tournament id
				$team = $this->db->query("SELECT team.team_name FROM teams team WHERE team.team_auto_id='$teamId' LIMIT 1")->row();//get team name
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('tournamentId' =>$gameId,'tournamentTitle'=>$tournamentTitle,'sportName'=>$sportName,'tournamentTeamName'=>$team->team_name)); 
			    

				$list['players']=$this->susermodel->activeUninjuredPlayersPerTeam($teamId);

				$list['matches']=$this->susermodel->gameMatches($gameId);

				foreach($list['matches'] as $id =>$match)//loop and create another array for matchPlayers and append it to original array of matches
				{
					$matchId=$match['match_auto_id'];
					$list['matches'][$id]['matchplayers'] =$this->susermodel->getMatchPlayersAndScores($matchId);//list of match players and scores--- append match players to array matches
					$list['matches'][$id]['cards'] =$this->susermodel->getMatchCards($matchId);//get count of cards per match

				}


				//dynamically generate links to tournament match views. Ensure that view pages of tournament matches for each sport are in folder -sportname- and are named following the convention sportnametournamentmatches Note: your sports table must have sport names as one word e.g. volleyball not volley ball
				$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'tournamentmatches';

				$this->load->view('suser/'.$link_to_view_per_sport,$list);
			}else{

				$sportName=$_SESSION['sessdata']['sportName'];//from session
				$teamId=$_SESSION['sessdata']['tournamentTeamId'];//from session
				$gameId=$_SESSION['sessdata']['tournamentId'];//from session
				
				$list['players']=$this->susermodel->activeUninjuredPlayersPerTeam($teamId);
				$list['team_name']=$_SESSION['sessdata']['tournamentTeamName'];//from session;
				$list['matches']=$this->susermodel->gameMatches($gameId);

				foreach($list['matches'] as $id =>$match)//loop and create another array for matchPlayers and append it to original array of matches
				{
					$matchId=$match['match_auto_id'];
					$list['matches'][$id]['matchplayers'] =$this->susermodel->getMatchPlayersAndScores($matchId);//list of match players and scores--- append match players to array matches
					$list['matches'][$id]['cards'] =$this->susermodel->getMatchCards($matchId);//get count of cards per match

				}
				$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'tournamentmatches';

				$this->load->view('suser/'.$link_to_view_per_sport,$list);
			}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
		
	}
	//new  tournament score input page
	public function tournamentscoresheet()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$sportName=$_SESSION['sessdata']['sportName'];//from login session
			
			$matchId=$this->input->post('matchId',TRUE);
			$matchLevel=$this->input->post('matchLevel',TRUE);
			
			$matchDetails=$this->input->post('matchDetails',TRUE);
			if(isset ($matchId) && strlen($matchId) && isset($matchDetails) && strlen($matchDetails))
				{
					// set session for tournament id
				    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('tournamentMatchId' =>$matchId,'matchDetails'=>$matchDetails,'tournamentMatchLevel'=>$matchLevel));

					$list['players']=$this->susermodel->getMatchPlayers($matchId);
					$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'scoresheet';
					
					$this->load->view('suser/'.$link_to_view_per_sport,$list);
				}else{
						$matchId=$_SESSION['sessdata']['tournamentMatchId'];//from session set above
						$list['players']=$this->susermodel->getMatchPlayers($matchId);
						$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'scoresheet';
						$this->load->view('suser/'.$link_to_view_per_sport,$list);
				}
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	//get hockey match score
	public function hockeymatchscore()
	{
		if($_SESSION['sessdata']['suser_login']==TRUE)
	    {
			$matchId=$this->input->post('pid',TRUE);
			$list =$this->susermodel->getHockeyMatchScores($matchId);
			$data = array();
			$count=0;
			foreach ($list as $score) 
			    {
			    	$count=$count+1;
			        
			    }
			echo json_encode($count);//data should be a plain array...
		}else{
                $_SESSION['msg']= array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('suser')));
        }
	}
	


}
