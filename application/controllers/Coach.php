<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*@author Mokoro Stephen 
*/
class Coach extends CI_Controller
{

	function __construct()
	{
	     parent::__construct();
	     $this->load->model('Coachmodel','coachmodel');
	}

	public function index()
	{
		$this->load->view('login');
	}
	public function logout()
    {
        $this->cas->logout();
    }
	//All sport players view page
	public function players()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];
			$list['players']=$this->coachmodel->activePlayersListPerCoachTeams($coachId);
			$list['teams']=$this->coachmodel->activeTeamsListPerCoachTeams($coachId);
			$list['courses']=$this->coachmodel->coursesList();
			$this->load->view('coaches/playerreg',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//Only student sport players view page
	public function studentplayers()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];
			$list['players']=$this->coachmodel->activeStudentPlayersListPerCoachTeams($coachId);
			$this->load->view('coaches/studentplayers',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//player passports view page
	public function playerpassports()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];
			$list['players']=$this->coachmodel->activePlayersAndPassports($coachId);
			$this->load->view('coaches/playerpassports',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//load new player passport adding page
	public function addplayerpspt()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId',TRUE);
			$playerName=$this->input->post('playerName',TRUE);

			if(isset ($playerId) && strlen($playerId) && isset($playerName) && strlen($playerName)){
				$_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('pst_player_id'=>$playerId,'pst_player_name'=>$playerName));
				$this->load->view('coaches/addplayerpassport');
			}else{
				$this->load->view('coaches/addplayerpassport');
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//load player passport editing page
	public function editplayerpspt()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$passportId=$this->input->post('passportId',TRUE);
			$playerName=$this->input->post('playerName',TRUE);

			if(isset ($passportId) && strlen($passportId) && isset($playerName) && strlen($playerName))
			{
				$_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('pst_player_id'=>$passportId,'pst_player_name'=>$playerName));

				$list['passport_details']=$this->coachmodel->getPassportDetails($passportId);

				$this->load->view('coaches/edit_passport',$list);
			}else{
					$passportId=$_SESSION['sessdata']['passportId'];//use session if web browser hard refreshed
					$list['passport_details']=$this->coachmodel->getPassportDetails($passportId);
					$this->load->view('coaches/edit_passport',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//

	//save new player passport
	public function newplayerpspt() 
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$player_id=$this->input->post('playerId', TRUE);
			$passportNumber=$this->input->post('passportNo', TRUE);
			$dateOfIssue=$this->input->post('dateOfIssue', TRUE);
			$dateOfExpiry=$this->input->post('dateOfExpiry', TRUE);
			$issueCountry=$this->input->post('issueCountry', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$config['upload_path']          = 'uploads/travel_documents';
			$config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
			// $config['max_size']             = 700;
			// $config['max_width']            = 700;
			// $config['max_height']           = 700;
			$config['file_name']			=md5(time().$passportNumber);
			$config['overwrite']           = true;
			$config['file_ext_tolower']     = true;

			$this->load->library('upload', $config);

			if(isset($player_id) && strlen($player_id) && isset($passportNumber) && strlen($passportNumber) && isset($dateOfIssue) && strlen($dateOfIssue) && isset($dateOfExpiry) && strlen($dateOfExpiry) && isset($issueCountry) && strlen($issueCountry))
				{
				    $this->db->select('*');
					$this->db->from('travel_documents');
					$this->db->where('player_id',$player_id);
					$this->db->or_where('passport_number',$passportNumber);
					$query = $this->db->get();
			        $num=$query->num_rows(); 

			        if($num>0)
			            {
			            	$_SESSION['msg'] = array('error' => "This player passport exists",'success' => "");
				        	redirect(base_url(('coach/playerpassports')));
			        	}else{
			        				
				           		//if no photo has been  uploaded
				           		if (empty($_FILES['photo']['name'])) 
								  	{
							           	$_SESSION['msg'] = array('error' => "Please select passport photo file",'success' => "");
							        	redirect(base_url(('coach/addplayerpspt')));
							        }else{
								  			//confirm upload success
											if (!$this->upload->do_upload('photo'))
										    	{ 
										    		$passport_details =array('player_id'=>$player_id,'passport_number'=>$passportNumber,'issue_date'=>$dateOfIssue,'expiry_date'=>$dateOfExpiry,'issue_country'=>$issueCountry,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

										    		$result=$this->coachmodel->newPlayer($player_details); 

											        if($result)
													{
														$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Passport info added. </span> However, ".$this->upload->display_errors());
											           redirect(base_url(('coach/addplayerpspt')));
													}else 
														{
															$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
											               redirect(base_url(('coach/addplayerpspt')));
														}
										    	}else{
														$data =$this->upload->data(); //load data to page
											    		$passport_details =array('player_id'=>$player_id,'passport_number'=>$passportNumber,'issue_date'=>$dateOfIssue,'expiry_date'=>$dateOfExpiry,'issue_country'=>$issueCountry,'passport_photo'=>$config['file_name'].$data['file_ext'],'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

															$result=$this->coachmodel->addPlayerPspt($passport_details);

															if($result)
																{
																	$_SESSION['msg'] = array('error' => "",'success' => "Passport for ".$_SESSION['sessdata']['pst_player_name']." added.");
													           		redirect(base_url(('coach/playerpassports')));
																}else 
																	{
																		$_SESSION['msg'] = array('error' => "Passport not added",'success' => "");
													               		redirect(base_url(('coach/playerpassports')));
																	}

												    }
							   			}
							}
				}else{
						$_SESSION['msg'] = array('error' => "A required field is missing",'success' => "");
			        	redirect(base_url(('coach/addplayerpspt')));
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
					
			
	}
	//update player passport
	public function updateplayerpspt()
	{
    	if($_SESSION['sessdata']['coach_login']==TRUE)
    	{
	    	$passportId=$this->input->post('passportId',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$passport_number=$this->input->post('passportNo',TRUE);
			$issue_date=$this->input->post('dateOfIssue',TRUE);
			$expiry_date=$this->input->post('dateOfExpiry',TRUE);
			$issue_country=$this->input->post('issueCountry',TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];


			$config['upload_path']          = 'uploads/travel_documents';
			$config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
			$config['max_size']             = 700;
			$config['max_width']            = 700;
			$config['max_height']           = 700;
			$config['file_name']			=md5(time().$passport_number);
			$config['overwrite']           = true;
			$config['file_ext_tolower']     = true;

			$this->load->library('upload', $config);

			if (empty($_FILES['photo']['name'])) 
			{
		    	$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

				$result=$this->coachmodel->updatePassportNoPhoto($passport_details,$passportId);

				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Passport updated");
			           redirect(base_url(('coach/playerpassports')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");;
			               redirect(base_url(('coach/playerpassports')));
						}

				}else{ 

						if(!$this->upload->do_upload('photo'))
						{
							
							$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);
							$result=$this->coachmodel->updatePassportNoPhoto($passport_details,$passportId);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>passport details updated. </span> However, ".$this->upload->display_errors());
									
						           redirect(base_url(('coach/playerpassports')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
						               redirect(base_url(('coach/playerpassports')));
									}
						}else{

							   	$data =$this->upload->data();//details of uploaded file

								$passport_details=array('passport_number'=>$passport_number,'issue_date'=>$issue_date, 'expiry_date'=>$expiry_date,'issue_country'=>$issue_country,'passport_photo'=>$config['file_name'].$data['file_ext'],'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

								//new session of admin profile photo
			                    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array("passport_photo"=>$config['file_name'].$data['file_ext']));

								$result=$this->coachmodel->updatePassport($passport_details,$passportId,$initFile);
								if($result)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "Passport info updated");
							           redirect(base_url(('coach/playerpassports')));
									}else 
										{
											$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
							               redirect(base_url(('coach/playerpassports')));
										}


						}

				}
		}else{
            	$_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
            	redirect(base_url(('coach')));
    	}

	}
	//get more details of a player's passport
	public function playerpsptdetails()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$passportId=$this->input->post('passportId',TRUE);
			if(isset($passportId))
			{
				//create a session to handle browser refresh i.e not lose posted passport id
				$_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('passportId'=>$passportId));

				$list['passport_details']=$this->coachmodel->getPassportDetails($passportId);
				$this->load->view('coaches/player_passport_details',$list);

			}else {
					$passportId=$_SESSION['sessdata']['passportId'];//use session if web browser hard refreshed
					$list['passport_details']=$this->coachmodel->getPassportDetails($passportId);
					$this->load->view('coaches/player_passport_details',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	
	//get players of a match
	public function getplayers()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$tournamentMatchId=$this->input->post('pid',TRUE);
			$list =$this->coachmodel->getMatchPlayers($tournamentMatchId);
			$data = array();
			foreach ($list as $player) 
			    {
			        $data[]= $player['player_fname'].' '.$player['player_lname'];
			    }

			echo json_encode($data);//data should be a plain array...
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	
	//get player for autocomplete in captain registration
	public function getplayer()
	{
			$keyword=$this->input->get("q");
			$json = [];
			if(!empty($this->input->get("q"))){
				$this->db->or_like(array('player_fname' => $keyword, 'player_lname' => $keyword, 'player_other_names' => $keyword));
				$this->db->select('player_auto_id as id,CONCAT(player_fname," ",player_lname) as text');
				$this->db->limit(10);
				$query=$this->db->get("players");
				$json = $query->result();
			}
			echo json_encode($json);
		
	}
	//dashboard view page for coach
	public function dashboard()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];
			$sportName=$_SESSION['sessdata']['coachSportName'];//from login session

			$list['players']=$this->coachmodel->activePlayersListPerCoachTeams($coachId);
			$list['activeplayerscount']=$this->coachmodel->activePlayersCount($coachId);//count players active per coach team(s)
			$list['allinjuriestodatecount']=$this->coachmodel->allInjuriesToDateCount($coachId);//count all injuries to date for active players per coach team(s)
			$list['allactiveplayersonphysio']=$this->coachmodel->activePlayersOnPhysioPerTeamCount($coachId);//count all active players who are on physio for active players per coach team(s)
			$list['allactiveplayersnotonphysio']=$this->coachmodel->activePlayersNotOnPhysioPerTeamCount($coachId);//count all active players who are not on physio for active players per coach team(s)

			$teams=$this->coachmodel->activeTeamsListPerCoachTeams($coachId);

			$_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('coachnav'=>"coaches/".strtolower($sportName)."/coachnav",'perCoachTeams'=>$teams));
			$this->load->view('coaches/dashboard',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}
	//Player profile (more_about_player) for coach
	public function playerprofile()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);
			if(isset ($playerId) && strlen($playerId)){
				$player_profile['player_profile']=$this->coachmodel->playerProfile($playerId);
				$this->load->view('coaches/player_profile',$player_profile);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('coach/players')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//player next of kin profile
	public function nextofkin()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$playerId=$this->input->post('playerId', TRUE);
			if(isset ($playerId) && strlen($playerId)){
				$kin['nxtkin']=$this->coachmodel->playerProfile($playerId);
			$this->load->view('coaches/nxt_kin',$kin);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('coach/players')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}

	//player profile edit (edit player ) page
	public function editplayer()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];//from login session
			$playerId=$this->input->post('playerId', TRUE);//
			if(isset ($playerId) && strlen($playerId)){
				$player_profile['courses']=$this->coachmodel->coursesList();
				// $player_profile['teams']=$this->coachmodel->activeTeamsList();
				$player_profile['player_profile']=$this->coachmodel->playerProfile($playerId);
				$list['teams']=$this->coachmodel->activeTeamsListPerCoachTeams($coachId);
				$this->load->view('coaches/edit_player',$player_profile);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('coach/players')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}

	//training days page view
	public function trainingdays()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];//from login session
			$list['tr_days']=$this->coachmodel->trDaysList($coachId);
			$list['teams']=$this->coachmodel->activeTeamsListPerCoachTeams($coachId);
			$this->load->view('coaches/training_days',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//edit page for training days vpage view
	public function editdays()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$trdId=$this->input->post('trdId', TRUE);
			if(isset ($trdId) && strlen($trdId)){
				$training_days['tr_days']=$this->coachmodel->trdList($trdId);
				$this->load->view('coaches/edit_trds',$training_days);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('coach/trainingdays')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}

	//training attendance view
	public function trainingattendance()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	    	$coachId=$_SESSION['sessdata']['coachId'];
			$list['players']=$this->coachmodel->activePlayersForAttendance($coachId);


			$list['trainingattendance']=$this->coachmodel->playerAttendanceGroupedPerDate($coachId);
			foreach($list['trainingattendance'] as $id => $attendance)//loop and create another array for subjects taken and append it to original array
			{
				$attendanceDate=$attendance['training_date'];
				$countOfPlayersMarkedPresentAbsentOrExcused=$this->coachmodel->countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$coachId);

				$list['trainingattendance'][$id]['countAll'] =$countOfPlayersMarkedPresentAbsentOrExcused;

				$list['trainingattendance'][$id]['countPresent'] =$this->coachmodel->countOfPlayersPresent($attendanceDate,$coachId);
				$list['trainingattendance'][$id]['countAbsent'] =$this->coachmodel->countOfPlayersAbsent($attendanceDate,$coachId);
				$list['trainingattendance'][$id]['countExcused'] =$this->coachmodel->countOfPlayersExcused($attendanceDate,$coachId);
			}

			$this->load->view('coaches/training_attendance',$list);
			// print_r($list);
		}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
	                redirect(base_url(('coach')));
	        }
	}
	//list of training attendance 
	public function traininglist()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {

	    	$coachId=$_SESSION['sessdata']['coachId'];
	    	$trainingDate=$this->input->post('trainingDate',TRUE);
			if(isset($trainingDate) && strlen($trainingDate)){
				// set session for  date and team id
				
			   $_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('trainingDate' =>$trainingDate)); 
		    	$list['traininglist']=$this->coachmodel->trainingList($trainingDate,$coachId);
				$this->load->view('coaches/players_attendance_list',$list);
			}else{
	    		$trainingDate=$_SESSION['sessdata']['trainingDate'];
	    		$list['traininglist']=$this->coachmodel->trainingList($trainingDate,$coachId);
				$this->load->view('coaches/players_attendance_list',$list);
			}

    	}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
    }
	//injuries
	public function injuries()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$coachId=$_SESSION['sessdata']['coachId'];
			$record['injuries']=$this->coachmodel->injuryRecords($coachId);
			$this->load->view('coaches/injury_records',$record);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	
	
	//more about injury record
	public function injurydetails()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$recordId=$this->input->post('recordId', TRUE);
			if(isset ($recordId) && strlen($recordId)){
				$record['injury_record']=$this->coachmodel->injuryRecord($recordId);
				$this->load->view('coaches/injury_more',$record);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('coach/injuries')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
		
	}
	
	//player registration
	public function newplayer()
		{
			if($_SESSION['sessdata']['coach_login']==TRUE)
	    	{
				//check if student id was not entered. If it was not entered, save null for both student id and course
				$studid=$this->input->post('studentID', TRUE);

				if($studid=="")
					{
						$student_id=NULL;
						$student_course_id=NULL;
					}else{
							$student_id=$this->input->post('studentID', TRUE);
			    			$student_course_id=$this->input->post('course', TRUE);
						}

				$player_fname=$this->input->post('firstName', TRUE);
				$player_lname=$this->input->post('lastName', TRUE);
				$player_other_names=$this->input->post('otherNames', TRUE);
				$player_dob=$this->input->post('dateOfBirth', TRUE);
				$player_gender=$this->input->post('gender', TRUE);
				$player_nid=$this->input->post('idNo', TRUE);
				$player_id_type=$this->input->post('idType', TRUE);
				$player_phone_no=$this->input->post('phoneNumber', TRUE);
				$player_email=$this->input->post('emailAddress', TRUE);
		       
		        $kin_other_names=$this->input->post('kinOtherNames', TRUE);
				$kin_fname=$this->input->post('kinFirstName', TRUE);
				$kin_lname=$this->input->post('kinLastName', TRUE);
				$kin_nid_no=$this->input->post('kinNationalID', TRUE);
		        $kin_phone_no=$this->input->post('kinPhoneNumber', TRUE);
		        $kin_alt_phone_no=$this->input->post('kinAltPhoneNumber', TRUE);
		        $kin_email=$this->input->post('kinEmailAddress', TRUE);
		        $kin_current_residence=$this->input->post('kinCurrentResidence', TRUE);
		        $current_height=$this->input->post('currentHeight', TRUE);
		        $current_weight=$this->input->post('currentWeight', TRUE);
		        $prev_high_school=$this->input->post('previousHighSchool', TRUE);
		        $prev_play_status=$this->input->post('prevStatus', TRUE);
		        $prev_team=$this->input->post('previousTeam', TRUE);
		        $teamId=$this->input->post('teamId', TRUE);
		        $highest_achievement=$this->input->post('highestAchievement', TRUE);
				$player_residence=$this->input->post('currentResidence', TRUE);
		        
				$dateRegistered= date("Y-m-d H:i:s"); 

				$userGroupId=$_SESSION['sessdata']['userGroupId'];
				$userAutoId=$_SESSION['sessdata']['userAutoId'];


				$this->db->select('*');
				$this->db->from('players');
				$this->db->where('player_nid',$player_nid);
				$query = $this->db->get();
		        $num=$query->num_rows(); 
		        if($num>0)
		            {
		            	$_SESSION['msg'] = array('error' => "Already registered",'success' => "");
			            redirect(base_url(('coach/players')));
		            }else 
			            {
			            	$config['upload_path']          = 'uploads/profile_photos/players';
						    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
						    // $config['max_size']             = 100;
						    // $config['max_width']            = 500;
						    // $config['max_height']           = 500;
						    $config['file_name']			=md5(time().$player_phone_no.$player_fname.$player_lname);
						    $config['overwrite']           = true;
						    $config['file_ext_tolower']     = true;

							 $this->load->library('upload', $config);

							 //if no photo uploaded
							  if (empty($_FILES['photo']['name'])) 
							  {
							  	$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'date_registered'=>$dateRegistered,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

						    	$result=$this->coachmodel->newPlayer($player_details);
								if($result)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "New player added");
							           redirect(base_url(('coach/players')));
									}else 
										{
											$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
							               redirect(base_url(('coach/players')));
										}
							  }else{

									    if (!$this->upload->do_upload('photo'))
									    {  

									    	$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'date_registered'=>$dateRegistered,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

						    			$result=$this->coachmodel->newPlayer($player_details); 
									        if($result)
											{
												$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Player info registered. </span> However, ".$this->upload->display_errors());
									           redirect(base_url(('coach/players')));
											}else 
												{
													$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
									               redirect(base_url(('coach/players')));
												}
									    }else{
							       
								       		$data =$this->upload->data();
								    		//details of uploaded file
								    			$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'date_registered'=>$dateRegistered,'player_profile_photo'=>$config['file_name'].$data['file_ext'],'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

									    	$result=$this->coachmodel->newPlayer($player_details);
											if($result)
												{
													$_SESSION['msg'] = array('error' => "",'success' => "New player added");
										           redirect(base_url(('coach/players')));
												}else 
													{
														$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
										               redirect(base_url(('coach/players')));
													}
										  
										}
						    		}
						   	}
			}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }

	}

	//update player information
	public function  updateplayer()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$player_auto_id=$this->input->post('playerAutoId',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			//check if student id was not entered. If it was not entered, save null for both student id and course
			$studid=$this->input->post('studentID', TRUE);
			if($studid=="")
				{
					$student_id=NULL;
					$student_course_id=NULL;
				}else{
						$student_id=$this->input->post('studentID', TRUE);
		    			$student_course_id=$this->input->post('course', TRUE);
					}

			$player_fname=$this->input->post('firstName', TRUE);
			$player_lname=$this->input->post('lastName', TRUE);
			$player_other_names=$this->input->post('otherNames', TRUE);
			$player_dob=$this->input->post('dateOfBirth', TRUE);
			$player_gender=$this->input->post('gender', TRUE);
			$player_nid=$this->input->post('idNo', TRUE);
			$player_id_type=$this->input->post('idType', TRUE);
			$player_phone_no=$this->input->post('phoneNumber', TRUE);
			$player_email=$this->input->post('emailAddress', TRUE);
		   
			$kin_fname=$this->input->post('kinFirstName', TRUE);
			$kin_lname=$this->input->post('kinLastName', TRUE);
		    $kin_other_names=$this->input->post('kinOtherNames', TRUE);
			$kin_nid_no=$this->input->post('kinNationalID', TRUE);
		    $kin_phone_no=$this->input->post('kinPhoneNumber', TRUE);
		    $kin_alt_phone_no=$this->input->post('kinAltPhoneNumber', TRUE);
		    $kin_email=$this->input->post('kinEmailAddress', TRUE);
		    $kin_current_residence=$this->input->post('kinCurrentResidence', TRUE);
		    $current_height=$this->input->post('currentHeight', TRUE);
		    $current_weight=$this->input->post('currentWeight', TRUE);
		    $prev_high_school=$this->input->post('previousHighSchool', TRUE);
		    $prev_play_status=$this->input->post('prevStatus', TRUE);
		    $prev_team=$this->input->post('previousTeam', TRUE);
		    $teamId=$this->input->post('teamId', TRUE);
		    $highest_achievement=$this->input->post('highestAchievement', TRUE);
			$player_residence=$this->input->post('currentResidence', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$config['upload_path']          = 'uploads/profile_photos/players';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		      // $config['max_size']             = 100;
		    $config['max_width']            = 500;
		    $config['max_height']           = 500;
			$config['file_name']			=md5(time().$player_phone_no.$player_fname.$player_lname);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) {
		    	$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

					$result=$this->coachmodel->updatePlayerNoPhoto($player_details,$player_auto_id);
					if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "Player updated!");
				           redirect(base_url(('coach/players')));
						}else 
							{
								$_SESSION['msg'] = array('error' => "No changes made",'success' => "");
				               redirect(base_url(('coach/players')));
							}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

						$result=$this->coachmodel->updatePlayerNoPhoto($player_details,$player_auto_id);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Player info updated. </span> However, ".$this->upload->display_errors());
					           redirect(base_url(('coach/players')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
					               redirect(base_url(('coach/players')));
								}
					}else{
						   	$data =$this->upload->data();//details of uploaded file

							$player_details =array('stud_id'=>$student_id,'player_fname'=>$player_fname,'player_lname'=>$player_lname,'player_other_names'=>$player_other_names,'player_dob'=>$player_dob,'player_nid'=>$player_nid,'id_type'=>$player_id_type,'player_phone'=>$player_phone_no,'player_email'=>$player_email,'player_residence'=>$player_residence,'kin_fname'=>$kin_fname, 'kin_lname'=>$kin_lname, 'kin_other_names'=>$kin_other_names, 'kin_nid'=>$kin_nid_no, 'kin_phone'=>$kin_phone_no,'kin_alt_phone'=>$kin_alt_phone_no, 'kin_email'=>$kin_email, 'kin_residence'=>$kin_current_residence,'player_weight'=>$current_weight,'player_height'=>$current_height,'prev_hschool'=>$prev_high_school,'prev_play_state'=>$prev_play_status,'prev_team'=>$prev_team,'player_team_id'=>$teamId,'player_gender'=>$player_gender, 'h_achievement'=>$highest_achievement,'stud_course_id'=>$student_course_id,'player_profile_photo'=>$config['file_name'].$data['file_ext'],'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

							$result=$this->coachmodel->updatePlayer($player_details,$player_auto_id,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Mentee info updated");
						           redirect(base_url(('coach/players')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
						               redirect(base_url(('coach/players')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}
	//disable Player
	public function disablePlayer()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	    	$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$player_auto_id=$this->input->post('playerAutoId', TRUE);
			$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
			$updateDetails=array('active_status'=>0, 'reason_inactive'=>$reasonToDisable,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);

			

			$result=$this->coachmodel->disablePlayer($updateDetails,$player_auto_id);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Player disabled!");
		           redirect(base_url(('coach/players')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Failed to disable!",'success' => "");
		               redirect(base_url(('coach/players')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//save new training days
	public function newdays()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
		   	$training_year=date("Y");
			$teamId=$this->input->post('teamId', TRUE);
			$january=$this->input->post('january', TRUE);
			$february=$this->input->post('february', TRUE);
			$march=$this->input->post('march', TRUE);
			$april=$this->input->post('april', TRUE);
			$may=$this->input->post('may', TRUE);
			$june=$this->input->post('june', TRUE);
			$july=$this->input->post('july', TRUE);
			$august=$this->input->post('august', TRUE);
			$september=$this->input->post('september', TRUE);
			$october=$this->input->post('october', TRUE);
			$november=$this->input->post('november', TRUE);
			$december=$this->input->post('december', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$tr_days_details=array('trd_year'=>$training_year,'trd_team_id'=>$teamId,'january'=>$january,'february'=>$february,'march'=>$march,'april'=>$april,'may'=>$may,'june'=>$june,'july'=>$july,'august'=>$august,'september'=>$september,'october'=>$october,'november'=>$november,'december'=>$december,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

			//check if training days for the year already exists
			$this->db->select('*');
			$this->db->from('training_days');
			$this->db->where('trd_year',$training_year);
			$this->db->where('trd_team_id',$teamId);
			$query=$this->db->get();
			 $num=$query->num_rows(); 
		    if($num>0)
		       {
		       		$_SESSION['msg'] = array('error' => "The team's training days  for ".$training_year." exist",'success' => "");
		            redirect(base_url(('coach/trainingdays')));
		       }else
		           	{
		           		$result=$this->coachmodel->newTrDays($tr_days_details);

						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "New training days added");
								
			                   redirect(base_url(('coach/trainingdays')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "Failed to add training days",'success' => "");
									
				                   redirect(base_url(('coach/trainingdays')));
								}
		           	}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('coach')));
        }
	}

	//update training days
	public function updatetrdays()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$trdId=$this->input->post('trdId', TRUE);
			$training_year=$this->input->post('trainingYear', TRUE);
			$january=$this->input->post('january', TRUE);
			$february=$this->input->post('february', TRUE);
			$march=$this->input->post('march', TRUE);
			$april=$this->input->post('april', TRUE);
			$may=$this->input->post('may', TRUE);
			$june=$this->input->post('june', TRUE);
			$july=$this->input->post('july', TRUE);
			$august=$this->input->post('august', TRUE);
			$september=$this->input->post('september', TRUE);
			$october=$this->input->post('october', TRUE);
			$november=$this->input->post('november', TRUE);
			$december=$this->input->post('december', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$tr_days_details=array('january'=>$january,'february'=>$february,'march'=>$march,'april'=>$april,'may'=>$may,'june'=>$june,'july'=>$july,'august'=>$august,'september'=>$september,'october'=>$october,'november'=>$november,'december'=>$december,'trd_auto_id'=>$trdId,'updating_user_id'=>$userAutoId,'updating_user_group_id'=>$userGroupId);
			
			$result=$this->coachmodel->updateTrDays($tr_days_details,$trdId);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Training days updated");
					
		           redirect(base_url(('coach/trainingdays')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made",'success' => "");
						
		               redirect(base_url(('coach/trainingdays')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('coach')));
        }
	}


	// Team Expenditure 
	public function expenditures()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$teamId=$this->input->get('team_id');
			if(isset($teamId) && strlen($teamId))
			{
				$list['expenses']=$this->coachmodel->expenses($teamId);
				$team = $this->db->query("SELECT team.team_name FROM teams team WHERE team.team_auto_id='$teamId' LIMIT 1")->row();
				
			    $_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('expenseTeamId' =>$teamId,'expenseTeamName'=>$team->team_name));
				$this->load->view('coaches/team_expenses',$list);
			}else{
				$teamId=$_SESSION['sessdata']['expenseTeamId'];
				$list['expenses']=$this->coachmodel->expenses($teamId);
				$this->load->view('coaches/team_expenses',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('coach')));
        }
	}
	//new team expenditure
	public function newexpenditure()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {

			$teamId=$this->input->post('teamId',TRUE);
			$expenseDate=$this->input->post('expenseDate', TRUE);
			$cash=$this->input->post('cash', TRUE);
			
			if(strlen($this->input->post('lpoNo', TRUE))=="")
			{
				$lpo=NULL;
				$lpoNo=NULL;
			}else{

				$lpo=$this->input->post('lpo', TRUE);
				$lpoNo=$this->input->post('lpoNo', TRUE);
			}
			$lunches=$this->input->post('lunches', TRUE);
			$receipt=$this->input->post('receipt',TRUE);
			$exComment=$this->input->post('comments', TRUE);
			$date_recorded= date("Y-m-d H:i:s");

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			$config['upload_path']          = 'uploads/expenditures/teams/';
		    $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx';
		    // $config['max_size']             = 100;
		    // $config['max_width']            = 500;
		    // $config['max_height']           = 500;
		    $config['file_name']			=md5(time().$expenseDate.$teamId.$date_recorded);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

			 $this->load->library('upload', $config);

			 //if no receipt uploaded
			  if (empty($_FILES['receipt']['name'])) 
			  {

				$expdetails=array('expense_team_auto_id'=>$teamId,'expense_date'=>$expenseDate,'expense_cash'=>$cash,'expense_lpo_no'=>$lpoNo,'expense_lpo_amount'=>$lpo,'actual_expenditure'=>$lunches,'expense_comment'=>$exComment,'expense_record_date'=>$date_recorded,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);
				$result=$this->coachmodel->newExpenseWithoutReceipt($expdetails);
					if($result)
						{
							$_SESSION['msg'] = array('edit'=>'','error' => "",'success' => "Expenditure added ");
		                   redirect(base_url(('coach/expenditures')));
						}else 
							{
								$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add expenditure",'success' => "");
			                   redirect(base_url(('coach/expenditures')));
							}
				}else{
						if (!$this->upload->do_upload('receipt'))
						    {   
						        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						       	redirect(base_url(('coach/expenditures')));
						    }else{
					   
						       		$data =$this->upload->data();
						    		//details of uploaded file
						    			$expdetails=array('expense_team_auto_id'=>$teamId,'expense_date'=>$expenseDate,'expense_cash'=>$cash,'expense_lpo_no'=>$lpoNo,'expense_lpo_amount'=>$lpo,'actual_expenditure'=>$lunches,'expense_comment'=>$exComment,'expense_record_date'=>$date_recorded,'receipt_file_name'=>$config['file_name'].$data['file_ext'],'receipt_file_ext'=>$data['file_ext'],'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

							    	$result=$this->coachmodel->newExpenseWithReceipt($expdetails);
									if($result)
										{
											 $_SESSION['msg'] = array('error' => "",'success' => "New expense added");
								           redirect(base_url(('coach/expenditures')));
										}else 
											{
												 $_SESSION['msg'] = array('error' => "Expense failed to add.",'success' => "");
								               redirect(base_url(('coach/expenditures')));
											}
							}
			
					}
			}else{
                 $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//more about team expenditure
	public function expenditureinfo()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$expsId=$this->input->post('expsId',TRUE);
			$list['expenses']=$this->coachmodel->expenseDetails($expsId);
			$this->load->view('coaches/expemore',$list);
		}else{
                 $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	public function viewteamexpense()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$expenseId=$this->input->post('expenseId',TRUE);
			$list =$this->coachmodel->expenditureDetails($expenseId);

	        $data = array();
	        foreach ($list as $expense) 
	            {
	                $data= array('recordId' => $expense['expense_auto_id'],'teamName'=>$expense['team_name'],'info'=>'
	                	<div class="col-md-6">
	                	<br>
		                	<div style="margin-bottom:10px;">
		                		<b>Expenditure Date</b>
		                	</div> '.date_format(date_create($expense['expense_date']),"D j<\s\up>S</\s\up> M, Y").'<br><br>
		                	<div style="margin-bottom:10px;">
		                		<b> Cash (Kshs)</b>
		                	</div> '.$expense['expense_cash'].'<br><br> 
		                	<div style="margin-bottom:10px;">
		                		<b>Actual Costs (Kshs)</b>
		                	</div> '.$expense['actual_expenditure'].'<br><br> 
		                </div>
	                	<div class="col-md-6">
	                	<br>
		                	<div style="margin-bottom:10px;">
		                		<b>LPO (Kshs)</b>
		                	</div> '.$expense['expense_lpo_amount'].'<br><br> 
		                	<div style="margin-bottom:10px;">
		                		<b>LPO No.</b>
		                	</div> '.$expense['expense_lpo_no'].'<br><br> 
		                	
		                	<div style="margin-bottom:10px;">
		                		<b>Comments</b>
		                	</div> '.$expense['expense_comment'].'<br><br></div>');
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                 $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	public function download_receipt($filename = NULL) 
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/expenditures/teams/'.$filename);
	        force_download($filename, $data);
	    }else{
                 $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	
	
	//load tournaments page
	public function tournaments()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$teamId=$this->input->get('team_id');


			if(isset($teamId) && strlen($teamId))
			{

				$list['tournaments']=$this->coachmodel->getTournaments($teamId);
				$team = $this->db->query("SELECT team.team_name FROM teams team WHERE team.team_auto_id='$teamId' LIMIT 1")->row();//get team name

				foreach($list['tournaments'] as $tournament)
				{
					//create a session for team id and team name
					
					 $_SESSION['sessdata'] =array_merge($_SESSION['sessdata'], array('tournamentTeamId' =>$tournament['team_auto_id'],'tournamentTeamName'=>$team->team_name));
					
				}

				$this->load->view('coaches/tournaments',$list);
			}else{
					$teamId= $_SESSION['sessdata']['tournamentTeamId'];//from session set earlier in the if statement
					$list['tournaments']=$this->coachmodel->getTournaments($teamId);
					$this->load->view('coaches/tournaments',$list);

			}

			
		}else{
                 $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	

	//more about team expenditure
	public function getgameinfo()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$gameId=$this->input->post('gameId',TRUE);
			$gameType=$this->input->post('gameType',TRUE);
			$action=$this->input->post('action',TRUE);
			$list =$this->coachmodel->gameDetails($gameId,$gameType);

	        $data = array();
	        foreach ($list as $game) 
	            {
	            	if($action=="add")
	            	{
		                $data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	<div class="col-md-12">
		                	<br>
			                	<div style="margin-bottom:10px;">
			                		<b>Start Date: </b>
			                	'.date_format(date_create($game['game_start_date']),"D j<\s\up>S</\s\up> M, Y").'</div> 		                
			                </div>
			                <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
			                <div class="form-group col-md-12 col-lg-12">
	                            <label for="recordId" class="control-label" style="font-weight:normal;"></label>
	                            <textarea type="number" name="recordId"  class=" form-control" id="recordId" required="required" placeholder="Summary of the tournament" style="height:150px;"></textarea>
	                        </div>
		                	');
	            	}else if($action=="summaryedit"){
	            		$data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	<div class="col-md-12">
		                	<br>
			                	<div style="margin-bottom:10px;">
			                		<b>Start Date: </b>
			                	'.date_format(date_create($game['game_start_date']),"D j<\s\up>S</\s\up> M, Y").'</div> 		                
			                </div>
			                <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
			                <div class="form-group col-md-12 col-lg-12">
	                            <label for="recordId" class="control-label" style="font-weight:normal;"></label>
	                            <textarea type="text" name="recordId"  class=" form-control" id="recordId" required="required" placeholder="Summary of the tournament" style="height:150px;">'.$game['game_summary'].'</textarea>
	                        </div>
		                	');
	            	}else if($action=="tournedit"){
	            		$data= array('recordId' => $game['game_auto_id'],'teamName'=>$game['team_name']." Tournament",'info'=>'
		                	
			                 
	                        <div class="form-group col-md-12 col-lg-12" style="display:none;">
	                            <label for="gameId" class="control-label" style="font-weight:normal;">Game Id<span class="star">*</span></label>
	                            <input type="number" name="gameId"  class=" form-control" id="gameId" required="required" value="'.$game['game_auto_id'].'">
	                        </div>
	                        <div class="col-md-12 col-lg-12">
	                		<br>
                              <label for="gameStartDate" class="control-label">Start Date</label>
                              <div class="form-group">
                                  <div class="input-group date" id="gameStartDate">
                                      <input type="text" class="form-control" readonly="true" name="gameStartDate" value="'.$game['game_start_date'].'"'.'/>
                                      <span class="input-group-addon">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                            </div>
                             <div class="form-group col-md-12 col-lg-12" style="display:;">
	                            <label for="gameTitle" class="control-label" style="font-weight:normal;">Title<span class="star">*</span></label>
	                            <input type="text" name="gameTitle"  class=" form-control" id="gameTitle" required="required" value="'.$game['game_title'].'">
	                        </div>
			                
		                	');
	            	}
	            }

	        	echo json_encode($data);//data should be a plain array...
        }else{
                 $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//load tournament matches page
	public function tournmatches()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
		    $teamId=$this->input->post('teamId',TRUE);
			$sportName= $_SESSION['sessdata']['coachSportName'];//from session
			$gameId=$this->input->post('gameId',TRUE);
			$tournamentTitle=$this->input->post('tournamentTitle',TRUE);

			if(isset($gameId) && strlen($gameId)){
				// set session for tournament id
				
			     $_SESSION['sessdata'] = array_merge($_SESSION['sessdata'], array('tournamentId' =>$gameId,'tournamentTitle'=>$tournamentTitle,'sportName'=>$sportName)); 

				$list['players']=$this->coachmodel->activeUninjuredPlayersPerTeam($teamId);

				$list['matches']=$this->coachmodel->gameMatches($gameId);

				foreach($list['matches'] as $id =>$match)//loop and create another array for matchPlayers and append it to original array of matches
				{
					$matchId=$match['match_auto_id'];
					$list['matches'][$id]['matchplayers'] =$this->coachmodel->getMatchPlayersAndScores($matchId);//list of match players and scores--- append match players to array matches
					$list['matches'][$id]['cards'] =$this->coachmodel->getMatchCards($matchId);//get count of cards per match

				}


				//dynamically generate links to tournament match views. Ensure that view pages of tournament matches for each sport are in folder -sportname- and are named following the convention sportnametournamentmatches Note: your sports table must have sport names as one word e.g. volleyball not volley ball
				$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'tournamentmatches';

				$this->load->view('coaches/'.$link_to_view_per_sport,$list);
			}else{

				$sportName= $_SESSION['sessdata']['sportName'];//from session
				$teamId=$_SESSION['sessdata']['tournamentTeamId'];//from session
				$gameId=$_SESSION['sessdata']['tournamentId'];//from session
				
				$list['players']=$this->coachmodel->activeUninjuredPlayersPerTeam($teamId);
				$list['team_name']=$_SESSION['sessdata']['tournamentTeamName'];//from session;
				$list['matches']=$this->coachmodel->gameMatches($gameId);

				foreach($list['matches'] as $id =>$match)//loop and create another array for matchPlayers and append it to original array of matches
				{
					$matchId=$match['match_auto_id'];
					$list['matches'][$id]['matchplayers'] =$this->coachmodel->getMatchPlayersAndScores($matchId);//list of match players and scores--- append match players to array matches
					$list['matches'][$id]['cards'] =$this->coachmodel->getMatchCards($matchId);//get count of cards per match

				}
				$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'tournamentmatches';

				$this->load->view('coaches/'.$link_to_view_per_sport,$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}

	
	//record new tournament match 
	public function newtournmatch()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$tournamentId=$_SESSION['sessdata']['tournamentId'];//from session created in loading tournaments matches page
			
			$matchDate=$this->input->post('matchDate',TRUE);
			$matchVenue=$this->input->post('matchVenue',TRUE);

			$matchStartTime= date("H:i", strtotime($this->input->post('matchStartTime',TRUE)));//convert to 24 hr
			$matchOpponents=$this->input->post('matchOpponents',TRUE);
			$matchLevel=$this->input->post('matchLevel',TRUE);

			$playerList=$this->input->post('playerList[]',TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];


			$match_details=array('match_game_id'=>$tournamentId,'match_date'=>$matchDate,'match_start_time'=>$matchStartTime,'match_opponents'=>$matchOpponents,'match_venue'=>$matchVenue,'match_level'=>$matchLevel,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

			if(!empty($playerList)){
				//create array of players list to be inserted to a different table once the match details are added successfully
				
			    $$_SESSION['sessdata'] =array_merge($_SESSION['sessdata'],array('playerList' =>$playerList)); 

			    $result=$this->coachmodel->newTournMatch($match_details);
			    if(!$result)
					{
						$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add match",'success' => "");
		               redirect(base_url(('coach/tournmatches')));
					}else{
							$tournMatchId=$result;//auto id of the just inserted row. this will be used to undo/delete the record if players insertion fails.
							$playInfo=array();
						     foreach($playerList as $playerId ) /*loop through and get individual player Ids*/
				            {
				                $playInfo[]=array('match_id'=>$tournMatchId,'match_player_id'=>$playerId,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

				                //pass array of data to model for saving
				                // $success=$this->model->tournMatchPlayers($playInfo);
				            }
					         	$success=$this->coachmodel->tournMatchPlayers($playInfo);
					         	if($success)
					         	{
					                $_SESSION['msg']  = array('edit'=>'','error' => "",'success' => "Match successfully added");
					                redirect(base_url(('coach/tournmatches')));
					              }else 
									{
										//delete last inserted match as a rollback
										$delresult=$this->coachmodel->deleteTournMatch($tournMatchId);
										if($delresult)
											{
												$_SESSION['msg']  = array('edit'=>'','error' => "Failed to add match",'success' => "");
												
							                   	redirect(base_url(('coach/tournmatches')));
							                 }else{
							                 	$_SESSION['sessdata']  = array_merge($_SESSION['sessdata'] ,array('edit'=>'','error' => "Failed to add match. Only match details added",'success' => ""));
							                 }
									}

							
						}

			}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }



	}

	
	//record new tournament 
	public function newtournament()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$teamId=$_SESSION['sessdata']['tournamentTeamId'];//from session
			$tournamentTitle=$this->input->post('tournamentTitle',TRUE);
			$startDate=$this->input->post('startDate',TRUE);
			$endDate=$this->input->post('endDate',TRUE);
			$dateRecorded= date("Y-m-d H:i:s"); 

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			//dynamically create functions to redirect to depending on coach sport

			$tournamentdetails=array('game_team'=>$teamId,'game_match_type'=>1,'game_title'=>$tournamentTitle,'game_start_date'=>$startDate,'game_end_date'=>$endDate,'game_date_recorded'=>$dateRecorded,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);

				$result=$this->coachmodel->newTournament($tournamentdetails);
					if($result)
						{
							$_SESSION['msg']  = array('edit'=>'','error' => "",'success' => "Tournament added");
		                   redirect(base_url(('coach/tournaments')));
						}else 
							{
								$_SESSION['msg']  = array('edit'=>'','error' => "Failed to add tournament",'success' => "");
			                   redirect(base_url(('coach/tournaments')));
							}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	

	//new  tournament score input page
	public function tournamentscoresheet()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$sportName=$_SESSION['sessdata']['coachSportName'];//from login session
			
			$matchId=$this->input->post('matchId',TRUE);
			$matchLevel=$this->input->post('matchLevel',TRUE);
			
			$matchDetails=$this->input->post('matchDetails',TRUE);
			if(isset ($matchId) && strlen($matchId) && isset($matchDetails) && strlen($matchDetails))
				{
					// set session for tournament id
					
				    $_SESSION['sessdata']  =array_merge($_SESSION['sessdata'] , array('tournamentMatchId' =>$matchId,'matchDetails'=>$matchDetails,'tournamentMatchLevel'=>$matchLevel)); 

					$list['players']=$this->coachmodel->getMatchPlayers($matchId);
					$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'scoresheet';
					
					$this->load->view('coaches/'.$link_to_view_per_sport,$list);
				}else{
						$matchId=$_SESSION['sessdata']['tournamentMatchId'];//from session set above
						$list['players']=$this->coachmodel->getMatchPlayers($matchId);
						$link_to_view_per_sport=strtolower($sportName).'/'.strtolower($sportName).'scoresheet';
						$this->load->view('coaches/'.$link_to_view_per_sport,$list);
				}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//saving new hockey score
	public function newhockeyscore()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$matchId=$_SESSION['sessdata']['tournamentMatchId'];//from session created on loading score sheet page
			$playerId=$this->input->post('playerId',TRUE);
			$scores=$this->input->post('scores',TRUE);
			$greenCards=$this->input->post('greenCards',TRUE);
			$yellowCards=$this->input->post('yellowCards',TRUE);
			$redCards=$this->input->post('redCards',TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			if(isset($playerId) && strlen($playerId) && isset($matchId) && strlen($matchId) ){
				$scoreDetails=array('player_id'=>$playerId,'match_id'=>$matchId,'scores'=>$scores,'green_cards'=>$greenCards,'yellow_cards'=>$yellowCards,'red_cards'=>$redCards,'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);
				
				$result=$this->coachmodel->newHockeyScore($scoreDetails);
				if($result)
					{
						$_SESSION['msg']  = array('error' => "",'success' => "New scores added");
                		redirect(base_url(('coach/tournamentscoresheet')));

					}else 
						{
							$_SESSION['msg']  = array('error' => "Failed to record scores",'success' => "");
                			redirect(base_url(('coach/tournamentscoresheet')));

						}
			}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}
	public function newhockeyopponentscore()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	    	$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];
			
			$matchId=$this->input->post('matchId',TRUE);
			$matchEndTime= date("H:i", strtotime($this->input->post('matchEndTime',TRUE)));//convert to 24 hr
			$matchOpponentScore=$this->input->post('opponentScore',TRUE);

			if(isset($matchId) && strlen($matchId) && isset($matchEndTime) && strlen($matchEndTime) ){
				$opponentScoreDetails=array('match_opponents_score'=>$matchOpponentScore,'match_end_time'=>$matchEndTime);
				$result=$this->coachmodel->newHockeyOpponentScore($opponentScoreDetails,$matchId);
				if($result)
					{
						$_SESSION['msg']  = array('edit'=>'','error' => "",'success' => "Opponent score added");
		               redirect(base_url(('coach/tournmatches')));
					}else{
							$_SESSION['msg']  = array('edit'=>'','error' => "Opponent score not added",'success' => "");
			               redirect(base_url(('coach/tournmatches')));
					}
			}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
		
	}

	//get hockey match score
	public function hockeymatchscore()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$matchId=$this->input->post('pid',TRUE);
			$list =$this->coachmodel->getHockeyMatchScores($matchId);
			$data = array();
			$count=0;
			foreach ($list as $score) 
			    {
			    	$count=$count+1;
			        
			    }
			echo json_encode($count);//data should be a plain array...
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//get hockey match green cards
	public function hockeygreen()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$tournamentMatchId=$this->input->post('pid',TRUE);
			$list =$this->coachmodel->getMatchGreenCards($tournamentMatchId);
			$data = array();
			$count=0;
			foreach ($list as $card) 
			    {
			    	$count=$count+1;
			        
			    }
			echo json_encode($count);//data should be a plain array...
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//get hockey match yellow cards
	public function hockeyyellow()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$tournamentMatchId=$this->input->post('pid',TRUE);
			$list =$this->coachmodel->getMatchYellowCards($tournamentMatchId);
			$data = array();
			$count=0;
			foreach ($list as $card) 
			    {
			    	$count=$count+1;
			        
			    }
			echo json_encode($count);//data should be a plain array...
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//get hockey match red cards
	public function hockeyred()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$tournamentMatchId=$this->input->post('pid',TRUE);
			$list =$this->coachmodel->getMatchRedCards($tournamentMatchId);
			$data = array();
			$count=0;
			foreach ($list as $card) 
			    {
			    	$count=$count+1;
			        
			    }
			echo json_encode($count);//data should be a plain array...
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	
	public function newTraining()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {


	    	$data=$this->input->post('selected[]');
			$dateOfTraining = date("Y-m-d");//today's date
			$yearOfTraining = date("Y");//current year 
			$sportId=$_SESSION['sessdata']['coachSportID'];

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			if(is_array($data)) /*if is array received*/
			{
				if(empty(array_filter($data))) 
				{
				    $message['empty'][]=  "Nothing submitted";
			        echo json_encode($message);//return no changes made
				}else{

		                	$attendanceInfo=array();
		                	foreach($data as $record ) /*loop through and get individual items*/
			                    {

			            			 $attendanceInfo[]=array('training_date'=>$dateOfTraining,'training_year'=>$yearOfTraining,'player_auto_id'=>$record['playerId'],'attendance_state'=>$record['attendanceStatus'],'registering_user_id'=>$userAutoId,'registering_user_group_id'=>$userGroupId);
			                    }
			                
				                // insert selected attendance as batch in model
			                	$result=$this->coachmodel->trainingAttendance($attendanceInfo);
			                    if($result)
		                    		{
		                    			$message['success'][]=  "Attendance recorded successfully";
		                                echo json_encode($message);//return no changes made

		                            }else
		                                {
		                                    $message['error'][]=  "An error occurred";
		                                    echo json_encode($message);//return no changes made
				                        }


										
					}			
			}else{
				$message['empty'][]=  "Nothing submitted";
			    echo json_encode($message);//return no changes made
			}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//download player profile photo
	public function download_playerphoto($filename = NULL) 
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/players/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	 //download of player passport/travel_document image
	public function download_playerpspt($filename = NULL) 
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/travel_documents/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//download coach_report
	public function download_coach_report($filename = NULL) 
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/coach_uploads/coach_reports/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//download receipt
	public function download_coach_exp_receipt($filename = NULL) 
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/expenditures/coaches/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	//team reports uploads view page
	public function teamuploads()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$teamId=$this->input->get('team_id');
			if(isset($teamId) && strlen($teamId))
			{
				$list['team_uploads']=$this->coachmodel->teamUploads($teamId);
				$team = $this->db->query("SELECT team.team_name FROM teams team WHERE team.team_auto_id='$teamId' LIMIT 1")->row();
				
				$_SESSION['sessdata']  =array_merge($_SESSION['sessdata'], array('uploadTeamName'=>$team->team_name,'reportTeamId'=>$teamId)); 
				$this->load->view('coaches/team_uploads',$list);
			}else{
				
				$teamId=$_SESSION['sessdata']['reportTeamId'];
				$list['team_uploads']=$this->coachmodel->teamUploads($teamId);

				$this->load->view('coaches/team_uploads',$list);
			}
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}
	
	//team reports uploads view page
	public function generaluploads()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$sportId=$_SESSION['sessdata']['coachSportID'];
			$list['general_uploads']=$this->coachmodel->generalCoachUploads($sportId);
			$this->load->view('coaches/general_uploads',$list);
			
		}else{
                $_SESSION['msg']  = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
	}

	//uploads by admin meant to be viewed by everyone
	public function adminuploads()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$list['general_uploads']=$this->coachmodel->generalAdminUploads();
			$this->load->view('coaches/admin_uploads',$list);
			
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
	}

	public function newcoachreport()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$teamId=$_SESSION['sessdata']['reportTeamId'];
	    	$descriptiveName=$this->input->post('descriptiveName', TRUE);
			$contentDescription=$this->input->post('contentDescription', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			if(isset($descriptiveName) && strlen($descriptiveName) && isset($contentDescription) && strlen($contentDescription) && isset($teamId) && strlen($teamId))
			{
        		$config['upload_path'] = 'uploads/coach_uploads/coach_reports/';
                // $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx';
                $config['allowed_types']        = 'pdf|PDF|docx|doc|DOCX|DOC|xls|xlsx';
                // $config['max_size']             = 4096;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;
				$config['file_name']			=md5(time().$descriptiveName);
                $config['overwrite']           = true;
                $config['file_ext_tolower']     = true;

                $this->load->library('upload', $config);
                $file="report";

                    //confirm upload success
					if (!$this->upload->do_upload($file))
				    	{   
				    		$_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
				       		
							redirect(base_url(('coach/teamuploads')));

				    	}else{
				    			$data =$this->upload->data(); //load data to page
					    		$report_details =array('report_file_name'=>$config['file_name'].$data['file_ext'],'report_descriptive_name'=>$descriptiveName,'report_date_uploaded'=>date('Y-m-d H:i:s'),'report_team_id'=>$teamId,'file_ext'=>$data['file_ext'],'uploading_user_id'=>$userAutoId,'uploading_user_group_id'=>$userGroupId);

								$result=$this->coachmodel->newCoachReport($report_details);

								if($result)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "Report added successfully");
						           		redirect(base_url(('coach/teamuploads')));
									}else 
										{
											$_SESSION['msg'] = array('error' => "Report not added",'success' => "");
						               		redirect(base_url(('coach/teamuploads')));
										}
				    }
			}else{
					$_SESSION['msg'] = array('error' => "Some field is missing",'success' => "");
					
					redirect(base_url(('coach/teamuploads')));
				}
	            
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
    }
    //coach general upload
    public function newgeneralupload()
	{
		if($_SESSION['sessdata']['coach_login']==TRUE)
	    {
			$sportId=$_SESSION['sessdata']['coachSportID'];//from login session
	    	$descriptiveName=$this->input->post('descriptiveName', TRUE);
			$contentDescription=$this->input->post('contentDescription', TRUE);

			$userGroupId=$_SESSION['sessdata']['userGroupId'];
			$userAutoId=$_SESSION['sessdata']['userAutoId'];

			if(isset($descriptiveName) && strlen($descriptiveName) && isset($contentDescription) && strlen($contentDescription) )
			{
        		$config['upload_path'] = 'uploads/coach_uploads/coach_reports/general_uploads/';
                // $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx';
                $config['allowed_types']        = 'pdf|PDF|docx|doc|DOCX|DOC|xls|xlsx';
                // $config['max_size']             = 4096;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;
				$config['file_name']			=md5(time().$descriptiveName);
                $config['overwrite']           = true;
                $config['file_ext_tolower']     = true;

                $this->load->library('upload', $config);
                $file="report";

                    //confirm upload success
					if (!$this->upload->do_upload($file))
				    	{   
				    		$_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
							redirect(base_url(('coach/generaluploads')));

				    	}else{
				    			$data =$this->upload->data(); //load data to page
					    		$upload_details =array('upload_file_name'=>$config['file_name'].$data['file_ext'],'upload_descriptive_name'=>$descriptiveName,'upload_date_uploaded'=>date('Y-m-d H:i:s'),'file_ext'=>$data['file_ext'],'upload_sport_id'=>$sportId,'uploading_user_id'=>$userAutoId,'uploading_user_group_id'=>$userGroupId);

								$result=$this->coachmodel->newGeneralUpload($upload_details);

								if($result)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "Report added successfully");
						           		redirect(base_url(('coach/generaluploads')));
									}else 
										{
											$_SESSION['msg'] = array('error' => "Report not added",'success' => "");
						               		redirect(base_url(('coach/generaluploads')));
										}
				    }
			}else{
					$_SESSION['msg'] = array('error' => "Some field is missing",'success' => "");
					redirect(base_url(('coach/generaluploads')));
				}
	            
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('coach')));
        }
    }

	 
}
