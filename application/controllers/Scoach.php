<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Scoach extends CI_Controller
{

	function __construct()
	{
	     parent::__construct();
	     $this->load->model('ScoachModel','scoachmodel');
	}

	public function index()
	{
		$this->load->view('login');
	}
	public function logout()
    {
        $this->cas->logout();
    }
	//dashboard view page for admin
	public function dashboard()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
			$list['activeplayerscount']=$this->scoachmodel->activePlayersCount();//count all players active 
			$list['allinjuriestodatecount']=$this->scoachmodel->allInjuriesToDateCount();//count all injuries to date for active players 
			$list['allactiveplayersonphysio']=$this->scoachmodel->activePlayersOnPhysioCount();//count all active players who are on physio for active players 
			$list['allactiveplayersnotonphysio']=$this->scoachmodel->activePlayersNotOnPhysioCount();//count all active players who are not on physio for active players 

			$this->load->view('scoach/dashboard',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
		
	}
	

	//injuries
	public function injuries()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {

			$list['injuries']=$this->scoachmodel->allInjuries();//get all injuries
			$list['teams']=$this->scoachmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];
				$list['teams'][$id]['injuries']=$this->scoachmodel->injuryRecords($teamId);
			}
				$this->load->view('scoach/injury_records',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
	}
	//add injury
	public function addinjury()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
	    	$list['players']=$this->scoachmodel->activePlayersList();

			$this->load->view('scoach/newinjury',$list);

		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
    }

    public function newinjury()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
		{
			$playerId=$this->input->post('playerId', TRUE);
			$injuryDate=$this->input->post('injuryDate', TRUE);
			$injuryDescription=$this->input->post('injuryDescription', TRUE);

			$date_seen=$this->input->post('dateSeen', TRUE);
			//check if date seen empty
			if($date_seen=="")
			{
				$dateSeen=NULL;//not seen physio	
			}else{
				$dateSeen=$date_seen; //date seen by physio	
			}

			$diagnosis=$this->input->post('diagnosis', TRUE);
			$treatment=$this->input->post('treatment', TRUE);
			$physioRemarks=$this->input->post('physioRemarks', TRUE);

			$phyStatus=$this->input->post('physioStatus', TRUE);
			//check if physio status empty
			if($phyStatus=="")
			{
				$physioStatus=0;//not on physio	
			}else{
				$physioStatus=1; //on physio	
			}
			$start_date=$this->input->post('physioStartDate', TRUE);

			//check if start date empty
			if($start_date=="")
			{
				$physioStartDate=NULL;//not on physio
			}else{
				$physioStartDate=$start_date; //physio start date	
			}

			$end_date=$this->input->post('physioEndDate', TRUE);

			//check if end date empty
			if($end_date=="")
			{
				$physioEndDate=NULL;//not on physio
			}else{
				$physioEndDate=$end_date; //physio end date	
			}

			$scoachRemarks=$this->input->post('scoachRemarks', TRUE);

			$dateRecorded= date("Y-m-d H:i:s"); 

			$injury_details=array('player_auto_id'=>$playerId,'injury_date'=>$injuryDate,'injury_description'=>$injuryDescription,'date_seen'=>$dateSeen,'diagnosis'=>$diagnosis,'treatment'=>$treatment,'physio_remarks'=>$physioRemarks,'physio_status'=>$physioStatus,'physio_start_date'=>$physioStartDate,'physio_end_date'=>$physioEndDate,'scoach_remarks'=>$scoachRemarks,'date_recorded'=>$dateRecorded);
			$result=$this->scoachmodel->newInjuryRecord($injury_details);
			if($result)
			{
				$_SESSION['msg'] = array('error' => "",'success' => "Injury Record Added");
           		redirect(base_url(('scoach/injuries')));
			}else 
				{
					$_SESSION['msg'] = array('error' => "Injury not added",'success' => "");
               		redirect(base_url(('scoach/injuries')));
				}

		}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
	                redirect(base_url(('scoach')));
	    	}
	
	}

    //team reports uploads view page
	public function scuploads()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
			$list['uploads']=$this->scoachmodel->scuploads();

			$this->load->view('scoach/scoach_reports',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
	}
	//s&c new report upload
	public function newscreport()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
	    	$descriptiveName=$this->input->post('descriptiveName', TRUE);
			$contentDescription=$this->input->post('contentDescription', TRUE);

			if(isset($descriptiveName) && strlen($descriptiveName) && isset($contentDescription) && strlen($contentDescription))
			{
        		$config['upload_path'] = 'uploads/scoach_reports/';
                // $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx';
                $config['allowed_types']        = 'pdf|PDF|docx|doc|DOCX|DOC|xls|xlsx';
                // $config['max_size']             = 4096;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;
				$config['file_name']			=md5(time().$descriptiveName);
                $config['overwrite']           = true;
                $config['file_ext_tolower']     = true;

                $this->load->library('upload', $config);
                $file="report";

                    //confirm upload success
					if (!$this->upload->do_upload($file))
				    	{   
				    		$_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
							
							redirect(base_url(('scoach/scuploads')));

				    	}else{
				    			$data =$this->upload->data(); //load data to page
					    		$report_details =array('report_file_name'=>$config['file_name'].$data['file_ext'],'report_descriptive_name'=>$descriptiveName,'report_date_uploaded'=>date('Y-m-d H:i:s'),'file_ext'=>$data['file_ext']);

								$result=$this->scoachmodel->newSCoachReport($report_details);

								if($result)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "Report added successfully");
						           		redirect(base_url(('scoach/scuploads')));
									}else 
										{
											$_SESSION['msg'] = array('error' => "Report not added",'success' => "");
						               		redirect(base_url(('scoach/scuploads')));
										}
				    }
			}else{
					$_SESSION['msg'] = array('error' => "Some field is missing",'success' => "");

					redirect(base_url(('scoach/scuploads')));
			}
					

	            
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
    }


	

	//more about injury record 
	public function injurydetails()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
			$recordId=$this->input->post('recordId', TRUE);
			if(isset ($recordId) && strlen($recordId)){
				$record['injury_record']=$this->scoachmodel->injuryRecord($recordId);
				$this->load->view('scoach/injury_details',$record);
			}else{
				$_SESSION['msg'] = array('edit'=>'','error' => "Session was unset",'success' => "");
		       redirect(base_url(('scoach/injury_details')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
		
		
	}
	//get player for injury recording
	public function getplayer()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	$activePlayersNotOnPhysio=$this->scoachmodel->activePlayersNotOnPhysio();

			$keyword=$this->input->get("q");
			$json = [];
			if(!empty($this->input->get("q"))){
				
	             $this->db->where("(player_fname LIKE '%".$keyword."%' OR player_lname LIKE '%".$keyword."%' OR player_other_names LIKE '%".$keyword."%')", NULL, FALSE);
	             // $this->db->where('active_status !=', 0);
	              $this->db->where("`player_auto_id` NOT IN ($activePlayersNotOnPhysio)", NULL, FALSE);//select only players whose player ids appear on the $activePlayersNotOnPhysio above 
				$query = $this->db->select('player_auto_id as id,CONCAT(player_fname," ",player_lname) as text')
							->limit(10)
							->get("players");
				$json = $query->result();
			}
			echo json_encode($json);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('scoach')));
        }
		
	}

	//physio attendance view
	public function physioattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
			$list['teams']=$this->scoachmodel->activeTeamsList();
			foreach($list['teams'] as $id => $team)
			{
				$teamId=$team['team_auto_id'];

				$list['teams'][$id]['players'] =$this->scoachmodel->activePlayersForAttendance($teamId);

				$list['teams'][$id]['trainingattendance'] =$this->scoachmodel->playerAttendanceGroupedPerDate($teamId);

				$count=0;//create a counter. this will be used to create unique attendance status count arrays within attendance array in teams array as below. The $count and team_auto_id are used to achieve this unique naming

				foreach($list['teams'][$id]['trainingattendance'] as $id2 => $attendance)
				{
					$attendanceDate=$attendance['training_date'];//get the training date. To be used to get attendance per training date
					$countOfPlayersMarkedPresentAbsentOrExcused=$this->scoachmodel->countOfPlayersMarkedPresentAbsentOrExcused($attendanceDate,$teamId);//pass attendance date and teamid (from first loop above) to get the attendance per team per date

					$countOfPlayersPresent=$this->scoachmodel->countOfPlayersPresent($attendanceDate,$teamId);//counting no of players in each attendance status
					$countOfPlayersAbsent=$this->scoachmodel->countOfPlayersAbsent($attendanceDate,$teamId);
					$countOfPlayersExcused=$this->scoachmodel->countOfPlayersExcused($attendanceDate,$teamId);

					$countOfPlayersOnPhysio=$this->scoachmodel->countOfPlayersOnPhysio($attendanceDate,$teamId);
					//here is how we create unique arrays as above described
					$list['teams'][$id]['trainingattendance'][$id2]['countAll_'.$team['team_auto_id'].$count] =$countOfPlayersMarkedPresentAbsentOrExcused;
					$list['teams'][$id]['trainingattendance'][$id2]['countPresent_'.$team['team_auto_id'].$count] =$countOfPlayersPresent;
					$list['teams'][$id]['trainingattendance'][$id2]['countAbsent_'.$team['team_auto_id'].$count] =$countOfPlayersAbsent;
					$list['teams'][$id]['trainingattendance'][$id2]['countExcused_'.$team['team_auto_id'].$count] =$countOfPlayersExcused;
					$list['teams'][$id]['trainingattendance'][$id2]['countPhysio_'.$team['team_auto_id'].$count] =$countOfPlayersOnPhysio;
					$count=$count+1;
				}

			}
			$this->load->view('scoach/physio_attendance',$list);
			// print_r($list);
		}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
	                redirect(base_url(('scoach')));
	        }
	}
	




























	//new injury recording
	public function new_injury_record()
	{
		$player_auto_id=$this->input->post('playerid', TRUE);
		$injury_date=$this->input->post('dateOfInjury', TRUE);
		$date_seen=$this->input->post('dateInHospital', TRUE);
		$injury_description=$this->input->post('injuryDescription', TRUE);
		$date_recorded= date("Y-m-d H:i:s");

		$injury_details=array('player_auto_id'=>$player_auto_id,'injury_date'=>$injury_date,'date_seen'=>$date_seen,'injury_description'=>$injury_description,'date_recorded'=>$date_recorded);

		$result=$this->scoachmodel->new_injury_record($injury_details);
			if($result)
				{
					$_SESSION['msg'] = array('edit'=>'','error' => "",'success' => "New record added");
	               redirect(base_url(('mc/injury_record')));
				}else 
					{
						$_SESSION['msg'] = array('edit'=>'','error' => "Failed to add record",'success' => "");
	                   redirect(base_url(('mc/injury_record')));
					} 
	}
	//injury record updating
	public function update_ir()
	{
		$record_id=$this->input->post('recordId', TRUE);
		// $player_nid=$this->input->post('playerid', TRUE);
		$injury_date=$this->input->post('dateOfInjury', TRUE);
		$date_seen=$this->input->post('dateInHospital', TRUE);
		$injury_description=$this->input->post('injuryDescription', TRUE);

		$record_details=array('injury_date'=>$injury_date,'date_seen'=>$date_seen,'injury_description'=>$injury_description);

		$result=$this->scoachmodel->update_injury_record($record_details,$record_id);
			if($result)
				{
					$_SESSION['msg'] = array('edit'=>'','error' => "",'success' => "Injury record updated");
	               redirect(base_url(('mc/injury_record')));
				}else 
					{
						$_SESSION['msg'] = array('edit'=>'','error' => "No changes made",'success' => "");
	                   redirect(base_url(('mc/injury_record')));
					} 
	}


	
}
