<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


    function __construct() 
    {
        parent::__construct();
        $this->load->model("LoginModel", "login");
        // $this->load->library('session');
    }
    public function index()
    {
        $this->load->view('login');
    }
    public function cas_auth()
    {
        $this->load->library('cas');
        $this->cas->force_auth();//calling CAS for authentication
        $user = $this->cas->user();//calling the array with username from CAS and assignin it to the variable $user - @smokoro
        //lower case
        $username=strtolower($user->userlogin);

        $admin = $this->login->validate_admin($username);//admin authentication
        $coach = $this->login->validate_coach($username);//coach authentication
        $sc_coach = $this->login->validate_sc_coach($username);//strngth and conditioning coach authentication

        if($admin) 
            {
                 foreach ($admin as $row)
                    { 
                        $roleId=$row->admin_role_id;

                        if($roleId=='1')
                            {
                                $fullName=$row->admin_fname." ".$row->admin_lname;
                                $userID=$row->admin_auto_id;
                                $_SESSION['sessdata'] = array('adminName' =>$fullName,'userAutoId'=>$userID,'suser_login'=>TRUE,'userGroupId'=>'1');

                                $redirectLink="suser/dashboard";
                            }
                        else if($roleId=='2')
                            {
                                 $fullName=$row->admin_fname." ".$row->admin_lname;
                                $userID=$row->admin_auto_id;
                                $_SESSION['sessdata'] = array('adminName' =>$fullName,'adminId'=>$userID,'admin_login'=>TRUE,'userGroupId'=>'2');
                                $redirectLink="admin/dashboard";
                            }
                                
                    }
                    redirect($redirectLink);
            } else if($coach)
                {
                    foreach ($coach as $row)
                    { 
                        $fullName=$row->coach_fname." ".$row->coach_lname;
                        $userID=$row->coach_auto_id;
                        $coachSportID=$row->coach_sport_id;
                        $coachSportName=$row->sport_name;
                       $_SESSION['sessdata'] = array('coachName' =>$fullName,'coachId'=>$userID,'coach_login'=>TRUE,'coachSportID'=>$coachSportID,'coachSportName'=>$coachSportName); 
                    }
                    redirect('coach/dashboard');
                } else if($sc_coach)
                        {
                            foreach ($sc_coach as $row)
                            { 
                                $fullName=$row->sc_fname." ".$row->sc_lname;
                                $userID=$row->sc_staff_id;
                                $_SESSION['sessdata']= array('scoachName' =>$fullName,'scoachId'=>$userID,'scoach_login'=>TRUE); 
                                        
                            }
                            redirect('scoach/dashboard');
                        }else
                            {
                                $_SESSION['msg'] = array('error' => "Wrong credentials. Please try again",'success'=>'');
                                // $this->cas->logout();
                                redirect(base_url('auth'));

                            }
    }
}
