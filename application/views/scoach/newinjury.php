<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | New Injury</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
<link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<style>.select2 {
width:100%!important;
}</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('scoach/scoachnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> New Injury</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
              <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <?php echo form_open('scoach/newinjury',array('id' => 'newInjury','method'=>'post'));?>
                      <div class="row setup-content" >
                        <div class="col-md-12">
                            <div class="form-group col-md-12 col-lg-12">
                                <label for="playerId" class="control-label green">Player</label>
                                <select type="text" name="playerId" placeholder="" class="form-control" id="playerId" required="required"></select>
                            </div>

                           <div class='col-md-12 col-lg-12'>
                              <label for="injuryDate" class="control-label">Injury Date<span class="star">*</span></label>
                              <div class="form-group">
                                  <div class='input-group date' id='injuryDate'>
                                      <input type='text' class="form-control" name="injuryDate" required="required"  />
                                      <span class="input-group-addon">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                            </div> 
                            <div class="form-group col-md-12 col-lg-12">
                                <label for="injuryDescription" class="control-label">Description/Symptoms<span class="star">*</span></label>
                                <textarea type="text" name="injuryDescription" placeholder="[non expert description]" class="form-control" id="injuryDescription" required="required" maxlength="150" style="resize: none;min-height: 80px;"></textarea>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                              <label for="" class="control-label">Physio Examination</label><br>
                                  <input type="checkbox" name="examined" id="examined"  autocomplete="off" style="display: inline;">
                                  <label class="control-label" style="font-weight: normal">&nbsp;Check [✔] if physio examined</label><br><br>
                            </div>
                            <div class='col-md-12 col-lg-12' id="date_seen" style="display: none;">
                              <label for="dateSeen" class="control-label">Date Seen</label>
                              <div class="form-group">
                                  <div class='input-group date' id='dateSeen'>
                                      <input type='text' class="form-control" name="dateSeen"  />
                                      <span class="input-group-addon">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                            </div>                                 
                            
                            <div class="form-group col-md-12 col-lg-12" id="injury_diagnosis" style="display: none;">
                                <label for="diagnosis" class="control-label">Diagnosis</label>
                                <textarea type="text" name="diagnosis" placeholder="[Physio diagnosis]" class="form-control" id="diagnosis" maxlength="150" style="resize: none;min-height: 80px;"></textarea>
                            </div>
                            
                            <div class="form-group col-md-12 col-lg-12" id="treat_status" style="display: none;">
                                <label for="treatment" class="control-label">Treatment</label>
                                <textarea type="text" name="treatment" class="form-control" id="treatment" maxlength="150" style="resize: none;min-height: 80px;" placeholder="[Physio administered]"></textarea>
                            </div>
                            <div class="form-group col-md-12 col-lg-12" id="physio_remarks" style="display: none;">
                                <label for="physioRemarks" class="control-label">Physio Remarks</label>
                                <textarea type="text" name="physioRemarks" class="form-control" id="physioRemarks" maxlength="150" style="resize: none;min-height: 80px;" placeholder="[Physio remarks]"></textarea>
                            </div>
                            <div class="form-group col-md-6 col-lg-6" style="display: none;" id="physio_status">
                              <label for="" class="control-label">Physio Status</label><br>
                                  <input type="checkbox" name="physioStatus" id="physioStatus" value="1" autocomplete="off" style="display: inline;">
                                  <label class="control-label" style="font-weight: normal">&nbsp;Check [✔] if immediately on physiotherapy</label><br><br>
                            </div>
                            <div class='col-md-12 col-lg-12' id="phystartdate" style="display: none;">
                              <label for="physioStartDate" class="control-label">Physio Start Date</label>
                                <div class="form-group">
                                    <div class='input-group date' id='physioStartDate'>
                                        <input type='text' class="form-control" readonly="true" name="physioStartDate" value="" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class='col-md-12 col-lg-12' id="phyenddate" style="display: none;">
                              <label for="physioEndDate" class="control-label">Physio end Date</label>
                                <div class="form-group">
                                    <div class='input-group date' id='physioEndDate'>
                                        <input type='text' class="form-control" readonly="true" name="physioEndDate" value="" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group col-md-12 col-lg-12" id="phyremarks" style="display: none;">
                                <label for="physioRemarks" class="control-label" style="margin-top: 10px;">Physio Remarks <span class="star">*</span></label>
                                <textarea type="text" name="physioRemarks" placeholder="" class=" form-control" id="physioRemarks"  maxlength="150" style="resize: none;min-height: 80px;"></textarea>
                            </div>  -->
                            <div class="form-group col-md-12 col-lg-12">
                                <label for="scoachRemarks" class="control-label">S&C Coach Remarks</label>
                                <textarea type="text" name="scoachRemarks" class="form-control" id="scoachRemarks" maxlength="150" style="resize: none;min-height: 80px;" placeholder="S&C Coach remarks on the injury"></textarea>
                              </div>
                            
                            <div class="form-group col-md-12 col-lg-12">
                                <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="reset" class="btn btn-default" value="Reset">
                            </div>
                      </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
  $(document).ready(function () {
   $('#captainslist').dataTable({responsive:true,"iDisplayLength": 7,"lengthMenu": [[7, 25, 50, 100, 200, -1], [7, 25, 50, 100, 200, "All"]],
         "aoColumnDefs": [{"aTargets": [3], "orderable": false}],'aaSorting':[]
      });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
     $('#physioStatus').click(function () {
         var $this = $(this);
         if ($this.is(':checked')) {$('#phyremarks').show();$('#phystartdate').show(); $('#phyenddate').show();} else {$('#phyremarks').hide();$('#phystartdate').hide();$('#phyenddate').hide();}
     });
     $('#examined').click(function () {
         var $this = $(this);
         if ($this.is(':checked')) {$('#date_seen').show();$('#injury_diagnosis').show(); $('#treat_status').show();$('#physio_remarks').show();$('#physio_status').show();} else {$('#date_seen').hide();$('#injury_diagnosis').hide();$('#treat_status').hide();$('#physio_remarks').hide();$('#physio_status').hide();}
     });
    $('#playerId').select2({
      placeholder: '--- Select Player ---',
      ajax: {
        url: "<?php echo base_url('scoach/getplayer'); ?>",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results: data
          };
          // console.log(data);
        },
        cache: true
      }
    });
  });
    
    $(function () {$('#physioStartDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});
    $('#physioEndDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});
    $('#injuryDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});
    $('#dateSeen').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});});

</script>
</body>
</html>