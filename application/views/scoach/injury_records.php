<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Injuries</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
<script src="<?php echo base_url();?>assets/jquery/dist/jquery.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('scoach/scoachnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Team Injuries</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
               <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <div class="box box-solid collapsed-box" style="background:#5D6D7E;">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #FFFFFF;" > All Injuries</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse" style="color: #FFFFFF;"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="injurylist"  >
                          <thead>
                              <tr>        
                                  <th class="text-left">Full Name</th>
                                  <th class="text-left">Team</th>
                                  <th class="text-left">Injury Date</th>
                                  <th class="text-left">Description</th>
                                  <th class="text-center"></th>
                               </tr>
                          </thead>
                          <tbody >
                          <?php foreach($injuries as $record)
                              { ?>
                              <tr>
                                   
                                   <td class="text-left"><img src="<?php echo base_url();?>assets/img/person.png" width="25" height="25" class="img-circle" alt=""> <?php  echo $record['player_fname']. " ".$record['player_lname']; ?></td>

                                   <td class="text-left"><?php echo $record['team_name'];?></td>
                                   <td class="text-left"><?php echo date_format(date_create($record['injury_date']),"D j<\s\up>S</\s\up> M, Y");?></td>

                                  <td class="text-left"><?php echo $record['injury_description'];?></td>
                                  <td class="text-center">
                                    <form style="display:inline;" name=<?php echo '"formMore_'. $record['injury_auto_id'].'"'; ?> method="post" action="<?php echo base_url('scoach/injurydetails');?>">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="recordId" class="control-label">Injury ID<span class="star">*</span></label>
                                            <input required="required" class="form-control" name="recordId" id="recordId" placeholder="" value="<?php echo $record['injury_auto_id']; ?>">
                                        </div>
                                        <button class="btn btn-default btn-s" title="View More" id=<?php echo '"more_'. $record['injury_auto_id'].'"';  ?> name=<?php echo '"more_'. $record['injury_auto_id'].'"';  ?>  type="submit" style="/*background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                    </form>
                                     <form style="display:inline;" name=<?php echo '"formEdit_'. $record['injury_auto_id'].'"'; ?> method="post" action="#">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="recordId" class="control-label">Injury ID<span class="star">*</span></label>
                                            <input required="required" class="form-control" name="recordId" id="recordId" placeholder="" value="<?php echo $record['injury_auto_id']; ?>">
                                        </div>
                                        <button class="btn btn-primary btn-s" data-title="Edit Record" id=<?php echo '"edit_'. $record['injury_auto_id'].'"';  ?> name=<?php echo '"edit_'. $record['injury_auto_id'].'"';  ?>  type="submit" style="/*background-color: #C0C0C0;color: #FFFFFF;"> <span class="fa fa-edit"></span> Edit </button>
                                    </form>
                                  </td>
                              </tr>
                              <?php }?>
                          </tbody>
                      </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php foreach($teams as $team){ ?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" > <?php  echo $team['team_name']; ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <?php $tableId="injurylist_".$team['team_auto_id'];?>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                          <thead>
                              <tr>        
                                  <th class="text-left">Full Name</th>
                                  <th class="text-left">Injury Date</th>
                                  <th class="text-left">Description</th>
                                  <th class="text-center"></th>
                               </tr>
                          </thead>
                          <tbody >
                          <?php foreach($team['injuries'] as $record)
                              { ?>
                              <tr>
                                   
                                   <td class="text-left"><img src="<?php echo base_url();?>assets/img/person.png" width="25" height="25" class="img-circle" alt=""> <?php  echo $record['player_fname']. " ".$record['player_lname']; ?></td>

                                   <td class="text-left"><?php echo date_format(date_create($record['injury_date']),"D j<\s\up>S</\s\up> M, Y");?></td>

                                  <td class="text-left"><?php echo $record['injury_description'];?></td>
                                  <td class="text-center">
                                    <form style="display:inline;" name=<?php echo '"formMore_'. $record['injury_auto_id'].'"'; ?> method="post" action="<?php echo base_url('scoach/injurydetails');?>">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="recordId" class="control-label">Injury ID<span class="star">*</span></label>
                                            <input required="required" class="form-control" name="recordId" id="recordId" placeholder="" value="<?php echo $record['injury_auto_id']; ?>">
                                        </div>
                                        <button class="btn btn-default btn-s" data-title="View More" id=<?php echo '"more_'. $record['injury_auto_id'].'"';  ?> name=<?php echo '"more_'. $record['injury_auto_id'].'"';  ?>  type="submit" style="background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                    </form>
                                    <form style="display:inline;" name=<?php echo '"formEdit_'. $record['injury_auto_id'].'"'; ?> method="post" action="#">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="recordId" class="control-label">Injury ID<span class="star">*</span></label>
                                            <input required="required" class="form-control" name="recordId" id="recordId" placeholder="" value="<?php echo $record['injury_auto_id']; ?>">
                                        </div>
                                        <button class="btn btn-primary btn-s" data-title="Edit Record" id=<?php echo '"edit_'. $record['injury_auto_id'].'"';  ?> name=<?php echo '"edit_'. $record['injury_auto_id'].'"';  ?>  type="submit" style="/*background-color: #C0C0C0;color: #FFFFFF;"> <span class="fa fa-edit"></span> Edit </button>
                                    </form>
                                  </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                      </table>
                        <!-- /.table-responsive -->
                         <?php  echo  "<script>
                        $(document).ready(function () { 
                             //datatable initialization
                            $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
                                 'aoColumnDefs': [{'aTargets': [3], 'orderable': false}] }); 
                        });//close document.ready

                        </script>";?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php }?>
                <div class="modal fade" id="delinjury">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <form method="post" action="<?php echo base_url(); ?>scoach/delinjury">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="playerName"></h4>
                          </div>
                          <div class="modal-body" id="delbody">
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger  pull-left">Confirm Delete</button>
                          </div>
                    </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () { 
     //datatable initialization
    $('#injurylist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
         'aoColumnDefs': [{'aTargets': [4], 'orderable': false}] }); 
});//close document.ready
 function delrecord(objButton)
  {
      var recordId=objButton.value;
       $.ajax({type:"post", url: "<?php echo base_url(); ?>suser/getinjuredplayer",data:{ recordId:recordId},dataType:'json',success:function(data){$('#delinjury #playerName').text(data.fullName);$('#delinjury #delbody').html(data.info);$('#delinjury').modal('toggle');} });
  }
</script>
</body>
</html>
