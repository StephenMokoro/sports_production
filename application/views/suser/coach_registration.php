<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Coaches</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row " style="margin-bottom: -15px;">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Coaches</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-s" data-title="Print All" type="button" href=""><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Coach</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                      <div class="modal-body">
                          <div class="stepwizard">
                              <div class="stepwizard-row setup-panel">
                                  <div class="stepwizard-step">
                                      <a href="#step-1" type="button" class="btn btn-primary btn-circle"><span style="color: #00FF00;">1</span></a>
                                      <p class="active-purple-bold p">Personal Details</p>
                                  </div>
                                  <div class="stepwizard-step">
                                      <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"><span style="color: #00FF00;">2</span></a>
                                      <p class="inactive-purple-bold p">Other Details</p>
                                  </div>
                              </div>
                          </div>
                          <?php echo form_open_multipart('suser/newcoach',array('id' => 'player_registration','method'=>'post'));?>
                              <div class="row setup-content" id="step-1">
                                  <div class="col-sm-12">
                                      <div class="col-md-6">
                                          <!-- <h3> Step 1</h3> -->
                                        <div class="col-md-9 col-md-offset-3">
                                          <div class="form-group">
                                              <div class="main-img-preview">
                                                <img class="thumbnail img-preview" src="<?php echo base_url();?>assets/img/person.png" title="Coach Photo" width="210" height="230">
                                              </div>
                                              <!-- <p class="help-block">* Upload suser passport photo.</p> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-4">
                                          <div class="input-group">
                                            <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" style="display: none; ">
                                            <div class="input-group-btn">
                                              <div class="fileUpload btn btn-default fake-shadow">
                                               <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="firstName" class="control-label" style="margin-top: 10px;">First Name <span class="star">*</span></label>
                                          <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                          <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="otherNames" class="control-label">Other Names</label>
                                          <input type="text" name="otherNames" placeholder="" class=" form-control" id="otherNames" maxlength="30">
                                        </div>
                                      </div><!--/.col-md-6-->
                                      <div class="col-md-6">
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label class="">Gender <span class="star">*</span></label><br>
                                          <label class="radio-inline ">
                                              <input type="radio" name="gender" id="gender" value="Male" required="required" autocomplete="off">Male
                                          </label>
                                          <label class="radio-inline ">
                                              <input type="radio" name="gender" id="gender" value="Female" required autocomplete="off">Female
                                          </label>
                                        </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="staffID" class="control-label">Staff ID <span class="star">*</span></label>
                                              <input type="text" name="staffID" placeholder="" class=" form-control" id="staffID" required="required">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="userName" class="control-label">SU Username <span class="star">*</span></label>
                                              <input type="text" name="userName" placeholder="e.g. smokoro" class=" form-control" id="userName" required="required" maxlength="15" minlength="2">
                                          </div>
                                      
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="nationalID" class="control-label">ID/Passport/Birth Cert No<span class="star">*</span></label>
                                              <input type="text" name="nationalID" placeholder="" class=" form-control" id="nationalID" required="required"  maxlength="12" minlength="4">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="idType" class="control-label">Type of ID Used <span class="star">*</span></label>
                                              <select type="text" name="idType" class=" form-control" id="idType" required="required">
                                                <option value="">--Select Type--</option>
                                                <option value="National ID">National ID</option>
                                                <option value="Passport">Passport</option>
                                                <option value="Birth Certificate">Birth Certificate</option>
                                              </select>
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="phoneNumber" class="control-label"> Current Phone No.<span class="star">*</span></label>
                                              <input type="text" name="phoneNumber" placeholder="" class=" form-control" id="phoneNumber" required="required" data-mask="0799999999">
                                          </div>

                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="emailAddress" class="control-label"> Current Email Address<span class="star">*</span></label>
                                              <input type="email" name="emailAddress" placeholder="" class=" form-control" id="emailAddress" required="required" maxlength="50">
                                          </div>
                                      </div><!--/.col-md-6-->
                                      <div class="col-md-12">
                                        <div class="form-group col-md-12 col-lg-12">
                                            <input class="btn btn-primary nextBtn pull-right" type="button" value="Next">
                                          </div>
                                      </div><!--/.col-md-6-->
                                  </div>
                              </div>
                              <div class="row setup-content" id="step-2">
                                  <div class="col-s-12">
                                      <div class="col-md-12">
                                          <div class="form-group col-md-6 col-lg-6">
                                              <label for="prevStatus" class="control-label" >Have you ever been a team Coach?<span class="star">*</span></label><br>
                                              <label class="radio-inline ">
                                                  <input type="radio" name="prevStatus" id="prevStatus" value="1" required="required" autocomplete="off">Yes
                                              </label>
                                              <label class="radio-inline ">
                                                  <input type="radio" name="prevStatus" id="prevStatus" value="0" required="required" autocomplete="off">No
                                              </label>
                                          </div>
                                          <div class="form-group col-md-6 col-lg-6" >
                                              <label for="previousTeam" class="control-label">Previous Team</label>
                                              <input type="text" name="previousTeam" placeholder="ABC FC" class=" form-control" id="previousTeam" maxlength="50">
                                          </div>
                                          <div class="form-group col-md-6 col-lg-6">
                                              <label for="sportId" class="control-label">Sport Joining <span class="star">*</span></label>
                                              <select type="text" name="sportId" class=" form-control" id="sportId" required="required">
                                                <option value="">--Select Sport--</option>
                                                  <?php  foreach($sports as $sport){ ?>
                                                    <option value=<?php  echo '"'.$sport['sport_auto_id'].'"';?>><?php  echo $sport['sport_name'];?></option>
                                                  <?php } ?>
                                              </select>
                                          </div>
                                          <div class="form-group col-md-6 col-lg-6">
                                              <label for="highestAchievement" class="control-label">Highest Sports Achievement <span class="star">*</span></label>
                                              <input type="text" name="highestAchievement" placeholder="Voted Coach of the Year 2016" class=" form-control" id="highestAchievement" required="required" maxlength="70">
                                          </div>
                                          <div class="form-group col-md-6 col-lg-6">
                                              <label for="currentResidence" class="control-label"> Current Residence <span class="star">*</span></label>
                                              <input type="text" name="currentResidence" placeholder=" Madaraka Estate, Tulia Court, HSE 1137" class=" form-control" id="currentResidence" required="required" maxlength="50" minlength="4" >
                                          </div>
                                            
                                           <div class="form-group col-md-6 col-lg-6">
                                            <input class="btn btn-success nextBtn  pull-right" type="submit" value="Submit">
                                            <input class="btn btn-primary prevBtn  pull-left" type="button" value="Back">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          <?php echo form_close();?>
                        </div>
                     <!--/.modal-body-->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="coacheslist"  >
                    <thead>
                        <tr>       
                            <th class="text-left">Full Name</th>
                            <th class="text-left">ID</th>
                            <th class="text-left">Phone Number</th>
                            <th class="text-left">Sport/Role</th>
                            <th class="text-center"></th>
                         </tr>
                    </thead>
                    <tbody >
                        <?php  
                        foreach($coaches as $coach){ 
                           ?>
                        <tr>
                            <td class="text-left"><?php  echo $coach['coach_fname']. " ".$coach['coach_lname']; ?></td>
                            <td class="text-left"><?php  echo $coach['coach_nid'];  ?></td>
                            <td class="text-left"><?php  echo $coach['coach_phone']; ?></td>
                            <td class="text-left"><?php  echo $coach['sport_name']; ?></td>
                            <td class="text-center">
                                <form style="display:inline;" name=<?php echo '"formMore_'. $coach['coach_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/coachprofile');?>">
                                  <div class="form-group col-md-12 col-lg-12" style="display:none">
                                      <label for="coachId" class="control-label">Coach Id*</label>
                                      <input required="required" class="form-control" name="coachId" id="coachId" placeholder="" value="<?php echo $coach['coach_auto_id']; ?>">
                                  </div>
                                  <button class="btn btn-default btn-s" data-title="More" id=<?php echo '"more_'. $coach['coach_auto_id'].'"';  ?> name=<?php echo '"more_'. $coach['coach_auto_id'].'"';  ?>  type="submit"><i class="fa fa-eye"></i> More</button>
                                </form>
                                <form style="display:inline;" name=<?php echo '"formEdit_'. $coach['coach_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/editcoach');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="coachId" class="control-label">Coach Id*</label>
                                        <input required="required" class="form-control" name="coachId" id="coachId" placeholder="" value="<?php echo $coach['coach_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-primary btn-s" data-title="Edit" id=<?php echo '"edit_'. $coach['coach_auto_id'].'"';  ?> name=<?php echo '"edit_'. $coach['coach_auto_id'].'"';  ?>  type="submit" style="/*background-color: #7B7D7D;color: #FFFFFF;"><i class="fa fa-edit"></i> Edit</button>
                                </form>
                                <form style="display:inline;" name=<?php echo '"formAssignTeam_'. $coach['coach_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/assignteam');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="coachId" class="control-label">Coach Id*</label>
                                        <input required="required" class="form-control" name="coachId" id="coachId" placeholder="" value="<?php echo $coach['coach_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-danger btn-s" data-title="Disable Coach" id=<?php echo '"disable_'. $coach['coach_auto_id'].'"';  ?> name=<?php echo '"disable_'. $coach['coach_auto_id'].'"';  ?>  type="submit" style="/*background-color: #7B7D7D;color: #FFFFFF;"><i class="fa fa-ban"></i> Disable</button>
                                </form>
                                
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    //datatable initialization
     $('#coacheslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
         "aoColumnDefs": [{"aTargets": [4], "orderable": false}],'aaSorting':[]
      });
    var  submitBtn = $('input[type="submit"]');
     var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allPrevBtn = $('.prevBtn'),
            paragraph=$('div.setup-panel div p'),
            allNextBtn = $('.nextBtn');
    allWells.hide();
    allPrevBtn.hide();
    navListItems.click(function (e) {
        e.preventDefault(e);
        var $target = $($(this).attr('href')),
                $item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            $item.parent().prev().children("p").addClass('inactive-purple-bold');
            $item.parent().children("p").removeClass('inactive-purple-bold').addClass('active-purple-bold');
            $item.parent().next().children("p").removeClass('active-purple-bold').addClass('inactive-purple-bold');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            nextP = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("p"),
            curInputs = curStep.find("input,select"),
            isValid = true;
            allPrevBtn.show();

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid){
            nextStepWizard.removeAttr('disabled').trigger('click');
             nextP.removeClass('inactive-purple-bold').addClass('active-purple-bold');
        }
       
    });
  allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
        prevParagraph = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("p");
        curParagraph = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().children("p");
        prevParagraph.removeClass('inactive-purple-bold').addClass('active-purple-bold');
        curParagraph.removeClass('active-purple-bold').addClass('inactive-purple-bold');
        prevStepWizard.removeAttr('disabled').trigger('click');

    });
    $('div.setup-panel div a.btn-primary').trigger('click');
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
