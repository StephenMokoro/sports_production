<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Coach Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row ">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Coach Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
            <div class="box-body"  >
             <?php foreach($coach_profile as $profile){ 
                $photo=$profile['coach_profile_photo']; if($photo==""){$ppic="person.png";}else{$ppic=$profile['coach_profile_photo'];}?>
                  <div class="col-md-3" style="text-align: center;margin-right: auto
                  <a href="<?php echo base_url();?>coach/download_coachphoto/<?php echo $ppic;?>">
                     <div class="col-md-12" style="display: inline-block;text-align: center">
                              <img img style="display : block;
                              margin : auto;" src="<?php echo base_url();echo 'uploads/profile_photos/coaches/'.$ppic?>" alt="" class="img-rounded img-responsive" width="150" />
                              <b><p style="color: #000000;"><?php echo $profile['coach_fname']." ".$profile['coach_lname'];?></p></b>
                          </div>
                      </div></a>
                        <div class="col-md-6" style="text-align: left">
                            <blockquote >
                                 <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['coach_phone'];?>  <span><cite title="<?php echo $profile['coach_fname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>
                                  <p><i class="fa fa-id-card-o text-warning fa-1x"></i> <?php echo $profile['coach_staff_id'];?>  <span><cite title="Staff ID"><small style="display: inline">Staff ID </small></cite></span> </p>
                                <p><i class="fa fa-envelope text-primary fa-1x"></i> <?php echo $profile['coach_email'];?>  <span><cite title="<?php echo $profile['coach_fname'];?>'s email address"><small style="display: inline">Email   </small></cite></span> </p>
                                 <p><i class="fa fa-id-card-o text-warning fa-1x"></i> <?php echo $profile['coach_nid'];?>  <span><cite title=" nID"><small style="display: inline">ID</small></cite></span> </p>
                                  <p><i class="fa fa-home text-primary fa-1x"></i> <?php echo $profile['coach_residence'];?>  <span><cite title="<?php echo $profile['coach_fname'];?>'s residence address"><small style="display: inline">Residence   </small></cite></span> </p>
                            </blockquote>
                        </div>
                        <div class="col-md-12" style="text-align: left">
                          <p><i class="fa fa-info-circle  text-info fa-1x"></i> <?php echo $profile['coach_fname']. " ".$profile['coach_lname'] ;?> coaches...</p>
                        </div>
                    <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>

