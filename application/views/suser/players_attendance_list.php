<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Training Attendance</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Training Attendance</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >

                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
               <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="allattendancelist">
                     <thead>
                          <tr>
                              <th class="text-left">Full Name</th>
                              <th class="text-left">Status</th>
                              <th class="text-center"><i class="fa fa-cog"></i></th>
                           </tr>
                      </thead>
                      <tbody >
                         <?php foreach($traininglist as $attendance){ 
                             ?>
                          <tr>
                              <?php $photo=$attendance['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$attendance['player_profile_photo'];}?>
                            <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt=""> <?php  echo $attendance['player_fname']. " ".$attendance['player_lname']; ?></td>
                              <td class="text-left"><?php  echo $attendance['attendance_state']; ?></td>
                              <td class="text-center">
                                  <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'. $attendance[ 'ta_auto_id']. '"'; ?> name=<?php echo '"edit_'. $attendance['ta_auto_id'].'"';  ?> value=<?php echo '"'. $attendance['ta_auto_id'].'"';  ?> onclick="editattendance(this);"><i class="fa fa-edit"> Edit</i> </button>

                                   <button class="btn btn-danger btn-s" data-placement="top" data-toggle="tooltip" title="Delete Record" id=<?php echo '"del_'. $attendance[ 'ta_auto_id']. '"'; ?> name=<?php echo '"del_'. $attendance['ta_auto_id'].'"';  ?> value=<?php echo '"'. $attendance['ta_auto_id'].'"';  ?> onclick="delattendance(this);"><i class="fa fa-trash"> Del</i> </button>
                              </td>
                          </tr>
                          <?php } ?>
                      </tbody>
                </table>
                <!-- /.table-responsive -->
                <div class="modal fade" id="updateAttendance">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <form method="post" action="<?php echo base_url(); ?>suser/updateplayerattendance">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="playerName"></h4>
                          </div>
                          <div class="modal-body" id="updatebody">
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success  pull-left">Save changes</button>
                          </div>
                    </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="delAttendance">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <form method="post" action="<?php echo base_url(); ?>suser/delplayerattendance">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="playerName"></h4>
                          </div>
                          <div class="modal-body" id="delbody">
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger  pull-left">Confirm Delete</button>
                          </div>
                    </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
  $(document).ready(function () {$('#allattendancelist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],'aoColumnDefs': [{'aTargets': [2], 'orderable': false}] });
  });//close document.ready
  function editattendance(objButton)
  {
      var recordId=objButton.value;
      var action="edit";
      $.ajax({type:"post",url: "<?php echo base_url(); ?>suser/getplayerattendance",
          data:{ recordId:recordId,action:action},dataType:'json',success:function(data){
                  $('#updateAttendance #playerName').text(data.fullName);
                  $('#updateAttendance #updatebody').html(data.status);
                  $('#updateAttendance').modal('toggle');
              }
      });
  }
  function delattendance(objButton)
  {
      var recordId=objButton.value;
       var action="del";
       $.ajax({type:"post", url: "<?php echo base_url(); ?>suser/getplayerattendance",data:{ recordId:recordId,action:action},dataType:'json',success:function(data){$('#delAttendance #playerName').text(data.fullName);$('#delAttendance #delbody').html(data.status);$('#delAttendance').modal('toggle');} });
  }

 
</script>
</body>
</html>
