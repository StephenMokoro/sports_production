<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Student Players</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Student Players</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <div class="box box-solid collapsed-box" style="background:#5D6D7E;">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #FFFFFF;" > All Student Players</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse" style="color: #FFFFFF;"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                       <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="allplayerslist">
                             <thead>
                                  <tr>
                                      <th class="text-left">Full Name</th>
                                      <th class="text-left">ID / Passport</th>
                                      <th class="text-left">Phone Number</th>
                                      <th class="text-center"><i class="fa fa-cog"></i></th>
                                   </tr>
                              </thead>
                              <tbody >
                                 <?php foreach($studentplayers as $player){ 
                                     ?>
                                  <tr>
                                      <?php $photo=$player['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$player['player_profile_photo'];}?>
                                    <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt=""> <?php  echo $player['player_fname']. " ".$player['player_lname']; ?></td>
                                      <td class="text-left"><?php  echo $player['player_nid'];  ?></td>
                                      <td class="text-left"><?php  echo $player['player_phone']; ?></td>
                                      <td class="text-center">
                                          <form style="display:inline;" name=<?php echo '"formMore_'. $player['player_auto_id'].'"'; ?> method="post" action="<?php echo base_url('suser/playerprofile');?>">
                                              <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                  <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                                  <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                              </div>
                                              <button class="btn btn-default btn-s" title="View More" id=<?php echo '"more_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"more_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                          </form>
                                          <form style="display:inline;" name=<?php echo '"formEdit_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/editplayer');?>">
                                              <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                  <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                                  <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                              </div>
                                              <button class="btn btn-primary btn-s" title="Edit Player" id=<?php echo '"edit_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"edit_'. $player['player_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;"><span class="fa fa-edit"></span> Edit </button>
                                          </form>
                                          <form style="display:inline;" name=<?php echo '"formNxt_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/nextofkin');?>">
                                              <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                  <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                                  <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                              </div>
                                              <button class="btn btn-success btn-s" title="View Next of Kin" id=<?php echo '"kinMore_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"kinMore_'. $player['player_auto_id'].'"';  ?>  type="submit" style="/*background-color:#808080;color:#FFFFFF;"> <span class="fa fa-eye"></span> Kin </button>
                                          </form>
                                         <span  title="Disable Player"><button class="btn btn-default btn-s" id="disable2_<?php  echo $player['player_auto_id'];?>" name="disable2_<?php  echo $player['player_auto_id'];?>" type="submit" style="background-color:#7B241C;color: #FFFFFF;" value="<?php  echo $player['player_auto_id'];?>" onclick="disable(this);"><span class ="fa fa-ban"></span> Disable</button></span>
                                      </td>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php foreach($teams as $team){ ?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" > <?php  echo $team['team_name']; ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <?php $tableId="playerslist_".$team['team_auto_id'];?>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                             <thead>
                                  <tr>
                                      <th class="text-left">Full Name</th>
                                      <th class="text-left">ID / Passport</th>
                                      <th class="text-left">Phone Number</th>
                                      <th class="text-center"><i class="fa fa-cog"></i></th>
                                   </tr>
                              </thead>
                              <tbody >
                                 <?php foreach($team['players'] as $player){ 
                                     ?>
                                  <tr>
                                      <?php $photo=$player['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$player['player_profile_photo'];}?>
                                    <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt=""> <?php  echo $player['player_fname']. " ".$player['player_lname']; ?></td>
                                      <td class="text-left"><?php  echo $player['player_nid'];  ?></td>
                                      <td class="text-left"><?php  echo $player['player_phone']; ?></td>
                                      <td class="text-center">
                                          <form style="display:inline;" name=<?php echo '"formMore_'. $player['player_auto_id'].'"'; ?> method="post" action="<?php echo base_url('suser/playerprofile');?>">
                                              <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                  <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                                  <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                              </div>
                                              <button class="btn btn-default btn-s" title="View More" id=<?php echo '"more_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"more_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                          </form>
                                          <form style="display:inline;" name=<?php echo '"formEdit_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/editplayer');?>">
                                              <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                  <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                                  <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                              </div>
                                              <button class="btn btn-primary btn-s" title="Edit Player" id=<?php echo '"edit_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"edit_'. $player['player_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;"><span class="fa fa-edit"></span> Edit </button>
                                          </form>
                                          <form style="display:inline;" name=<?php echo '"formNxt_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/nextofkin');?>">
                                              <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                  <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                                  <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                              </div>
                                              <button class="btn btn-success btn-s" title="View Next of Kin" id=<?php echo '"kinMore_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"kinMore_'. $player['player_auto_id'].'"';  ?>  type="submit" style="/*background-color:#808080;color:#FFFFFF;"> <span class="fa fa-eye"></span> Kin </button>
                                          </form>
                                         <span  title="Disable Player"><button class="btn btn-default btn-s" id="disable2_<?php  echo $player['player_auto_id'];?>" name="disable2_<?php  echo $player['player_auto_id'];?>" type="submit" style="background-color:#7B241C;color: #FFFFFF;" value="<?php  echo $player['player_auto_id'];?>" onclick="disable(this);"><span class ="fa fa-ban"></span> Disable</button></span>
                                      </td>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <?php  echo  "<script>
                        $(document).ready(function () { 
                             //datatable initialization
                            $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
                                 'aoColumnDefs': [{'aTargets': [3], 'orderable': false}] }); 
                        });//close document.ready

                        </script>";?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php }?>
              <div class="modal fade" id="disablePlayer">
                  <div class="modal-dialog">
                    <form method="post" action="<?php echo base_url(); ?>suser/disableplayer">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="playerName"></h4>
                        </div>
                        <div class="modal-body" id="disbody">
                          <div class="row">
                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                              <label for="playerId" class="control-label">Player Id <span class="star">*</span></label>
                              <input type="text" name="playerId" placeholder="" class=" form-control" id="playerId" required="required" maxlength="20">
                            </div>
                          <div class="form-group col-md-12 col-lg-12">
                                <label for="reasonInactive" class="control-label">Reason for Deactivating</label>
                                <textarea type="text" name="reasonInactive" class="form-control" id="reasonInactive" maxlength="150" style="resize: none;min-height: 80px;" placeholder="" required="required"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-success pull-right" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-danger pull-left">Confirm & Disable</button>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </form>
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
  $(document).ready(function () { 
       //datatable initialization
      $('#allplayerslist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
           'aoColumnDefs': [{'aTargets': [3], 'orderable': false}] }); 
  });//close document.ready
function disable(objButton)
    { var playerId=objButton.value;
      $.ajax({type:"post",url: "<?php echo base_url(); ?>suser/getplayer",
      data:{ playerId:playerId},dataType:'json',success:function(data){
              $('#disablePlayer #playerName').text(data.fullName);
              $('#disablePlayer #playerId').val(data.playerId);
              $('#disablePlayer').modal('toggle');
          }
        });
    }
</script>
</body>
</html>
