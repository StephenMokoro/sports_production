<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Expenditure Details</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Expenditure Details</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
               <?php foreach($expenses as $exp){ ?>
                    <div class="col-md-12 text-left">
                        <div class="col-md-12">
                            <p style="color: " class="fa-2x"><strong>Team: </strong><?php echo $exp['team_name'];?></p> 
                            <small><b><span class="fa fa-calendar text-primary fa-2x"></span></b> <span style="color: black;font-size: 17px;"> <strong> &nbsp;&nbsp;Expense Date:</strong> <?php echo date_format(date_create($exp['expense_date']),"D j<\s\up>S</\s\up> M, Y");?></span> </small>
                            <br><br>
                        </div>
                        <div class="col-md-12" style="text-align: left">
                            <blockquote >
                                 <p><span class="text-primary fa-1x">Kshs. </span> <?php echo number_format($exp['expense_cash'],2);?>  <span><cite title="<?php echo $exp['expense_cash'];?> Kshs."><small style="display: inline">Cash  </small></cite></span> </p>
                                 <p><span class="text-primary fa-1x">Kshs. </span> <?php echo number_format($exp['expense_lpo_amount'],2);;?>  <span><cite title="LPO: <?php echo $exp['expense_lpo_amount'];?>' Kshs."><small style="display: inline">LPO Amount (<span style="color: black;font-weight: bold">LPO No: <?php echo $exp['expense_lpo_no'];?></span>)  </small></cite></span> </p>
                                 <p><span class="text-primary fa-1x">Kshs. </span> <?php echo number_format($exp['expense_lunches'],2);?>  <span><cite title="<?php echo $exp['expense_lunches'];?> Kshs."><small style="display: inline">Lunches  </small></cite></span> </p>
                            </blockquote>
                            <hr>
                        </div>
                        <div class="col-md-12" style="text-align: left">
                            <blockquote >
                                 <p><span class="fa fa-comment-o text-warning "></span> <?php echo $exp['expense_comment'];?></p>
                            </blockquote>
                        </div>
                    </div>
                <?php }?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>
