<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Super Users</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
 <style>
    @media (max-width:767px){.select2 {width: 100% !important;}}
    @media (min-width:768px){.select2 {width: 100% !important;}}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row " style="margin-bottom: -15px;">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Super Users</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-s" data-title="Print All" type="button" href=""><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Super User</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-s" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                      <?php echo form_open_multipart('suser/newsuser',array('id' => 'suser_registration','method'=>'post'));?>
                              <div class="row setup-content">
                                  <div class="col-sm-12">
                                      <div class="col-md-6">
                                          <div class="col-md-11 col-md-offset-1">
                                          <div class="form-group">
                                              <div class="main-img-preview">
                                                <img class="thumbnail img-preview" src="<?php echo base_url();?>assets/img/person.png" title="Suser Photo" width="210" height="230">
                                              </div>
                                              <!-- <p class="help-block">* Upload admin passport photo.</p> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-3">
                                          <div class="input-group">
                                            <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" style="display: none; ">
                                            <div class="input-group-btn">
                                              <div class="fileUpload btn btn-default fake-shadow">
                                               <span><i class="fa fa-upload"></i>upload photo</span>
                                                <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="firstName" class="control-label" style="margin-top: 10px;">First Name <span class="star">*</span></label>
                                          <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                          <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20">
                                        </div>
                                        </div><!--/.col-md-6-->
                                        <div class="col-md-6">
                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="staffId" class="control-label">Staff ID*</label>
                                              <input type="text" name="staffId" placeholder="" class=" form-control" id="staffId" required="required">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="userName" class="control-label">SU Username*</label>
                                              <input type="text" name="userName" placeholder="smokoro" class=" form-control" id="userName" required="required" maxlength="15" minlength="4">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="nationalId" class="control-label">ID/Passport/Birth Cert No<span class="star">*</span></label>
                                              <input type="text" name="nationalId" class=" form-control" id="nationalId" required="required"  maxlength="12" minlength="4">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="idType" class="control-label">Type of ID Used <span class="star">*</span></label>
                                              <select type="text" name="idType" class=" form-control" id="idType" required="required">
                                                <option value="">--Select Type--</option>
                                                <option value="National ID">National ID</option>
                                                <option value="Passport">Passport</option>
                                                <option value="Birth Certificate">Birth Certificate</option>
                                              </select>
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="phoneNumber" class="control-label"> Current Phone No.<span class="star">*</span></label>
                                              <input type="text" name="phoneNumber" placeholder="" class=" form-control" id="phoneNumber" required="required" data-mask="0799999999">
                                          </div>

                                          <div class="form-group col-md-12 col-lg-12">
                                              <label for="email" class="control-label"> Current Email Address<span class="star">*</span></label>
                                              <input type="email" name="email" placeholder="" class=" form-control" id="email" required="required" maxlength="50">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                              <input class="btn btn-success nextBtn  pull-right" type="submit" value="Submit">
                                          </div>
                                      </div><!--/.col-md-6-->
                                  </div>
                              </div>
                              <?php echo form_close();?>

                            </div>
                        </div>
                          <?php if(isset($_SESSION['msg']))
                          {
                            $msg = $_SESSION['msg'];
                            $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                            <div class="messagebox alert alert-danger" style="display: block">
                              <button type="button" class="close" data-dismiss="alert">*</button>
                              <div class="cs-text">
                                  <i class="fa fa-close"></i>
                                  <strong><span>';echo $msg['error']; echo '</span></strong>
                              </div> 
                            </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                            <div class="messagebox alert alert-success" style="display: block">
                              <button type="button" class="close" data-dismiss="alert">*</button>
                              <div class="cs-text">
                                  <i class="fa fa-check-circle-o"></i>
                                  <strong><span>';echo $msg['success'];echo '</span></strong>
                              </div> 
                              </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="suserslist"  >
                    <thead>
                        <tr>       
                            <th class="text-left">Full Name</th>
                            <th class="text-left">ID</th>
                            <th class="text-left">Staff ID</th>
                            <th class="text-left">Phone number</th>
                            <th class="text-left"></th>
        
                         </tr>
                    </thead>
                    <tbody >
                        <?php  
                        foreach($susers as $user){ 
                           ?>
                        <tr>
                            <td class="text-left"><?php  echo $user['super_fname']. " ".$user['super_lname']; ?></td>
                            <td class="text-left"><?php  echo $user['super_nid'];  ?></td>
                            <td class="text-left"><?php  echo $user['super_staff_id'];  ?></td>
                            <td class="text-left"><?php  echo $user['super_phone'];  ?></td>
                            <td class="text-center">
                              <form style="display:inline;" name=<?php echo '"formMore_'. $user['super_staff_id'].'"';  ?> method="post" action="<?php echo base_url('suser/suser_profile');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="staffId" class="control-label">Admin Id*</label>
                                        <input required="required" class="form-control" name="staffId" id="staffId" placeholder="" value="<?php echo $user['super_staff_id']; ?>">
                                    </div>
                                    <button class="btn btn-default btn-s" data-title="More" id=<?php echo '"more_'. $user['super_staff_id'].'"';  ?> name=<?php echo '"more_'. $user['super_staff_id'].'"';  ?>  type="submit"><i class="fa fa-eye"></i> View</button>
                                </form>
                                <form style="display:inline;" name=<?php echo '"formEdit_'. $user['super_staff_id'].'"';  ?> method="post" action="<?php echo base_url('suser/edit_suser');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="staffId" class="control-label">Staff ID*</label>
                                        <input required="required" class="form-control" name="staffId" id="staffId" placeholder="" value="<?php echo $user['super_staff_id']; ?>">
                                    </div>
                                    <button class="btn btn-primary btn-s" data-title="Edit" id=<?php echo '"edit_'. $user['super_staff_id'].'"';  ?> name=<?php echo '"edit_'. $user['super_staff_id'].'"';  ?>  type="submit" style="/*background-color: #7B7D7D;color: #FFFFFF;"><i class="fa fa-edit"></i> Edit</button>
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
    // Limit scope pollution from any deprecated API
(function() {

    var matched, browser;

// Use of jQuery.browser is frowned upon.
// More details: http://api.jquery.com/jQuery.browser
// jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

// Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    jQuery.sub = function() {
        function jQuerySub( selector, context ) {
            return new jQuerySub.fn.init( selector, context );
        }
        jQuery.extend( true, jQuerySub, this );
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init( selector, context ) {
            if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
                context = jQuerySub( context );
            }

            return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        return jQuerySub;
    };

})();$(document).ready(function () {
   $('#suserslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
         "aoColumnDefs": [{"aTargets": [4], "orderable": false}],'aaSorting':[]
      });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


//autocomplete for  to visit
      $('#suserId').select2({
        placeholder: '--- Select Super User ---',
        ajax: {
          url: "<?php echo base_url('suser/getsuser'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
            // console.log(data);
          },
          cache: true
        }
      });

    $(function () {$('#dateAppointed').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});$('#dateOfBirth').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});});

});
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo-id").change(function() {
        readURL(this);
    });
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
