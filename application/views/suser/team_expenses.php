<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Expenditures</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Team Expenditures</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <div class="box box-solid collapsed-box" style="background:#5D6D7E;">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #FFFFFF;" > All Expenditures</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse" style="color: #FFFFFF;"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                         <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="expenselist"  >
                            <thead>
                                <tr>
                                    <th class="text-left">Expense Date</th>
                                    <th class="text-left">Cash(Kshs)</th>
                                    <th class="text-left">LPO(Kshs)</th>
                                    <th class="text-left">Actual Costs(Kshs)</th>
                                    <th class="text-center"><i class="fa fa-cog"></i></th>
                                 </tr>
                            </thead>
                            <tbody >
                               <?php foreach($expenses as $exps){ 
                                   ?>
                                <tr>
                                    <td class="text-left"><?php  echo date_format(date_create($exps['expense_date']),"D j<\s\up>S</\s\up> M, Y"); ?></td>
                                    <td class="text-left"><?php  echo number_format($exps['expense_cash'],2); ?></td>
                                    <td class="text-left"><?php  echo number_format($exps['expense_lpo_amount'],2); ?></td>
                                    <td class="text-left"><?php  echo number_format($exps['actual_expenditure'],2); ?></td>
                                    <td class="text-center">
                                        <?php if($exps['receipt_file_name']=="")
                                        {?>
                                           <button class="btn btn-default btn-s" title="View Record" id=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="/*background-color: #7B241C;color: #FFFFFF;"  onclick="viewrecord(this);"> <span class="fa fa-eye"></span> View </button>


                                            <!-- disabled button for receipt download -->
                                              <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="No Receipt(s)" id=<?php echo '"nor_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"nor_'. $exps['expense_auto_id'].'"';  ?>  type="submit" disabled="true" style="background-color:#D4EFDF;color:#000000;"> <span class="fa fa-ban"></span> RCP(s) </button>

                                            
                                              <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;" onclick="editexpense(this);"><span class="fa fa-edit"></span> Edit </button>


                                             <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Delete Record" id=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?>   value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="background-color: #7B241C;color: #FFFFFF;" onclick="delexpense(this);"> <span class="fa fa-trash"></span> Del </button>
                                         
                                        <?php }else{ ?> 
                                              <button class="btn btn-default btn-s" title="View Record" id=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="/*background-color: #7B241C;color: #FFFFFF;"  onclick="viewrecord(this);"> <span class="fa fa-eye"></span> View </button>


                                              <span data-placement="top" data-toggle="tooltip" title="Download Receipt(s)">
                                                <a href="<?php echo base_url();echo 'suser/download_receipt/'; echo $exps['receipt_file_name'];?>" style="background-color:#F5CBA7;color:#000000;" class="btn btn-default" > <span class="fa fa-download"></span> RCP(s)</a></span>

                                            <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;" onclick="editexpense(this);"><span class="fa fa-edit"></span> Edit </button>

                                            <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Delete Record" id=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?>   value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="background-color: #7B241C;color: #FFFFFF;" onclick="delexpense(this);"> <span class="fa fa-trash"></span> Del </button>
                                       <?php }?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php foreach($teams as $team){ ?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" > <?php  echo $team['team_name']; ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <?php $tableId="expenselist_".$team['team_auto_id'];?>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                         <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>" >
                            <thead>
                                <tr>        
                                    <th class="text-left">Expense Date</th>
                                    <th class="text-left">Cash(Kshs)</th>
                                    <th class="text-left">LPO(Kshs)</th>
                                    <th class="text-left">Actual Costs(Kshs)</th>
                                    <th class="text-center"><i class="fa fa-cog"></i></th>
                                 </tr>
                            </thead>
                            <tbody >
                               <?php foreach($team['expenses'] as $exps){ 
                                   ?>
                                <tr>
                                    <td class="text-left"><?php  echo date_format(date_create($exps['expense_date']),"D j<\s\up>S</\s\up> M, Y"); ?></td>
                                    <td class="text-left"><?php  echo number_format($exps['expense_cash'],2); ?></td>
                                    <td class="text-left"><?php  echo number_format($exps['expense_lpo_amount'],2); ?></td>
                                    <td class="text-left"><?php  echo number_format($exps['actual_expenditure'],2); ?></td>
                                     <td class="text-center">
                                        <?php if($exps['receipt_file_name']=="")
                                        {?>
                                           <button class="btn btn-default btn-s" title="View Record" id=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="/*background-color: #7B241C;color: #FFFFFF;"  onclick="viewrecord(this);"> <span class="fa fa-eye"></span> View </button>


                                            <!-- disabled button for receipt download -->
                                              <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="No Receipt(s)" id=<?php echo '"nor_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"nor_'. $exps['expense_auto_id'].'"';  ?>  type="submit" disabled="true" style="background-color:#D4EFDF;color:#000000;"> <span class="fa fa-ban"></span> RCP(s) </button>

                                            
                                              <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;" onclick="editexpense(this);"><span class="fa fa-edit"></span> Edit </button>


                                             <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Delete Record" id=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?>   value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="background-color: #7B241C;color: #FFFFFF;" onclick="delexpense(this);"> <span class="fa fa-trash"></span> Del </button>
                                         
                                        <?php }else{ ?> 
                                              <button class="btn btn-default btn-s" title="View Record" id=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="/*background-color: #7B241C;color: #FFFFFF;"  onclick="viewrecord(this);"> <span class="fa fa-eye"></span> View </button>


                                              <span data-placement="top" data-toggle="tooltip" title="Download Receipt(s)">
                                                <a href="<?php echo base_url();echo 'suser/download_receipt/'; echo $exps['receipt_file_name'];?>" style="background-color:#F5CBA7;color:#000000;" class="btn btn-default" > <span class="fa fa-download"></span> RCP(s)</a></span>

                                            <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"edit_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;" onclick="editexpense(this);"><span class="fa fa-edit"></span> Edit </button>

                                            <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="Delete Record" id=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"del_'. $exps['expense_auto_id'].'"';  ?>   value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="background-color: #7B241C;color: #FFFFFF;" onclick="delexpense(this);"> <span class="fa fa-trash"></span> Del </button>
                                       <?php }?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                         <?php  echo  "<script>
                            $(document).ready(function () { 
                                 //datatable initialization
                                $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
                                     'aoColumnDefs': [{'aTargets': [4], 'orderable': false}] }); 
                            });//close document.ready
                        </script>";?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php }?>
                <div class="modal fade" id="viewexpense">
                  <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="teamName" style="color: #800000;font-weight: bolder;"></h4>
                          </div>
                          <div  id="infobody" >
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                          </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="editexpense" >
                  <div class="modal-dialog" style="width: 80%;margin-left:10%; margin-right: 10%;">
                    <div class="modal-content" >
                      <form method="post" action="<?php echo base_url(); ?>suser/editeamexpense">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="teamName"></h4>
                          </div>
                          <div  id="infobody" >
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success  pull-left">Save changes</button>
                          </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="delexpense" >
                  <div class="modal-dialog" style="width: 80%;margin-left:10%; margin-right: 10%;">
                    <div class="modal-content" >
                      <form method="post" action="<?php echo base_url(); ?>suser/delexpense">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="teamName"></h4>
                          </div>
                          <div  id="infobody" >
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger  pull-left">Confirm Delete </button>
                          </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
 $(document).ready(function () { 
     //datatable initialization
    $('#expenselist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
         'aoColumnDefs': [{ 'aTargets': [4],'bSortable':false}] }); 
});//close document.ready

function viewrecord(objButton)
  {
      var expenseId=objButton.value;
       $.ajax({type:"post", url: "<?php echo base_url(); ?>suser/viewteamexpense",data:{ expenseId:expenseId},dataType:'json',success:function(data){$('#viewexpense #teamName').text(data.teamName);$('#viewexpense #infobody').html(data.info);$('#viewexpense').modal('toggle');} });
  }
  function editexpense(objButton)
  {
      var expenseId=objButton.value;
      var action="edit";
       $.ajax({type:"post", url: "<?php echo base_url(); ?>suser/geteamexpense",data:{ expenseId:expenseId,action:action},dataType:'json',success:function(data){$('#editexpense #teamName').text(data.teamName);$('#editexpense #infobody').html(data.info);$('#editexpense').modal('toggle');} });
  }
   function delexpense(objButton)
  {
      var expenseId=objButton.value;
      var action="del";
       $.ajax({type:"post", url: "<?php echo base_url();?>suser/geteamexpense",data:{ expenseId:expenseId,action:action},dataType:'json',success:function(data){$('#delexpense #teamName').text(data.teamName);$('#delexpense #infobody').html(data.info);$('#delexpense').modal('toggle');} });
  }
</script>
</body>
</html>
