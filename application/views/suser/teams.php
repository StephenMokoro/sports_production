<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Teams</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
<link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
 <style>
    @media (max-width:767px){.select2 {width: 100% !important;}}
    @media (min-width:768px){.select2 {width: 100% !important;}}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row " style="margin-bottom: -15px;">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Teams</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-s" data-title="Print All" type="button" href=""><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Team</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-s" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                      <?php echo form_open_multipart('suser/newteam',array('id' => 'team_registration','method'=>'post'));?>
                              <div class="row setup-content" >
                              <div class="col-xs-12">
                                  <div class="col-md-12">
                                    <div class="form-group col-md-12 col-lg-12">
                                          <label for="sportId" class="control-label">Sport<span class="star">*</span></label>
                                          <select type="text" name="sportId"  class=" form-control" id="sportId" required="required">
                                              <option value="">--Select Name of Sport--</option>
                                            <?php  foreach($sports as $sport){ 
                                                  ?>
                                              <option value=<?php  echo '"'.$sport['sport_auto_id'].'"';?>><?php  echo $sport['sport_name'];}?></option>
                                          </select>
                                      </div>
                                      <div class="form-group col-md-6 col-lg-6 ">
                                          <label for="teamName" class="control-label">Team Name <span class="star">*</span></label>
                                          <input type="text" name="teamName" placeholder="e.g. Gladiators" class="form-control" id="teamName" required="required" >
                                      </div>
                                       <div class="form-group col-md-6 col-lg-6 ">
                                          <label for="teamAlias" class="control-label">Team Alias</label>
                                          <input type="text" name="teamAlias" placeholder="e.g. Glads" class="form-control" id="teamAlias" >
                                      </div>
                                      <div class="form-group col-md-12 col-lg-12">
                                          <label class="">Category <span class="star">*</span></label><br>
                                          <label class="radio-inline ">
                                              <input type="radio" name="teamCategory" id="teamCategory" value="Men" required="required" autocomplete="off">Men
                                          </label>
                                          <label class="radio-inline ">
                                              <input type="radio" name="teamCategory" id="teamCategory" value="Women" required autocomplete="off">Women
                                          </label>
                                           <label class="radio-inline ">
                                              <input type="radio" name="teamCategory" id="teamCategory" value="Mixed" required autocomplete="off">Mixed
                                          </label>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <?php echo form_close();?>

                            </div>
                        </div>
                         <?php if(isset($_SESSION['msg']))
                        {
                          $msg = $_SESSION['msg'];
                          $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                          <div class="messagebox alert alert-danger" style="display: block">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-close"></i>
                                <strong><span>';echo $msg['error']; echo '</span></strong>
                            </div> 
                          </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                          <div class="messagebox alert alert-success" style="display: block">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-check-circle-o"></i>
                                <strong><span>';echo $msg['success'];echo '</span></strong>
                            </div> 
                            </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="captainslist"  >
                    <thead>
                        <tr>    
                            <th class="text-left">Sport Name</th>
                            <th class="text-left">Team Name</th>
                            <th class="text-left">Team Coach</th>
                            <th class="text-center"></th>
                         </tr>
                    </thead>
                    <tbody >
                      <?php  foreach($teams as $team){ 
                           ?>
                        <tr>
                            <td class="text-left"><?php  echo $team['sport_name']; ?></td>
                            <td class="text-left"><?php  echo $team['team_name']; ?></td>
                            <td class="text-left"><?php  if(!empty($team['coach'])){echo $team['coach']->coach_lname .' '.$team['coach']->coach_fname;} ?></td>
                            <td class="text-left"></td>
                            <td class="text-center">
                              <form style="display:inline;" name=<?php echo '"formEdit_'. $team['team_auto_id'].'"';  ?> method="post" action="#">
                                  <div class="form-group col-md-12 col-lg-12" style="display:none">
                                      <label for="teamId" class="control-label">Team ID<span class="star">*</span></label>
                                      <input required="required" class="form-control" name="teamId" id="teamId" placeholder="" value="<?php echo $team['team_auto_id']; ?>">
                                  </div>
                                  <button class="btn btn-primary btn-s" title="Edit Team" id=<?php echo '"edit_'. $team['team_auto_id'].'"';  ?> name=<?php echo '"edit_'. $team['team_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;"><span class="fa fa-edit"></span> Edit Team </button>
                              </form>
                              <?php if(empty($team['coach'])){?>
                                <form style="display:inline;" name=<?php echo '"formAssignCoach_'. $team['team_auto_id'].'"';  ?> method="post" action="<?php echo base_url('suser/assigncoach');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="teamId" class="control-label">Team ID*</label>
                                        <input required="required" class="form-control" name="teamId" id="teamId" placeholder="" value="<?php echo $team['team_auto_id']; ?>">
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="teamName" class="control-label">Team ID*</label>
                                        <input required="required" class="form-control" name="teamName" id="teamName" placeholder="" value="<?php echo $team['team_name']; ?>">
                                    </div>
                                    <button class="btn btn-primary btn-s" data-title="Assign Coach" id=<?php echo '"assign_'. $team['team_auto_id'].'"';  ?> name=<?php echo '"assign_'. $team['team_auto_id'].'"';  ?>  type="submit" style="/*background-color: #7B7D7D;color: #FFFFFF;"><span class="fa fa-plus-circle"></span> Add &nbsp; Coach  </button>
                                </form>
                                <?php }else{?>
                                    <form style="display:inline;" name=<?php echo '"formChangeCoach_'. $team['team_auto_id'].'"';  ?> method="post" action="#">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="teamId" class="control-label">Team ID*</label>
                                        <input required="required" class="form-control" name="teamId" id="teamId" placeholder="" value="<?php echo $team['team_auto_id']; ?>">
                                    </div>
                                     <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="teamName" class="control-label">Team ID*</label>
                                        <input required="required" class="form-control" name="teamName" id="teamName" placeholder="" value="<?php echo $team['team_name']; ?>">
                                    </div>
                                    <button class="btn btn-success btn-s" data-title="Change Coach" id=<?php echo '"change_'. $team['team_auto_id'].'"';  ?> name=<?php echo '"change_'. $team['team_auto_id'].'"';  ?>  type="submit" style="/*background-color: #7B7D7D;color: #FFFFFF;"><span class="fa fa-edit"></span> Edit Coach  </button>
                                </form>
                               <?php }  ?>

                            </td>
                        </tr><?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
$(document).ready(function () {
   $('#captainslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
         "aoColumnDefs": [{"aTargets": [3], "orderable": false}],'aaSorting':[]
      });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

// $("#teamID").change(function(){
//     var teamId = $(this).val();
//     $('#playerId').select2('data', {id: null, text: null});
//     $("#playerinfo").show(); 
//     if(teamId !="")
//     {
//         $.ajax({type: 'POST',
//                  url: "<?php echo base_url('suser/seteam'); ?>",
//                 dataType:'json',
//                  data: {teamId: teamId},
//                  success: function(data){console.log(data)}});
//     }else{$("#playerinfo").hide();}  

// });
//autocomplete for  to visit
      $('#playerId').select2({
        placeholder: '--- Select Player ---',
        ajax: {
          url: "<?php echo base_url('coach/getplayer'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
            // console.log(data);
          },
          cache: true
        }
      });


});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
