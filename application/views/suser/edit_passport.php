<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Edit Travel Document</title>
<?php $this->load->view('headerlinks/headerlinks.php');?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span>Edit Travel Document for <b class="text-success"><?php echo $_SESSION['sessdata']['pst_player_name'];?></b></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>

                 <?php foreach($passport_details as $pspt){

                  echo form_open_multipart('suser/updateplayerpspt',array('id'=>'updateplayerpspt','method'=>'post'));
                   $photo=$pspt['passport_photo']; if($photo==""){$image="passportdefault.png";}else{$image=$pspt['passport_photo'];}
                   ?>
                            <div class="row setup-content" >
                                <div class="col-xs-12">
                                    <div class="col-md-6">
                                      <div class="col-md-11 col-md-offset-1">
                                          <div class="form-group">
                                            <div class="main-img-preview">
                                              <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/travel_documents/'.$image?>" alt="Passport Image" width="340" height="230">
                                            </div>
                                            <!-- <p class="help-block">* Upload mentee passport photo.</p> -->
                                          </div>
                                      </div>
                                      <div class="col-md-6 col-md-offset-4">
                                        <div class="input-group">
                                          <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" style="display: none; " required="required">
                                          <div class="input-group-btn">
                                            <div class="fileUpload btn btn-default fake-shadow">
                                             <span><i class="fa fa-upload"></i> Upload Bio Page</span>
                                              <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                        <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                        <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$pspt['passport_photo'].'"';?> >
                                      </div>
                                      <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                        <label for="passportId" class="control-label">Passport UID <span class="star">*</span></label>
                                        <input type="number" name="passportId" placeholder="" class=" form-control" id="passportId" required="required" value=<?php echo '"'.$pspt['passport_auto_id'].'"';?>>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                       
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="passportNo" class="control-label" style="margin-top: 10px;">Passport Number <span class="star">*</span></label>
                                            <input type="text" name="passportNo" placeholder="" class=" form-control" id="passportNo" required="required" maxlength="30" value=<?php echo '"'.$pspt['passport_number'].'"';?>>
                                        </div>
                                        <div class='col-md-12'>
                                            <label for="dateOfIssue" class="control-label">Date of Issue</label>
                                            <div class="form-group">
                                                <div class='input-group date' id='dateOfIssue'>
                                                    <input type='text' class="form-control" readonly="true" name="dateOfIssue" value=<?php echo '"'.$pspt['issue_date'].'"';?> />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                           
                                        <div class='col-md-12'>
                                            <label for="dateOfExpiry" class="control-label">Date of Expiry</label>
                                            <div class="form-group">
                                                <div class='input-group date' id='dateOfExpiry'>
                                                    <input type='text' class="form-control" readonly="true" name="dateOfExpiry" value=<?php echo '"'.$pspt['expiry_date'].'"';?>/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label class="control-label"> Country of Issue <span class="star">*</span> </label>
                                            <select type="text" id="issueCountry" name="issueCountry" placeholder="" class=" form-control" required="required" >
                                                <option value=<?php echo '"'.$pspt['issue_country'].'"';?>><?php echo $pspt['issue_country'];?></option>
                                                <option value="">--Select Country--</option>
                                                <option value="BURUNDI">BURUNDI</option>
                                                <option value="KENYA">KENYA</option>
                                                <option value="RWANDA">RWANDA</option>
                                                <option value="TANZANIA">TANZANIA</option>
                                                <option value="UGANDA">UGANDA</option>
                                                <option value="ZIMBAMWE">ZIMBAMBWE</option>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12">
                                        <input class="btn btn-warning nextBtn  pull-right" type="submit" value="Update">
                                    </div>
                                </div><!--/.col-xs-12-->
                            </div><!--/.setup-content-->
                        <?php echo form_close();?>
                      <?php }?>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


    var brand = document.getElementById('photo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo-id").change(function() {
        readURL(this);
    });

//datepicker
$(function () {$('#dateOfIssue').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});$('#dateOfExpiry').datepicker({format: "yyyy-mm-dd",todayHighlight: true});});
});
</script>
</body>
</html>
