<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Edit Injury</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Injury Edit</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-xs" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <form role="form" method="post" action="<?php echo base_url(); ?>suser/updateinjury">
                  <div class="row setup-content" >
                            <div class="col-md-12">
                                <?php foreach($injury_record as $record){ ?>
                                 <div class="form-group col-md-6 col-lg-6" hidden="true">
                                    <label for="recordId" class="control-label">Injury ID<span class="star">*</span></label>
                                    <input required="required" class="form-control" name="recordId" id="recordId" placeholder="" value="<?php echo $record['injury_auto_id']; ?>">
                                </div>
                                <br>

                                <?php $photo=$record['player_profile_photo']; if($photo==""){$ppic="person.png";}else{$ppic=$record['player_profile_photo'];}?>
                                <div class="form-group col-md-12 col-lg-12">
                                    <strong class="fa-1x"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$ppic?>" width="40" height="40" class="img-circle" alt="">  <?php echo $record['player_fname']. " ".$record['player_lname']; ?></strong>
                                </div>
                                <br><br><br>

                                <div class='col-md-6 col-lg-6'>
                                  <label for="injuryDate" class="control-label">Injury Date <span class="star">*</span></label>
                                    <div class="form-group">
                                        <div class='input-group date' id='injuryDate'>
                                            <input type='text' class="form-control" readonly="true" name="injuryDate" value="<?php echo $record['injury_date']; ?>" required="required" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class='col-md-6 col-lg-6'>
                                  <label for="dateSeen" class="control-label">Date Seen</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='dateSeen'>
                                            <input type='text' class="form-control" readonly="true" name="dateSeen" value="<?php echo $record['date_seen']; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-lg-6">
                                  <label for="injuryDescription" class="control-label" style="margin-top: 10px;">Description/Symptoms <span class="star">*</span></label>

                                  <textarea type="text" name="injuryDescription" placeholder="" class=" form-control" id="injuryDescription" required="required" maxlength="150" style="resize: none;min-height: 80px;"><?php echo $record['injury_description']; ?></textarea>
                                </div>

                                <div class="form-group col-md-6 col-lg-6">
                                  <label for="diagnosis" class="control-label" style="margin-top: 10px;">Diagnosis <span class="star">*</span></label>
                                  
                                  <textarea type="text" name="diagnosis" placeholder="" class=" form-control" id="diagnosis" required="required" maxlength="150" style="resize: none;min-height: 80px;"><?php echo $record['diagnosis']; ?></textarea>
                                </div>

                                <div class="form-group col-md-6 col-lg-6">
                                  <label for="treatment" class="control-label" style="margin-top: 10px;">Treatment <span class="star">*</span></label>
                                  
                                  <textarea type="text" name="treatment" placeholder="" class=" form-control" id="treatment" required="required" maxlength="150" style="resize: none;min-height: 80px;"><?php echo $record['treatment']; ?></textarea>
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                  <label for="scoachRemarks" class="control-label" style="margin-top: 10px;">S&C Remarks <span class="star">*</span></label>
                                  
                                  <textarea type="text" name="scoachRemarks" placeholder="" class=" form-control" id="scoachRemarks" required="required" maxlength="150" style="resize: none;min-height: 80px;" ><?php echo $record['scoach_remarks']; ?></textarea>
                                </div>

                                <?php $physiostate=$record['physio_status'];  ?>
                                <div class="form-group col-md-12 col-lg-12">
                                  <label for="physioStatus" class="control-label">Physio Status</label><br>
                                  <?php if($physiostate="1"){?>
                                  <input type="checkbox" name="physioStatus" id="physioStatus"  autocomplete="off" style="display: inline;" checked="checked">
                                  <label for="physioStatus" class="control-label" style="font-weight: normal">&nbsp;Check if player on physio</label><br>
                                  <?php } else{ ?>
                                    <input type="checkbox" name="physioStatus" id="physioStatus"  autocomplete="off" style="display: inline;" checked="checked">
                                  <label for="physioStatus" class="control-label" style="font-weight: normal">&nbsp;Check if player on physio</label><br>
                                  <?php }?>
                                <hr>
                                </div>
                                <div class='col-md-6 col-lg-6' id="phystartdate">
                                  <label for="startDate" class="control-label">Physio Start Date</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='startDate'>
                                            <input type='text' class="form-control" readonly="true" name="startDate" value="<?php echo $record['physio_start_date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class='col-md-6 col-lg-6' id="phyenddate">
                                  <label for="endDate" class="control-label">Physio end Date</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='endDate'>
                                            <input type='text' class="form-control" readonly="true" name="endDate" value="<?php echo $record['physio_end_date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-lg-6" id="phyremarks">
                                    <label for="physioRemarks" class="control-label" style="margin-top: 10px;">Physio Remarks <span class="star">*</span></label>
                                    <textarea type="text" name="physioRemarks" placeholder="" class=" form-control" id="physioRemarks" required="required" maxlength="150" style="resize: none;min-height: 80px;"><?php echo $record['physio_remarks']; ?></textarea>
                                </div>
                                

                                <div class="form-group col-md-12 col-lg-12">
                                  <br>
                                  <input type="submit" class="btn btn-warning" value="Update">
                                  <input type="reset" class="btn btn-default" value="Reset">
                                </div>
                            <?php }?>
                            </div>
                          </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
 $(document).ready(function () {
     $('#physioStatus').click(function () {
         var $this = $(this);
         if ($this.is(':checked')) {
             $('#phyremarks').show();
             $('#phystartdate').show();
             $('#phyenddate').show();
         } else {
             $('#phyremarks').hide();
             $('#phystartdate').hide();
             $('#phyenddate').hide();
         }
     });
 });
//datepicker
$(function () {$('#dateAppointed').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});});

$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    $(function () {$('#startDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});
    $('#endDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});
    $('#injuryDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});
    $('#dateSeen').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});});
//to refresh the page
});
</script>
</body>
</html>
