<!DOCTYPE html>
<html>
<head>
   <title>SU Sports |  Admin Uploads</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row " style="margin-bottom: -15px;">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Admin Uploads</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh
                          </button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                  <?php if(isset($_SESSION['msg']))
                        {
                          $msg = $_SESSION['msg'];
                          $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                          <div class="messagebox alert alert-danger" style="display: block">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-close"></i>
                                <strong><span>';echo $msg['error']; echo '</span></strong>
                            </div> 
                          </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                          <div class="messagebox alert alert-success" style="display: block">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-check-circle-o"></i>
                                <strong><span>';echo $msg['success'];echo '</span></strong>
                            </div> 
                            </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>

                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="reportlist"  >
                    <thead>
                        <tr>
                            <th class="text-left">Document Descriptive Name</th>
                            <th class="text-center">Upload Person</i></th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                         </tr>
                    </thead>
                    <tbody >
                       <?php foreach($admin_uploads as $file){ 
                           ?>
                        <tr>
                            <td class="text-left"><?php

                             $file_ext=$file['file_ext'];
                                if($file_ext==".doc"||$file_ext==".docx")
                                  {
                                    $i='<span><i class="fa fa-file-word-o fa-lg" style="color:blue"></i></span>';
                                  }else  if($file_ext==".pdf"||$file_ext==".PDF"){
                                    $i='<span><i class="fa fa-file-pdf-o fa-lg" style="color:red"></i></span>';
                                  }else  if($file_ext==".xls"||$file_ext==".xlsx"){
                                    $i='<span><i class="fa fa-file-excel-o fa-lg" style="color:#33bbff"></i></span>';
                                  }
                                echo  $i." &nbsp;".$file['upload_descriptive_name']; ?>
                            </td>
                            <td class="text-center"><?php echo $file['admin_lname'].' '.$file['admin_lname']; ?></td>
                            <td class="text-center" width="20%">
                                <span data-placement="top" data-toggle="tooltip" title="Summary">
                                    <button class="btn btn-default btn-s" id="<?php  echo 'more_'. $file['upload_auto_id'];?>" name="<?php  echo 'more_'. $file['upload_auto_id'];?>" type="submit" style="background-color: #F5CBA7;color: #000000;"><span class ="fa fa-eye"></span> Summary</button>
                                </span>
                               
                                <span data-placement="top" data-toggle="tooltip" title="Open/Download">
                                    <a class="btn btn-s btn-primary" type="button" href="<?php echo base_url();echo 'uploads/admin_uploads/'; echo $file['upload_file_name'];?>" target="_blank"><span class="fa fa-download"></span>&nbsp;Download</a>
                                </span>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    //datatable initialization
    $('#reportlist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
         "aoColumnDefs": [{"bSortable":false, "aTargets": [2]}],'aaSorting':[]
   });
});
</script>
</body>
</html>
