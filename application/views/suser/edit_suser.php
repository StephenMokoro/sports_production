<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Super Users</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row ">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Edit Super User</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
                <br>
                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                    
                    <?php foreach($suser_profile as $profile){?>
                    <?php echo form_open_multipart('suser/updateSuser',array('id' => 'super_update','method'=>'post'));
                    $photo=$profile['super_profile_photo']; if($photo==""){$ppic="person.png";}else{$ppic=$profile['super_profile_photo'];}
                   ?>
                              <div class="row setup-content">
                                  <div class="col-sm-12">
                                      <div class="col-md-6">
                                        <div class="col-md-9 col-md-offset-3">
                                            <div class="form-group">
                                                <div class="main-img-preview">
                                                  <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/profile_photos/susers/'.$ppic?>" alt="Suser Photo" width="210" height="230">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="input-group">
                                              <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" style="display: none; ">
                                              <div class="input-group-btn">
                                                <div class="fileUpload btn btn-default fake-shadow">
                                                 <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                  <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                         <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                            <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                            <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$profile[ 'super_profile_photo']. '"';?> >
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                            <label for="scId" class="control-label">suser Auto ID*</label>
                                            <input type="text" name="scId" placeholder="Suser Auto ID" class="form-control" id="scId" required="required"  value=<?php echo '"'.$profile['super_auto_id'].'"';?> >
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="firstName" class="control-label" style="margin-top: 10px;">First Name <span class="star">*</span></label>
                                          <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20" value=<?php echo '"'.$profile['super_fname'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                          <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20" value=<?php echo '"'.$profile['super_lname'].'"';  ?>>
                                        </div>   
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="userName" class="control-label">SU User Name <span class="star">*</span></label>
                                          <input type="text" name="userName" class=" form-control" id="userName" required="required" maxlength="20" value=<?php echo '"'.$profile['super_username'].'"';  ?> >
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="staffId" class="control-label">Staff ID*</label>
                                            <input type="text" name="staffId" placeholder="" class=" form-control" id="staffId" required="required" value=<?php echo '"'.$profile['super_staff_id'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="nationalId" class="control-label">ID/Passport/Birth Cert No<span class="star">*</span></label>
                                              <input type="text" name="nationalId" placeholder="" class=" form-control" id="nationalId" required="required"  maxlength="12" minlength="4" value=<?php echo '"'.$profile['super_nid'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="idType" class="control-label">Type of ID Used</label>
                                            <select type="text" name="idType" placeholder="ID Type" class=" form-control" id="idType" required="required">
                                                  <option value=<?php echo '"'.$profile['id_type'].'"';   ?>><?php echo $profile['id_type'];   ?></option>
                                                    <option value="National ID">National ID</option>
                                                    <option value="Passport">Passport</option>
                                                    <option value="Birth Certificate">Birth Certificate</option>
                                              </select>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="phoneNumber" class="control-label"> Current Phone No.<span class="star">*</span></label>
                                              <input type="text" name="phoneNumber" placeholder="" class=" form-control" id="phoneNumber" required="required" data-mask="0799999999" value=<?php echo '"'.$profile['super_phone'].'"';  ?>>
                                        </div>

                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="emailAddress" class="control-label"> Current Email Address<span class="star">*</span></label>
                                              <input type="email" name="emailAddress" placeholder="" class=" form-control" id="emailAddress" required="required" maxlength="50" value=<?php echo '"'.$profile['super_email'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <input class="btn btn-warning nextBtn  pull-right" type="submit" value="Update">
                                        </div>
                                    </div>
                                </div>
                             </div>
                          <?php echo form_close();}?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


    var brand = document.getElementById('photo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo-id").change(function() {
        readURL(this);
    });

//datepicker
$(function () {$('#dateOfBirth').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});$('#dateOfAppointed').datepicker({format: "yyyy-mm-dd",todayHighlight: true});});
});
</script>
</body>
</html>
