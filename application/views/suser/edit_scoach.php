<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | S&C Coach</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row ">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span>Edit S&C Coach</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
                <br>
                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                    <?php foreach($coach_profile as $profile){?>
                    <?php echo form_open_multipart('suser/updatescoach',array('id' => 'SCcoach_update','method'=>'post'));
                    $photo=$profile['sc_profile_photo']; if($photo==""){$ppic="person.png";}else{$ppic=$profile['sc_profile_photo'];};
                   ?>
                              <div class="row setup-content">
                                  <div class="col-sm-12">
                                      <div class="col-md-6">
                                        <div class="col-md-9 col-md-offset-3">
                                            <div class="form-group">
                                                <div class="main-img-preview">
                                                  <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/profile_photos/SCcoaches/'.$ppic?>" alt="Coach Photo" width="210" height="230">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="input-group">
                                              <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" style="display: none; ">
                                              <div class="input-group-btn">
                                                <div class="fileUpload btn btn-default fake-shadow">
                                                 <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                  <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                         <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                            <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                            <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$profile[ 'sc_profile_photo']. '"';?> >
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="scId" class="control-label" style="margin-top: 10px; display: none;">UID <span class="star">*</span></label>
                                          <input type="text" name="scId" placeholder="" class=" form-control" id="scId" required="required" maxlength="20" style ="display: none;" value=<?php echo '"'.$profile['sc_auto_id'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="firstName" class="control-label" style="margin-top: 10px;">First Name <span class="star">*</span></label>
                                          <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20" value=<?php echo '"'.$profile['sc_fname'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                          <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20" value=<?php echo '"'.$profile['sc_lname'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="otherNames" class="control-label">Other Names</label>
                                          <input type="text" name="otherNames" placeholder="" class=" form-control" id="otherNames" maxlength="20" value=<?php echo '"'.$profile['sc_other_names'].'"';  ?>>
                                        </div>

                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="dateOfBirth" class="control-label">Date of Birth</label>
                                            <div class="form-group">
                                                <div class='input-group date' id='dateOfBirth'>
                                                    <input type='text' class="form-control" readonly="true" name="dateOfBirth" value=<?php echo '"'.$profile['sc_dob'].'"';?> />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="userName" class="control-label">User Name <span class="star">*</span></label>
                                          <input type="text" name="userName" placeholder="" class=" form-control" id="userName" required="required" maxlength="20" value=<?php echo '"'.$profile['sc_username'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="staffId" class="control-label">Staff ID*</label>
                                            <input type="text" name="staffId" placeholder="" class=" form-control" id="staffId" required="required" value=<?php echo '"'.$profile['sc_staff_id'].'"';  ?>>
                                        </div>
                                    <div class="form-group col-md-12 col-lg-12">
                                            <label for="dateAppointed" class="control-label">Date Appointed</label>
                                            <div class="form-group">
                                                <div class='input-group date' id='dateAppointed'>
                                                    <input type='text' class="form-control" readonly="true" name="dateAppointed" value=<?php echo '"'.$profile['date_appointed'].'"';?> />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="nationalId" class="control-label">ID/Passport/Birth Cert No<span class="star">*</span></label>
                                              <input type="text" name="nationalId" placeholder="" class=" form-control" id="nationalId" required="required"  maxlength="12" minlength="4" value=<?php echo '"'.$profile['sc_nid'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="idType" class="control-label">Type of ID Used</label>
                                            <select type="text" name="idType" placeholder="ID Type" class=" form-control" id="idType" required="required">
                                                  <option value=<?php echo '"'.$profile['id_type'].'"';   ?>><?php echo $profile['id_type'];   ?></option>
                                                    <option value="National ID">National ID</option>
                                                    <option value="Passport">Passport</option>
                                                    <option value="Birth Certificate">Birth Certificate</option>
                                              </select>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="phoneNumber" class="control-label"> Current Phone No.<span class="star">*</span></label>
                                              <input type="text" name="phoneNumber" placeholder="" class=" form-control" id="phoneNumber" required="required" data-mask="0799999999" value=<?php echo '"'.$profile['sc_phone'].'"';  ?>>
                                        </div>

                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="emailAddress" class="control-label"> Current Email Address<span class="star">*</span></label>
                                              <input type="email" name="emailAddress" placeholder="" class=" form-control" id="emailAddress" required="required" maxlength="50" value=<?php echo '"'.$profile['sc_email'].'"';  ?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                              <label for="residence" class="control-label"> Current Residence*</label>
                                              <input type="text" name="residence" placeholder=" Madaraka Estate, Tulia Court, HSE 1137" class=" form-control" id="residence" required="required" maxlength="50" minlength="10" value=<?php echo '"'.$profile['sc_residence'].'"';  ?>>
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                            <label for="" class="control-label">User Agreement</label><br>
                                            <input type="checkbox" name="agreement" id="agreement" value="1" required="required" autocomplete="off" style="display: inline;" value=<?php echo '"'.$profile['user_agreement'].'"';  ?>>
                                            <label for="agreement" class="control-label" style="font-weight: normal">&nbsp;I agree with set rules<span class="star">*</span></label><br><br>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <input class="btn btn-success nextBtn  pull-right" type="submit" value="Submit">
                                        </div>
                                    </div>
                                </div>
                             </div>
                          <?php echo form_close();}?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


    var brand = document.getElementById('photo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo-id").change(function() {
        readURL(this);
    });

//datepicker
$(function () {$('#dateOfBirth').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});$('#dateOfAppointed').datepicker({format: "yyyy-mm-dd",todayHighlight: true});});
});
</script>
</body>
</html>
