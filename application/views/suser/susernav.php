  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b class="fa fa-expand" data-toggle="push-menu"></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><small>SU<b> SP <i class="fa fa-soccer-ball-o fa-spin" style="color: #000080;"></i> RTS</b></span></small>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background-color: #1a2226;">
      <style>#sidebar-toggle:hover{background-color: #222d32;}</style>
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" id="sidebar-toggle">
        <span class="sr-only">Toggle navigation</span><span style="font-weight: bolder;color: #FFFFFF;">D.O.S</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url();?>assets/img/person.png"  class="user-image" alt="">
              <span class="hidden-xs"><?php echo $_SESSION['sessdata']['adminName'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('suser/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/img/person.png"  class="img-circle" alt="">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['sessdata']['adminName'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo base_url();?>suser/dashboard""><i class="fa fa-dashboard text-aqua"></i> <span>Dashboard</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-circle"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="<?php echo base_url();?>suser/administrators"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Administrators</span></a>
            </li>
            <li>
                <a href="<?php echo base_url();?>suser/coaches"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Coaches</span></a>
            </li>
            <li>
                <a href="<?php echo base_url();?>suser/captains"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Captains</span></a>
            </li>
            <li>
                <a href="<?php echo base_url();?>suser/sc_coaches"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; S&C Coach</span></a>
            </li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-life-ring"></i> <span>Teams</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="<?php echo base_url();?>suser/teamslist"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Teams List</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Players</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="<?php echo base_url();?>suser/players"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Players</span></a>
            </li>
             <li>
                <a href="<?php echo base_url();?>suser/studentplayers"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Student Players</span></a>
            </li>
            <li>
                <a href="<?php echo base_url();?>suser/playerpassports"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Passports</span></a>
            </li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-bars"></i> <span>Training</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                 <a  href="<?php echo base_url(); ?>suser/trainingdays"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw" aria-hidden="true" ></i> Training Days</span></a>   
            </li>
            <li>
                <a href="<?php echo base_url('suser/trainingattendance');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw" ></i> Attendance</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-medkit"></i> <span>Injuries</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li>
                <a href="<?php echo base_url('suser/injuries');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i> Injuries</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>Expenditures</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="<?php echo base_url('suser/expenditures');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i> Expenditures</span></a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-puzzle-piece"></i> <span>Matches</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li>
                <a href="<?php echo base_url('suser/tournaments');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i> Tournaments</span></a>
            </li>
          </ul>
        </li>
        <!-- Uploads -->
       <li class="treeview">
          <a href="#">
            <i class="fa fa-upload"></i> <span>File Uploads</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li>
                    <a href="<?php echo base_url('suser/teamuploads');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i>Team Uploads</span></a>
              </li>
              <li>
                  <a href="<?php echo base_url('suser/sportuploads');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i>Sport Uploads</span></a>
              </li>
              <li>
                  <a href="<?php echo base_url('suser/adminuploads');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i>Admin Uploads</span></a>
              </li>
              <li>
                  <a href="<?php echo base_url('suser/adminbroadcast');?>"><span style="color: #1ABC9C;"><i class="fa fa-caret-right fa-fw " aria-hidden="true" ></i>Admin Broadcasts</span></a>
              </li>
          </ul>
        </li>
        <li class="header" style="color: #909497;" > DoS | Sports Department</li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>