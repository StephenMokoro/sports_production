<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Next of Kin</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Next of Kin</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
               <?php foreach($nxtkin as $nxt){ 
                 $photo=$nxt['player_profile_photo']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$nxt['player_profile_photo'];}?>
                    <div class="col-md-9 text-left">
                        <div class="col-md-12">
                            <p class="fa-2x"><?php echo $nxt['kin_fname']." ".$nxt['kin_lname']. " ".$nxt['kin_other_names'];?></p> 
                            <small><cite title="Player"><b><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$ppic?>" width="30" height="30" class="img-circle" alt=""> </b> Player: <?php echo $nxt['player_fname']." ".$nxt['player_lname']. " ".$nxt['player_other_names'];?>  </cite></small>
                            <br><br>
                        </div>
                        <div class="col-md-12" style="text-align: left">
                            <blockquote >
                                 <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $nxt['kin_phone'];?>  <span><cite title="<?php echo $nxt['kin_fname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>
                                 <p><i class="fa fa-phone text-warning fa-1x"></i> <?php echo $nxt['kin_alt_phone'];?>  <span><cite title="<?php echo $nxt['kin_fname'];?>'s alternative phone number"><small style="display: inline">Alternate Cell Phone  </small></cite></span> </p>

                                 <p><i class="fa fa-id-card-o text-default fa-1x"></i> <?php echo $nxt['kin_nid'];?>  <span><cite title="ID Type"><small style="display: inline"><?php echo $nxt['id_type'];?>  </small></cite></span> </p>
                                 <p><i class="fa fa-home text-success fa-1x"></i> <?php echo $nxt['kin_residence'];?>  <span><cite title="Residence"><small style="display: inline"> Residence </small></cite></span> </p>

                                 
                            </blockquote>
                        </div>
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>
