<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | New Team Coach</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
<link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
 <style>
    @media (max-width:767px){.select2 {width: 100% !important;}}
    @media (min-width:768px){.select2 {width: 100% !important;}}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('suser/susernav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row " style="margin-bottom: -15px;">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> New <?php echo $_SESSION['sessdata']['assignCoachTeamName'];?> Coach</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <?php echo form_open_multipart('suser/newteamcoach',array('id' => 'team_coach_assignment','method'=>'post'));?>
                      <div class="row setup-content" >
                        <div class="col-xs-12">
                            <div class="col-md-12">
                               <div class="form-group col-md-12 col-lg-12">
                                  <label for="coachId" class="control-label green">Coach <span class="star">*</span></label>
                                  <select type="text" name="coachId" placeholder="" class="form-control" id="coachId" required="required"></select>
                                </div>
                                
                                  <div class="form-group col-md-12 col-lg-12">
                                      <label for="dateAppointed" class="control-label">Start Date<span class="star">*</span></label>
                                      <div class="form-group">
                                      <div class='input-group date' id='dateAppointed'>
                                          <input type='text' class="form-control" readonly="true" name="dateAppointed" />
                                          <span class="input-group-addon">
                                              <span class="fa fa-calendar"></span>
                                          </span>
                                      </div>
                                  </div>
                                  </div>
                                  <div class="form-group col-md-12 col-lg-12">
                                      <input type="submit" class="btn btn-primary" value="Submit">
                                  </div>
                              </div>
                          </div>
                      </div>
                      <?php echo form_close();?>

               
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
$(document).ready(function () {
   $('#captainslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
         "aoColumnDefs": [{"aTargets": [3], "orderable": false}],'aaSorting':[]
      });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


//autocomplete for  to visit
      $('#coachId').select2({
        placeholder: '--- Select Coach ---',
        ajax: {
          url: "<?php echo base_url('suser/getcoach'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
            // console.log(data);
          },
          cache: true
        }
      });

    $(function () {$('#dateAppointed').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});});

});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
