<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Injury Details</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Injury Details</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
                <?php foreach($injury_record as $record){ ?>
                <div class="form-group col-md-12 col-lg-12">
                    <strong class="fa-2x"><img src="<?php echo base_url();?>assets/img/person.png" width="40" height="40" class="img-circle" alt="">  <?php echo $record['player_fname']. " ".$record['player_lname']; ?></strong>
                </div>
                <div class="form-group col-md-12 col-lg-12">
                    <label for="dateOfInjury" class="control-label">Date of Injury</label>
                    <?php echo ": ".date_format(date_create($record['injury_date']),"D j<\s\up>S</\s\up> M, Y"); ?>
                    <br>
                    <label for="dateSeen" class="control-label">Date Seen </label>
                   <?php echo ": ".date_format(date_create($record['date_seen']),"D j<\s\up>S</\s\up> M, Y"); ?>
                </div>
                <div class="form-group col-md-12 col-lg-12 ">
                    <label for="injuryDescription" class="control-label">Injury Description</label>
                    <br><?php echo $record['injury_description']; ?>
                </div>
                <!-- <div class="form-group col-md-12 col-lg-12">
                    <div class="modal-header"></div>
                </div> -->
                <div class="form-group col-md-12 col-lg-12 ">
                    <label for="diagnosis" class="control-label">Diagnosis</label>
                    <br><?php echo $record['diagnosis']; ?>
                </div>
                <!-- <div class="form-group col-md-12 col-lg-12">
                    <div class="modal-header"></div>
                </div> -->
                 <div class="form-group col-md-12 col-lg-12 ">
                    <label for="treatment" class="control-label">Treatment</label>
                    <br><?php echo $record['treatment'];?>
                </div>
               <!--  <div class="form-group col-md-12 col-lg-12">
                    <div class="modal-header"></div>
                </div> -->
                 <div class="form-group col-md-12 col-lg-12 ">
                    <label for="physioRemarks" class="control-label">Physio Remarks</label>
                    <br><?php echo $record['physio_remarks']; ?>
                </div>
                <!-- <div class="form-group col-md-12 col-lg-12">
                    <div class="modal-header"></div>
                </div> -->
                 <div class="form-group col-md-12 col-lg-12 ">
                    <label for="physioRemarks" class="control-label">S&C Coach Remarks</label>
                    <br><?php echo $record['scoach_remarks'];} ?>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>
