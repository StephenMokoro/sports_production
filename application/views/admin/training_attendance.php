<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Attendance</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Training Attendance</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
                <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <?php foreach($teams as $team){ ?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" > <?php  echo $team['team_name']; ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    
                    <?php $tableId="training_".$team['team_auto_id'];?>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                            <thead>
                                <tr>
                                    <th class="text-left">Record Date</th>
                                    <th class="text-center">Present</th>
                                    <th class="text-center">Absent</th>
                                    <th class="text-center">Excused</th>
                                    <th class="text-center">Physio</th>
                                    <th class="text-center">Count</th>
                                    <th class="text-center"><i class="fa fa-cog"></i></th>
                                 </tr>
                            </thead>
                            <tbody >
                               <?php $count=0; foreach($team['trainingattendance'] as $attendance){ 
                                   ?>
                                <tr>
                                    <td class="text-left"><?php  echo date_format(date_create($attendance['training_date']),"D j<\s\up>S</\s\up> M, Y"); ?></td>
                                    <td class="text-center"><?php echo $attendance['countPresent_'.$team['team_auto_id'].$count]; echo " (".number_format(($attendance['countPresent_'.$team['team_auto_id'].$count]/$attendance['countAll_'.$team['team_auto_id'].$count])*100, 1 )."%)"; ?></td>

                                    <td class="text-center"><span class="text-danger"><?php echo $attendance['countAbsent_'.$team['team_auto_id'].$count]; echo " (".number_format(($attendance['countAbsent_'.$team['team_auto_id'].$count]/$attendance['countAll_'.$team['team_auto_id'].$count])*100, 1 )."%)";?></td>

                                    <td class="text-center"><span class="text-warning"><?php echo $attendance['countExcused_'.$team['team_auto_id'].$count]; echo " (".number_format(($attendance['countExcused_'.$team['team_auto_id'].$count]/$attendance['countAll_'.$team['team_auto_id'].$count])*100, 1 )."%)";?></td>

                                    <td class="text-center"><span class="text-warning"><?php echo $attendance['countPhysio_'.$team['team_auto_id'].$count]; echo " (".number_format(($attendance['countPhysio_'.$team['team_auto_id'].$count]/$attendance['countAll_'.$team['team_auto_id'].$count])*100, 1 )."%)";?></td>
                                    <td class="text-center"><span class="text-info"><?php echo $attendance['countAll_'.$team['team_auto_id'].$count]; ?></td>

                                    <td class="text-center">

                                      <form style="display:inline;" name=<?php echo '"formMore_'. $team['team_auto_id'].'"'; ?> method="post" action="<?php echo base_url('admin/traininglist');?>">
                                          <div class="form-group col-md-12 col-lg-12" style="display:none">
                                              <label for="teamId" class="control-label">Team ID<span class="star">*</span></label>
                                              <input required="required" class="form-control" name="teamId" id="teamId" placeholder="" value="<?php echo $team['team_auto_id']; ?>">
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12" style="display:none">
                                              <label for="trainingDate" class="control-label">Training Date<span class="star">*</span></label>
                                              <input required="required" class="form-control" name="trainingDate" id="trainingDate" placeholder="" value="<?php echo $attendance['training_date']; ?>">
                                          </div>
                                          <button class="btn btn-success btn-s" data-title="View More" id=<?php echo '"more_'. $team['team_auto_id'].'"';  ?> name=<?php echo '"more_'. $team['team_auto_id'].'"';  ?>  type="submit" style="/*background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                      </form>
                                    </td>
                                </tr>
                                <?php $count=$count+1; } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <?php  echo  "<script>
                        $(document).ready(function () { 
                             //datatable initialization
                            $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
                                 'aoColumnDefs': [{'aTargets': [6], 'orderable': false}] }); 
                        });//close document.ready

                        </script>";?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php }?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
</body>
</html>
