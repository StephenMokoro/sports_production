<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Coach Uploads</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> General Uploads</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
                <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                 <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="sportslist"  >
                    <thead>
                        <tr style="display: none;">
                            <th class="text-left"></th>
                         </tr>
                    </thead>
                    <tbody style="color: #17202A  ;">
                      <?php foreach($sports as $sport){ ?>
                        <tr>
                            <td colspan=100% style="padding: 0px!important;margin: 0px!important; ">
                                <div class="box box-solid collapsed-box" style="background:lightgrey">
                                  <div class="box-header">
                                      <h3 class="box-title" style="color: #21618C;" > <?php  echo $sport['sport_name']; ?></h3>
                                      <div class="box-tools pull-right">
                                          <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                          <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                      </div>
                                  </div>
                                 <?php $tableId="sportlist_".$sport['sport_auto_id'];?>
                                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                                           <thead>
                                                <tr>
                                                    <th class="text-left">Report Name</th>
                                                    <th class="text-center text-success"> <i class="fa fa-cog"></i></th>
                                                 </tr>
                                            </thead>
                                            <tbody >
                                               <?php foreach($sport['sport_uploads'] as $file){ ?>
                                                <tr>
                                                    <td class="text-left"><?php
                                                     $file_ext=$file['file_ext'];
                                                        if($file_ext==".doc"||$file_ext==".docx")
                                                          {
                                                            $i='<span><i class="fa fa-file-word-o fa-lg" style="color:blue"></i></span>';
                                                          }else  if($file_ext==".pdf"||$file_ext==".PDF"){
                                                            $i='<span><i class="fa fa-file-pdf-o fa-lg" style="color:red"></i></span>';
                                                          }else  if($file_ext==".xls"||$file_ext==".xlsx"){
                                                            $i='<span><i class="fa fa-file-excel-o fa-lg" style="color:#33bbff"></i></span>';
                                                          }

                                                        echo  $i." &nbsp;".$file['upload_descriptive_name']; ?></td>
                                                    <td class="text-center" width="20%">
                                                        <span data-placement="top" data-toggle="tooltip" title="Summary">
                                                            <button class="btn btn-default btn-s" id="<?php  echo 'more_'. $file['upload_auto_id'];?>" name="<?php  echo 'more_'. $file['upload_auto_id'];?>" type="submit" style="background-color: #F5CBA7;color: #000000;"><span class ="fa fa-eye"></span> Summary</button>
                                                        </span>
                                                       
                                                        <span data-placement="top" data-toggle="tooltip" title="Open/Download">
                                                            <a class="btn btn-s btn-primary" type="button" href="<?php echo base_url();echo 'uploads/coach_uploads/coach_reports/'; echo $file['upload_file_name'];?>" target="_blank"><span class="fa fa-download"></span>&nbsp;Download</a>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <!-- /.table-responsive -->
                                          <?php  echo  "<script>
                                            $(document).ready(function () { 
                                                 //datatable initialization
                                                $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
                                                     'aoColumnDefs': [{'aTargets': [1], 'orderable': false}] }); 
                                            });//close document.ready

                                            </script>";?>

                                    </div><!-- /.box-body -->
                                </div>
                              <!-- /.box -->
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

                

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
  $(document).ready(function () { 
       //datatable initialization
      $('#sportslist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[] }); 
  });//close document.ready

</script>
</body>
</html>
