<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Passport Details</title>
<?php $this->load->view('headerlinks/headerlinks.php');?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span>Passport Details</b></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>

                 <?php foreach($passport_details as $pspt){

                   $photo=$pspt['passport_photo']; if($photo==""){$image="passportdefault.png";}else{$image=$pspt['passport_photo'];}
                   ?>
                            <div class="row setup-content" >
                                <div class="col-xs-12">
                                    <div class="col-md-6" >
                                      <div class="col-md-11 col-md-offset-1">
                                          <div class="form-group" style="text-align: center;">
                                            <div class="main-img-preview">

                                              <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/travel_documents/'.$image?>" alt="Passport Image" width="430" height="230">

                                              <b><p style="color: #566573;text-transform: ;margin-top: 10px;"><?php echo $pspt['player_fname']." ".$pspt['player_lname'];?> Passport</p></b>
                                            <p><i class="fa fa-globe text-success fa-1x"></i> <?php echo $pspt['issue_country'];?>  <span><cite title="Country Issued "><small style="display: inline">Country Issued </small></cite></span> </p>
                                            </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                       
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="passportNo" class="control-label" style="margin-top: 10px;">Passport Number </label>
                                            <input type="text" name="passportNo" placeholder="" class=" form-control" id="passportNo" required="required" maxlength="30" value=<?php echo '"'.$pspt['passport_number'].'"';?> readonly="true">
                                        </div>
                                         <div class='col-md-12'>
                                            <label for="dateOfIssue" class="control-label">Date of Issue</label>
                                            <div class="form-group">
                                                <input type='text' class="form-control" readonly="true" name="dateOfIssue" value="<?php echo date_format(date_create($pspt['issue_date']),'d  M, Y');?>" >
                                            </div>
                                        </div>
                                           
                                        <div class='col-md-12'>
                                            <label for="dateOfExpiry" class="control-label">Date of Expiry</label>
                                            <div class="form-group">
                                                <input type='text' class="form-control" readonly="true" name="dateOfExpiry" value="<?php echo date_format(date_create($pspt['expiry_date']),'d  M, Y');?>" >
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label class="control-label"> Country of Issue  </label>
                                            <input type="text" id="issueCountry" name="issueCountry" placeholder="" class=" form-control" readonly="readonly" value=<?php echo '"'.$pspt['issue_country'].'"';?>>
                                        </div>  
                                    </div>
                                </div><!--/.col-xs-12-->
                            </div><!--/.setup-content-->
                      <?php }?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>
