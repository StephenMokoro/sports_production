<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Training Days</title>
  <?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Training Days</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
                <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <?php foreach($teams as $team){ ?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" > <?php  echo $team['team_name']; ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <?php $tableId="dayslist_".$team['team_auto_id'];?>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                            <thead>
                              <tr>
                              <!-- <tr  style="background:#374850;color: #FFFFFF;"> -->
                                  <th class="text-center">Year</th>
                                  <th class="text-center">Jan</th>
                                  <th class="text-center">Feb</th>
                                  <th class="text-center">Mar</th>
                                  <th class="text-center">Apr</th>
                                  <th class="text-center">May</th>
                                  <th class="text-center">Jun</th>
                                  <th class="text-center">Jul</th>
                                  <th class="text-center">Aug</th>
                                  <th class="text-center">Sep</th>
                                  <th class="text-center">Oct</th>
                                  <th class="text-center">Nov</th>
                                  <th class="text-center">Dec</th>
                                  <th class="text-center"><i class="fa fa-cog"></i></th>
                               </tr>
                          </thead>
                          <tbody >
                              <?php foreach($team['tr_days'] as $tr_day){ 
                                 ?>
                              <tr>
                                  <td class="text-center"><?php  echo $tr_day['trd_year']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['january']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['february']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['march']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['april']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['may']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['june']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['july']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['august']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['september']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['october']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['november']; ?></td>
                                  <td class="text-center"><?php  echo $tr_day['december']; ?></td>
                                  
                                  <td class="text-center">
                                  <form style="display:inline;" name=<?php echo '"formEdit_'. $tr_day['trd_auto_id'].'"';  ?> method="post" action="<?php echo base_url('admin/editdays');?>">
                                      <div class="form-group col-md-12 col-lg-12" style="display:none">
                                          <label for="trdId" class="control-label">Trds ID*</label>
                                          <input required="required" class="form-control" name="trdId" id="trdId" placeholder="" value="<?php echo $tr_day['trd_auto_id']; ?>">
                                      </div>
                                      <button class="btn btn-primary btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'.$tr_day['trd_auto_id'].'"';  ?> name=<?php echo '"edit_'.$tr_day['trd_auto_id'].'"';  ?>  type="submit" ><span class="fa fa-edit fa-fw"></span> Edit</button>
                                  </form>
                                  </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <?php  echo  "<script>
                        $(document).ready(function () { 
                             //datatable initialization
                            $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
                                 'aoColumnDefs': [{'aTargets': [13], 'orderable': false}] }); 
                        });//close document.ready

                        </script>";?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php }?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>
