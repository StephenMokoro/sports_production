<!DOCTYPE html>
<html>
<head>
  <title>Sports | Game Sheet</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
<style>
@media (max-width:767px){input{display: block!important;margin-bottom: 5px!important;width: 100%!important;} 
   }
@media (min-width:768px){ input{display: block!important;margin-bottom: 5px!important;width: 100%!important; }

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php  $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row ">
            <div class="col-lg-12 ">
                <div class="pull-right">
                     <span data-placement="top" data-toggle="tooltip" title="Back to Matches" >
                        <a href="<?php echo base_url('admin/tournmatches');?>" class="btn btn-s" data-title="Back to Matches " style="text-decoration: none;color: #000000;" ><span class="fa fa-mail-reply "></span>&nbsp;Back
                        </a>
                    </span>
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span><span style="color: ;font-weight: normal;color: #512E5F;font-size: 14px!important;"> <?php echo $this->session->userdata('matchDetails');?></span></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                  <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <br>
                <div class="col-lg-12 col-md-12 text-center" id="success" style="display: none;">
                     
                </div>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="playerlist">
                    <thead>
                        <tr style="display: none;">
                            <th class="text-left text-primary" style=""></th>
                         </tr>
                    </thead>
                    <tbody style="color: #17202A  ;">
                     <?php foreach($players as $player){ ?>
                        <tr>
                            <td colspan=100% style="padding: 0px!important;margin: 0px!important; ">
                                 <div class="box box-solid collapsed-box" style="background: #5D6D7E;">
                                    <div class="box-header">
                                        <h3 class="box-title" style="color: #FFFFFF;;;font-size: 14px!important;">
                                            <?php $photo=$player['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$player['player_profile_photo'];}?>
                                                <img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt="User Image">  <?php  echo $player['player_fname']. " ".$player['player_lname'];?></h3>
                                        <div class="box-tools pull-right clicked" >
                                            <button class="btn btn-default btn-sm " data-widget="collapse"><i class="fa fa-plus" style="color: #000000;"></i></button>
                                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                        </div>
                                    </div>
                                    <div style="background-color: #FFFFFF;color: #000000;" class="box-body">
                                      <form method="post" action="<?php echo base_url(); ?>admin/newhockeyscore">
                                        <div class="col-md-2 col-sm-12" style="display: none;">
                                            <input type="number" name="playerId" class="form-control" id="scores" placeholder="Player Id" style="color: #000000;text-align: center;" value="<?php echo $player['player_auto_id']; ?>" >
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <input type="number" name="scores" class="form-control" id="scores" placeholder="Score" min="0" style="color: #000000;text-align: center;" >
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <input type="number" name="greenCards" class="form-control" id="greenCards" placeholder="0" min="0" style="background-color: #229954;color: #000000;text-align: center;" >
                                        </div>
                                        <div class="col-md-2 col-sm-12" >
                                            <input type="number" name="yellowCards" class="form-control" id="yellowCards" placeholder="0" min="0" style="background-color: #F1C40F;color: #000000;text-align: center;" >
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                             <input type="number" name="redCards" class="form-control" id="redCards" placeholder="0" min="0" style="background-color: #800000;color: #000000;text-align: center;" >
                                        </div>
                                         <div class="col-md-2 col-sm-12">
                                            <button type="submit" name="submit_"<?php  echo $player['player_auto_id'];?> class="btn btn-primary" id="submit_"<?php  echo $player['player_auto_id'];?> style="width: 100%;">Submit</button>
                                        </div>
                                      </form>
                                    </div><!-- /.box-body -->
                                </div>
                              <!-- /.box -->
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
  $('#playerlist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]});
});

</script>
</body>
</html>
