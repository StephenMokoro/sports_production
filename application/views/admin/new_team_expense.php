<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Expenditures</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> >
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> New Expenditure</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body" >
                <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <!-- /.box -->
                <?php foreach($teams as $team){ ?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" > <?php  echo $team['team_name']; ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <?php $tableId="injurylist_".$team['team_auto_id'];?>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <?php echo form_open_multipart('admin/newexpenditure',array('id' => 'newExpenditure','method'=>'post'));?>
                            <div class="row setup-content" >
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                        <label for="teamId" class="control-label">#Team Id<span class="star">*</span></label>
                                        <input type="text" name="teamId" class="form-control" id="teamId" required="required" value="<?php echo $team['team_auto_id']; ?>" readonly="true">
                                    </div>
                                                                    
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="cash" class="control-label">Cash (Kshs)<span class="star">*</span></label>
                                        <input type="number" name="cash" placeholder="1975.00" class="form-control" id="cash" required="required" step="0.01" min="0">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="lpoNo" class="control-label">LPO No.</label>
                                        <input type="number" name="lpoNo" placeholder="102" class=" form-control" id="lpoNo"  min="1">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="lpo" class="control-label">LPO (Kshs)</label>
                                        <input type="number" name="lpo" placeholder="2000.00" class=" form-control" id="lpo" step="0.01" min="0">
                                    </div>
                                     <div class="form-group col-md-6 col-lg-6">
                                        <label for="actualCost" class="control-label">Actual Costs (Kshs)<span class="star">*</span></label>
                                        <input type="number" name="actualCost" placeholder="575.50" class=" form-control" id="actualCost" required="required" step="0.01" min="0">
                                    </div>
                                    <div class='col-md-6 col-lg-6'>
                                      <label for="expenseDate" class="control-label">Expenditure Date<span class="star">*</span></label>
                                      <div class="form-group">
                                          <div class='input-group date' id='expenseDate'>
                                              <input type='text' class="form-control" readonly="true" name="expenseDate" required="required" />
                                              <span class="input-group-addon">
                                                  <span class="fa fa-calendar"></span>
                                              </span>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="receipt" class="control-label">Receipt(s)</label>
                                        <input type="file" name="receipt" placeholder="" class=" form-control" id="receipt">
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12">
                                        <label for="comments" class="control-label"> Comments<span class="star">*</span></label>
                                        <textarea type="text" name="comments" placeholder="Fare and Lunch" class=" form-control" id="comments" required="required" style="min-height: 40px;resize: none;overflow:hidden;" maxlength="100"></textarea>
                                    </div>
                                    
                                   
                                    <div class="form-group col-md-12 col-lg-12">
                                    <!-- <div class="modal-header"></div> -->
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                        <input type="reset" class="btn btn-default" value="Reset">
                                    </div>
                              </div>
                          </div>
                      <?php echo form_close();?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <?php }?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () { 
     //datatable initialization
    $('#injurylist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],'aaSorting':[],
         'aoColumnDefs': [{ 'aTargets': [0],'bSortable':false, 'orderable': false},{'aTargets': [4], 'orderable': false}] }); 
});//close document.ready
    $(function () {$('#expenseDate').datepicker({format: 'yyyy-mm-dd',minDate:new Date(),todayHighlight: true});});

</script>
</body>
</html>
