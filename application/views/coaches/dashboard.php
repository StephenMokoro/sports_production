<!DOCTYPE html>
<html>
<head>
 
  <title>Sports | Dashboard</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
  
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Home</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <!-- Small boxes (Stat box) -->
              <div class="row">
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #85929E; color: #FFFFFF;">
                    <div class="inner">
                      <h3 class="counter"><?php echo $activeplayerscount;  ?></h3>
                      <p>Total Players</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-users text-info"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #808080;color: #FFC0CB;">
                    <div class="inner">
                      <h3 class="counter"><?php echo $allactiveplayersonphysio; ?><sup style="font-size: 20px"></sup></h3>
                      <p>Physio</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-user-circle-o text-warning"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #B2BABB;color: #34495E;">
                    <div class="inner">
                      <h3 class="counter"><?php echo $allinjuriestodatecount; ?></h3>
                      <p>Injuries</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-medkit text-danger"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #A3E4D7;">
                    <div class="inner">
                      <h3 class="counter"><?php echo $allactiveplayersnotonphysio; ?></h3>
                      <p>Players fit to play</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-check-circle text-success"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/jquery/jquery-1.10.2/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/jquery/jquery-1.10.2/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/waypoints/2.0.3/waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/counterup/jquery.counterup.min.js"></script>

<script>
  $(document).ready(function () {
     //counter on dashboard
     $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
  });
  //to refresh the page
  $( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>