<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Training Days</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 

</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Training Days</h4>
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <div class="box box-solid collapsed-box" style="background:lightgrey">
                <div class="box-header">
                  <h3 class="box-title" style="color: #21618C;" >New Training Days</h3>
                  <div class="box-tools pull-right">
                      <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                      <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                  </div>
                </div>
                <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                  <div class="modal-body">
                     <?php echo form_open('coach/newdays',array('id' => 'newdays','method'=>'post'));?>
                      <div class="row setup-content" >
                          <div class="col-sm-12">
                                <div class="form-group col-md-12 col-lg-12">
                                  <label for="teamId" class="control-label"> Team <span class="star">*</span></label>
                                  <select type="text" name="teamId" class=" form-control" id="teamId" required="required">
                                      <option value="">--Select Team--</option>
                                    <?php  foreach($teams as $team){ 
                                          ?>
                                      <option value=<?php  echo '"'.$team['team_auto_id'].'"';?>><?php  echo $team['team_name'];}?></option>
                                  </select>
                              </div>
                              <div class="form-group col-md-2 col-lg-2">
                                  <label for="january" class="control-label">January*</label>
                                  <input type="number" name="january" required="required" placeholder="" class="form-control" id="january" max="31" min="0" >
                              </div>
                              <div class="form-group col-md-2 col-lg-2">
                                  <label for="february" class="control-label">February*</label>
                                  <input type="number"  name="february" required="required" placeholder="" class="form-control" id="february" max="29" min="0" >
                              </div>
                               <div class="form-group col-md-2 col-lg-2">
                                  <label for="march" class="control-label">March*</label>
                                  <input type="number" name="march"required="required" placeholder="" class="form-control" id="march" max="31" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="april" class="control-label">April*</label>
                                  <input type="number"  name="april" required="required" placeholder="" class="form-control" id="april" max="30" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="may" class="control-label">May*</label>
                                  <input type="number" name="may" required="required" placeholder="" class="form-control" id="may" max="31" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="june" class="control-label">June*</label>
                                  <input type="number" name="june" required="required" placeholder="" class="form-control" id="june" max="30" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="july" class="control-label">July*</label>
                                  <input type="number" required="required" name="july" placeholder="" class="form-control" id="july" max="31" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="august" class="control-label">August*</label>
                                  <input type="number" name="august" required="required" placeholder="" class="form-control" id="august" max="31" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="september" class="control-label">September*</label>
                                  <input type="number" name="september" required="required" placeholder="" class="form-control" id="september" max="30" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="october" class="control-label">October*</label>
                                  <input type="number" name="october" required="required"  placeholder="" class="form-control" id="october" max="31" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="november" class="control-label">November*</label>
                                  <input type="number" name="november" required="required" placeholder="" class="form-control" id="november" max="30" min="0" >
                              </div>
                             <div class="form-group col-md-2 col-lg-2">
                                  <label for="december" class="control-label">December*</label>
                                  <input type="number" name="december"  required="required" placeholder="" class="form-control" id="december" max="31" min="0" > 
                              </div>
                              <div class="form-group col-md-12 col-lg-12">
                                  <input type="submit" class="btn btn-primary" value="Submit">
                                  <input type="reset" class="btn btn-default" value="Reset">
                              </div>
                          </div>
                      </div>
                    <?php echo form_close();?>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
              <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="tr_days_list"  >
                    <thead>
                        <tr>
                        <!-- <tr  style="background:#374850;color: #FFFFFF;"> -->
                            <th class="text-center">Year</th>
                            <th class="text-center">Team</th>
                            <th class="text-center">Jan</th>
                            <th class="text-center">Feb</th>
                            <th class="text-center">Mar</th>
                            <th class="text-center">Apr</th>
                            <th class="text-center">May</th>
                            <th class="text-center">Jun</th>
                            <th class="text-center">Jul</th>
                            <th class="text-center">Aug</th>
                            <th class="text-center">Sep</th>
                            <th class="text-center">Oct</th>
                            <th class="text-center">Nov</th>
                            <th class="text-center">Dec</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                         </tr>
                    </thead>
                    <tbody >
                        <?php foreach($tr_days as $tr_day){ 
                           ?>
                        <tr>
                            <td class="text-center"><?php  echo $tr_day['trd_year']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['team_alias']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['january']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['february']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['march']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['april']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['may']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['june']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['july']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['august']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['september']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['october']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['november']; ?></td>
                            <td class="text-center"><?php  echo $tr_day['december']; ?></td>
                            
                            <td class="text-center">
                                <form style="display:inline;" name=<?php echo '"formEdit_'. $tr_day['trd_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/editdays');?>">
                                      <div class="form-group col-md-12 col-lg-12" style="display:none">
                                          <label for="trdId" class="control-label">Trds ID*</label>
                                          <input required="required" class="form-control" name="trdId" id="trdId" placeholder="" value="<?php echo $tr_day['trd_auto_id']; ?>">
                                      </div>
                                      <button class="btn btn-primary btn-s" data-placement="top" data-toggle="tooltip" title="Edit Record" id=<?php echo '"edit_'.$tr_day['trd_auto_id'].'"';  ?> name=<?php echo '"edit_'.$tr_day['trd_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;"><span class="fa fa-edit fa-fw"></span> Edit</button>
                                  </form>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    //datatable initialization
    //define datatable
     var table=$('#tr_days_list').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],'aaSorting':[],
         "aoColumnDefs": [{"bSortable":false, "aTargets": [14],"bSearchable": false,"sWidth":"10%" }]
   });
      var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
  });
</script>
</body>
</html>
