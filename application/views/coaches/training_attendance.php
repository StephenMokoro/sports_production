<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Training Attendance</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Training Attendance</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh
                        </button>
                    </span>                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Attendance</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <div class="messagebox alert alert-success" style="display: none;" id="success">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-check"></i>
                                <strong><span>Attendance Recorded</span></strong>
                            </div> 
                        </div>
                        <div class="messagebox alert alert-danger" style="display: none;" id="isEmpty">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-info-circle"></i>
                                <strong><span>No attendance selected</span></strong>
                            </div> 
                        </div>
                        <div class="messagebox alert alert-info" style="display: none;" id="error">
                            <button type="button" class="close" data-dismiss="alert">*</button>
                            <div class="cs-text">
                                <i class="fa fa-close"></i>
                                <strong><span>An error occurred</span></strong>
                            </div> 
                        </div>
                        <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="list"  >
                            <thead>
                                <tr>
                                    <th class="text-left">Full Name</th>
                                    <th class="text-center" id="pid">Player PID</th>
                                    <th class="text-center">Present</th>
                                    <th class="text-center">Absent</th>
                                    <th class="text-center">Excused</th>
                                    <th class="text-center">Physio</th>
                                 </tr>
                            </thead>
                            <tbody >
                               <?php foreach($players as $player){ 
                                   ?>
                                <tr>
                                    <?php $photo=$player['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$player['player_profile_photo'];}?>
                                  <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt="">  <?php  echo $player['player_fname']. " ".$player['player_lname'];?></td>
                                    <td class="text-left"><?php  echo $player['player_auto_id']; ?></td>

                                    <td class="text-center"><span class="text-success">PRESENT <input  type="radio" name=<?php echo '"player_'. $player['player_auto_id'].'"';  ?> value="PRESENT" ></span> &nbsp;&nbsp;</td>

                                    <td class="text-center"><span class="text-danger">ABSENT <input type="radio" name=<?php echo '"player_'. $player['player_auto_id'].'"';  ?> value="ABSENT"></span>&nbsp;&nbsp;</td>

                                    <td class="text-center"><span class="text-info">EXCUSED <input type="radio" name=<?php echo '"player_'. $player['player_auto_id'].'"';  ?> value="EXCUSED"></span> &nbsp;&nbsp;</td>

                                    <td class="text-center"><span class="text-primary">PHYSIO <input type="radio" name=<?php echo '"player_'. $player['player_auto_id'].'"';  ?> value="PHYSIO"></span>  </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                      <br>
                      <input type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit">
                      <!-- /.table-responsive -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="attendancelist">
                  <thead>
                      <tr>
                          <th class="text-left">Record Date</th>
                          <th class="text-center">Present</th>
                          <th class="text-center">Absent</th>
                          <th class="text-center">Excused</th>
                          <th class="text-center">Count</th>
                          <th class="text-center"><i class="fa fa-cog"></i></th>
                       </tr>
                  </thead>
                  <tbody >
                     <?php $counter=1;foreach($trainingattendance as $attendance){ 
                         ?>
                      <tr>
                          <td class="text-left"><?php  echo date_format(date_create($attendance['training_date']),"D j<\s\up>S</\s\up> M, Y"); ?></td>
                          <td class="text-center"><?php echo $attendance['countPresent']; echo " (".number_format(($attendance['countPresent']/$attendance['countAll'])*100, 1 )."%)"; ?></td>

                          <td class="text-center"><span class="text-danger"><?php echo $attendance['countAbsent']; echo " (".number_format(($attendance['countAbsent']/$attendance['countAll'])*100, 1 )."%)";?></td>

                          <td class="text-center"><span class="text-warning"><?php echo $attendance['countExcused']; echo " (".number_format(($attendance['countExcused']/$attendance['countAll'])*100, 1 )."%)";?></td>
                          <td class="text-center"><span class="text-info"><?php echo $attendance['countAll']; ?></td>

                          <td class="text-center">
                            <form style="display:inline;" method="post" action="<?php echo base_url('coach/traininglist');?>">
                                   <div class="form-group col-md-12 col-lg-12" style="display:none">
                                      <label for="trainingDate" class="control-label">Training Date<span class="star">*</span></label>
                                      <input required="required" class="form-control" name="trainingDate" id="trainingDate" placeholder="" value="<?php echo $attendance['training_date']; ?>">
                                  </div>
                                  <button class="btn btn-default btn-s" data-title="View More"  type="submit" style="/*background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                              </form>
                          </td>
                      </tr>
                      <?php $counter=$counter + 1; } ?>
                  </tbody>
              </table>
              <!-- /.table-responsive -->
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>

$(document).ready(function () {
    $('#attendancelist').dataTable({responsive:true, "aoColumnDefs": [{ "aTargets": [5], "orderable": false},{ "aTargets":1,responsivePriority:1}],'aaSorting':[]});
    $('#list').dataTable({paging:false,responsive:true, "aoColumnDefs": [{ "aTargets": [2,3,4,5], "orderable": false},{ "aTargets":1,responsivePriority:1}],'aaSorting':[]});
     $( "#submit").on('click', function()
        {
            var selected = [];
            $('#list input:radio:checked').each(function() {
               var $item = $(this);
                selected.push({
                    // id: $item.attr("id"),
                   playerId: $('td:nth-child(2)', $(this).parents('tr')).text(),
                     attendanceStatus: $item.val()
                });
            });
            // alert(JSON.stringify(selected));
               $.ajax(//ajax script to post the data without page refresh
                {
                    type:"post",
                    url: "<?php echo base_url();?>coach/newTraining",
                    dataType: "json",//note the contentType defintion
                    data: {selected:selected},
                    
                    success:function(data)
                    {
                        if(data.success) {
                              $("#success ").fadeTo(2000, 500).fadeOut("slow");
                              // window.setTimeout(function(){location.reload()},5000);
                            $("#attendancelist").load(window.location + " #attendancelist");//reload table on success
                            $("#list").load(window.location + " #list");//reload table on success
                            state="false";
                            }else if(data.error){
                                $("#error").fadeTo(2000, 500).fadeOut("slow");
                            }else if(data.empty){
                                $("#isEmpty").fadeTo(2000, 500).fadeOut("slow");
                            }
                    }
                });
             
        });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
