<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | <?php echo $_SESSION['sessdata']['tournamentTeamName'];?> Tornaments</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
     <section class="content-header">
        <div class="row">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> <?php echo $_SESSION['sessdata']['tournamentTeamName'];?> Tournaments</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-xs" data-title="Refresh "  id="refresh" style="text-decoration: none;color: #000000;"><span class="fa fa-refresh"></span>&nbsp;Refresh
                        </button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-xs" data-title="Print All" type="button" href="#" style="text-decoration: none;color: #000000;"><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Tournament</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                          <?php echo form_open('coach/newtournament',array('id' => 'tournament_registration','method'=>'post'));?>
                           <div class="col-md-12" >
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for="tournamentTitle" class="control-label">Tournament Title<span class="star">*</span></label>
                                    <input type="text" name="tournamentTitle" placeholder="KUSA Open" class="form-control" id="tournamentTitle" required="required" maxlength="80">
                                </div>
                                <div class='col-md-12'>
                                    <label for="startDate" class="control-label">Start Date</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='startDate'>
                                            <input type='text' class="form-control" readonly="true" name="startDate" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class='col-md-12'>
                                    <label for="endDate" class="control-label">End Date</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='endDate'>
                                            <input type='text' class="form-control" readonly="true" name="endDate" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 col-lg-12">
                                    <br>
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                    <input type="reset" class="btn btn-default" value="Reset">
                                </div>
                            </div>
                          <?php echo form_close();?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                 <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="tournamentslist"  >
                     <thead>
                                <tr>
                                   <th class="text-left">Tournament Dates</th>
                                    <th class="text-left">Tournament Title</th>
                                    <th class="text-center"></th>
                                 </tr>
                            </thead>
                           <tbody >
                               <?php foreach($tournaments as $tournament){ 
                                   ?>
                                <tr>
                                    <td class="text-left"><?php  echo date_format(date_create($tournament['game_start_date']),"j<\s\up>S</\s\up> M, Y")." - ".date_format(date_create($tournament['game_end_date']),"j<\s\up>S</\s\up> M, Y"); ?></td>
                                    <td class="text-left"><?php  echo $tournament['game_title']; ?></td>
                                    <td class="text-center">
                                        <form style="display:inline;" name=<?php echo '"formMatch_'. $tournament['game_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/tournmatches');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="teamId" class="control-label">Team ID*</label>
                                                <input required="required" class="form-control" name="teamId" id="teamId" value="<?php echo $tournament['team_auto_id']; ?>">
                                            </div>
                                             <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="gameId" class="control-label">Tournament ID*</label>
                                                <input required="required" class="form-control" name="gameId" id="gameId" value="<?php echo $tournament['game_auto_id']; ?>">
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="tournamentTitle" class="control-label">Tournament Title*</label>
                                                <input required="required" class="form-control" name="tournamentTitle" id="tournamentTitle" value="<?php echo $tournament['game_title']; ?>">
                                            </div>
                                            <button class="btn btn-default btn-s" data-title="Add Match" id=<?php echo '"newmatch_'. $tournament['game_auto_id'].'"';  ?> name=<?php echo '"newmatch_'. $tournament['game_auto_id'].'"';  ?>  type="submit" style="background-color: ;color: ;"> <span class="fa fa-bars"></span> Matches </button>
                                        </form>
                                        
                                        <button class="btn btn-primary btn-s" data-title="Edit Summary" id=<?php echo '"editSummary_'. $tournament['game_auto_id'].'"';  ?> name=<?php echo '"editSummary_'. $tournament['game_auto_id'].'"';  ?> value=<?php echo '"'. $tournament['game_auto_id'].'"';  ?> type="submit" style="/*background-color: #C0C0C0;color: #FFFFFF;" onclick="tournedit(this);"> <span class="fa fa-edit"></span> Edit TMT</button>
                                        <?php $summaryExists=$tournament['game_summary']; if($summaryExists==""){?>
                                       <button class="btn btn-default btn-s" data-title="Add Summary" id=<?php echo '"summary_'. $tournament['game_auto_id'].'"';  ?> name=<?php echo '"summary_'. $tournament['game_auto_id'].'"';  ?> value=<?php echo '"'. $tournament['game_auto_id'].'"';  ?>  type="submit" style="background-color: #F5CBA7;color: #000000;" value=<?php echo '"'. $tournament['game_auto_id'].'"';?> onclick="addsummary(this);"> <span class="fa fa-plus-circle"></span> Summary </button>
                                       <?php }else{?>
                                         <button class="btn btn-default btn-s" data-title="Edit Summary" id=<?php echo '"summaryEdit_'. $tournament['game_auto_id'].'"';  ?> name=<?php echo '"summaryEdit_'. $tournament['game_auto_id'].'"';  ?> value=<?php echo '"'. $tournament['game_auto_id'].'"';  ?>  type="submit" style="background-color: #F5CBA7;color: #000000;" value=<?php echo '"'. $tournament['game_auto_id'].'"';?> onclick="editsummary(this);"> <span class="fa fa-edit"></span> Summary </button>
                                      <?php }?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
         <div class="modal fade" id="addsummary" >
                  <div class="modal-dialog" style="width: 80%;margin-left:10%; margin-right: 10%;">
                    <div class="modal-content" >
                      <form method="post" action="<?php echo base_url(); ?>admin/newtournsummary">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="teamName"></h4>
                          </div>
                          <div  id="infobody" >
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success  pull-left">Submit</button>
                          </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="editsummary" >
                  <div class="modal-dialog" style="width: 80%;margin-left:10%; margin-right: 10%;">
                    <div class="modal-content" >
                      <form method="post" action="<?php echo base_url(); ?>admin/updatetournsummary">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="teamName"></h4>
                          </div>
                          <div  id="infobody" >
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success  pull-left">Save Changes</button>
                          </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                  <div class="modal fade" id="edittournament" >
                  <div class="modal-dialog">
                    <div class="modal-content" >
                      <form method="post" action="<?php echo base_url(); ?>admin/updatetournament">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="teamName"></h4>
                          </div>
                          <div  id="infobody" >
                            <!-- json html goes here -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success  pull-left">Save Changes</button>
                          </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     $('#tournamentslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],columnDefs: [{ orderable: false, targets: [2] },{targets: [1],render: function ( data, type, row ) {return type === 'display' && data.length > 15 ? data.substr( 0,15 ) +'<small>...</small>' : data;} }], "aaSorting": []
   });  
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});

 $(function () {$('#startDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});$('#endDate').datepicker({format: "yyyy-mm-dd",todayHighlight: true});});
function addsummary(objButton)
  {
      var gameId=objButton.value;
      var gameType="1";
      var action="add";
       $.ajax({type:"post", url: "<?php echo base_url();?>coach/getgameinfo",data:{ gameId:gameId,gameType:gameType,action:action},dataType:'json',success:function(data){$('#addsummary #teamName').text(data.teamName);$('#addsummary #infobody').html(data.info);$('#addsummary').modal('toggle');} });
  }
  function editsummary(objButton)
  {
      var gameId=objButton.value;
      var gameType="1";
      var action="summaryedit";
       $.ajax({type:"post", url: "<?php echo base_url();?>coach/getgameinfo",data:{ gameId:gameId,gameType:gameType,action:action},dataType:'json',success:function(data){$('#editsummary #teamName').text(data.teamName);$('#editsummary #infobody').html(data.info);$('#editsummary').modal('toggle');} });
  }
    function tournedit(objButton)
  {
      var gameId=objButton.value;
      var gameType="1";
      var action="tournedit";
       $.ajax({type:"post", url: "<?php echo base_url();?>coach/getgameinfo",data:{ gameId:gameId,gameType:gameType,action:action},dataType:'json',success:function(data){$('#edittournament #teamName').text(data.teamName);$('#edittournament #infobody').html(data.info);$('#edittournament').modal('toggle');} });
  }
</script>
</body>
</html>
