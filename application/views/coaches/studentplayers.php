<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Student Players</title>
<?php $this->load->view('headerlinks/headerlinks.php');?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Student Players</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">

                  <table  class="table table-striped table-hover display responsive nowrap" cellspacing="0" width="100%" id="plslist">
                    <thead>
                        <tr>
                            <th class="text-left">Full Name</th>
                            <th class="text-left">Phone Number</th>
                            <th class="text-left">Team</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                         </tr>
                    </thead>
                    <tbody >
                       <?php foreach($players as $player){ 
                           ?>
                        <tr>
                            <?php $photo=$player['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$player['player_profile_photo'];}?>
                          <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt=""> <?php  echo $player['player_fname']. " ".$player['player_lname']; ?></td>
                            <td class="text-left"><?php  echo $player['player_phone']; ?></td>
                            <td class="text-left"><?php  echo $player['team_name']; ?></td>
                            <td class="text-center">
                                <form style="display:inline;" name=<?php echo '"formMore_'. $player['player_auto_id'].'"'; ?> method="post" action="<?php echo base_url('coach/playerprofile');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-default btn-s" title="View More" id=<?php echo '"more_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"more_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                </form>
                                <form style="display:inline;" name=<?php echo '"formEdit_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/editplayer');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-primary btn-s" title="Edit Player" id=<?php echo '"edit_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"edit_'. $player['player_auto_id'].'"';  ?>  type="submit" style="/*background-color:#C0C0C0;color:#FFFFFF;"><span class="fa fa-edit"></span> Edit </button>
                                </form>
                                <form style="display:inline;" name=<?php echo '"formNxt_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/nextofkin');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-success btn-s" title="View Next of Kin" id=<?php echo '"kinMore_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"kinMore_'. $player['player_auto_id'].'"';  ?>  type="submit" style="/*background-color:#808080;color:#FFFFFF;"> <span class="fa fa-eye"></span> Kin </button>
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    //datatable initialization
     $('#plslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],'aaSorting':[],
         "aoColumnDefs": [{"aTargets": [3], "orderable": false}]
      });
  });
//to refresh the page
$( "#refresh").click( function(event){window.setTimeout(function(){location.reload()},1)});
</script>
</body>
</html>
