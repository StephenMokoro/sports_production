<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Tournaments</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
<script src="<?php echo base_url();?>assets/jquery/dist/jquery.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <div class="pull-right">
                    <!--  <span data-placement="top" data-toggle="tooltip" title="Back to Tournaments">
                        <a href="<?php //echo base_url('coach/').strtolower($_SESSION['sessdata']['tournamentTeamCategory']).'tournaments';?>" class="btn btn-xs" title="Back to Tournaments " style="text-decoration: none;color: #000000;"><span class="fa fa-mail-reply"></span>&nbsp;Back
                        </a>
                    </span> -->
                </div> 
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span><span style="color: ;font-weight: normal;color: #512E5F;font-size: 14px!important;"> <?php echo $_SESSION['sessdata']['tournamentTeamName'].' '.$_SESSION['sessdata']['tournamentTitle'];?> Tournament</span></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Match</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <?php echo form_open('coach/newtournmatch',array('id' => 'tournament_match_registration','method'=>'post'));?>
                        <div class="row setup-content" >
                            <div class="col-md-12">
                                

                                
                                <div class="form-group col-md-6 col-lg-6">
                                    <label for="matchLevel" class="control-label">Game Level <span class="star">*</span></label>
                                    <select type="text" name="matchLevel" class=" form-control" id="matchLevel" required="required">
                                        <option value="">--Select Level--</option>
                                        <option value="Preliminaries">Preliminaries</option>
                                        <option value="Quarters">Quarters</option>
                                        <option value="Semis">Semis</option>
                                        <option value="Finals">Finals</option>
                                    </select>
                                </div>
                                 <div class="form-group col-md-6 col-lg-6">
                                    <label for="matchVenue" class="control-label green">Match Venue</label>
                                    <input type="text" name="matchVenue" placeholder="" class="form-control" id="matchVenue" required="required">
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    <label for="matchOpponents" class="control-label green">Opponents</label>
                                    <input type="text" name="matchOpponents" placeholder="" class="form-control" id="matchOpponents" required="required">
                                </div>
                                <div class='col-md-6 col-lg-6'>
                                    <label for="matchDate" class="control-label">Start Date</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='matchDate'>
                                            <input type='text' class="form-control" readonly="true" name="matchDate" required="true" value="<?php echo date('Y-m-d'); ?>" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    <label>Start Time</label>
                                     <div class='input-group time' id='matchStartTime'>
                                        <input class="form-control" placeholder="9:15 AM" name="matchStartTime" required="required" autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-12 col-lg-12">
                                    <table id="list" class="table table-bordred table-striped">
                                       <thead>
                                            <th class="text-left" style="font-weight: normal;color: #800000;"><input type="checkbox" id="checkall"/> &nbsp;&nbsp;Select All</th>
                                            <th class="text-center" id="pid">Player PID</th>
                                        </thead>
                                        <tbody>
                                            <?php foreach($players as $player){ 
                                                   ?>
                                                <tr>
                                                    <td class="text-left"><input type="checkbox" name="playerList[]" class="checkthis" value="<?php  echo $player['player_auto_id']; ?>"  /> &nbsp;&nbsp;<?php  echo $player['player_fname']. " ".$player['player_lname'];?></td>
                                                    <td class="text-left"><?php  echo $player['player_auto_id']; ?></td><!--td not used. Important when submiting via ajax-->
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>

                                    <button id="submitmatch" class="btn btn-primary" style="margin-top: 10px" >Submit</button>
                                </div><!--/.form-group-->
                            </div><!--/.col-md-12-->
                        </div><!--/.setup-content-->
                        <?php echo form_close();?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
              
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="matchlist"  >
                    <thead>
                        <tr style="display: none;">
                            <th class="text-left"></th>
                         </tr>
                    </thead>
                    <tbody style="color: #17202A  ;">
                       <?php   foreach($matches as $match){
                           ?>
                        <tr>
                            <td colspan=100% style="padding: 0px!important;margin: 0px!important; ">
                                 <div class="box box-solid collapsed-box" style="background: #222d32;">
                                    <div class="box-header">
                                        <h3 class="box-title" style="color: #FFFFFF;font-size: 12px!important;margin-right: 30px;"> SU vs <?php  echo $match['match_opponents']; ?> on <?php  echo date_format(date_create($match['match_date']),"j<\s\up>S</\s\up> M, Y"); ?>  at <?php  echo $match['match_venue']; ?> </h3>
                                        <div class="box-tools pull-right clicked" >
                                            <button class="btn btn-default btn-sm " data-widget="collapse"><i class="fa fa-plus" style="color: #FFFF00;"></i></button>
                                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                        </div>
                                    </div>
                                    <div style="background-color: #FFFFFF;color: #000000;font-size: 12px!important;" class="box-body" style="padding: 0px!important;margin: 0px!important">
                                         <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">From</th>
                                                        <th class="text-center">To</th>
                                                        <th class="text-center"></th>
                                                     </tr>
                                                </thead>
                                                <tbody style="color: #17202A;">
                                                    <tr>
                                                        <td class="text-center"> <?php echo date('h:i A', strtotime($match['match_start_time']));  ?> </td>
                                                        <td class="text-center"><?php $endtime=$match['match_end_time']; if($endtime==""){echo "";}else{ echo date('h:i A', strtotime($match['match_end_time'])); } ?></td>
                                                        <td class="text-center pull-right">
                                                            <form style="display:inline;" name=<?php echo '"formScores_'. $match['match_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/tournamentscoresheet');?>" >
                                                                <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                                    <label for="matchId" class="control-label">Match ID*</label>
                                                                    <input required="required" class="form-control" name="matchId" id="matchId" value="<?php echo $match['match_auto_id']; ?>">
                                                                </div>
                                                                 <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                                    <label for="matchLevel" class="control-label">Match ID*</label>
                                                                    <input required="required" class="form-control" name="matchLevel" id="matchLevel" value="<?php echo $match['match_level']; ?>">
                                                                </div>
                                                                
                                                                <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                                    <label for="matchDetails" class="control-label">Match Details*</label>
                                                                    <input required="required" class="form-control" name="matchDetails" id="matchDetails" value="<?php echo $_SESSION['sessdata']['tournamentTeamName'].' vs. '; echo $match['match_opponents'].' match on '; echo date_format(date_create($match['match_date']),'j<\s\up>S</\s\up> M, Y').' at ';  echo $match['match_venue']; ?> from  <?php echo date('h:i A', strtotime($match['match_start_time']));?>">
                                                                </div>
                                                                <button class="btn btn-primary btn-s " title="New Team Scores" id=<?php echo '"teamscore_'. $match['match_auto_id'].'"';  ?> name=<?php echo '"teamscore_'. $match['match_auto_id'].'"';  ?>  type="submit" style="margin-right: 5px;margin-top: 5px;font-size: 12px!important;" disabled="true"><span class="fa fa-plus-circle"></span> Scores</button>  
                                                            </form>
                                                            <button class="btn btn-danger btn-s " title="Opponent Scores" id=<?php echo '"oppscore_'. $match['match_auto_id'].'"';  ?> name=<?php echo '"oppscore_'. $match['match_auto_id'].'"';  ?>  type="submit" value="<?php echo $match['match_auto_id'];?>" onclick="opponentscore(this);" style="margin-top:5px; font-size: 12px!important;" disabled="true"><span class="fa fa-plus-circle"></span> Rival Score</button>
                                                            <button class="btn btn-success btn-s " title="Edit Match" id=<?php echo '"editMatch_'. $match['match_auto_id'].'"';  ?> name=<?php echo '"editMatch_'. $match['match_auto_id'].'"';  ?>  type="submit" value="<?php echo $match['match_auto_id'];?>"  style="margin-top:5px; font-size: 12px!important;" ><span class="fa fa-edit"></span> Edit Match</button>  
                                                        </td>
                                                    </tr>

                                                    <td colspan=100% style="padding: 0px!important;margin: 0px!important; ">
                                                         <div class="box box-solid collapsed-box" style="background-color: #D0D3D4;" >
                                                            <div class="box-header">
                                                                <h3 class="box-title"> <span style="color: #000000;font-weight: normal;font-size: 12px!important;">Summary</span></h3>
                                                                <div class="box-tools pull-right clicked2" >
                                                                <button class="btn btn-default btn-sm " data-widget="collapse" value=<?php echo '"'. $match['match_auto_id'].'"';  ?> id="matchid"><i class="fa fa-plus"></i></button>
                                                                </div>
                                                            </div>
                                                            <div style="display: none;background-color: #FFFFFF;color: #000000;" class="box-body">
                                                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                           <th class="text-center text-warning">Feature Coming Soon</th>
                                                                        </tr>
                                                                    </thead>
                                                                     <tbody style="color: #17202A;">
                                                                        <tr>
                                                                            
                                                                            <td class="text-center">Nothing to display</td>
                                                                            
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div><!-- /.box-body -->
                                                        </div>
                                                        <!-- /.box -->
                                                        <div class="box box-solid collapsed-box" style="background-color: #D0D3D4;">
                                                            <div class="box-header">
                                                                <h3 class="box-title"> <span style="color: #000000;font-weight: normal;font-size: 12px!important;">Players</span></h3>
                                                                <div class="box-tools pull-right clicked3" >
                                                                <button class="btn btn-default btn-sm " data-widget="collapse" value=<?php echo '"'. $match['match_auto_id'].'"';  ?> id="matchid"><i class="fa fa-plus"></i></button>
                                                                </div>
                                                            </div>
                                                            <?php $tableId="players_scores_".$match['match_auto_id'];?>
                                                            <div style="display: none;background-color: #FFFFFF;color: #000000;" class="box-body">
                                                                 <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center text-warning">Feature Coming Soon</th>
                                                                        </tr>
                                                                    </thead>
                                                                     <tbody style="color: #17202A;">
                                                                        <tr>
                                                                            <td class="text-center">Nothing to display yet
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div><!-- /.box-body -->
                                                        </div>
                                                    </td>
                                                </tbody>
                                            </table>
                                            <!-- /.table-responsive -->

                                    </div><!-- /.box-body -->
                                </div>
                              <!-- /.box -->
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!--  opponentscore -->
        <div id="opponentsScoreModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">Opponent Score</strong></h4>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open('coach/new'.strtolower($_SESSION['sessdata']['sportName']).'opponentscore',array('id' => 'opponentscore','method'=>'post'));?>
                            <div class="row setup-content" >
                                <div class="col-xs-12">
                                    <div class="col-md-12" >
                                         <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                            <label for="matchId" class="control-label">Match Id*</label>
                                            <input type="text" name="matchId" class="form-control" id="matchId" required="required" >
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="opponentScore" class="control-label"> Opponent Score*</label>
                                            <input type="number" name="opponentScore" placeholder="" class=" form-control" id="opponentScore" required="required" min="0">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label>End Time</label>
                                             <div class='input-group time' id='matchEndTime'>
                                                <input class="form-control" placeholder="9:15 AM" name="matchEndTime" required="required" autocomplete="off" id="endTime" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <input type="submit" class="btn btn-primary" value="Submit" id="opponentscorebtn">
                                            <input type="reset" class="btn btn-default" value="Reset">
                                            <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                        </div>
                                    </div>
                                </div>
                            </div>
                          <?php echo form_close();?>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- /.opponents-modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () { 
     //datatable initialization
     $('#matchlist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],'aaSorting':[]
   }); 
      var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    
$("#list #checkall").click(function () {if ($("#list #checkall").is(':checked')) {$("#list input[type=checkbox]").each(function () {$(this).prop("checked", true);});} else {$("#list input[type=checkbox]").each(function () {$(this).prop("checked", false);}); }});
       // get players for playerList match
});//close document.ready
function opponentscore(obj)
{
    var match_id=obj.value;
    // alert(matchid);
    $('#opponentsScoreModal #matchId').val(match_id);
    $('#opponentsScoreModal').modal('show');
};
//datepicker
$(function () {$('#matchDate').datepicker({format: "yyyy-mm-dd",minDate:new Date(),todayHighlight: true});});
 //datetimepicker
  $(function () {$('#matchStartTime').datetimepicker({format: 'LT'});});
  $(function () {$('#matchEndTime').datetimepicker({format: 'LT'});});

</script>
</body>
</html>
