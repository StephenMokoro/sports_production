<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Travel Documents</title>
<?php $this->load->view('headerlinks/headerlinks.php');?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Travel Documents</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
                   <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                  <table  class="table table-striped table-hover display responsive nowrap" cellspacing="0" width="100%" id="plslist">
                    <thead>
                        <tr>
                            <th class="text-left">Full Name</th>
                            <th class="text-left">Passport No.</th>
                            <th class="text-left">Expiry Date</th>
                            <th class="text-left">Months Due</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                         </tr>
                    </thead>
                    <tbody >
                       <?php foreach($players as $player){ 
                           ?>
                        <tr>
                             <?php $photo=$player['player_profile_photo']; if($photo==""){$profile="defaultimage.png";}else{$profile=$player['player_profile_photo'];}?>
                                  <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/players/'.$profile?>" width="25" height="25" class="img-circle" alt=""> <?php  echo $player['player_fname']. " ".$player['player_lname']; ?></td>
                                    <?php  $passportNumber= $player['passport_number']; if($passportNumber==""){echo '<td class="text-center">-</td>';}else{echo '<td class="text-left">'. $passportNumber.'</td>';}?>
                                    <?php  $dateOfExpiry= $player['expiry_date']; if($dateOfExpiry==""){echo'<td class="text-center">-</td>';}else{echo '<td class="text-left">'. $dateOfExpiry.'</td>';}?>
                            <td class="text-center">
                                <?php 
                                if($player['expiry_date']=="")
                                {
                                    echo "-";
                                }else{
                                        $ts1 = strtotime(date('Y-m-d'));
                                        $ts2 = strtotime($player['expiry_date']);

                                        $year1 = date('Y', $ts1);
                                        $year2 = date('Y', $ts2);

                                        $month1 = date('m', $ts1);
                                        $month2 = date('m', $ts2);

                                        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                                        if($diff<6){echo $diff. " <span class='fa fa-warning text-danger'></span>";}else{echo $diff. " <span class='fa fa-check-circle-o text-success'></span>";}
                                    }
                                 ?>
                            </td>
                          <td class="text-center">
                                <?php $passport= $player['passport_number']; if($passport==""){?>
                                 <button class="btn btn-default btn-s" data-title="View More" id=<?php echo '"more_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"more_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color: #ECF0F1;color: #000000;" disabled="true"> <span class="fa fa-ban"></span> View </button>

                                 <form style="display:inline;" name=<?php echo '"formPassport_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/addplayerpspt');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerId" class="control-label">Player ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerName" class="control-label">Player ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="playerName" id="playerName" placeholder="" value="<?php echo $player['player_fname']." ".$player['player_lname']; ?>">
                                    </div>
                                    <button class="btn btn-primary btn-s" data-title="View Player Passport" id=<?php echo '"playerPspt_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"playerPspt_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color: ;color: ;"> <span class="fa fa-plus-circle"></span> Pspt </button>
                                </form>
                               
                                <?php } else{?>
                                <form style="display:inline;" name=<?php echo '"formMore_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/playerpsptdetails');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerId" class="control-label">Player ID*</label>
                                        <input required="required" class="form-control" name="playerId" id="playerId" placeholder="" value="<?php echo $player['player_auto_id']; ?>">
                                    </div><!--not using player id. May be used in future-->
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="passportId" class="control-label">Passport ID*</label>
                                        <input required="required" class="form-control" name="passportId" id="passportId" placeholder="" value="<?php echo $player['passport_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-default btn-s" data-title="View More" id=<?php echo '"more_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"more_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color: ;color: ;"> <span class="fa fa-eye"></span> View </button>
                                </form>
                                <form style="display:inline;" name=<?php echo '"formEdit_'. $player['player_auto_id'].'"';  ?> method="post" action="<?php echo base_url('coach/editplayerpspt');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="passportId" class="control-label">Passport ID*</label>
                                        <input required="required" class="form-control" name="passportId" id="passportId" placeholder="" value="<?php echo $player['passport_auto_id']; ?>">
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="playerName" class="control-label">Player ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="playerName" id="playerName" placeholder="" value="<?php echo $player['player_fname']." ".$player['player_lname']; ?>">
                                    </div>
                                    <button class="btn btn-default btn-s" data-title="Edit Player Passport" id=<?php echo '"playerPspt_'. $player['player_auto_id'].'"';  ?> name=<?php echo '"playerPspt_'. $player['player_auto_id'].'"';  ?>  type="submit" style="background-color:#C0C0C0;color:#FFFFFF;"> <span class="fa fa-edit"></span> Pspt </button>
                                </form>
                                
                                <?php }?>
                                
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    //datatable initialization
     $('#plslist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],'aaSorting':[],
         "aoColumnDefs": [{"aTargets": [4], "orderable": false}]
      });
   
});
</script>
</body>
</html>
