<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Expenditures</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> <?php echo $_SESSION['sessdata']['expenseTeamName'];?> Expenditures</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh
                          </button>
                    </span>
                    <span data-placement="top" data-toggle="tooltip" title="Print All">
                        <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                    </span>
                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New <?php echo $_SESSION['sessdata']['expenseTeamName'];;?> Expenditure </h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                      <?php echo form_open_multipart('coach/newexpenditure',array('id' => 'newExpenditure','method'=>'post'));?>
                        <div class="row setup-content" >
                            <div class="col-md-12">
                                <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                    <label for="teamId" class="control-label">#Team Id<span class="star">*</span></label>
                                    <input type="text" name="teamId" class="form-control" id="teamId" required="required" value="<?php echo $_SESSION['sessdata']['expenseTeamId'];?>" readonly="true">
                                </div>
                                                              
                                <div class="form-group col-md-6 col-lg-6">
                                    <label for="cash" class="control-label">Cash (Kshs)<span class="star">*</span></label>
                                    <input type="number" name="cash" placeholder="1975.00" class="form-control" id="cash" required="required" step="0.01" min="0">
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    <label for="lpoNo" class="control-label">LPO No.</label>
                                    <input type="number" name="lpoNo" placeholder="102" class=" form-control" id="lpoNo"  min="1">
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    <label for="lpo" class="control-label">LPO (Kshs)</label>
                                    <input type="number" name="lpo" placeholder="2000.00" class=" form-control" id="lpo" step="0.01" min="0">
                                </div>
                                 <div class="form-group col-md-6 col-lg-6">
                                    <label for="lunches" class="control-label">Lunches (Kshs)<span class="star">*</span></label>
                                    <input type="number" name="lunches" placeholder="575.50" class=" form-control" id="lunches" required="required" step="0.01" min="0">
                                </div>
                                  <div class='col-md-6 col-lg-6'>
                                  <label for="expenseDate" class="control-label">Expenditure Date<span class="star">*</span></label>
                                  <div class="form-group">
                                      <div class='input-group date' id='expenseDate'>
                                          <input type='text' class="form-control" readonly="true" name="expenseDate" required="required" />
                                          <span class="input-group-addon">
                                              <span class="fa fa-calendar"></span>
                                          </span>
                                      </div>
                                  </div>
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    <label for="receipt" class="control-label">Receipt</label>
                                    <input type="file" name="receipt" placeholder="" class=" form-control" id="receipt">
                                </div>
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for="comments" class="control-label"> Comments<span class="star">*</span></label>
                                    <textarea type="text" name="comments" placeholder="Fare and Lunch" class=" form-control" id="comments" required="required" style="min-height: 40px;resize: none;overflow:hidden;" maxlength="100"></textarea>
                                </div>
                                
                               
                                <div class="form-group col-md-12 col-lg-12">
                                <!-- <div class="modal-header"></div> -->
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                    <input type="reset" class="btn btn-default" value="Reset">
                                </div>
                          </div>
                      </div>
                      <?php echo form_close();?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="expenselist"  >
                    <thead>
                        <tr>
                            <th class="text-left">Expense Date</th>
                            <th class="text-left">Cash(Kshs)</th>
                            <th class="text-left">LPO(Kshs)</th>
                            <th class="text-left">Actual Costs(Kshs)</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                         </tr>
                    </thead>
                    <tbody >
                       <?php foreach($expenses as $exps){ 
                           ?>
                        <tr>
                            <td class="text-left"><?php  echo date_format(date_create($exps['expense_date']),"D j<\s\up>S</\s\up> M, Y"); ?></td>
                            <td class="text-left"><?php  echo number_format($exps['expense_cash'],2); ?></td>
                            <td class="text-left"><?php  echo number_format($exps['expense_lpo_amount'],2); ?></td>
                            <td class="text-left"><?php  echo number_format($exps['actual_expenditure'],2); ?></td>
                           <td class="text-center">
                              <?php if($exps['receipt_file_name']=="")
                                        {?>
                                           <button class="btn btn-default btn-s" title="View Record" id=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="/*background-color: #7B241C;color: #FFFFFF;"  onclick="viewrecord(this);"> <span class="fa fa-eye"></span> View </button>


                                            <!-- disabled button for receipt download -->
                                              <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" title="No Receipt(s)" id=<?php echo '"nor_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"nor_'. $exps['expense_auto_id'].'"';  ?>  type="submit" disabled="true" style="background-color:#D4EFDF;color:#000000;"> <span class="fa fa-ban"></span> RCP(s) </button>

                                              <?php }else{ ?> 
                                              <button class="btn btn-default btn-s" title="View Record" id=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> name=<?php echo '"view_'. $exps['expense_auto_id'].'"';  ?> value=<?php echo '"'. $exps['expense_auto_id'].'"';  ?> type="submit" style="/*background-color: #7B241C;color: #FFFFFF;"  onclick="viewrecord(this);"> <span class="fa fa-eye"></span> View </button>
                                              <span data-placement="top" data-toggle="tooltip" title="Download Receipt(s)">
                                                <a href="<?php echo base_url();echo 'coach/download_receipt/'; echo $exps['receipt_file_name'];?>" style="background-color:#F5CBA7;color:#000000;" class="btn btn-default" > <span class="fa fa-download"></span> RCP(s)</a></span>
                                       <?php }?>
                                     </td>
                                    </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
               
                <div class="modal fade" id="viewexpense">
                    <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="teamName"></h4>
                            </div>
                            <div  id="infobody" >
                              <!-- json html goes here -->
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                <!-- /.modal-dialog -->
              </div>
                <!-- /.modal -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
    $('#expenselist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],"aoColumnDefs": [{"bSortable":false, "aTargets": [4]}],'aaSorting':[]});
});

function viewrecord(objButton)
  {var expenseId=objButton.value;
    $.ajax({type:"post", url: "<?php echo base_url(); ?>coach/viewteamexpense",data:{ expenseId:expenseId},dataType:'json',success:function(data){$('#viewexpense #teamName').text(data.teamName);$('#viewexpense #infobody').html(data.info);$('#viewexpense').modal('toggle');} });
  }
$(function () {$('#expenseDate').datepicker({format: 'yyyy-mm-dd',minDate:new Date(),todayHighlight: true});});

</script>
</body>
</html>