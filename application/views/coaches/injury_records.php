<!DOCTYPE html>
<html>
<head>
   <title>SU Sports | Injury Records</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" style="background-color: #222d32;">
<div class="wrapper">
<?php $coachnav= $_SESSION['sessdata']['coachnav']; $this->load->view($coachnav); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 " >
                <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Injury Records</h4>
                <div class="pull-right">
                    <span data-placement="top" data-toggle="tooltip" title="Refresh">
                        <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh
                        </button>
                    </span>                </div> 
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                
                 <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                 <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="injurylist"  >
                    <thead>
                        <tr>        
                            <th class="text-center">Full Name</th>
                            <th class="text-center">Injury Date</th>
                            <th class="text-center">Diagnosis</th>
                            <th class="text-center">Treatment</th>
                            <th class="text-center"></th>
                         </tr>
                    </thead>
                    <tbody >
                    <?php foreach($injuries as $record)
                        { ?>
                        <tr>
                             
                             <td class="text-left"><img src="<?php echo base_url();?>assets/img/person.png" width="25" height="25" class="img-circle" alt=""> <?php  echo $record['player_fname']. " ".$record['player_lname']; ?></td>

                             <td class="text-left"><?php echo date_format(date_create($record['injury_date']),"D j<\s\up>S</\s\up> M, Y");?></td>

                            <td class="text-left"><?php echo $record['diagnosis'];?></td>
                            <td class="text-left"><?php echo $record['treatment'];?></td>
                            <td class="text-center">
                                <form style="display:inline;" name=<?php echo '"formMore_'. $record['injury_auto_id'].'"'; ?> method="post" action="<?php echo base_url('coach/injurydetails');?>">
                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                        <label for="recordId" class="control-label">Injury ID<span class="star">*</span></label>
                                        <input required="required" class="form-control" name="recordId" id="recordId" placeholder="" value="<?php echo $record['injury_auto_id']; ?>">
                                    </div>
                                    <button class="btn btn-default btn-s" data-title="View More" id=<?php echo '"more_'. $record['injury_auto_id'].'"';  ?> name=<?php echo '"more_'. $record['injury_auto_id'].'"';  ?>  type="submit" style="background-color: #ECF0F1;color: #000000;"> <span class="fa fa-eye"></span> View </button>
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     $('#injurylist').dataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
          columnDefs: [ 
        { orderable: false, targets: [4] },{targets: [2,3],render: function ( data, type, row ) {
            return type === 'display' && data.length > 25 ? data.substr( 0,25 ) +'<small>...</small>' : data;} }]
            , "aaSorting": []
   });  
  });
</script>
</body>
</html>
